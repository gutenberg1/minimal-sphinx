```{role} latexlogo
```
```{role} tttexlogo
```
# Comment déposer un fichier sur le CTAN ?

Vous avez quelque chose à déposer sur le {doc}`CTAN </1_generalites/glossaire/qu_est_ce_que_le_ctan>` ? C'est une excellente nouvelle ! C'est grâce à toutes les contributions individuelles que l'écosystème LaTeX se développe.

## Que vérifier avant le dépôt ?

Avant de commencer, voici une check-list des choses à régler :

### Licence

Dans l'esprit de TeX, on attend des logiciels libres; dans l'esprit de la société juridicisée d'aujourd'hui, le CTAN propose une [liste de licences « standards »](http://mirror.ctan.org/help/Catalogue/licenses.html) parmi lesquelles vous pouvez choisir. Assurez-vous de faire apparaître clairement la licence sous laquelle votre contribution sera distribuée, quelque part dans les fichiers que vous déposez.

N'oubliez pas qu'une fois sur le CTAN, votre contribution est susceptible d'être incluse dans des distributions LaTeX... et par la suite, également dans des distributions de systèmes d'exploitation... Pour cela, les personnes qui vont ainsi s'occuper de redistribuer vos fichiers ont besoin d'une déclaration claire de vos intentions.

### Documentation

C'est bien que les utilisateurs puissent consulter la documentation avant de télécharger une extension. Vous avez besoin d'au moins un fichier `README` en texte brut (les noms possibles sont `README`, `README.txt` et `README.md`, mais aucune autre variante).

L'audience du CTAN étant internationale, le mieux est d'écrire le fichier `README` en anglais, même si votre travail concerne une autre langue. Bien sûr, vous pouvez fournir un autre fichier `README` dans une autre langue si vous le souhaitez. Tous ces fichiers doivent être encodés en UTF-8.

De plus, un fichier PDF de la documentation de votre travail, préparé pour la lecture à l'écran, est hautement souhaitable. Le code-source de cette documentation doit aussi être fourni.

### Nom

Les conflits de noms sont une source de confusion sans fin. Si votre extension a le même nom qu'une autre déjà présente sur le CTAN, ou si l'installation de votre extension produit des fichiers de même nom que d'autres qu'on peut trouver dans une distribution « normale », l'équipe du CTAN retardera la procédure pendant qu'elle vérifiera que vous faites bien ce qu'il faut : elle pourra vous harceler pour changer le nom, ou pour négocier une collaboration avec l'auteur de l'extension originale. {doc}`Parcourez le CTAN </5_fichiers/comment_trouver_un_fichier>` pour vous assurer de l'unicité du nom que vous voulez.

Le nom que vous choisissez doit également (dans la mesure du possible) être quelque peu descriptif du contenu de votre contribution; bien sûr, c'est toujours un peu subjectif, mais il est clair que des noms tels que `mon_extension` ou `exemple` ne conviennent pas.

### Numéro de version

Chaque soumission d'une extension au CTAN doit contenir un «identifiant de version» qui permet de distinguer cette version des versions antérieures ou ultérieures. Cet identifiant doit être placé à un endroit où il est facile à trouver, comme le début du fichier `README` ou, mieux, un fichier `VERSION`.

Ne vous contentez pas de ce qui est mentionné dans le fichier d'historique de votre travail.

Cet identifiant de version peut consister soit en un numéro de version (c'est-à-dire quelque chose comme `1.0`, ou `3.0.17`, ou `2.1a`), ou une date de version (de préférence en notation `AAAA-MM-JJ` ou `AAAA/MM/JJ`, comme `2021-04-01` ou `2021/04/01`), ou une chaîne de caractères composée des deux données susmentionnées.

## Comment uploader mes fichiers ?

Le dépôt s'effectue via la [page d'upload du CTAN](https://ctan.org/upload) (la page d'accueil du CTAN fournit un lien). La page d'upload propose un formulaire, et beaucoup d'aide, pour vous indiquer les informations à entrer. Elle ne peut accepter qu'un seul fichier par upload : si vous avez l'intention de télécharger de nombreux fichiers, vous devez les regrouper dans un fichier archive quelconque; les formats acceptables sont `zip` et `tar.gz`. Une fois que vous avez terminé votre upload, le serveur le confie à un membre de l'équipe du CTAN pour qu'il le traite.

Si vous ne pouvez pas utiliser cette méthode, ou si quelque chose n'est pas clair, demandez conseil à [l'équipe de gestion du CTAN](https://ctan.org/contact).

## Que va-t-il se passer ensuite ?

Chaque soumission est décompressée et vérifiée par un responsable de l'équipe de gestion du CTAN.

Les petits défauts seront immédiatement corrigés par cette personne. Vous recevrez un compte-rendu à ce sujet, par mail, et il vous sera demandé d'appliquer le(s) même(s) type(s) de correction(s) à vos propres fichiers en vue du prochain upload.

S'il y a un problème non trivial qui ne peut être résolu sans votre aide, l'équipe vous demandera quoi faire et le processus d'installation sera momentanément interrompu jusqu'à ce qu'elle reçoive une réponse de votre part. Dans certains cas, un nouvel upload peut s'avérer nécessaire (si c'est à vous de faire les corrections).

Après le dépôt de votre contribution sur le serveur central du CTAN, le fichier XML correspondant du {doc}`Catalogue </1_generalites/documentation/le_catalogue_du_ctan>` sera soit créé (pour les nouvelles extensions), soit mis à jour (dans tous les autres cas).

Lorsque tout cela aura été fait, l'un des responsables du téléchargement vous enverra un courriel à l'adresse que vous avez indiquée dans le formulaire d'upload. Cela peut prendre quelques heures (jusqu'à 24 heures) pour que vos fichiers soient propagés à tous les miroirs du monde.

Si votre contribution est nouvelle ou si vous avez demandé une annonce, un message sera envoyé à [CTAN-Announce](https://www.ctan.org/ctan-ann) peu après la fin de ce temps de propagation.

C'est uen bonne idée de vérifier ce que vous voyez aux URL indiquées dans le courrier d'accusé de réception, environ 24 heures après l'installation et d'informer l'équipe du CTAN de tout problème éventuel.

______________________________________________________________________

*Sources :*

- {faquk}`Contributing a file to the archives <FAQ-uploads>`,
- [Additional Information for CTAN Uploaders](https://www.ctan.org/file/help/ctan/CTAN-upload-addendum).

```{eval-rst}
.. meta::
   :keywords: LaTeX,faire un package,écrire une extension,publier une extension,documentation sur le CTAN
```
