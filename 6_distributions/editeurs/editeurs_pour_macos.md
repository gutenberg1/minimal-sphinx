```{role} latexlogo
```
```{role} tttexlogo
```
# Quels sont les éditeurs utilisables sous Mac OS ?

```{eval-rst}
.. todo:: *Page sans doute à revoir car certaines références semblent datées.*
```

Il faut noter que le dernier système d'exploitation des Macintosh est un dérivé de BSD, et donc un dérivé d'Unix. Il existe un portage de quasi tous les éditeurs/outils disponibles sous Unix grâce à une couche X Window portée sur ce système (pour les éditeurs graphiques).

Il y a aussi des ports directs des logiciels Unix graphiques sur la couche graphique native des Macintosh.

## Alpha

[Alpha](http://www.kelehers.org/alpha/) est un éditeur pour Mac OS, shareware assez proche d`'Emacs`. Cet éditeur est hautement configurable grâce à un langage de programmation intégré, `Tcl`. Il possède entre autres un mode {latexlogo}`LaTeX` très convivial. `Alpha` permet aussi une interaction avec le compilateur (`Texture`, `CMACTEX`, `OzTex` ou `Direct-Tex`) en lançant la compilation d'une combinaison de touches. La dernière version d`'Alpha` est la 7.5. Elle comprend la version 3.2 des macros freeware `Alpha` {latexlogo}`LaTeX` de Tom Scavo.

## BBEdit

[BBEdit](http://www.barebones.com/products/bbedit/index.shtml) n'est pas limité à l'édition des textes pour {latexlogo}`LaTeX`, mais permet aussi d'éditer du `HTML`.

## Emacs

[Emacs](http://mac-emacs.sourceforge.net/index.html) a été porté sur Mac OS.

## jedit

[jedit](http://jedit.sourceforge.net/) est écrit en `java` et est donc disponible sur toutes les plate-formes où une [machine virtuelle Java](https://fr.wikipedia.org/wiki/Machine_virtuelle_Java) (JVM) a été portée.

## MathType

[MathType](http://spssscience.co.kr/mathtype/) est un éditeur de formules mathématiques. Il semble payant (la page d'accueil est en caractères coréens).

## Nedit

[Nedit](http://nedit.gmxhome.de/text_editor/index.html) est présenté en allemand. Il est disponible aussi pour d'autres systèmes d'exploitation.

## Pepper

[Pepper](http://www.hekkelman.com/), est un éditeur très puissant pour toutes les versions (Mac OS/9 et X). C'est aussi un éditeur UTF8, ce qui permet d'éditer des textes multilingues en utilisant `ucs-latex` ou `omega`.

## PPKedit

[PPKedit](http://www.asahi-net.or.jp/~gf6d-kmym/en/).

## Scientific WorkPlace & Scientific Word

[Scientific WorkPlace & Scientific Word](http://www.mackichan.com/techtalk/v60/FreeSW.htm).

## STEAD

[STEAD](http://www.ensta.fr/~diam/stead/distrib/old_stead36/stead_readme.old.html) est un éditeur écrit avec le langage `Tcl`. Son adaptation à des besoins spécifiques est aisée.

## The Right Tool

[The Right Tool](http://c2.com/cgi/wiki?TheRightTool).

## XEmacs

[XEmacs](http://homepage.mac.com/pjarvis/xemacs.html) est un éditeur aussi disponible pour toutes les versions de Mac OS.

## TeXshop

[TeXshop](http://pages.uoregon.edu/koch/texshop/index.html) combine un éditeur et un shell permettant un traitement cohérente dans l'environnement OS X. TeXShop est distribué dans le cadre du système MacTeX, et est donc prêt à l'emploi sur les machines sur lesquelles MacTeX a été installé.

## Vi(m)

`vi(m)` avec son mode {latexlogo}`LaTeX` est un éditeur libre très puissant et disponible sur beaucoup de plates-formes.

```{eval-rst}
.. meta::
   :keywords: LaTeX,éditeurs de texte,éditeurs LaTeX,Mac OS,Apple
```
