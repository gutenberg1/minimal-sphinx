```{role} latexlogo
```
```{role} tttexlogo
```
# Quels sont les éditeurs utilisables sous Linux ?

## Éditeurs généralistes

### (X)Emacs

[GNU emacs](http://www.gnu.org/software/emacs/emacs.html) et [XEmacs](http://www.xemacs.org/) sont des éditeurs sous Unix/Linux qui offrent en standard un mode d'édition, un peu fruste mais néanmoins pratique, facilitant la composition de documents {latexlogo}`LaTeX`. Une extension à `Emacs`, {ctanpkg}`AUCTeX <auctex>` (utilisez plutôt le gestionnaire d'extension d'Emacs que le CTAN pour l'installer), fournit de nombreuses facilités supplémentaires (indentation automatique, messages d'erreur en anglais compréhensible, gestion des documents multifichiers... et même un mode de prévisualisation)

`Emacs` reconnaît automatiquement certaines extensions de noms de fichiers (TEX, STY) et active le mode en question automatiquement. Si votre fichier n'est pas reconnu comme un document {latexlogo}`LaTeX`, vous pouvez spécifier sur la première ligne de votre fichier :

```latex
% !TEX noedit
% -*-latex-*-
```

L'extension (standard) `imenu` donne accès à un menu listant les en-têtes de section du document, et permet de retrouver celles-ci facilement dans un grand document. Une extension à ce mécanisme permet de mieux visualiser la structure du document, en indentant les sous-sections.

### Jed

`LaTeX4Jed` offre une prise en charge améliorée de {latexlogo}`LaTeX` pour l'éditeur [jed](http://www.jedsoft.org/jed/). `LaTeX4Jed` est similaire à {ctanpkg}`AUCTeX <auctex>` : menus, raccourcis, modèles, coloration syntaxique, plan de document, débogage intégré, complétion de symboles, intégration complète avec des programmes externes, etc. Il a été conçu à la fois pour les utilisateurs débutants et avancés de {latexlogo}`LaTeX`.

### NEdit

[NEdit](https://sourceforge.net/projects/nedit/) (*The Nirvana Text Editor*) et son *fork* [NEdit-NG](https://github.com/eteran/nedit-ng) sont des éditeurs entièrement conçus pour [X Window](https://fr.wikipedia.org/wiki/X_Window_System) dès le début. Notez que leurs sites web ne proposent pas de binaires à jour, mais qu'ils encouragent les mainteneurs de distributions Linux à fournir des extensions à partir des sources (dernière mise à jour de 2017 pour NEdit et 2021 pour NEdit-NG).

Il offre un aspect semblable aux éditeurs que l'on trouve sur Windows et MacOS, par exemple, les raccourcis standards sont Ctrl+X pour couper, Ctrl+C pour copier, Ctrl+V pour coller. Ainsi, les nouveaux utilisateurs venant des autres systèmes d'exploitation n'auront aucun mal à s'adapter à `NEdit`. C'est un éditeur très configurable, notamment la police de caractères, les couleurs (enfin, parfois il faut être un peu bidouilleur aussi). C'est un éditeur à usage général. Il n'est pas seulement utile pour {latexlogo}`LaTeX`.

Si ses fonctionnalités de bases ne vous suffisent pas pour {latexlogo}`LaTeX`, il existe {ctanpkg}`une extension pour NEdit sur le CTAN <NEdit-LaTeX-Extensions>`.

### ViM

[ViM](https://vim8.org/), développé par Bram Moolenaar et disponible également pour Windows et MacOS, dispose de la coloration syntaxique, à l'instar d'Emacs ; c'est un éditeur très peu gourmand en ressources. Il dispose d'un mode spécial {latexlogo}`LaTeX` qui facilite la saisie des textes : <http://vim-latex.sourceforge.net/>.

### Xcoral

[Xcoral](http://xcoral.free.fr/), sous Unix, offre des fonctionnalités de même type que Emacs mais non interactives. Il est toujours maintenu en 2020.

`Xcoral` est un éditeur multifenêtres pour X Window, offrant un certain nombre de facilités notamment pour écrire des programmes `perl`, `ada`, `fortran`, `C`, `C++`, `java` ainsi que des documents {latexlogo}`LaTeX` ou `HTML`.

Cet éditeur comprend un interpréteur `Ansi C` intégré qui permet aux utilisateurs d'étendre ses fonctionnalités facilement. Un manuel d'aide complet indexé est disponible en ligne.

## Éditeurs spécialisés pour LaTeX

### Gummi

[Gummi](https://gummi.app/) est un éditeur *open-source* simple, et développé spécialement pour {latexlogo}`LaTeX` Parmi ses [nombreuses fonctionnalités](<https://fr.wikipedia.org/wiki/Gummi_(logiciel)>), il permet de prévisualiser le document en cours de rédaction. Il est encore développé en 2020.

### Kile

[Kile](https://kile.sourceforge.io/) est fourni avec le gestionnaire de fenêtres de KDE. Des informations (et des fichiers téléchargeables) sont disponibles sur la [page SourceForge](http://kile.sourceforge.net/) du projet.

### TeXmaker

[TeXmaker](https://www.xm1math.net/texmaker/) (multiplateforme).

### TeXstudio

[TeXstudio](http://texstudio.sourceforge.net/)

### TeXworks

Le {doc}`TUG </1_generalites/gutenberg>` parraine le développement d'un éditeur et d'un shell multiplateformes, inspirés de l'excellent TeXshop pour Macintosh. Le résultat, [TeXworks](http://www.tug.org/texworks/), est recommandé si vous cherchez un environnement de développement {latexlogo}`LaTeX`. Il est distribué avec `Live` et `MiKTeX`.

## Traitements de texte WYSIWYG

[LyX](http://www.lyx.org/) est un véritable traitement de texte sous X11 qui offre une sortie {latexlogo}`LaTeX`. Il est presque {doc}`WYSIWYG </1_generalites/bases/wysiwyg>`. Parmi ses fonctionnalités, on trouve :

- éditeur d'équations ;
- éditeur de tables ;
- inclusion d'images au format `EPS` ;
- correction orthographique...

En revanche, il est peu apprécié des utilisateurs avancés de {latexlogo}`LaTeX`, car il s'occupe lui-même de générer le code {latexlogo}`LaTeX`, alors que vous saisissez votre document dans une interface de type traitement de texte. Le code est ensuite difficile à retravailler à la main.

Si vous avez besoin d'aide avec `LyX`, regardez plutôt du côté des [listes de discussion spécifiques](https://www.lyx.org/MailingLists) plutôt que des listes orientées {latexlogo}`LaTeX`.

## Quelques éditeurs obsolètes

On trouve sur le web la trace de nombreuses initiatives pour développer l'éditeur idéal : le plus léger, le plus configurable, le plus complet, le plus ergonomique...

Voici quelques noms de projets :

- `Funtek`, de V. Vidal, sous X Window et Motif ;
- `TeXShell` pour X Window (Tcl/Tk) ;
- `STEAD` (*Sympathetic Tk-based Editor for Average Dummies*), de Maurice Diamantini, est un éditeur de texte convivial pour Unix (ressemblant à `Alpha` sur MacOS), entièrement interprété, écrit en [Tcl/Tk](https://fr.wikipedia.org/wiki/Tool_Command_Language). Il a conservé une [page d'accueil](https://perso.ensta-paris.fr/~diam/stead/).

______________________________________________________________________

*Sources :*

- [9 best LaTeX editors for Linux](https://itsfoss.com/latex-editors-linux/).
- {faquk}`TeX-friendly editors and shells <FAQ-editors>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,éditeur de texte,éditeur de code LaTeX,éditeurs spécialisés pour LaTeX,éditeur avec prévisualisation,DVI
```
