```{role} latexlogo
```
```{role} tttexlogo
```
# Quels sont les éditeurs utilisables sur Windows ?

## editeur

Le shareware [editeur](http://www.studioware.com/) (la dernière version datant de 2005) est un éditeur multifichiers avec coloration syntaxique qui permet entre autre d'éditer des sources {latexlogo}`LaTeX`.

## Emacs

[Emacs](http://www.gnu.org/software/emacs/emacs.html) a été porté sur Windows. Beaucoup plus qu'un éditeur, c'est plutôt un [IDE](https://fr.wikipedia.org/wiki/Environnement_de_d%C3%A9veloppement), avec possibilités de lire et écrire son courrier, les news, etc.

## NEdit

[nedit](https://sourceforge.net/projects/nedit/) (*The Nirvana Text Editor*) est un éditeur considéré comme stable et mûr.

## PCTeX

[PCTeX](http://www.pctex.com/) est un environnement complet pour écrire du {latexlogo}`LaTeX`.

## Scientific Word

[Scientific Word](https://www.sciword.co.uk/) pour Windows est un éditeur qui permet presque de visualiser un document {latexlogo}`LaTeX` en WYSIWYG (What You See Is What You Get). Il facilite l'édition d'un document {latexlogo}`LaTeX` en permettant une insertion aisée des symboles, l'édition de tableaux. Pour l'utiliser pleinement, il vaut mieux connaître {latexlogo}`LaTeX`.

:::{note}
Ce produit est commercial et cher, mais intègre un moteur de mathématiques formelles, [MuPAD](https://fr.wikipedia.org/wiki/MuPAD).
:::

## TeXmaker

[TeXmaker](https://www.xm1math.net/texmaker/) est un éditeur libre, multiplateforme et moderne. Il dispose d'un correcteur orthographique et d'une prévisualisation du document compilé (avec {ctanpkg}`synctex`).

## TeXnicCenter

[TeXnicCenter](https://www.texniccenter.org/) est un environnement de développement intégré (IDE en anglais) {latexlogo}`LaTeX` pour Windows distribué sous licence GPL. L'interface est très proche de celle que l'on peut trouver dans des outils du style de `MS Visual Studio`. Il offre la coloration des mots-clefs, une gestion aisée de gros projets, une vue structurée des fichiers, des sections du document, des flottants et plein d'autres fonctionnalités intéressantes. Il intègre aussi un correcteur orthographique Son développement semble cependant s'être arrêté en 2014.

## TeXShell

{ctanpkg}`TeXShell <texshell>`, écrit par J. Schlegelmilch ([voir aussi sa page d'accueil](http://www.projectory.de/texshell/)) est un éditeur Windows qui offre une coloration des mots clés {latexlogo}`LaTeX`, une aide en ligne et d'autres petites fonctionnalités telles que les compilations associées à des boutons. Sa dernière version date de 2007.

## TeXstudio

[TeXstudio](http://texstudio.sourceforge.net/) est un éditeur libre et multiplateforme. Il vise à « rendre {latexlogo}`LaTeX` confortable » : il dispose d'un correcteur orthographique, d'une assistance pour composer les tableaux et insérer des figures.

## TextPad

[TextPad](http://textpad.com/) présente les caractéristiques données :

- Étudié et conforme au passage à l'an 2000 ;
- respecte les recommandations d'accessibilité de Microsoft ;
- la version 32 bits peut éditer des fichiers de la taille de la mémoire virtuelle. La version 16 bits est limitée à 32500 lignes, avec une longueur de maximum de 4096 octets ;
- le nombre de fichiers édités ensemble n'est limité qu'à l'épuisement des ressources ;
- support de l'unicode UTF8, UTF16 ainsi que des textes 8 bits ;
- les fins de ligne de type Mac OS, doc, Windows, Unix sont supportées ;
- le codage caractère peut être de type ANSI ou DOS ;
- chaque fichier a sa propre fenêtre d'édition ainsi que quatre vues différentes simultanées ;
- l'ensemble des fontes Windows est supporté ;
- trente-deux tabulations peuvent être posées ;
- indique les 16 derniers plus récents fichiers utilisés ;
- 64 commandes clavier peuvent être définies...

## TeXworks

[TeXworks](http://www.tug.org/texworks/) est également disponible sur Windows.

## Vim

[Vim](http://www.vim.org/) a également été porté sous Windows.

## Visual Studio Code

[Visual Studio Code](https://code.visualstudio.com/) est un éditeur gratuit développé par Microsoft. L'extension [LaTeX-Workshop](https://github.com/James-Yu/LaTeX-Workshop) lui apporte des fonctionnalités pour {latexlogo}`LaTeX`.

## WinShell

[WinShell](http://www.winshell.de/) a pour principales caractéristiques :

- interface localisée (anglais, allemand, français, espagnol, italien, polonais) ;
- gestion de projets ;
- éditeur de tables ;
- programme MDI ;
- commandes pouvant être définies par l'utilisateur ;
- barre d'outils configurable ;
- macros ;
- choix des fontes ;
- lecture de fichiers Unix ;
- éditeur à coloration syntaxique ;
- drag and drop.

## WinEdt

[WinEdt](http://www.winedt.com/) actuellement en version 10, logiciel shareware pour Windows, est un éditeur avec menu AllTeX qui permet de repérer les commandes {latexlogo}`LaTeX` et de compter les délimiteurs. Il est pourvu d'un correcteur orthographique.

Il existe un [site de la communauté des utilisateurs de WinEdt](http://www.winedt.org/).

## wintex2000

> {ctanpkg}`wintex2000` est un éditeur shareware flexible offrant une coloration syntaxique {latexlogo}`LaTeX`. Il offre également une complétion automatique, des palettes de symboles, un éditeur de tableaux, des touches de raccourci, le lancement de programmes externes, un correcteur orthographique, des dictionnaires de synonymes, etc.

## Autres éditeurs

Voici une liste d'URL sans détails qui proposent des éditeurs de sources {latexlogo}`LaTeX` :

- <http://www.oxedit.com/> ;
- <http://www.editplus.com/>.

______________________________________________________________________

*Sources :*

- [Awesome LaTeX : editors](https://github.com/egeerardyn/awesome-LaTeX#editors),
- [LaTeX Editors/IDEs](https://tex.stackexchange.com/questions/339/latex-editors-ides),
- [Création de texte en LaTeX](http://prof.math.free.fr/afficher.php?id=37).
- {faquk}`TeX-friendly editors and shells <FAQ-editors>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,éditeurs de texte,écrire en LaTeX,interface,Microsoft Windows
```
