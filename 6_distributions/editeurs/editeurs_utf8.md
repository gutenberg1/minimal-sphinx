```{role} latexlogo
```
```{role} tttexlogo
```
# Éditeurs UTF8

## Quels sont les éditeurs UTF8 pour Windows ?

- Voici un site qui recense les éditeurs UTF8 pour Windows : <http://www.alanwood.net/unicode/utilities_editors.html>.

Suit une liste d'éditeurs avec une URL pour avoir de plus amples informations :

- `SC Unipad` à l'url : <http://www.unipad.org/main/>
- `Unired` à l'url : <http://www.esperanto.mv.ru/UniRed/ENG/>
- `SimRedo` à l'url : <http://www4.vc-net.ne.jp/~klivo/sim/simeng.htm> ; écrit en `java`, il devrait tourner sur toutes les machines disposant d'une JVM `java`
- `LeKHO` à l'url : <http://lekho.sourceforge.net/>
- `Mà` à l'url : <http://www.vovisoft.com/VisualBasic/UniEditor.htm>
- `Mined 2000` à l'url : <http://towo.net/mined/>
- `UltraEdit` à l'url : <http://www.idmcomp.com/products/>
- `AbiWord` à l'url : <http://www.abisource.com/products.phtml>
- `Aprotool TM Editor` à l'url : <http://hp.vector.co.jp/authors/VA002891/READTM.TXT>
- `BabelPad` à l'url : <http://uk.geocities.com/babelstone1357/Software/BabelPad.html>
- `EmEditor` à l'url : <http://www.emurasoft.com/emeditor3/>
- `Global Office` à l'url : <http://www.unitype.com/globaloffice.htm>
- `Global Writer` à l'url : <http://www.unitype.com/globalwriter.htm>
- `jEdit` à l'url : <http://www.jedit.org/>
- `OpenOffice` à l'url : <http://www.openoffice.org/>
- `TextPad 4.5` à l'url : <http://www.textpad.com/>
- `UnicEdit` à l'url : <http://heiner-eichmann.de/software/unicedit/unicedit.htm>
- `UniEdit` à l'url : <http://www.humancomp.org/uniintro.htm>
- `UniPad` à l'url : <http://www.sharmahd.com/unipad/>
- `Microsoft Word` : vous savez où l'acheter
- `WorldPad` à l'url : <http://fieldworks.sil.org/WorldPad/worldpad.html>
- `WPS Office 2003` à l'url : <http://www.kingsoft.net/>
- `Vim` à l'url : <http://www.vim.org/>

## Quels sont les éditeurs UTF8 pour Mac OS ?

- Voici un site qui recense les éditeurs UTF8 pour Mac OS : <http://www.alanwood.net/unicode/utilities_editors_macosx.html>. Il faut distinguer Mac OS9 et Mac OSX. Certains éditeurs Unix/Linux fonctionnent avec la librairie d'affichage X qui a été portée sur MacOS X.
- `BBedit` à l'url : <http://www.barebones.com/>
- `jEdit` à l'url : <http://www.jedit.org/>
- `Mellel` à l'url : <http://www.redlers.com/>
- `Nisus Writer Express` à l'url : <http://www.nisus.com/Express/>
- `Pepper` à l'url : <http://www.digitalwandering.com/>
- `Simredo 3` à l'url : <http://www4.vc-net.ne.jp/~klivo/sim/simeng.htm>
- `Style` à l'url : <http://www.merzwaren.com/style/index.html>
- `SUE` à l'url : <http://free.abracode.com/sue/>
- `TextEdit`, fourni par défaut avec le système d'exploitation
- `ThinkFree Write` à l'url : <http://www.thinkfree.com/>
- `MLTE Demo` à l'url : <http://www.merzwaren.com/snippets/index.html#mltedemo>
- `Nisus Writer 6.5` à l'url : <http://www.nisus.com/>
- `Vim` à l'url : <http://www.vim.org/>

## Quels sont les éditeurs UTF8 pour Unix ?

- Voici un site qui recense des éditeurs UTF8 pour Unix : <http://www.alanwood.net/unicode/utilities_editors_unix.html>. Suit une liste d'éditeurs avec une url pour avoir de plus amples informations :
- `Yudit` à l'url : <http://www.yudit.org/>
- `LeKHO` à l'url : <http://lekho.sourceforge.net/>
- `Mined 2000` à l'url : <http://towo.net/mined/>
- `CoolEdit` à l'url : <http://cooledit.sourceforge.net/>
- `Emacs` à l'url : <http://www.emacs.org/>
- `Vim` à l'url : <http://www.vim.org/>
- `gedit` à l'url : <http://gedit.sourceforge.net/>

## Ou trouver des utilitaires UTF8 ?

- Voyez l'adresse suivante : <http://www.alanwood.net/unicode/utilities.html>. Vous y trouverez des utilitaires de conversion, des claviers virtuels qui permettent d'entrer n'importe quel caractère, etc.

```{eval-rst}
.. meta::
   :keywords: LaTeX,Unicode,UTF-8,UTF8,éditeurs de texte,éditeurs compatibles UTF8
```
