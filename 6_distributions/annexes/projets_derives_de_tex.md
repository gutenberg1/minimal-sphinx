```{role} latexlogo
```
```{role} tttexlogo
```
# Existe-t-il des projets dérivés de TeX ?

## kerTeX

[kerTeX](https://kertex.kergis.com/fr/index.html) est une distribution TeX minimaliste visant à pouvoir s'installer sur tous les systèmes informatiques, même dans des environnements extrêmement restreints. Le projet est mené par [Thierry Laronde](http://kertex.kergis.com/fr/contacts.html), et son code spécifique est diffusé sous licence BSD.

Voir la question « {doc}`Quelles sont les particularités de la distribution kerTeX? </6_distributions/installation/kertex>` »

En pratique, d'après ses auteurs, il n'a besoin que d'une bibliothèque C standard (`libc`) pour fonctionner. Il embarque son propre système de gestion des extensions.

## Tectonic typesetting

[Tectonic](https://tectonic-typesetting.github.io/) se veut un moteur TeX modernisé et complet, basé sur XeTeX et TeXlive. Il est constitué d'une unique bibliothèque, contenant tout ce qui est nécessaire. Par rapport au moteur TeX usuel, il a la particularité de ne pas produire de fichiers intermédiaires, et de délivrer le document final en une seule exécution.

Le projet est [en développement sur GitHub](https://github.com/tectonic-typesetting/tectonic/), sous licence MIT.

:::{note}
Le projet revendique d'orthographier son nom de façon simple, avec un `C` (Te**c**tonic), par opposition à la complexité de {doc}`l'orthographe et la prononciation </1_generalites/bases/comment_prononcer_tex>` de {tttexlogo}`TeX`.
:::

## KaTeX

[KaTeX](https://katex.org/) est une bibliothèque logicielle destinée à afficher des formules mathématiques dans les navigateurs web. La syntaxe et la mise en forme suivent les principes de {tttexlogo}`TeX` mais il s'agit d'une réimplémentation en Javascript d'un sous-ensemble limité de fonctionnalités du moteur {tttexlogo}`TeX` standard.

[MathJax](https://fr.wikipedia.org/wiki/MathJax) a le même usage, et implémente un nombre plus grand de fonctionnalités que KaTeX.

Pour plus d'informations :

- [KaTeX sur Wikipedia](https://en.wikipedia.org/wiki/KaTeX) (en anglais),
- [TeX as a three-stage rocket : Cookie-cutterpage breaking](https://tug.org/TUGboat/tb36-2/tb113venkatesan.pdf), TUGboat, Volume 36 (2015), n{sup}`o`2.

```{eval-rst}
.. meta::
   :keywords: mise en page de documents,autres projets,à partir de TeX,dérivés de LaTeX
```
