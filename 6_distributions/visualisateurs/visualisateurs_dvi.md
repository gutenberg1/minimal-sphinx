```{role} latexlogo
```
```{role} tttexlogo
```
# Comment visualiser des fichiers DVI ?

{tttexlogo}`TeX` et {latexlogo}`LaTeX` compilent les fichiers sources et produisent des fichiers de type DVI. Disposer d'un prévisualiseur spécifique à ce type de fichier même si cette étape est de moins en moins nécessaire, le fichier DVI n'étant normalement qu'un fichier intermédiaire à la production d'un fichier Postscript ou PDF (comme précisé en bas de cette page).

## Sous Mac OS

```{eval-rst}
.. todo:: *À compléter.*
```

## Sous Unix ou Linux

### kdvi

> {ctanpkg}`kdvi`, créé pour l'environnement KDE, est maintenant remplacé par [Okular](https://fr.wikipedia.org/wiki/okular). Il permet la recherche textuelle intégrale dans le fichier DVI, le couper-coller entre applications, la synchronisation de l'endroit où vous cliquez dans le fichier DVI et le positionnement dans l'éditeur de votre choix. Il supporte les liens hypertextes cliquables, l'affichage des parties Postscript, la couleur pour le texte et le fond, ainsi que tous les types de fontes telles les fontes truetype, Postscript, et virtuelles.

### xdvi

> {ctanpkg}`xdvi` pour X Window est est fourni en standard avec toutes les distributions Linux. Il est devenu extrêmement puissant et permet de visualiser grâce à l'interpréteur Postscript `ghostscript` des fichiers contenant des parties en Postscript encapsulé `eps`, des images de différents types `gif`, `jpg` et autres. La FAQ de `xdvi` est disponible à l'adresse suivante <http://math.berkeley.edu/~vojta/xdvi_faq.txt>

### Anciennes alternatives

{octicon}`alert;1em;sd-text-warning` Voici quelques solutions considérées comme obsolètes :

- {ctanpkg}`xdvik`, développé par Karl Berry. Il s'agissait d'une version de {ctanpkg}`xdvi` qui utilisait les bibliothèques `web2c`. Les deux branches de développement ont fusionné sous le nom {ctanpkg}`xdvi`, qui est distribué avec {tttexlogo}`TeX` Live ;
- [xtex](http://www.funet.fi/pub/TeX/previewer/xtex/) ;
- {ctanpkg}`DVItoVDU <dvgt>` et {ctanpkg}`dvgtk` (pour Tektronix 4010...) sont largement obsolètes, mais toujours disponibles.

## Sous Windows

### yap

Le programme [yap](https://en.wikipedia.org/wiki/Yet_Another_Previewer) (*Yet another previewer*) est un visualiseur livré avec la distribution MikTeX.

(anciennes-alternatives-1)=

### Anciennes alternatives

{octicon}`alert;1em;sd-text-warning` Voici quelques solutions considérées comme obsolètes :

- {ctanpkg}`dviwin`.

## Autres solutions

Au lieu de visualiser le fichier DVI, vous pouvez :

- le convertir en texte brut. Voir la question « {doc}`Comment convertir un fichier DVI en ASCII ? </5_fichiers/dvi/convertir_un_fichier_dvi_en_ascii>` » puis l'ouvrir avec un simple éditeur de texte ;
- {doc}`produire une version PostScript de votre document </5_fichiers/postscript/generer_un_fichier_postscript_a_partir_d_un_dvi>` et l'ouvrir avec un visualiseur présenté à la question « {doc}`Comment visualiser des fichiers Postscript ? </6_distributions/visualisateurs/visualisateurs_postscript>` » ;
- {doc}`générer une sortie PDF </5_fichiers/pdf/convertir_du_latex_en_pdf>` et utiliser un visualisateur présenté à la question « {doc}`Comment visualiser des fichiers PDF ? </6_distributions/visualisateurs/visualisateurs_pdf>` ».

______________________________________________________________________

*Source :* {faquk}`DVI previewers <FAQ-previewers>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,logiciels,Format DVI,fichier DVI,ouvrir un fichier DVI,visualisateur DVI,device independant,imprimer un fichier DVI
```
