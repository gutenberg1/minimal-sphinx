```{role} latexlogo
```
```{role} tttexlogo
```
# Comment visualiser des fichiers Postscript ?

Le traitement des fichiers DVI par des programmes de type `dvips` et assimilés donnent des fichiers au format [PostScript](https://fr.wikipedia.org/wiki/PostScript). Ils sont directement imprimables par des imprimantes dites Postscript, c'est à dire des imprimantes qui contiennent un interpréteur Postscript.

Pour les autres imprimantes, il y a des interpréteurs dans les ordinateurs qui transforment ce fichier Postscript en fichier assimilable par l'imprimante (transformation en langage PCL, ou autre format brut).

## Sous Mac OS

### Ghostview

Les programmes [Ghostscript](http://www.cs.wisc.edu/~ghost/index.html), `Ghostview` et `GSview` sont disponibles pour Mac.

### viewps

Le programme `viewps`, de Tom Kiffe, est inclu dans la distribution {ctanpkg}`CMacTeX` (dernière mise à jour : avril 2009).

```{eval-rst}
.. todo:: // Il y a sans doute des solutions plus actuelles.//
```

## Sous Unix ou Linux

(ghostview-1)=

### Ghostview

Les programmes [Ghostscript](http://www.cs.wisc.edu/~ghost/index.html), `Ghostview` et `GSview` sont des outils classiques sur ce sujet.

### gv

[gv](https://www.gnu.org/software/gv/) est une interface basée sur `Ghostview` et améliorée. Les actions disponibles sont nombreuses et il est possible de lire un document sur écran. On dispose aussi de l'anti-aliasing et de zooms impressionnants sur les parties de la page que l'on veut. `gv` peut même afficher des fichiers `pdf` si l'on dispose de la version de `GhostScript` adéquate.

Si `gv` tourne sur les versions d'Unix les plus courantes, il faut néanmoins disposer du widget [Xaw3d](https://en.wikipedia.org/wiki/X_Athena_Widgets).

### ggv

[ggv](https://www.unix.com/man-page/posix/1/ggv/) qui utilise les librairies graphiques GTK 1 ou 2.

### gnome-gv

Le programme `gnome-gv` est fourni avec l'environnement GNOME. Voir le site de GNOME <http://www.gnome.org/>.

## Sous Windows

(ghostview-2-1)=

### Ghostview

Les programmes [Ghostscript](http://www.cs.wisc.edu/~ghost/index.html), `Ghostview` et `GSview` évoqués ci-dessus sont aussi disponibles pour Windows.

______________________________________________________________________

*Source :* [Ghostscript](https://fr.wikipedia.org/wiki/Ghostscript).

```{eval-rst}
.. meta::
   :keywords: LaTeX,Postscript,format PS,fichier PS,Adobe,ouvrir un fichier PS
```
