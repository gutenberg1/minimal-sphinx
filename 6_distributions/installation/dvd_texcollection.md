```{role} latexlogo
```
```{role} tttexlogo
```
# Où trouver le DVD « TeX collection » ?

Si vous n'avez pas accès à internet, ou si la qualité de votre connexion ne vous permet pas de télécharger des giogaoctets de façon fiable, vous avez intérêt à vous procurer LaTeX sur un support physique.

Et même si vous y avez accès, vous avez peut-être envie d'avoir à portée de main une version figée et testée des extensions disponibles à un instant donné.

Pour cela, le DVD « TeX Collection » est édité chaque année par les {doc}`Groupes d'utilisateurs de TeX </1_generalites/gutenberg>`.

En dehors des cas ci-cessus, il est également très utilisé pour installer LaTeX sur toutes les machines d'une salle de formation

## Que contient le DVD ?

La «TeX Collection» contient des distributions LaTeX prêtes à être installées pour toutes les architectures courantes actuellement, ainsi que toutes les extensions du CTAN sous licence libre. Elle remplit un DVD double couche, et propose :

- La distribution [TeX Live](https://fr.wikipedia.org/wiki/TeX_Live), compatible avec la plupart des architectures (les différents Unix/Linux, MacOS, Windows, etc) ; TeX Live peut être exécutée directement à partir du DVD ou installée sur votre disque dur.
- [MacTeX](https://fr.wikipedia.org/wiki/MacTeX) : un système TeX facile à installer pour MacOS, basé sur TeX Live ; cette distribution comprend un installateur Mac natif, [l'éditeur TeXShop](http://pages.uoregon.edu/koch/texshop/) et quelques autres outils spécifiques au Mac.
- {doc}`ProTeXt </6_distributions/installation/protext_et_miktex>` : un système TeX facile à installer pour Windows, basé sur [MiKTeX](https://fr.wikipedia.org/wiki/MiKTeX), avec une excellente documentation pour vous guider pendant l'installation.
- enfin, une image du CTAN contenant toutes les extensions sous licence libre, prise au moment de l'édition du DVD.

La plupart des groupes d'utilisateurs de TeX offrent gratuitement des exemplaires du DVD à leurs membres. Certains groupes d'utilisateurs peuvent également en vendre des copies supplémentaires, ou les donner, dans le but de promouvoir l'usage de TeX/LaTeX. Contactez votre {doc}`groupe d'utilisateurs local </1_generalites/gutenberg>` ou [le TUG](https://www.tug.org/), en expliquant succinctement votre but (avoir une copie pour votre usage, en distribuer aux participants d'une conférence ou d'une formation...).

Vous pouvez également télécharger l'image du disque à partir du CTAN; le disque d'installation, ProTeXt et MacTeX sont tous également disponibles séparément, ce qui est sans doute un meilleur choix si vous voulez juste installer LaTeX sur votre machine.

{octicon}`alert;1em;sd-text-warning` Ce sont tous des téléchargements assez lourds.

:::{tip}
TeX Live, une fois installée, peut être mise à jour par internet, même si votre connexion est de qualité médiocre.
:::

## Comment accéder aux anciennes versions ?

[Plusieurs serveurs](https://www.tug.org/historic/) hébergent des copies des anciennes versions de la TeX Collection. Ces anciennes versions sont très utiles si vous avez besoin d'installer un système LaTeX tel qu'il était en 2017 ou 2013, par exemple.

Pour plus de détails, référez-vous [au site web du TUG](http://www.tug.org/texcollection/) (en anglais).

[L'association GUTenberg](https://www.gutenberg.eu.org/DVD-TeX-Collection) conserve aussi quelques exemplaires des anciennes versions du DVD. N'hésitez pas à les contacter, ils seront contents de leur donner une nouvelle vie !

______________________________________________________________________

*Sources :*

- {faquk}`The TeX collection <FAQ-CD>`,
- [TeX Collection DVD](https://www.tug.org/texcollection/)

```{eval-rst}
.. meta::
   :keywords: LaTeX,installer LaTeX, DVD LaTeX, LaTeX sans accès à internet
```
