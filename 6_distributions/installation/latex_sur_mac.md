```{role} latexlogo
```
```{role} tttexlogo
```
# Quelles sont les distributions TeX pour les systèmes Mac ?

## MacTeX

Le DVD {doc}`TeX collection </6_distributions/installation/dvd_texcollection>` comprend `MacTeX`, une version [Mac](https://fr.wikipedia.org/wiki/Macintosh) de {tttexlogo}`TeX` Live, décrite sur le [site web](https://tug.org/mactex) du {doc}`TUG </1_generalites/gutenberg>`. `MacTeX` dispose par exemple d'un gestionnaire graphique {tttexlogo}`TeX` Live personnalisé pour Mac, afin que vous puissiez maintenir votre distribution à jour.

Si vous ne disposez pas de ce disque, vous pouvez télécharger la distribution (notez qu'elle est assez volumineuse) depuis :

- la [page dédiée](https://tug.org/mactex) du TUG ;
- la {ctanpkg}`page dédiée <mactex>` du CTAN.

Notez que l'installation de `MacTeX` nécessite le privilège `root`. C'est dommage car il propose plusieurs extras non disponibles sur une {tttexlogo}`TeX` Live standard et donc non accessibles aux personnes qui n'ont pas ce privilège `root`. Pour ces derniers, l'alternative revient donc à utiliser l'installateur `tlinstall` de {tttexlogo}`TeX` Live.

De plus amples informations sont disponibles sur la [page d'aide](http://www.tug.org/mactex/gettinghelp.html) de `MacTeX`.
