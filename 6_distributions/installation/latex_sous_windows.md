```{role} latexlogo
```
```{role} tttexlogo
```
# Quelles sont les distributions TeX pour les systèmes Windows ?

## MikTeX ou TeX Live

Les utilisateurs de [Windows](https://fr.wikipedia.org/wiki/Microsoft_Windows) ont de nos jours le choix entre deux excellentes distributions :  [MiKTeX](https://miktex.org/) et [TeX Live](https://www.tug.org/texlive/). Ce dernier, sur Windows, n'est devenu que ces dernières années un véritable concurrent pour `MiKTeX` établi de longue date. D'ailleurs, même maintenant, `MiKTeX` possède des fonctionnalités qui manquent à {tttexlogo}`TeX` Live. Les deux sont des distributions complètes, offrant toutes les variantes {tttexlogo}`TeX` établies (de {tttexlogo}`TeX` à `XeTeX` et `LuaTeX`), ainsi qu'une large gamme d'outils associés.

`MiKTeX` et {tttexlogo}`TeX` Live offrent toutes deux des outils de gestion permettant en particulier de maintenir son installation à jour, en réinstallant au besoin les extensions qui ont été mises à jour sur CTAN. Ainsi, le délai entre l'apparition d'une mise à jour d'extension et sa mise à disposition auprès des utilisateurs de la distribution peut souvent se limiter à une journée.

`MiKTeX`, de [Christian Schenk](https://tug.org/interviews/schenk.html), est la plus ancienne distribution dans ce duo et compte un large public d'utilisateurs satisfaits. Les dernières versions de `MiKTeX` nécessitent Windows 10 ou une version ultérieure. De son côté, {tttexlogo}`TeX` Live est la distribution dominante utilisée dans le monde des systèmes de type Unix : sa version Windows devrait plaire à ceux qui utilisent à la fois des systèmes de type Unix et Windows.

Les deux distributions peuvent être utilisées dans une configuration qui n'implique aucune installation :

- la [distribution « portable »](https://miktex.org/howto/portable-edition) de `MiKTeX` peut être décompressée sur une clé USB et utilisée sur n'importe quel ordinateur Windows sans utiliser directement le disque dur ;
- le site du TUG décrit la [configuration à adopter](http://www.tug.org/texlive/portable.html) avec {tttexlogo}`TeX` Live pour l'installer {tttexlogo}`TeX` Live sur une clé USB ou pour utiliser le DVD {tttexlogo}`TeX` Live sans installation sur tout.

`MiKTeX` et {tttexlogo}`TeX` Live peuvent être téléchargées et installées, extension par extension, sur le web. Il s'agit d'une opération conséquente, qui ne doit être entreprise que par ceux qui disposent d'une bonne connexion réseau (et de patience !).

Une copie prête à l'emploi de la distribution `MiKTeX`, sur DVD, peut être achetée via le [site web](http://www.miktex.org/cd/) de `MiKTeX`. `MiKTeX` peut également être installé à l'aide de `ProTeXt`, sur le DVD {doc}`TeX collection </6_distributions/installation/dvd_texcollection>`.

Le DVD {doc}`TeX collection </6_distributions/installation/dvd_texcollection>` fournit également un programme d'installation hors ligne pour {tttexlogo}`TeX` Live.

## ProTeXt

Voir la question « {doc}`Quelle est la différence entre proTeXt et MiKTeX ? </6_distributions/installation/protext_et_miktex>` ».

## Les distributions disponibles en passant par CygWin

[CygWin](http://www.cygwin.com), l'environnement de type Unix dans les systèmes Windows (qui fournit également un serveur X-windows), peut gérer des distributions {tttexlogo}`TeX` :

- la distribution `teTeX` (maintenant obsolète) est fournie dans le cadre de la distribution CygWin ;
- il existe une version CygWin de {tttexlogo}`TeX` Live afin que vous puissiez avoir un système à jour.

{tttexlogo}`TeX` sous CygWin est réputé un peu plus lent que les implémentations Win32 telles que `MiKTeX` et, bien sûr, les applications {tttexlogo}`TeX` s'y comportent comme les applications du système Unix.
