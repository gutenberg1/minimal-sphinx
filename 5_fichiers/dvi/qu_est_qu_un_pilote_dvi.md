```{role} latexlogo
```
```{role} tttexlogo
```
# Qu'est-ce qu'un pilote DVI ?

Un pilote {doc}`DVI </5_fichiers/dvi/start>` (*DVI driver* en anglais) est un programme qui prend en entrée un {doc}`fichier DVI </5_fichiers/dvi/qu_est_qu_un_fichier_dvi>` et produit (généralement) un fichier dans un autre format, qui pourra être traité par un programme extérieur à l'écosystème {tttexlogo}`TeX`.

Un pilote peut être conçu pour produire une sortie destinée :

- à l'impression (du PostScript, par exemple) ;
- à un traitement ultérieur (du PostScript encapsulé ou du [PNG](http://savannah.nongnu.org/projects/dvipng/), par exemple pour inclusion dans un autre document) ;
- ou à l'échange de documents (du PDF, par exemple).

Outre le fichier DVI, le pilote a généralement besoin d'informations sur les polices. Celles-ci peuvent être stockées sous forme de bitmaps ou de contours, ou simplement sous forme d'un ensemble de pointeurs vers les polices fournies par l'imprimante elle-même. Chaque pilote attend les informations de police sous un format particulier.

Pour plus d'information sur les formats de polices, voir les pages concernant :

- {doc}`les fichiers PK </5_fichiers/fontes/que_sont_les_fichiers_pk>` ;
- {doc}`les fichiers TFM </5_fichiers/fontes/que_sont_les_fichiers_tfm>` ;
- {doc}`les fontes virtuelles </5_fichiers/fontes/que_sont_les_fontes_virtuelles>` ;
- [l'utilisation des fontes PostScript avec TeX](/5_fichiers/fontes/utiliser_des_fontes_adobe_t1_avec_tex).

______________________________________________________________________

*Sources :*

- {faquk}`What is a DVI driver? <FAQ-driver>`
- [TEX DVI Driver Family Status](https://tex.loria.fr/divers/dvistatus.pdf), Nelson H.F. Beebe, 1989.

```{eval-rst}
.. meta::
   :keywords: LaTeX,concepts,format DVI,conversion d'un fichier DVI,DeVice-Independent file,fichier device-indépendant
```
