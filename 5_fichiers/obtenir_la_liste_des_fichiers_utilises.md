```{role} latexlogo
```
```{role} tttexlogo
```
# Comment connaître les versions des fichiers utilisées dans un document ?

- La commande `\listfiles` affiche, lors de la compilation, les versions, dates, auteurs et commentaires disponibles sur les fichiers inclus. Cette commande n'est autorisée que dans le préambule.

## À quoi ça sert ?

Lorsque vous échangez un document avec quelqu'un d'autre (dans le cadre d'un développement en commun, par exemple), il est préférable que les deux correspondants disposent du même jeu de fichiers, en plus du document en question. Pour être plus précis, votre correspondant a évidemment besoin d'avoir les mêmes extensions (si vous utilisez l'extension {ctanpkg}`url`, il doit aussi l'avoir, par exemple), mais il y a des cas où il devra avoir en plus les mêmes versions. Si par exemple vous disposez d'une version exempte de bogues du package {ctanpkg}`tout_nouveau_tout_neuf` mais que votre collaborateur a toujours une version antérieure, instable... vous allez vous arracher les cheveux jusqu'à ce que vous compreniez que le problème ne vient pas de votre code, mais de cette extension !

La solution la plus simple est d'utiliser la commande LaTeX `\listfiles`. Elle écrit une liste des fichiers utilisés et de leur numéro de version dans le fichier journal. Vous pouvez ainsi récupérer cette liste et la transmettre avec votre fichier, et votre collaborateur n'aura qu'à la comparer à la sienne en cas de problème.

Exemple de sortie :

```latex
% !TEX noedit
\listfiles

\documentclass{article}
  \usepackage[T1]{fontenc}
  \usepackage{lmodern}
  \usepackage[french]{babel}
\begin{document}
Penser à se coucher de bonne heure.
\end{document}
```

```
*File List*
article.cls    2020/04/10 v1.4m Standard LaTeX document class
 size10.clo    2020/04/10 v1.4m Standard LaTeX file (size option)
fontenc.sty
lmodern.sty    2009/10/30 v1.6 Latin Modern Fonts
  babel.sty    2021/03/03 3.55 The Babel package
 french.ldf    2020/10/10 v3.5l French support from the babel system
scalefnt.sty
 keyval.sty    2014/10/28 v1.15 key=value parser (DPC)
  t1lmr.fd    2009/10/30 v1.6 Font defs for Latin Modern
l3backend-pdftex.def    2021-03-18 L3 backend support : PDF output (pdfTeX)
***********
```

Attention, `\listfiles` n'enregistre que les fichiers qui sont inclus par les mécanismes LaTeX « standards » (comme `\documentclass`, `\usepackage`, `\include`, `\includegraphics` et ainsi de suite). La commande `\input`, fournie par LaTeX et utilisée avec la syntaxe LaTeX, comme ceci :

```latex
% !TEX noedit
  \input{mymacros}
```

enregistre bien les détails du fichier `mymacros.tex`, mais si vous l'utilisez avec la syntaxe d'une primitive TeX, comme ceci :

```latex
% !TEX noedit
  \input mymacros
```

`mymacros.tex` ne sera *pas* enregistré, et ne sera donc pas répertorié par `\listfiles`, puisque vous avez contourné son mécanisme qui enregistre les appels à des fichiers.

## Comment faire une archive des fichiers utilisés ?

L'extension {ctanpkg}`snapshot` vous fournit une liste des dépendances d'un document LaTeX, sous une forme que vous pourrez intégrer en tête de document. L'objectif de cette extension est de créer des copies d'archives de documents, mais elle est également utile pour simplement d'échanger des documents.

Par exemple avec le document précédent, cette extension crée un fichier `mon_document.dep` qui contient :

```latex
% !TEX noedit
\RequireVersions{
  *{application}{pdfTeX}  {0000/00/00 v1.40.21}
  *{format} {LaTeX2e}     {2020-10-01 v2.e}
  *{class}  {article}     {2020/04/10 v1.4m}
  *{file}   {size10.clo}  {2020/04/10 v1.4m}
  *{package}{fontenc}     {0000/00/00 v0.0}
  *{package}{lmodern}     {2009/10/30 v1.6}
  *{package}{babel}       {2021/03/03 v3.55}
  *{file}   {french.ldf}  {2020/10/10 v3.5l}
  *{package}{scalefnt}    {0000/00/00 v0.0}
  *{package}{keyval}      {2014/10/28 v1.15}
  *{package}{snapshot}    {2020/06/17 v2.14}
  *{file}   {t1lmr.fd}    {2009/10/30 v1.6}
  *{file}   {l3backend-pdftex.def}{2021-03-18 v3}
}
```

Vous pourrez ensuite appeler le package {ctanpkg}`bundledoc`, qui utilisera cet « instantané » pour construire une archive (une fichier `.tar.gz` ou `.zip`) contenant les fichiers nécessaires à votre document ; il est fourni avec des fichiers de configuration pour une utilisation avec TeX Live sous Unix ou MiKTeX sous Windows. Il est surtout utile lorsque vous envoyez la première copie d'un document à un collaborateur.

## Comment surveiller la compilation ?

Le programme {texdoc}`mkjobtexmf` lance `tex` (ou autre) et surveille quels fichiers sont utilisés pendant la compilation, soit grâce à l'option `-recorder` de `tex`, soit en appelant la commande Unix [strace](https://fr.wikipedia.org/wiki/Strace) pour garder un œil sur ce que fait `tex`. Les fichiers ainsi trouvés sont copiés (ou liés) dans un répertoire de type `texmf`, qui pourra être sauvegardé pour être transmis ou archivé.

______________________________________________________________________

*Sources :*

- {faquk}`All the files used by this document <FAQ-filesused>`,
- [Embed nicely formatted « \\listfiles » into document](https://tex.stackexchange.com/questions/265726/embed-nicely-formatted-listfiles-into-document).

```{eval-rst}
.. meta::
   :keywords: LaTeX,versions des fichiers,versions des modules,versions des packages
```
