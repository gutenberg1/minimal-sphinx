```{role} latexlogo
```
```{role} tttexlogo
```
# Quelles sont les extensions de noms de fichiers utilisées par LaTeX ?

En plus des fichiers TEX et DVI ou PDF, {tttexlogo}`TeX`, {latexlogo}`LaTeX` et ConTeXt produisent et utilisent beaucoup d'autres fichiers. Voici un peu plus d'information sur ces fichiers, en particulier celle permettant de savoir si un fichier peut être supprimé en toute sécurité.

## Définition des fichiers

### Fichiers sources

- **CLS** : fichier de classe de document. Il contient les définitions d'une classe de document (comme un article ou un rapport).
- **DTX** : {doc}`source documentée </1_generalites/documentation/documents/documents_extensions/fichiers_sources_dtx>`. Il sert à générer une extension {latexlogo}`LaTeX` et sa documentation associée.
- **INS** : script d'installation pour un nouveau package.
- **STY** : les fichiers de style ou packages contiennent un ensemble de macros que l'on peut utiliser dans les documents AllTeX.
- **TEX** : fichier source. Il contient le code source de votre document {latexlogo}`LaTeX`.

:::{note}
Mais les fichiers `TEX` ne sont pas les seuls à contenir du code AllTeX. Ainsi, il y a des fichiers qui permettent de faire de la programmation littéraire : on écrit des commandes {latexlogo}`LaTeX` dans les commentaires les programmes, puis on compile avec un outil adapté (Pascal fut le premier langage concerné, C, Scheme, OCaml).
:::

- **WEB** : bien avant l'invention du World Wide Web, le WEB était un langage de programmation mélangeant, historiquement, du Pascal, pour le code, et du TeX, pour la documentation. Il existe de nos jours du CWeb, contenant du C à la place du Pascal, et quelques autres variantes pour les autres langages. On croise parfois le WEB dans le monde {tttexlogo}`TeX` parce que {tttexlogo}`TeX` est écrit en WEB, d'une part, et parce que le WEB produit des documentations en {tttexlogo}`TeX`, d'autre part.

### Fichiers annexes

- **AUX** : fichier auxiliaire {latexlogo}`LaTeX`. Créé lorsque {latexlogo}`LaTeX` est exécuté, il contient des informations qui sont ensuite utilisées par `BibTeX` ou {latexlogo}`LaTeX` lui-même lors d'exécutions ultérieures (par exemple, pour traiter les références croisées). Ce fichier est créé en exécutant {latexlogo}`LaTeX` mais également utilisé la prochaine fois que {latexlogo}`LaTeX` est exécuté.
- **LOF** : fichier de la liste des figures (« List Of Figures »). Il contient les éléments de la liste des figures. Comme le fichier TOC, il est généré lors d'une compilation et est aussi appelé lors de la compilation suivante.
- **LOG** : journal de compilation de {tttexlogo}`TeX` / {latexlogo}`LaTeX`. Il contient l'ensemble des messages d'information générés lors de la compilation : commentaires, avertissements et {doc}`erreurs </2_programmation/erreurs/start>`.
- **LOT** : fichier de la liste des tableaux (« List Of Tables »). Il contient les éléments de la liste des tableauxs. Comme le fichier TOC, il est généré lors d'une compilation et est aussi appelé lors de la compilation suivante.
- **TOC** : fichier de {doc}`table des matières </3_composition/annexes/tables/generer_une_table_des_matieres>` (« Table Of Contents »). Il contient les éléments de la table des matières. Généré lors d'une compilation, il est aussi appelé lors de la compilation suivante (principe similaire à celui du fichier AUX).

### Fichiers en sortie

- **DVI** : «DeVice Independent» est le format de fichier que produit AllTeX de manière naturelle comme le résultat de la compilation d'un document. Ce format est une coquille vide, il indique, pour chaque document, quelle fonte est utilisée (simplement son nom, le `dvi` ne contient pas de police) et, page par page, la position de chaque caractère. Un fichier `dvi` est donc inexploitable si l'on ne dispose pas des polices et des images qui ont été utilisées par le document.
- **PS** : Postscript, format de fichier compréhensible directement par certaines imprimantes, et souvent utilisé comme intermédiaire d'impression : `GhostScript` (un programme) est capable de lire du Postscript et de l'imprimer, il est classique de traduire un `dvi` (avec les polices associées) en Postscript pour imprimer ensuite avec `GhostScript`. Un fichier Postscript peut être vectoriel (*i.e.* indépendant de la résolution de l'imprimante) ou bitmap (\\emph{i.e.} intimement lié à la résolution de l'imprimante). Traditionnellement, {tttexlogo}`TeX` produit des fichier Postscript en bitmap en incluant directement dans le fichier Postscript le contenu du `dvi` et des différents `pk` utiles, ainsi que les images. Le fichier Postscript est normalement autonome.
- **PDF** : «Portable Document Format», format définit par Adobe, et de plus en plus répandu, comme le fichier `dvi`, le fichier PDF est indépendant de la résolution du support final (imprimante, écran, etc.), mais, lui, inclut les images et les polices, il est donc autonome. C'est ce que produisent pdfTeX et pdfLaTeX.

### Fichiers de bibliographie

- **BBL** : fichier de bibliographie triée et insérable. Il correspond au résultat du traitement de BibTeX que {latexlogo}`LaTeX` insère lors de sa prochaine exécution.
- **BIB** : {doc}`références bibliographiques </3_composition/annexes/bibliographie/construire_un_fichier_bibtex>`. Il contient les données bibliographiques utilisées par `BibTeX`.
- **BLG** : journal de `BibTeX`. Il est l'équivalent du fichier LOG de {latexlogo}`LaTeX` pour `BibTeX`.
- **BST** : {doc}`style BibTeX </3_composition/annexes/bibliographie/choisir_un_style_de_bibliographie>`. Il définit une grande part de la mise en forme de la bibliographie avec `BibTeX`.

### Fichiers d'index

- **IDX** : fichier créé par le biais de la commande `makeindex` de {tttexlogo}`TeX` recensant tous les items à mettre dans l'index.
- **ILG** : fichier `log` pour les index.
- **IND** : fichier créé par `makeindex` et utilisé par {tttexlogo}`TeX` lors de l'affichage de l'index.
- **IST** : fichier de style d'index.

## Fichiers liés aux polices

- **FD** : fichier de définition de fonte. Il sert à générer le document en sortie. voir la question « {doc}`Que signifient les sigles T1, mf, fd, etc. ? </5_fichiers/fontes/codage_t1_et_fichiers_de_description_des_fontes>` ».

```{eval-rst}
.. todo:: Le précédent paragraphe appelle une révision.
```

- **GF** : «Generic Font», un fichier dans lequel {tttexlogo}`TeX` et ses affidés (logiciels de visualisation, d'impression, etc.) trouvent les dessins des caractères en bit à bit, pour un fichier `tfm`, il existe normalement un ou plusieurs fichiers `gf` (un par résolution prévu, classiquement un pour l'écran et un pour l'imprimante).
- **MF** : voir la question « {doc}`Que signifient les sigles T1, mf, fd, etc. ? </5_fichiers/fontes/codage_t1_et_fichiers_de_description_des_fontes>` ».

```{eval-rst}
.. todo:: Le précédent paragraphe appelle une révision.
```

- **PK** : Packed, version plus compressée du fichier `gf`, le fichier `gf` est en fait tombé en désuétude et n'existe que de manière transitoire sur un système~ : quand le dessin d'une police manque à une résolution donnée, le fichier `gf` est créé à la volée, puis immédiatement converti en fichier `pk` pour économiser de la place.
- **TFM** : {tttexlogo}`TeX` font metric, un fichier dans lequel {tttexlogo}`TeX` peut lire les dimensions et les espacements à respecter pour chaque caractère d'une police donnée, ainsi que toutes les infos relatives au positionnement des caractères (ligatures, règles de choix des symboles de grande taille pour les maths, etc.).
- **VF** : voir la question « {doc}`Que signifient les sigles T1, mf, fd, etc. ? </5_fichiers/fontes/codage_t1_et_fichiers_de_description_des_fontes>` ».

```{eval-rst}
.. todo:: Le précédent paragraphe appelle une révision.
```

### Fichiers liés à des extensions spécifiques

- **BRF** : fichier pour {ctanpkg}`backref`. Il collecte les informations liées aux commandes de {ctanpkg}`backref`.

## Fichiers supprimables

Parmi ceux-ci, les fichiers AUX, LOG et BLG peuvent être supprimés en toute sécurité. Le fichier BBL peut également être supprimé si cela ne vous dérange pas de relancer `BibTeX` (et que vous avez accès aux fichiers BIB nécessaires). D'autres fichiers ne devraient pas être supprimés mais peut-être déplacés vers un endroit plus approprié. En particulier, les fichiers FD, CLS et DTX sont bien plus utiles dans l'arborescence {tttexlogo}`TeX` que dans le dossier du document sur lequel vous travaillez.

______________________________________________________________________

*Source :* [File extensions related to LaTeX, etc.](https://tex.stackexchange.com/questions/7770/file-extensions-related-to-latex-etc)

```{eval-rst}
.. todo:: Ce dernier paragraphe appelle une révision : « // À utiliser pour compléter la page.// »
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,suffixes,noms de fichiers,type de fichiers
```
