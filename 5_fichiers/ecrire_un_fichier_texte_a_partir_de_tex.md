```{role} latexlogo
```
```{role} tttexlogo
```
# Comment écrire des fichiers texte avec TeX ?

Indépendamment de sa sortie en PDF ou DVI, TeX peut écrire dans des fichiers à partir de votre document. Cette fonction est souvent pratique, mais elle est surtout vitale pour les fonctions de base de LaTeX (et en fait pour presque toutes les extensions TeX de haut niveau).

Les usages de base de la création de fichiers externes sont évidentes : mémoriser les titres des sections pour construire la table des matières, se souvenir des noms d'étiquettes et des numéros des sections ou des figures correspondantes, tout cela pour que la compilation suivante de votre document puisse les utiliser. Un usage moins évident concerne les numéros de pages : dans TeX, ils changent au cours de la compilation, au fur et à mesure que le texte et les flottants sont positionnés. Il faut donc un certain soin pour les enregistrer. L'astuce est que les opérations d'écriture, faites par la commande `\write` ne sont exécutées qu'au moment où la page est envoyée vers le fichier DVI ou PDF. Ainsi, si vous faites en sorte que votre macro contenant le numéro de page (`\thepage`, en LaTeX) ne soit pas développée avant que la page ne soit écrite, le numéro écrit est correct, puisqu'à ce moment-là TeX peut garantir que le numéro de page correspond réellement à la page envoyée.

Maintenant, il y a des moments où vous voulez écrire quelque chose **immédiatement** : par exemple, pour interagir avec l'utilisateur. TeX se plie à cette exigence avec la primitive `\immediate` :

```latex
% !TEX noedit
\immediate\write\terminal{J'attends...}
```

qui écrit sur le terminal son message à destination de l'utilisateur.

Ce qui nous amène à la raison de ce `\terminal`. TeX peut écrire jusqu'à 16 flux simultanément, et l'argument de `\write` indique lequel doit être utilisé. Des macros existent pour allouer des flux à vos usages : Plain TeX fournit une macro `\newwrite` (utilisée comme `\newwrite\streamname`, qui définit `\streamname` comme numéro du flux). En fait, `\terminal` (ou son équivalent) est le premier flux de sortie qui n'a pas été configuré : il n'est pas attaché à un fichier, et si on demande à TeX d'écrire dans un flux qui n'est pas attaché à un fichier, il enverra la sortie vers le terminal (et le fichier `.log`).

______________________________________________________________________

*Sources :*

- {faquk}`Writing (text) files from TeX <FAQ-write>`,
- [Write values to a file](https://tex.stackexchange.com/questions/290054/write-values-to-a-file).

```{eval-rst}
.. meta::
   :keywords: LaTeX,fichiers externes,créer un fichier depuis TeX,primitive \\write
```
