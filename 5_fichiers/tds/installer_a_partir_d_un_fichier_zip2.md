```{role} latexlogo
```
```{role} tttexlogo
```
# Existe-t-il des fichiers d'installation prêts à l'emploi ?

La {doc}`TDS </5_fichiers/tds/la_tds>` est une structure simple et presque tous les fichiers peuvent être installés simplement en les plaçant au « bon » endroit et en mettant à jour un seul index. Les polices de caractères sont une grande exception à cette règle, à moins qu'elles ne soient distribuées en tant que source `MetaFont`.

Le {doc}`CTAN </1_generalites/glossaire/qu_est_ce_que_le_ctan>` propose donc des fichiers « TDS-ZIP » qui intègrent une structure de répertoire correspondant à la TDS. L'équipe CTAN a demandé aux auteurs d'extension de fournir ce type de fichier (quitte à aider les auteurs s'ils rencontrent des difficultés) : elle espère que ceci contribuera à la simplification de vie des utilisateurs des extensions et donc à la bonne santé de la communauté {tttexlogo}`TeX`.

Si il y a encore peu de fichiers `tds.zip` (en comparaison avec le grand nombre d'extensions disponibles), ce nombre de fichiers augmente régulièrement. Il faudra cependant beaucoup de temps avant que l'ensemble des extensions ne soit couvert.

L'utilisation des fichiers est décrite dans la question « {doc}`Comment installer des fichiers ZIP prêts à l'emploi ? </5_fichiers/tds/installer_a_partir_d_un_fichier_zip>` ».

______________________________________________________________________

*Source :* {faquk}`Ready-built installation files on the archive <FAQ-tds-zip>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,installing
```
