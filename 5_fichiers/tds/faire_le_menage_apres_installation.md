```{role} latexlogo
```
```{role} tttexlogo
```
# Comment faire le ménage après l'installation d'une extension ?

Il n'y a généralement pas grand-chose à faire une fois que vous avez terminé l'installation d'une extension.

Les choses que vous pouvez supprimer sont :

- le fichier d'archive lui-même, si vous avez récupéré l'extension sous forme d'un fichier « `.zip` » ou équivalent ;
- les fichiers « `.dtx` » et « `.ins` », si vous avez choisi de ne pas les installer avec la documentation ;
- et les fichiers « `.aux` », « `.log` », « `.idx` », etc., générés par la compilation de la documentation.

Un moyen de se faciliter la vie est de télécharger l'archive dans un répertoire temporaire, puis de supprimer ce répertoire et son contenu une fois l'installation terminée.

______________________________________________________________________

*Source :* {faquk}`Tidying up after installation <FAQ-inst-tidy>`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,installing,installer LaTeX,installer un package LaTeX,nettoyer une installation
```
