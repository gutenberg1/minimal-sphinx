```{role} latexlogo
```
```{role} tttexlogo
```
# Comment installer des éléments sur un système (La)TeX ?

Installer (ou remplacer) des éléments sur votre système {tttexlogo}`TeX` ou {latexlogo}`LaTeX` peut être assez compliqué. Les questions listées ici tentent de fournir une approche étape par étape, à partir du moment où vous avez décidé ce que vous voulez installer :

- « {doc}`Où télécharger cette extension ? </6_distributions/comment_trouver_un_package>` » ;
- « {doc}`Comment générer la documentation d'une extension ? </1_generalites/documentation/documents/documents_extensions/documentation_des_packages3>` ;
- « {doc}`Où installer cette extension ? </5_fichiers/tds/ou_installer_les_packages>` » ;
- « {doc}`Comment installer des fichiers « là où (La)TeX peut les trouver » ? </5_fichiers/installer_des_fichiers_pour_latex>` » ;
- enfin, « {doc}`Comment faire le ménage après l'installation d'une extension ? </5_fichiers/tds/faire_le_menage_apres_installation>` ».

______________________________________________________________________

*Source :* {faquk}`Installing things on a (La)TeX system <FAQ-installthings>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,installation
```
