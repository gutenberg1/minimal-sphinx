```{role} latexlogo
```
```{role} tttexlogo
```
# Qu'est-ce que la « programmation lettrée » ?

La [programmation lettrée](https://fr.wikipedia.org/wiki/Programmation_lettrée) (*literate programming* en anglais) est la combinaison de la documentation et de la source d'une manière adaptée à la lecture par des êtres humains. En général, les programmes lettrés combinent source et documentation dans un seul fichier. Des outils de programmation dédiés analysent ensuite le fichier pour produire soit une documentation lisible soit une source compilable.

Ce type de programmation est utilisé dans le cadre de {tttexlogo}`TeX` et {latexlogo}`LaTeX` :

- le style WEB, utilisant la programmation lettré, a été créé par Donald Knuth lors du développement de {tttexlogo}`TeX` ;
- le style de programmation par « {doc}`source documentée </1_generalites/documentation/documents/documents_extensions/fichiers_sources_dtx>` » est considéré par certains comme une forme de programmation lettrée, bien qu'il ne contienne qu'un sous-ensemble des constructions utilisées par Knuth.

Une discussion sur la programmation lettrée est menée dans le groupe de discussion `comp.programming.literate` (le CTAN propose une [FAQ de ce groupe](https://ctan.org/tex-archive/help/)). Une autre bonne source d'informations est le site <http://www.literateprogramming.com/>.

______________________________________________________________________

*Source :* {faquk}`What is Literate Programming? <FAQ-lit>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,misc
```
