```{role} latexlogo
```
```{role} tttexlogo
```
# Comment convertir un fichier PostScript en ASCII ?

- Veuillez vous reporter à la section \\vref\{postscriptascii}.

```{eval-rst}
.. meta::
   :keywords: LaTeX,PostScript
```
