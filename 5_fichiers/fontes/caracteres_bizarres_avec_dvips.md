```{role} latexlogo
```
```{role} tttexlogo
```
# Caractères bizarres dans la sortie de \`dvips\`

Vous avez innocemment généré des fichiers Postscript en utilisant `dvips`, et il y a des transpositions bizarres dedans : par exemple, la ligature « ﬁ » (`fi`) apparaît comme un symbole « Â£ ». Il s'agit d'un effet secondaire indésirable des précautions {doc}`décrites à propos de la génération de PostScript destinés à être transformés en PDF </5_fichiers/pdf/generer_un_fichier_pdf_de_qualite>`. L'option `-G1` évoquée à ce sujet est appropriée pour les polices de texte de Knuth, mais ne fonctionne pas avec les polices de texte qui ne suivent pas les modèles de Knuth (comme les polices fournies par Adobe).

Si le problème se pose, supprimez l'option `-G1` :

- si vous l'utilisiez explicitement, ne le faites pas ;
- si vous utilisiez `-Ppdf`, ajoutez `-G0` pour supprimer l'option implicite dans le fichier de la pseudo-imprimante.

Le problème a été corrigé dans dvips v 5.90 (et les versions ultérieures) ; il est peu probable qu'il se reproduise un jour...

______________________________________________________________________

*Source :* {faquk}`Weird characters in \`dvips\` output <FAQ-charshift>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,fonts,fichier Postscript,caractères étranges,DVI to PS,conversion de fichier DVI
```
