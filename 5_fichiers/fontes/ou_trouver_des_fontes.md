```{role} latexlogo
```
```{role} tttexlogo
```
# Où trouver des polices Metafont ?

- Sur <https://www.ctan.org/> bien sûr, dans <https://www.ctan.org/%7Bfonts/%7D> !

# Où trouver des polices TrueType et Postscript ?

- En dehors des fondeurs professionnels connus (Adode, Bitstream, etc.), il a des sites qui proposent des polices librement téléchargeables.

En voici quelques uns, qui proposent des fontes libres ou payantes :

- <http://www.1001freefonts.com/>
- <http://hopi.dtcc.edu/~berlin/fonts.html>
- <http://www.excite.co.uk/directory/Computers/Software/Fonts/Multiscript_Fonts>
- <http://www.fontasy.de/>
- <http://www.fontface.com/>
- <http://www.threeweb.ad.jp/logos/ainet/fontland.html>
- <http://www.fontsearchengine.com/cgi-bin/searchcsv.pl?search=unicode&method=exact>

```{eval-rst}
.. meta::
   :keywords: LaTeX,Postscript
```
