```{role} latexlogo
```
```{role} tttexlogo
```
# Unicode Maths using OpenType fonts

The [ISO standard Universal Coding Scheme](https://fr.wikipedia.org/wiki/ISO/CEI_10646) (UCS), which is commonly known as [Unicode](https://fr.wikipedia.org/wiki/Unicode), was adopted early by the designers of TrueType (TTF) and OpenType (OTF) fonts. The flexibility of the fonts offers hope, for the first time, of a uniform method for typesetting essentially any language.

TeX users have been eagerly adopting the fonts, for some time, using {doc}`XeTeX </1_generalites/glossaire/qu_est_ce_que_xetex>` and {doc}`LuaTeX </1_generalites/glossaire/qu_est_ce_que_luatex>`.

While TeX users were investigating the use of these text fonts, ISO was extending Unicode to provide a means of expressing mathematics. As this work proceeded, Microsoft and (separately) a consortium of publishing companies were developing OpenType maths fonts (Microsoft contributed on the development of the concepts, within the ISO process). MicroSoft's OpenType Maths font, [Cambria Math](<https://fr.wikipedia.org/wiki/Cambria_(police_d'écriture)>) ([article en anglais plus détaillé](<https://en.wikipedia.org/wiki/Cambria_(typeface)#Cambria_Math>)) has been available for purchase for some time.

The first free OpenType Maths font to appear was [Asana Math](https://en.wikipedia.org/wiki/Asana-Math), which was eventually followed by the publishers' consortium's offer of an interim version of their font, [STIX](https://en.wikipedia.org/wiki/STIX_Fonts_project), which has been redeveloped to provide a more usable whole, [XITS](https://en.wikipedia.org/wiki/XITS_font_project), by a group of TeX users.

Other fonts are appearing, including `TeX Gyre Termes Math` (based on Times-like fonts) and `Tex Gyre Pagella Math` (based on Palatino-like fonts), and [LM Math](http://www.gust.org.pl/projects/e-foundry/latin-modern) extending the OpenType version of the [Latin Modern](https://fr.wikipedia.org/wiki/Computer_Modern) font family.

Actually using a unicode maths font is quite a complicated business, but the LaTeX package {ctanpkg}`unicode-math` (supported by the {ctanpkg}`fontspec` package) does the essential groundwork.

______________________________________________________________________

*Sources :*

- {faquk}`Unicode Maths using OpenType fonts <FAQ-otf-maths>`,
- [TUGboat Vol. 27 (2006), n°2 : The New Font Project : TEX Gyre](https://www.tug.org/TUGboat/tb27-2/tb87hagen-gyre.pdf),
- [TUGboat Vol. 39 (2018), n°3 : TEX Gyre text fonts revisited](https://tug.org/TUGboat/tb39-3/tb123jackowski-gyre.pdf).

```{eval-rst}
.. meta::
   :keywords: LaTeX,fontes mathématiques,police mathématique,luatex,xetex,composition des mathématiques
```
