```{role} latexlogo
```
```{role} tttexlogo
```
# How to do bold-tt or bold-sc

LaTeX, as delivered, offers no means of handling bold "teletype" or small-caps fonts. There's a practical reason for this (Knuth never designed such fonts), but there are typographical considerations too (the "medium weight" `cmtt` font is already pretty bold (by comparison with other fixed-width fonts), and bold small-caps is not popular with many professional typographers).

There's a set of "extra" MetaFont files on CTAN that provide bold versions of both `cmtt` and `cmcsc` (the small caps font). With modern TeX distributions, one may bring these fonts into use simply by placing them in an {doc}`appropriate place in the « texmf » tree </5_fichiers/tds/ou_installer_les_packages>` (these are (La)TeX-specific files, so the "*public*" supplier would be an appropriate place). Once you've {doc}`rebuilt the file indexes as necessary </5_fichiers/installer_des_fichiers_pour_latex>`, TeX (and friends) will automatically build whatever font files they need when you first make reference to them. There's a jiffy package {ctanpkg}`bold-extra` that builds the necessary font data structures so that you can use the fonts within LaTeX.

Another alternative is to use the {doc}`EC fonts </5_fichiers/fontes/que_sont_les_fontes_ec>`, which come with bold variants of the small-caps fonts.

If you need to use Type 1 fonts, you can't proceed with Knuth-style fonts, since there are no Type 1 versions of the {ctanpkg}`mf-extra <cm-mf-extra-bold>` set. There are, however, Type 1 distributions of the EC fonts, so you can switch to EC and use them; alternatives are discussed in {doc}`8-bit Type 1 fonts </5_fichiers/fontes/fontes_t1_8bits>`.

Of course, commercial fixed-width fonts (even the default `Courier`) almost always come with a bold variant, so that's not a problem. Furthermore {doc}`PSNFSS </5_fichiers/fontes/utiliser_des_fontes_adobe_t1_avec_tex>` will usually provide "faked" small caps fonts, and has no compunctions about providing them in a bold form. `Courier` is (as we all know, to our cost) freely available; a far more presentable monospace font is `LuxiMono`, which is also freely available (monospace text in the typeset version of this FAQ uses `LuxiMono`, with the metrics and LaTeX support available on the archive.

______________________________________________________________________

*Source :* {faquk}`How to do bold-tt or bold-sc <FAQ-bold-extras>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,polices,fontes,petites capitales grasses,gras typewriter
```
