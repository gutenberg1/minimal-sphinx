```{role} latexlogo
```
```{role} tttexlogo
```
# Comment utiliser `\textsc` dans un titre en conservant le gras ?

- Depuis les dernières modifications dans les polices EC, la simple utilisation de ces polices suffit à activer les petites capitales grasses.

:::{note}
Les petites capitales grasses n'existent pas dans les polices de D. Knuth. En revanche, elles existent dans les polices EC.
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
