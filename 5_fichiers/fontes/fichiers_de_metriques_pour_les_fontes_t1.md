```{role} latexlogo
```
```{role} tttexlogo
```
# TeX font metric files for Type 1 fonts

Reputable font vendors such as Adobe supply metric files for each font, in AFM (Adobe Font Metric) form; these can be converted to TFM (TeX Font Metric) form. Most modern distributions have prebuilt metrics which will be more than enough for many people; but you may need to do the conversion yourself if you have special needs or acquire a new font. One important question is the *encoding* of (Latin character) fonts; while we all more or less agree about the position of about 96 characters in fonts (the basic ASCII set), the rest of the (typically) 256 vary. The most obvious problems are with floating accents and special characters such as the "pounds sterling" sign. There are three ways of dealing with this : either you change the TeX macros which reference the characters (not much fun, and error-prone); or you change the encoding of the font (easier than you might think); or you use {doc}`virtual fonts </5_fichiers/fontes/que_sont_les_fontes_virtuelles>` to *pretend* to TeX that the encoding is the same as it is used to. LaTeX2e has facilities for dealing with fonts in different encodings; read the {doc}`LaTeX Companion </1_generalites/documentation/livres/documents_sur_latex>` for more details. In practice, if you do much non-English (but Latin script) typesetting, you are strongly recommended to use the {ctanpkg}`fontenc` package with option `T1` to select {doc}`Cork </5_fichiers/fontes/que_sont_les_fontes_ec>`" encoding.

An alternative favoured by some is Y&Y's "private" LY1 encoding, which is designed to sit well with "Adobe standard" encoded fonts. Basic macro support of LY1 is available : note that the "relation with Adobe's encoding" means that the LY1 user needs no virtual fonts.

Alan Jeffrey's `fontinst` package is an AFM to TFM converter written in TeX; it is used to generate the files used by LaTeX2e's PSNFSS package to support use of PostScript fonts. It is a sophisticated package, not for the faint-hearted, but is powerful enough to cope with most needs. Much of its power relies on the use of {doc}`virtual fonts </5_fichiers/fontes/que_sont_les_fontes_virtuelles>`.

For slightly simpler problems, Rokicki's `afm2tfm`, distributed with `dvips`, is fast and efficient; note that the metrics and styles that come with `dvips` are *not* currently LaTeX2ε compatible.

For the Macintosh (classic), there is a program called `EdMetrics` which does the job (and more). `EdMetrics` comes with the (commercial) {doc}`Textures </6_distributions/implementations_commerciales>` distribution, but is itself free software, and is available on CTAN.

______________________________________________________________________

*Source :* {faquk}`TeX font metric files for Type 1 fonts <FAQ-metrics>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,fontes,polices de caractères,polices postscript,polices T1,fontes T1,fichier de métriques
```
