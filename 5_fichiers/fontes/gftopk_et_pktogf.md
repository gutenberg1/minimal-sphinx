```{role} latexlogo
```
```{role} tttexlogo
```
# À quoi servent « gftopk » et « pktogf » ?

Les programmes `gftopk` et `pktogf` sont deux petits utilitaires qui servent à compresser et décompresser les fichiers de fontes produits par {doc}`MetaFont </1_generalites/glossaire/qu_est_ce_que_metafont>`.

Typiquement, le programme MetaFont (`mf`) compile un fichier source, d’extension MF, pour produire :

- un fichier de fonte matricielle (*raster font*) dont l'extension se termine par GF (pour « *generic font* ») ;
- un fichier d'extension TFM, pour « *TeX font metrics* » ;
- et un fichier journal.

Généralement, après cette compilation, on compresse le fichier GF en un fichier PK (*packed raster file*), de taille beaucoup plus petite, à l’aide du programme `gftopk`. Les fichiers PK peuvent être directement utilisés par les logiciels qui lisent les fichiers DVI (pour affichage sur écran ou conversion en PostScript ou PDF).

`pktogf` réalise l'opération inverse, en décompressant les fichiers PK pour donner des fichiers GF.

Leur syntaxe est très simple :

```bash
gftopk fichier_GF [ fichier_PK ]

pktogf fichier_PK [ fichier_GF ]
```

Si l'utilisateur veut avoir des détails sur la conversion en cours, il est possible d'ajouter l'option `-v` (mode *v*erbeux) à la ligne de commande.

:::{important}
Le nom de fichier `⟨fichier_GF⟩` donné en argument doit être complet (avec son extension). En effet, pour chaque `⟨fichier_GF⟩`, la résolution de la fonte fait partie de l'extension complète (par exemple `.300gf` pour un fichier à 300 DPI) et ça n'aurait donc pas de sens qu'une extension par défaut soit attendue.

S'il n'est pas donné, le nom du fichier PK de sortie sera construit à partir du nom de fichier d'entrée, en remplaçant GF par PK : par exemple `cmr10.300gf` deviendra `cmr10.300pk`.
:::

______________________________________________________________________

*Sources :*

- [gftopk manual page](https://linux.die.net/man/1/gftopk),
- [pktogf manual page](https://linux.die.net/man/1/pktogf).

```{eval-rst}
.. meta::
   :keywords: LaTeX,metafont,définition,conversion de format de fonte
```
