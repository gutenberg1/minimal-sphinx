```{role} latexlogo
```
```{role} tttexlogo
```
# Comment utiliser une police ?

## Avec pdflatex

```{eval-rst}
.. todo:: Faire que les exemples compilent (ils fonctionnent si on les compile sur sa machine)
```

- Pour déclarer une police de caratères par défaut dans un document, on peut utiliser les commandes `\familydefault`, `\encodingdefault`, `\seriesdefault` et `\shapedefault`.

Exemple~ :

```latex
% !TEX noedit
\documentclass{report}
\renewcommand{\familydefault}{cmtt}
\begin{document}
\verb=\renewcommand{\familydefault}{cmtt}=
dans le préambule d'un document permet de
sélectionner la police computer modern de
type machine à écrire pour tout le corps
du document.
\end{document}
```

```latex
\documentclass{report}
\begin{document}
\begin{verbatim}\renewcommand{\familydefault}{cmtt}\end{verbatim}
\texttt{dans le préambule d'un document permet de
sélectionner la police computer modern de
type machine à écrire pour tout le corps
du document.}
\end{document}
```

En {latexlogo}`LaTeX`, un certain nombre de packages permettent de faire appel à une police particulière.

Exemple~ :

```latex
% !TEX noedit
\documentclass{report}
\usepackage{helvet}
\begin{document}
\verb=\usepackage{helvet}= dans le préambule
d'un document permet de sélectionner la police
helvet pour tout le corps du document.
\end{document}
```

```latex
\documentclass{report}
\usepackage{helvet}
\begin{document}
\verb=\usepackage{helvet}= dans le préambule
d'un document permet de sélectionner la police
helvet pour tout le corps du document.
\end{document}
```

- À un niveau plus bas, dans la création d'un style par exemple, la sélection d'une police peut se faire de la manière suivante :

```latex
% !TEX noedit
\fontfamily{ccr}\fontencoding{T1}\fontseries{c}%
\fontshape{sl}\fontsize{9}{11pt}\selectfont
```

ou encore :

```latex
% !TEX noedit
\fontsize{14}{16pt}\usefont{OT1}{cmdh}{bc}{it}
```

- Pour définir une commande de changement de police, on peut utiliser `DeclareFixedFont`.

Exemple~ :

```latex
% !TEX noedit

 \DeclareFixedFont{\petitefonte}%
                  {\encodingdefault}%
                  {\familydefault}%
                  {\seriesdefault}%
                  {\shapedefault}%
                  {6pt}
 \newcommand{\petit}{\petitefonte}
```

## Avec Xe(La)TeX et Lua(La)TeX

```{eval-rst}
.. todo:: Donner des exemples
```

Utiliser l'extension {ctanpkg}`fontspec` (éventuellement via {ctanpkg}`polyglossia`).

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
