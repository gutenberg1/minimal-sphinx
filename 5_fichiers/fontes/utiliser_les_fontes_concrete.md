```{role} latexlogo
```
```{role} tttexlogo
```
# Using the « Concrete » fonts

The [Concrete Roman](https://en.wikipedia.org/wiki/Concrete_Roman) fonts were designed by Don Knuth for a book called "Concrete Mathematics", which he wrote with Graham and Patashnik (*the* Patashnik, of BibTeX fame). Knuth only designed text fonts, since the book used the Euler fonts for mathematics. The book was typeset using Plain TeX, of course, with additional macros that may be viewed in the file `gkpmac.tex` at {ctanpkg}`gkpmac`.

The packages {ctanpkg}`beton`, {ctanpkg}`concmath`, and {ctanpkg}`ccfonts` are LaTeX packages that change the default text fonts from Computer Modern to Concrete. Packages {ctanpkg}`beton` and {ctanpkg}`ccfonts` also slightly increase the default value of `\baselineskip` to account for the rather heavier weight of the Concrete fonts. If you wish to use the `Euler` fonts for mathematics, as Knuth did, there's the {ctanpkg}`euler` package which has been developed from Knuth's own Plain TeX-based set : these macros are currently deprecated (they clash with many things, including {ctanpkg}`amsmath`). The independently-developed {ctanpkg}`eulervm` bundle is therefore preferred to the {ctanpkg}`euler` package. (Note that installing the {ctanpkg}`eulervm` bundle involves installing a series of virtual fonts. While most modern distributions seem to have the requisite files installed by default, you may find you have to install them. If so, see the file `readme` in the {ctanpkg}`eulervm` distribution.)

A few years after Knuth's original design, Ulrik Vieth designed the Concrete Math fonts. Packages {ctanpkg}`concmath`, and {ctanpkg}`ccfonts` also change the default math fonts from Computer Modern to Concrete and use the Concrete versions of the AMS fonts (this last behaviour is optional in the case of the {ctanpkg}`concmath` package).

There are no bold Concrete fonts, but it is generally accepted that the Computer Modern Sans Serif demibold condensed fonts are an adequate substitute. If you are using {ctanpkg}`concmath` or {ctanpkg}`ccfonts` and you want to follow this suggestion, then use the package with `boldsans` class option (in spite of the fact that the {ctanpkg}`concmath` documentation calls it `sansbold` class option). If you are using {ctanpkg}`beton`, add

```latex
% !TEX noedit
\renewcommand{\bfdefault}{sbc}
```

to the preamble of your document.

Type 1 versions of the fonts are available. For OT1 encoding, they are available from [MicroPress](FAQ-psfchoice). The {doc}`CM-Super fonts </5_fichiers/fontes/tracer_les_contours_d_une_police_metafont>` contain Type 1 versions of the Concrete fonts in T1 encoding.

______________________________________________________________________

*Source :* {faquk}`Using the "Concrete" fonts <FAQ-concrete>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,fontes,polices de caractères
```
