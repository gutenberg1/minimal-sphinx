```{role} latexlogo
```
```{role} tttexlogo
```
# Installing a Type 1 font

Once you have a prepared Type 1 font, either direct from CTAN or the like, or having {doc}`"prepared" it yourself </5_fichiers/fontes/preparer_une_fonte_t1>`, you can get on with installation.

The procedure is merely an extension of that for packages, etc., so much of what follows will be familiar :

- **Install the files**, in your local `texmf` tree (the advice about installing non-standard things applies here, too). The following list gives reasonable destinations for the various files related to a font family `⟨fname⟩` :

| Extension      | Path                                |
| -------------- | ----------------------------------- |
| `.pfb`, `.pfa` | `.../fonts/type1/⟨foundry⟩/⟨fname⟩` |
| `.tfm`         | `.../fonts/tfm/⟨foundry⟩/⟨fname⟩`   |
| `.vf`          | `.../fonts/vf/⟨foundry⟩/⟨fname⟩`    |
| `.sty`, `.fd`  | `.../tex/latex/⟨fname⟩`             |
| `.map`         | `.../fonts/map/dvips/⟨foundry⟩`     |

but if you are lucky, you will be starting from a distribution from CTAN and there is a corresponding `tds.zip` file : using this TDS-file saves the bother of deciding where to put your files in the TDS tree.

- **Regenerate the file indexes** (as described in {doc}`package installation </5_fichiers/installer_des_fichiers_pour_latex>`) :
- **Update the maps** for `dvips`, pdfTeX and others :

On any current TeX Live-based system, or a teTeX v3.0 system, execute the command

```bash
updmap-sys --enable Map ⟨fname⟩.map
```

as root. (If you *can* use `updmap-sys` --- do; if not --- presumably because your (La)TeX system was set up by someone else --- you have to fall back on plain `updmap`, but be aware that it's a potent source of confusion, setting up map sets that might be changed behind your back.)

On a current MiKTeX system, update the system file `updmap.cfg`, using the shell command

```bash
initexmf --edit-config-file updmap
```

adding a line at the end :

```text
Map ⟨fname⟩.map
```

for each font family ⟨*fname*⟩ you are adding to the system. Now generate revised maps with the shell command

```bash
initexmf --mkmaps
```

This, and other matters, are described in MiKTeX ["advanced" documentation](http://docs.miktex.org/manual/advanced.html).

Both processes (preparing and installing a font) are very well (and thoroughly) described in Philipp Lehman's {ctanpkg}`guide to font installation <fontinstallationguide>`.

______________________________________________________________________

*Source :* {faquk}`Installing a Type 1 font <FAQ-instt1font>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,installation,fontes,polices de caractères,polices PostScript
```
