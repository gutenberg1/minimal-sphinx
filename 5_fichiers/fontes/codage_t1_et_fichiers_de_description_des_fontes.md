```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifient les sigles T1, mf, fd, etc. ?

- En 1990, lors d'une réunion d'utilisateurs de {tttexlogo}`TeX`, à Cork, il a été décidé de développer une table de codage particulière pour les polices de {tttexlogo}`TeX`. Cette table contient des signes diacritiques et un certains nombre de symboles qui permettent de composer des textes dans un bon nombre de langues européennes.

Ce codage s'appelle T1 (parfois Cork encoding), et l'ancien codage de {tttexlogo}`TeX` s'appelle OT1 (Old T1). Les autres codages (U, etc.) concernent des polices particulières et/ou des polices qui ne respectent ni T1 ni OT1.

Les polices « standard » de {tttexlogo}`TeX` qui respectent ce codage s'appellent fontes EC (pour european coding). Les fontes DC étaient une pré-version des fontes EC. La phase de mise au point des fontes EC ayant duré un certain nombre d'années, on s'y perd un peu.

Les fontes TC (text companion) contiennent un certain nombre de caractères textuels utilisés en mode mathématique. À l'origine, D. Knuth prenait ces caractères dans les polices textes normales, mais cela pose des problèmes si vous essayez d'utiliser d'autres polices de texte. C'est pour cela qu'on préfère maintenant les mettre à part.

Tout ce qui vient d'être vu concerne les problèmes de codage.

Pour les `pk`, `mf`, etc. voici un rapide aperçu de la question :

- **fichiers `mf`:** sources METAFONT. À partir de là, METAFONT génère les fichiers `tfm` et `pk`;
- **fichiers `tfm`:** métriques des fontes. Contient la taille des caractères, les corrections d'espacement éventuelles, etc. {tttexlogo}`TeX` a impérativement besoin de ces fichiers pour compiler un document;
- **fichiers `pk`:** polices au format bitmap. C'est ce qui est utilisé pour la prévisualisation et l'impression (même en Postscript, et ne me demandez pas pourquoi !);
- **fichiers `vf`:** `vf` pour Virtual Font. Les polices virtuelles ont été mises au point par Knuth pour vous permettre d'utiliser des polices de provenances diverses. Les fichiers `vf` sont utiles lorsque vous essayez d'utiliser des polices Postscript;
- **fichiers `fd`:** description des fontes. Ça, c'est en rapport avec NFSS.

NFSS, c'est la manière dont {latexlogo}`LaTeX` sélectionne une police. {latexlogo}`LaTeX` ne le faisait pas assez proprement, alors on a fait le ménage. Un fichier `fd` dit à {latexlogo}`LaTeX` quels sont les fichiers `mf` à utiliser pour telle police, dans telle taille, dans telle famille et avec telle variation. C'est avec ce fichier que vous dites à {latexlogo}`LaTeX` de prendre la version sans empattement dans tel fichier, la taille `9pt` dans tel autre, et le gras dans ce troisième fichier.

En tout état de cause, `tfm` et `fd` sont indispensables à la bonne marche de {latexlogo}`LaTeX`. Les `mf` sont indispensables à la création des `tfm` et des `pk`, et les `pk` sont indispensables à la visualisation et à l'impression.

```{eval-rst}
.. meta::
   :keywords: LaTeX,Postscript
```
