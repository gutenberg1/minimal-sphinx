```{role} latexlogo
```
```{role} tttexlogo
```
# Comment créer une extension ?

Il n'y a rien de particulièrement extraordinaire dans les commandes que vous utilisez lors de l'écriture d'une extension :

- vous regroupez simplement un ensemble de commandes `\(re)newcommand` et `\(re)newenvironment`. La lecture de la question « [À quoi servent « \\makeatletter » et « \\makeatother » ?](/2_programmation/macros/makeatletter_et_makeatother) » pourra d'ailleurs vous éviter une erreur sur la gestion du caractère « @ » ;
- vous les enregistrez en un fichier ''*mon-extension*.sty '' ;
- et c'est fait !

Cependant, vos extensions pourront demander un peu plus de sophistication :

- des informations sur les commandes {latexlogo}`LaTeX` utilisées pour cette tâche se trouvent dans le document « {ctanpkg}`Class and package programming guide <clsguide>` » (en PDF {texdoc}`ici <clsguide>`) ;
- une bonne connaissance de {tttexlogo}`TeX` lui-même est importante et des livres comme le {doc}`TeXbook </1_generalites/documentation/livres/documents_sur_tex>` ou {doc}`TeX by topic </1_generalites/documentation/livres/documents_sur_tex>` sont ici précieux. Dès lors, il vous sera possible d'utiliser la source documentée de {latexlogo}`LaTeX` comme matériel de référence. Cette source peut être obtenue en compilant le fichier « {ctanpkg}`source2e.tex <source2e>` » ou en consultant directement sa {texdoc}`version PDF <source2e>` ;
- les {doc}`sources documentées </1_generalites/documentation/documents/documents_extensions/fichiers_sources_dtx>` (fichiers « dtx ») des extensions sont également une bonne aide.

______________________________________________________________________

*Source :* {faquk}`Learning to write LaTeX classes and packages <FAQ-writecls>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentation,classe,extension,écrire une classe,écrire une extension,créer une classe,créer une extension
```
