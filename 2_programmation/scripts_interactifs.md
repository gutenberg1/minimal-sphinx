```{role} latexlogo
```
```{role} tttexlogo
```
# Comment écrire des scripts interactifs ?

- L'extension {ctanpkg}`dialogl` permet d'écrire des scripts interactifs en {latexlogo}`LaTeX`.

Le but de cette extension est surtout de rassembler beaucoup d'informations et d'exemples (probablement plus que ce que vous vouliez vraiment) sur la façon d'écrire des macros TeX pour dialoguer avec l'utilisateur, par exemple en posant des questions pendant la compilation [^footnote-1] :

```text
Select the font base you wish to use :
[1] Computer Modern        [4] Malibu
[2] Garamond               [5] Times
[3] Helvetica

(Default : Times)

l.56
% Enter a number (1...5) and press Return TWICE
?
```

La pièce maîtresse de l'extension est le fichier `dialog.sty`, qui permet d'ajouter facilement des éléments de dialogue à un document LaTeX ; il devrait également pouvoir être utilisé dans des documents {doc}`Plain TeX </1_generalites/glossaire/qu_est_ce_que_plain_tex>`. Le fichier d'exemple `listout.tex` montre un système de menus construit avec les fonctions définies dans `menus.sty`. C'est un bon point de départ pour comprendre le fonctionnement de {ctanpkg}`dialogl`. `listout.tex` a été écrit à l'origine pour fonctionner avec Plain TeX, mais le code a évolué par la suite et a surtout été testé avec LaTeX.

:::{important}
L'extension {ctanpkg}`dialogl` est plus ancienne que ne le laisse penser sa date de publication.

Son développement initial date de 1994. Après le décès de son auteur, Michael J. Downes, en 2003, elle est restée « enfermée » sous une licence non libre. L'extension a été republiée sous une licence libre (LPPL v1 ou suivantes) en 2013, avec l'accord des héritiers, mais le code est resté tel qu'il était dix ans plus tôt.
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX,script interactif,dialogue,interface utilisateur,terminal,menu
```

[^footnote-1]: Cet exemple est tiré de {texdoc}`la documentation de « dialogl » <dialogl>`.
