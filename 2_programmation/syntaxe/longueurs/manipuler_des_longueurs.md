```{role} latexlogo
```
```{role} tttexlogo
```
# Comment gérer des longueurs ?

## Commandes de base

Plusieurs longueurs sont prédéfinies en {latexlogo}`LaTeX`. Par exemple les dimensions de la feuille et de la zone d'écriture.

Pour utiliser une nouvelle longueur, il faut avant tout la déclarer à l'aide de la commande `\newlength{longueur}` où l'argument obligatoire est le nom de la longueur, qui doit obligatoirement commencer par une contre-oblique.

Initialement, la longueur est nulle. Il existe des commandes pour modifier une longueur :

- `\setlength{\longueur}{valeur}` et
- `\addtolength{\longueur}{valeur}`.

On peut utiliser plusieurs unités pour spécifier les longueurs. Voir le paragraphe {doc}`unités </2_programmation/syntaxe/longueurs/unites_de_mesure_de_tex>` à ce sujet.

Pour afficher la valeur d'une longueur en points (pt), on utilisera la commande `\the\longueur`.

```latex
\newlength{\largeur}
\setlength{\largeur}{12cm}

La largeur est : \the\largeur
```

## Longueurs fixes et longueurs flexibles

Une longueur peut être rendue flexible en ajoutant les mots-clés `plus` et `minus` à sa définition. Cela permet d'ajuster l'espacement quand un respect strict de la valeur donnée produirait un mauvais effet : ainsi, un espacement vertical peut être réduit si cela permet d'éviter qu'un paragraphe se termine sur la première ligne de la page suivante. L'exemple suivant traite le cas d'un espacement horizontal.

```latex
% !TEX noedit
\documentclass{article}

\newlength{\pasflexible}
\setlength{\pasflexible}{0.5em}
\newlength{\flexible}
\setlength{\flexible}%
 {0.5em plus 0.3em minus 0.3em}

\newcommand*{\seppasflexible}%
{\hspace{\pasflexible}|\hspace{\pasflexible}}

\newcommand*{\sepflexible}%
{\hspace{\flexible}|\hspace{\flexible}}

\begin{document}

Voici du texte\seppasflexible que l'on a\seppasflexible
voulu diviser\seppasflexible en micro-unités\seppasflexible
de quelques petits mots\seppasflexible qui vont\seppasflexible
plus ou moins\seppasflexible ensemble.\seppasflexible
En revoici\seppasflexible en revoilà un.

Voici du texte\sepflexible que l'on a\sepflexible
voulu diviser\sepflexible en micro-unités\sepflexible
de quelques petits mots\sepflexible qui vont\sepflexible
plus ou moins\sepflexible ensemble.\sepflexible
En revoici\sepflexible en revoilà un.

\end{document}
```

```latex
\newlength{\pasflexible}
\setlength{\pasflexible}{0.5em}
\newlength{\flexible}
\setlength{\flexible}{0.5em plus 0.3em minus 0.3em}

\newcommand*{\seppasflexible}{\hspace{\pasflexible}|\hspace{\pasflexible}}
\newcommand*{\sepflexible}{\hspace{\flexible}|\hspace{\flexible}}

\begin{document}

\thispagestyle{empty}

Voici du texte\seppasflexible que l'on a\seppasflexible voulu diviser\seppasflexible en micro-unités\seppasflexible
de quelques petits mots\seppasflexible qui vont\seppasflexible plus ou moins\seppasflexible ensemble.\seppasflexible
En revoici\seppasflexible en revoilà un.

Voici du texte\sepflexible que l'on a\sepflexible voulu diviser\sepflexible en micro-unités\sepflexible
de quelques petits mots\sepflexible qui vont\sepflexible plus ou moins\sepflexible ensemble.\sepflexible
En revoici\sepflexible en revoilà un.

\end{document}
```

Dans le premier paragraphe, où les espaces autour du séparateur `|` ont une longueur fixe, le compilateur ne peut pas éviter que la dernière ligne ne contienne que trois caractères : pour y parvenir, il aurait dû resserrer les mots de la ligne précédente de manière excessive. Dans le second paragraphe, au contraire, le compilateur a tiré parti de la possibilité de réduire légèrement la longueur de ces espaces pour placer la chaîne de caractères `un.` dans la deuxième ligne, évitant ainsi de produire une « ligne à voleur ».

```{eval-rst}
.. meta::
   :keywords: LaTeX, espacement, espace horizontaux, ligne trop courte
```
