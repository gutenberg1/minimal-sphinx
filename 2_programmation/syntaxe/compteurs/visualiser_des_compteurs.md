```{role} latexlogo
```
```{role} tttexlogo
```
# Comment visualiser des compteurs ?

Pour afficher la valeur d'un compteur dans le document, on peut utiliser les commandes `\arabic` (ou ses {doc}`variantes </2_programmation/syntaxe/compteurs/les_differents_compteurs>`) ou `\the` (suivie du nom du compteur). On fera par exemple :

```latex
% !TEX noedit
Le compteur footnote vaut \thefootnote.
Le compteur footnote vaut \arabic{footnote}.
```

La commande `\showthe` permet de visualiser un compteur lors de la compilation en interrompant celle-ci. Ainsi, `\showthe\value{moncompteur}` affiche la valeur du compteur et fait une pause. Pour que la compilation ne soit pas interrompue, on peut utiliser le mode `nonstopmode` ou bien la commande `message`, de la manière suivante :

```latex
% !TEX noedit
\message{Le compteur page vaut \thepage}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
