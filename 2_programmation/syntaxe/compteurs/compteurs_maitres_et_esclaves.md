```{role} latexlogo
```
```{role} tttexlogo
```
# Comment définir un compteur dépendant d'un autre compteur ?

Il est courant d'avoir des éléments numérotés tenant compte du numéro de chapitre (par exemple, dans les classes standard {ctanpkg}`book` et {ctanpkg}`report`, figures, tableaux et notes de bas de page sont tous numérotés ainsi). Le processus de réinitialisation se fait automatiquement, lorsque le compteur « maître » est incrémenté (lorsque la commande `\chapter` qui démarre le chapitre ⟨N⟩ est traitée, le compteur `chapter` est incrémenté, et tout les compteurs dépendants sont mis à zéro).

Mais comment faire cela par vous-même ? Vous souhaitez peut-être numéroter les algorithmes en tenant compte du numéro de section, ou les corollaires en tenant compte du numéro de théorème, par exemple.

## Avec des commandes de base

Si vous définissez cela manuellement, vous devez déclarer cette relation entre compteurs lorsque vous définissez le compteur qui dépend d'un autre (ici *esclave* qui dépend de *maitre*) :

```latex
% !TEX noedit
\newcounter{esclave}[maitre]
```

Ainsi, à chaque fois le compteur nommé *maitre* est incrémenté, le compteur nommé *esclave* est réinitialisé.

Mais que se passe-t-il si vous avez une extension qui définit les objets pour vous mais qui ne fournit pas d'interface de programmation pour que les compteurs se comportent comme vous le souhaitez ?

## Avec des commandes internes

La commande `\newcounter` se sert d'une commande interne {latexlogo}`LaTeX` que vous pouvez utiliser (avec, [au besoin](/2_programmation/macros/makeatletter_et_makeatother), `\makeatletter` et `\makeatother`) :

```latex
% !TEX noedit
\@addtoreset{esclave}{maitre}
```

## Avec l'extension « chngcntr »

L'extension {ctanpkg}`chngcntr` encapsule la commande `\@addtoreset` dans une commande `\counterwithin`, ce qui permet la solution suivante (rendant le compteur des corollaires dépendant du compteur des théorèmes nommé *theorem*) :

```latex
% !TEX noedit
\counterwithin*{corollaire}{theorem}
```

La commande utilisée sans l'astérisque aura un comportement légèrement différent :

```latex
% !TEX noedit
\counterwithin{corollaire}{theorem}
```

Par rapport à ce que fait `\counterwithin*`, elle ajoute une redéfinition de `\thecorollaire` sous la forme ⟨numéro du théorème⟩.⟨numéro du corollaire⟩, ce qui est une bonne approche si jamais vous voulez vous référer aux corollaires (il y a potentiellement beaucoup de « corollaire 1 » dans n'importe quel document, autant donc lier son numéro à celui du compteur du théorème auquel il appartient). Si vous n'utilisez pas {ctanpkg}`chngcntr`, vous pouvez ici lire la question référez-vous à la réponse à « [Comment redéfinir les commandes de compteur \\the(...) ?](/2_programmation/syntaxe/compteurs/comment_fonctionnent_les_compteurs) » pour les techniques nécessaires.

La version 2018 de {latexlogo}`LaTeX` a intégré les commandes de {ctanpkg}`chngcntr` au format : `\counterwithin` et `\counterwithout` sont donc désormais directement disponibles sans nécessiter d'extension.

## Cas particulier du compteur des pages

Notez que la technique ne fonctionne pas si le compteur maître est `page`, le numéro de la page courante. Le compteur `page` est inséré profondément dans la routine de sortie, généralement appelée souvent bien après que le texte de la nouvelle page ait commencé à apparaître : des techniques spéciales sont donc nécessaires pour gérer ce cas, comme par exemple avec les {doc}`notes de bas de page numérotées par page </3_composition/texte/pages/footnotes/numeroter_les_notes_de_bas_de_page_page_par_page>`. Une des techniques, utilisant l'extension {ctanpkg}`perpage`, peut être appliquée à n'importe quel compteur. Pour que la réinitialisation du compteur ⟨compteur⟩ s'effecture à chaque page, l'extension met à disposition la commande :

```latex
% !TEX noedit
\MakePerPage{⟨compteur⟩}
```

L'extension utilise un mécanisme semblable à une étiquette et peut nécessiter plus d'une exécution de {latexlogo}`LaTeX` pour stabiliser les valeurs des compteurs. De fait, {latexlogo}`LaTeX` générera les avertissements habituels concernant le changement d'étiquette.

______________________________________________________________________

*Source :* {faquk}`Master and slave counters <FAQ-addtoreset>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,latex,programmation,compteurs,dépendance
```
