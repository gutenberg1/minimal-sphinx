```{role} latexlogo
```
```{role} tttexlogo
```
# Quels sont les différents styles de compteur ?

Pour obtenir la valeur d'un compteur, plusieurs formats sont accessibles avec les commandes suivantes :

- `\arabic` pour un nombre arabe ;
- `\roman` pour un nombre romain minuscule ;
- `\Roman` pour un nombre romain majuscule ;
- `\alph` pour une lettre minuscule ;
- `\Alph` pour une lettre majuscule ;
- `\fnsymbol` pour un symbole.

```latex
\newcounter{moutons}
\stepcounter{moutons}
Comptons les moutons :
\arabic{moutons} moutons,
\stepcounter{moutons}
\roman{moutons} moutons,
\stepcounter{moutons}
\Roman{moutons} moutons,
\stepcounter{moutons}
\alph{moutons} moutons,
\stepcounter{moutons}
\Alph{moutons} moutons,
\stepcounter{moutons}
\fnsymbol{moutons} moutons.
```

:::{note}
Certains de ces formats peuvent provoquer {doc}`une erreur </2_programmation/erreurs/c/counter_too_large>` si vous dépassez les possibilités d'énumération du format (au-delà de la lettre `Z` par exemple pour `Alph`).
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX,programmation,compteur,style,format
```
