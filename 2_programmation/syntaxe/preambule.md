```{role} latexlogo
```
```{role} tttexlogo
```
# Qu'est ce que le préambule d'un document LaTeX ?

Le **préambule** d'un fichier source {latexlogo}`LaTeX` est la partie du fichier comprise entre les commandes `\documentclass[...]{...}` et `\begin{document}`.

C’est dans le préambule que sont, entre autres :

- chargées les extensions souhaitées avec `\usepackage` ;
- définies des commandes personnelles avec `\newcommand` ;
- faits des réglages généraux pour le document ;
- définis les noms, auteurs et dates du document (voir la question « {doc}`Comment obtenir une page de garde ? </3_composition/texte/pages/composer_une_jolie_page_de_garde>` »).

Voici un exemple de préambule pour compiler les exemples :

```latex
% !TEX noedit
\documentclass[10pt,french]{article}

\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb,amssymb}
\usepackage{xcolor}

\pagestyle{empty}

\begin{document}
  ...
\end{document}
```

:::{note}
Certaines commandes --- par exemple `\DeclareMathOperator` de l’extension {ctanpkg}`AMSmath <amsmath>` --- ne peuvent être utilisées **que** dans le préambule.
:::
