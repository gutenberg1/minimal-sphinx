```{role} latexlogo
```
```{role} tttexlogo
```
# Le modèle de boîtes de TeX (donc de LaTeX)

Pour {tttexlogo}`TeX`, produire un document revient à produire des listes de boîtes (contenant éventuellement d'autres boîtes) et à les placer sur la page. C'est au moment de la visualisation que les caractères sont placés dans les boîtes et que le document ressemble à ce qui était souhaité.

```{image} https://web.archive.org/web/20140608074141im/https://elzevir.fr/imj/latex/img/textisboxes.png
:alt: https://web.archive.org/web/20140608074141im/https://elzevir.fr/imj/latex/img/textisboxes.png
:class: align-middle
```

:::{note}
Un caractère n'est pas toujours « complètement » contenu dans sa boîte.
:::

Contrairement à ce que suggère l'intuition, une boîte {tttexlogo}`TeX` ne possède pas deux, mais trois dimensions : une hauteur, une largeur, et une profondeur. La troisième dimension est liée à l'existence d'une ligne de base sans laquelle il serait difficile d'aligner les caractères. On pourrait spécifier les trois dimensions de façon équivalente sous la forme : largeur, hauteur totale et décalage vertical. C'est ce qui est fait pour certaines commandes comme `\rule` et la syntaxe {latexlogo}`LaTeX` manque un peu d'homogénéité sur ce point.

```{image} https://web.archive.org/web/20140608074141im/https://elzevir.fr/imj/latex/img/boxdimen.png
:alt: https://web.archive.org/web/20140608074141im/https://elzevir.fr/imj/latex/img/boxdimen.png
:class: align-middle
```

Dans certaines circonstances (rotation de graphiques, `minipage`), on doit repérer un point verticalement et/ou horizontalement dans une boîte. Une convention courante est d'utiliser deux lettres, dont la première est `t` (haut), `b` (bas), `c` (centre) ou `B` (ligne de base) pour le positionnement vertical, et la deuxième est `l` (gauche), `r` (droite) ou `c` (centre) pour le positionnement horizontal.

```{image} https://web.archive.org/web/20140608074141im/https://elzevir.fr/imj/latex/img/boxplaces.png
:alt: https://web.archive.org/web/20140608074141im/https://elzevir.fr/imj/latex/img/boxplaces.png
:class: align-middle
```

______________________________________________________________________

*Source :* <https://web.archive.org/web/20170314080827/https://elzevir.fr/imj/latex/tips.html#boxmodel>
