```{role} latexlogo
```
```{role} tttexlogo
```
# Que faire si vous trouvez un bug ?

Pour commencer, vérifiez que vous avez *vraiment* trouvé un bug :

- regardez bien dans des livres traitant de {tttexlogo}`TeX`, {latexlogo}`LaTeX` ou tout autre variante que vous utilisez ;
- comparez ce que vous observez avec les réponses de cette FAQ ;
- demandez aux personnes que vous connaissez et qui ont une expertise en lien avec {tttexlogo}`TeX`.

**Les raisons de cette prudence sont nombreuses.**

Si vous avez trouvé un bug dans {tttexlogo}`TeX` lui-même, vous appartenez en effet à une espèce fort rare. Donald Knuth est tellement sûr de la qualité de son code qu'il [offre de l'argent](https://www-cs-faculty.stanford.edu/~knuth/abcde.html) aux découvreurs de bug ; les chèques qu'il rédige sont si rares qu'ils ne sont quasiment [jamais encaissés](https://en.wikipedia.org/wiki/Knuth_reward_check). Si *vous* pensez que vous avez réellement trouvé une erreur dans {tttexlogo}`TeX` (ou MetaFont ou les fontes Computer Modern ou le {tttexlogo}`TeX`​book), n'écrivez pas immédiatement à Knuth. Il analyse les bugs de façon assez sporadique, même si ces derniers ont été validés par une petite équipe d'experts. Dans un premier temps, contactez [Karl Berry](mailto:karl@freefriends.org) du {doc}`TUG </1_generalites/gutenberg>` (en anglais).

Si vous avez trouvé un bug dans {latexlogo}`LaTeX`, **déclarez-le** (voir ci-dessous) en utilisant les mécanismes mis en place par l'équipe {latexlogo}`LaTeX`. Soyez bien attentif à vous assurer qu'il s'agit d'un bug de {latexlogo}`LaTeX` ou d'une des extensions distribuées par l'équipe {latexlogo}`LaTeX`.

Si vous avez trouvé un bug dans une extension, le mieux est de commencer par les lieux usuels pour obtenir de {doc}`l'aide en ligne </1_generalites/documentation/listes_de_discussion>` ou les différentes {doc}`listes de diffusion spécialisées </1_generalites/documentation/listes_de_discussion/start>`. L'auteur de l'extension pourrait bien vous répondre en ligne mais, si personne d'autre ne vous fournit d'aide, vous pouvez tenter de lui écrire directement (en supposant que vous arrivez à trouver son adresse).

Si vous avez trouvé un bug dans {latexlogo}`LaTeX` 2.09 ou dans un des autres logiciels qui ne sont plus maintenus, votre seul espoir est {doc}`l'aide en ligne </1_generalites/documentation/listes_de_discussion>`.

Si tout ceci échoue, envisagez de payer pour obtenir de l'aide : le {doc}`TUG </1_generalites/gutenberg>` tient à jour un [registre](http://www.tug.org/consultants.html) de consultants {tttexlogo}`TeX`. Ceci suppose que vous avez les ressources et un besoin tel que vous puissiez recruter quelqu'un.

## Comment savoir si un bug vient de LaTeX ou d'une extension ?

L'équipe {latexlogo}`LaTeX` fait la maintenance de {latexlogo}`LaTeX` et traite tout rapport de bug. Mais elle *ne traite pas* les bugs des multiples extensions dont elle n'est pas l'auteur : elle ne suit que les logiciels qui font partie de la distribution {latexlogo}`LaTeX`, à savoir {latexlogo}`LaTeX` et les seules extensions qui lui sont « indispensables ».

Si le bug que vous avez trouvé est dans une extension maintenue par d'autres développeurs, c'est à eux que vous devez faire votre rapport.

Pour vous aidez à savoir à qui déclarer le bug, l'équipe de développement de LaTeX fournit l'extension {ctanpkg}`latexbug`. Vous devez vous en servir pendant que vous composez l'exemple qui illustrera votre rapport de bug. Pour cela, il suffit d'ajouter la ligne `\RequirePackage{latexbug}` en tout début de fichier :

```latex
% !TEX noedit
\RequirePackage{latexbug} % Première ligne obligatoire

\documentclass{article} % Ensuite, votre exemple
...                     % de code qui illustre le bug
\end{document}
```

Pendant la compilation, sur le terminal, vous verrez des indications à propos des extensions chargées, qui les maintient, et où vous devez envoyer votre rapport de bug :

```text
Package latexbug Error : Third-party file(s)
(latexbug)
(latexbug)         This test file uses third-party file(s)
(latexbug)
[...]
[... Liste des extensions et de leurs mainteneurs ...]
[...]
(latexbug)         So you should contact the authors
(latexbug)         of these files, not the LaTeX Team!
(latexbug)         (Or remove the packages that load
(latexbug)         them, if they are not necessary to
(latexbug)         exhibit the problem).
```

Si vous pensez que l'une ou l'autre des extensions indiquées n'est pas à l'origine du bug, adaptez votre exemple pour les retirer. Ceci vous permettra de réduire votre exemple pour former un {doc}`ECM </1_generalites/documentation/listes_de_discussion/comment_faire_un_exemple_complet_minimal>`, et de cerner l'origine du bug, ce qui aidera les mainteneurs à vous aider.

## Comment déclarer un bug de LaTeX ?

Dans tous les cas, vous devez bien faire attention à produire un rapport de bug qui soit utilisable par l'équipe. Voici les étapes à suivre :

1. Utilisez-vous une version récente de {latexlogo}`LaTeX` ? La maintenance n'est possible que sur des versions suffisamment à jour de {latexlogo}`LaTeX`.
2. Votre bug a-t-il déjà été rapporté ? Parcourez [l'actuelle base d'anomalies](https://github.com/latex3/latex2e/issues) {latexlogo}`LaTeX` et [l'ancienne base](https://www.latex-project.org/cgi-bin/ltxbugs2html?introduction=yes&state=open) GNATS {latexlogo}`LaTeX` pour trouver un éventuel rapport antérieur de votre bug. Dans de nombreux cas, ces bases listeront des solutions de contournement.
3. Préparez un {doc}`exemple minimal </1_generalites/documentation/listes_de_discussion/comment_faire_un_exemple_complet_minimal>` qui présente le problème. Idéalement, ce fichier ne doit contenir que des extensions « indispensables » suivies par l'équipe {latexlogo}`LaTeX` car les autres extensions font l'objet de suivis par leurs propres auteurs. L'exemple minimal doit être auto-suffisant : si un membre de l'équipe le compile dans un répertoire propre avec un système n'ayant que les extensions « indispensables », l'anomalie doit se produire. Vous pouvez consulter [les conseils](https://www.latex-project.org/bugs/#how-do-i-report-a-bug) de l'équipe {latexlogo}`LaTeX` pour plus de détails.
4. Compilez votre fichier de test avec {latexlogo}`LaTeX` : le rapport de bug doit inclure le fichier `.log` créé par la compilation.
5. Enfin, [déclarez l'incident](https://github.com/latex3/latex2e/issues/new) dans la [base d'anomalies](https://github.com/latex3/latex2e/issues) {latexlogo}`LaTeX` .

______________________________________________________________________

*Sources :*

- {faquk}`What to do if you find a bug <FAQ-bug>`,
- {faquk}`Reporting a LaTeX bug <FAQ-latexbug>`,
- [LaTeX2e kernel development moves to GitHub](https://www.texdev.net/2017/12/16/latex2e-kernel-development-moves-to-github/), sur le blog de Joseph Wright.

```{eval-rst}
.. meta::
   :keywords: LaTeX,problème,bug,débogage,rapport de bug,bugs dans TeX,bogues dans TeX,bugs dans LaTeX
```
