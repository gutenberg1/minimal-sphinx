```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « You already have nine parameters » ?

- **Message** : `You already have nine parameters`
- **Origine** : *TeX*.

{latexlogo}`LaTeX` accepte des définitions de commandes ou d'environnements qui ont un maximum de neuf paramètres. L'erreur signale qu'un `\newcommand` ou un `\newenvironment` en spécifie dix ou plus.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=Y>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,trop de paramètres,trop d'arguments,combien de paramètres,combien d'arguments
```
