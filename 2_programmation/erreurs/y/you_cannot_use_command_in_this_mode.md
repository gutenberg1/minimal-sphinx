```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « You can't use \`⟨commande⟩' in ⟨mode⟩ mode » ?

- **Message** : ```` You can't use `⟨commande⟩' in ⟨mode⟩ mode ````
- **Origine** : *TeX*.

{tttexlogo}`TeX` indique que la `⟨commande⟩` n'est pas permise dans le mode `⟨mode⟩`. On a déjà vu des variations spécifiques sur ce thème. Si la `⟨commande⟩` n'a pas été directement utilisée, la cause la plus fréquente de cette erreur est une commande fragile à l'intérieur d'un argument mouvant.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=Y>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,protect,syntaxe
```
