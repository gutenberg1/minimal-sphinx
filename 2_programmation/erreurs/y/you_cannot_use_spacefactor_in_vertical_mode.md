```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « You can't use \``\spacefactor`' in vertical mode » ?

- **Message** : ```` You can't use `\spacefactor' in vertical mode ````
- **Origine** : *TeX*.

{tttexlogo}`TeX` indique que l'on ne peut utiliser `\spacefactor` que lors de la construction d'une liste horizontale. Deux erreurs similaires existent :

- [You can't use \``\spacefactor`' in math mode](/2_programmation/erreurs/y/you_cannot_use_spacefactor_in_math_mode) »
- [Improper `\spacefactor`](/2_programmation/erreurs/i/improper_spacefactor).

Elles surviennent toutes lors de l'utilisation de la commande {latexlogo}`LaTeX` `\@` en dehors d'un paragraphe. Comme de nombreux noms de commandes internes débutent par un@'', cette erreur arrive lorsqu'on les utilise dans le préambule du document (par exemple, `\@startsection`), sans avoir entouré ce code avec un `\makeatletter` et un `\makeatother`. Dans ce cas, {tttexlogo}`TeX` voit `\@` suivi par les lettres `startsection`, et à la prochaine utilisation de ce code, il exécute donc `\@`, ce qui produit finalement l'erreur.

Ce sujet est détaillé dans la question « {doc}`Que font \\@ et @ dans les noms des commandes ? </2_programmation/macros/arobase_dans_les_noms_de_macros>` ».

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=Y>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur,
- {faquk}`\\spacefactor complaints <FAQ-atvert>`.

```{eval-rst}
.. meta::
   :keywords: TeX,LaTeX,messages d'erreur de LaTeX,mode vertical,mode horizontal,arobe,arobase,\\spacefactor
```
