```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Bad `\line` or `\vector` argument » ?

- **Message** : `Bad \line or \vector argument`
- **Origine** : *LaTeX*.

{latexlogo}`LaTeX` indique cette erreur lorsqu'on utilise une longueur négative ou une pente non autorisée soit avec `\line`, soit avec `\vector`.

```{eval-rst}
.. todo:: Pour le dernier cas, voir le chapitre 10 du *LaTeX Companion* pour d'autres façons de procéder.
```

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=B>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,commande line,commande vector,mauvais argument
```
