```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « No declaration for shape ⟨forme-fonte⟩ » ?

- **Message** : `No declaration for shape ⟨forme-fonte⟩`

Les fonctions de taille `sub` et `ssub` utilisées en argument d'une commande `\DeclareFontShape` se réfèrent à une substitution de forme inconnue de la méthode de sélection de fonte de {latexlogo}`LaTeX`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=N>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,problème avec les fontes,problème avec les polices
```
