```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « No Cyrillic encoding definition files were found » ?

- **Message** : `No Cyrillic encoding definition files were found`
- **Origine** : package *babel*.

Les fichiers de définition de langues pour les « langues cyrilliques » supportées cherchent où tous les codages de fontes connus pour le cyrillique (par exemple `T2A`, `T2B`) peuvent être trouvés. Si cette recherche échoue, ce message s'affiche et il faut d'abord installer le support {latexlogo}`LaTeX` pour le cyrillique.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=N>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,encodage des caractères,encodage cyrillique,écrire en russe,encodage T2A,encodage T2B,fontenc
```
