```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Suggested extra height (⟨valeur⟩) dangerously large » ?

- **Message** : `Suggested extra height (⟨valeur⟩) dangerously large`
- **Origine** : *LaTeX*.

Le fait d'utiliser `⟨valeur⟩` avec `\enlargethispage` rendrait la page résultante trop haute (plus de 2,88 mètres) aux yeux de {latexlogo}`LaTeX`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=S>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,page trop grande,taille maximale de page pour LaTeX
```
