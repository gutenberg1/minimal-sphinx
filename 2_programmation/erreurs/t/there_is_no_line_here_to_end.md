```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « There's no line here to end » ?

- **Message** : `There's no line here to end`

Cette erreur est déclenchée lorsqu'une commande `\newline` ou ```` \\ ```` est trouvée à l'extérieur d'un paragraphe (c'est-à-dire après un `\par` ou une ligne vide).

Si l'intention est d'obtenir un espace vertical supplémentaire, voir {doc}`la page dédiée </3_composition/texte/pages/inserer_un_espace_vertical_dans_une_page>`.

______________________________________________________________________

L'erreur

```latex
% !TEX noedit
! LaTeX Error : There's no line here to end.

See the LaTeX manual or LaTeX Companion for explanation.
```

apparaît lorsque vous donnez une commande ```` \\ ```` à LaTeX à un moment où il ne l'attend pas ; il s'agit d'une commande de *saut de ligne*, et elle est inattendue si LaTeX n'est pas en train de construire un paragraphe. Un cas courant est celui où vous voulez mettre l'étiquette d'un élément de liste sur une ligne à part, en écrivant par exemple :

```latex
% !TEX noedit
\begin{description}
\item[Très longue description] \\
  Texte...
\end{description}
```

La bonne solution est de définir une nouvelle sorte d'environnement `description`, qui fait exactement ce que vous voulez (le {doc}`LaTeX Companion </1_generalites/documentation/livres/documents_sur_latex>` propose plusieurs solutions pour ça).

Une solution simple, qui évite l'avertissement, est d'écrire ceci :

```latex
% !TEX noedit
\begin{description}
\item[Très longue description] \leavevmode \\
  Texte...
\end{description}
```

qui commence un paragraphe avant de forcer une fin de ligne. Le paquet {ctanpkg}`expdlist` fournit la même fonctionnalité avec sa commande `\breaklabel`, et {ctanpkg}`mdwlist` la fournit avec sa commande `\desclabelstyle`.

Un autre cas fréquent donnant ce message d'erreur est lorsque vous utilisez l'environnement `center` (ou `flushleft` ou `flushright`), et que vous voulez une séparation supplémentaire entre les lignes à l'intérieur de l'environnement :

```latex
% !TEX noedit
\begin{center}
  Première ligne (titre)\\
  \\
  Corps du texte centré...
\end{center}
```

La solution est simple : utilisez la commande ```` \\ ```` comme elle est censée être utilisée, pour fournir plus qu'un simple espace de séparation entre les lignes. `\``\` prend un argument optionnel, qui spécifie combien d'espace supplémentaire il faut ajouter ; l'effet requis dans le texte ci-dessus peut être obtenu en écrivant :

```latex
% !TEX noedit
\begin{center}
  Première ligne (titre)\\[\baselineskip]
  Corps du texte centré...
\end{center}
```

Vous *pouvez* aussi utiliser `\leavevmode`, comme ci-dessus :

```latex
% !TEX noedit
\begin{center}
  Première ligne (titre)\\
  \leavevmode\\
  Corps du texte centré...
\end{center}
```

mais c'est tout aussi fastidieux à taper que ```` \\ ```` avec un argument optionnel, et ne peut être recommandé.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=T>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur,
- {faquk}`"No line here to end" <FAQ-noline>`.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,espaces verticaux,ajouter de l'espace,fin de ligne dans un paragraphe
```
