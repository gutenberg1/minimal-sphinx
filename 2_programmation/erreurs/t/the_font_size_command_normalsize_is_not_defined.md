```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « The font size command `\normalsize` is not defined... » ?

- **Message** : `The font size command \normalsize is not defined...`
- **Origine** : *LaTeX*.

Un fichier de classe nécessite une initialisation minimale, et la commande `\normalsize` fait partie de ce minimum vital. Si vous êtes en train d'écrire votre propre fichier de classe, il se pourrait bien que vous ayez fait une erreur fondamentale dedans... mais plus fréquemment, le problème est que vous avez simplement oublié de commencer votre document par la commande `\documentclass`.

Le message complet est :

```
The font size command \normalsize is not defined :
there is probably something wrong with the class file.
```

## Fichier de classe minimal

Tout fichier de classe *doit* contenir au moins quatre éléments :

- une définition de `\normalsize`,
- des valeurs pour `\textwidth` et `\textheight`
- une spécification pour la numérotation des pages.

Ainsi, un fichier minimal de classe de document ressemble à ceci, présent sur toutes les distributions standards :

```latex
% !TEX noedit
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{minimal}[2001/05/25 Standard LaTeX minimal class]

\renewcommand\normalsize{\fontsize{10pt}{12pt}\selectfont}

\setlength{\textwidth}{6.5in}
\setlength{\textheight}{8in}

\pagenumbering{arabic}
```

La ligne `\pagenumbering{arabic}` est nécessaire, et pourtant aucun numéro de page ne sera affiché, parce que le style par défaut est `\pagestyle{empty}`.

______________________________________________________________________

*Sources :*

- {faquk}`\\normalsize not defined <FAQ-normalszmiss>`,
- <https://latex.developpez.com/faq/erreurs?page=T>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,écrire une classe,commandes obligatoires dans une classe,classe minimale
```
