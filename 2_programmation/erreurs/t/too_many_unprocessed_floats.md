```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Too many unprocessed floats » ?

- **Message** : `Too many unprocessed floats`
- **Origine** : *LaTeX*.

Les flottants ne pouvant pas être placés immédiatement sont mis en réserve par {latexlogo}`LaTeX`, ce qui oblige éventuellement les flottants ultérieurs à être placés en réserve à leur tour. {latexlogo}`LaTeX` peut mettre en attente jusqu'à 18 flottants. Au-delà, on reçoit ce message d'erreur. Cette limite peut être augmentée à 36 en utilisant l'extension {ctanpkg}`morefloats`, mais si un flottant ne peut pas être placé pour une raison ou pour une autre, cette modification ne fera que retarder l'apparition de l'erreur. Le chapitre 6 du *LaTeX Companion*

```{eval-rst}
.. todo:: Ce dernier paragraphe appelle une révision : « indique » comment gérer cette situation.
```

Cette erreur peut également être déclenchée par des commandes `\marginpar` trop nombreuses à l'intérieur d'un paragraphe. Un `\marginpar` utilise temporairement deux emplacements de stockage des flottants en attente tant que le paragraphe en cours n'est pas composé (cela permet donc un maximum de neuf notes marginales par paragraphe, voire moins s'il y a déjà des flottants en attente).

______________________________________________________________________

Parfois, {latexlogo}`LaTeX` répond à une commande `\begin{figure}` ou `\begin{table}` par le message d'erreur suivant :

```latex
% !TEX noedit
! LaTeX Error : Too many unprocessed floats.
```

En vous indiquant que « Trop de flottants n'ont pas été traités », {latexlogo}`LaTeX` vous indique que vos figures et tables n'ont alors pas été placées proprement. {latexlogo}`LaTeX` dispose en effet d'une quantité limitée d'espace de stockage pour les flottants (en tout, il peut stocker par défaut 18 figures, tables ou tout flottant que vous avez définis vous-même avec l'extension {ctanpkg}`float`) : si un de vos choix empêche {latexlogo}`LaTeX` de composer vos flottants et donc de les déstocker, il finira par manquer d'espace de stockage.

## Le cas de la saturation de la mémoire

Cela correspond à des cas extrêmes de {doc}`flottants beaucoup trop mobiles </3_composition/flottants/pourquoi_faire_flotter_ses_figures_et_tableaux>` : {latexlogo}`LaTeX` a trouvé un flottant qu'il n'arrive pas à placer. Or, il suit deux principes :

- lorsque un flottant est créé, son numéro lui est définitivement attribué ;
- les numéros des flottants (par type identique) se suivent dans le document.

Aussi, si {latexlogo}`LaTeX` ne peut positionner un flottant, il ne peut traiter les suivants. Tous les flottants du même type s'empilent donc derrière celui-ci.

Les techniques pour résoudre le problème sont discutées dans la question « {doc}`Comment gérer proprement des flottants </3_composition/flottants/pourquoi_faire_flotter_ses_figures_et_tableaux>` ? ».

### Pour des versions de LaTeX antérieures à 2015

Une solution alternative revient à utiliser l'extension {ctanpkg}`morefloats`. L'extension allouera plus d'emplacements mémoires pour des flottants. Attention, cependant, le nombre d'emplacements que vous pouvez allouer restera limité; même en utilisant l'extension {ctanpkg}`etex`.

### Pour des versions de LaTeX à compter de 2015

Le nombre par défaut de flottants pouvant être stockés passe de 18 à 52. De plus, il existe une commande `\extrafloats` qui (en supposant qu'un format \$\\epsilon\$-{tttexlogo}`TeX` est utilisé) vous permettra d'allouer beaucoup plus de boîtes flottantes avec une limite supérieure fixée à plusieurs milliers. Dans les faits, les versions actuelles de LaTeX exécuteront `\extrafloats{1}` avant de déclarer l'erreur. Il est donc hautement improbable que vous obteniez cette erreur sauf si plus de 32 000 registres ont été alloués.

## Le cas d'une longue séquence de flottants

Une longue séquence d'environnements flottants, sans texte intermédiaire, peut aussi conduire à cette erreur. À moins que les environnements ne se positionnent « ici » (et que vous leur avez permis d'y aller), il n'y aura jamais de saut de page, et il n'y aura donc jamais pour {latexlogo}`LaTeX` d'opportunité pour reconsidérer le positionnement des flottants. Même si les versions modernes de LaTeX permettent de démultiplier le nombre de flottants stockables, cette situation doit être évitée car elle peut ralentir {latexlogo}`LaTeX` et, dans des cas extrêmes, provoquer d'autres erreurs de mémoire insuffisante.

Bien sûr, les flottants ne peuvent pas tous tenir « ici » si la séquence de flottant est suffisamment longue : une fois la page remplie, {latexlogo}`LaTeX` ne placera plus de flottants, ce qui conduira à l'erreur.

Les techniques de résolution peuvent impliquer de redéfinir les flottants en utilisant la marque de position `[H]` de l'extension {ctanpkg}`float`. Il est cependant peu probable que vous vous en tiriez sans utiliser de temps en temps la commande `\clearpage`.

______________________________________________________________________

*Sources :*

- {faquk}`"Too many unprocessed floats" <FAQ-tmupfl>`,
- <https://latex.developpez.com/faq/erreurs?page=T>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,trop de flottants,problème avec les flottants,floatbarrier,flottants non processés
```
