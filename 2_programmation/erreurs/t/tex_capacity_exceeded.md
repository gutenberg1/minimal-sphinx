```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « TeX capacity exceeded, ⟨explication⟩ » ?

- **Message** : `TeX capacity exceeded, ⟨explication⟩`
- **Origine** : *TeX*.

{tttexlogo}`TeX` a dépassé la capacité de l'un de ses types de mémoires et arrête son travail. Plusieurs variantes existent au titre de l'*explication*, certaines faisant l'objet d'une réponse dédiée :

- « {doc}`text input levels </2_programmation/erreurs/t/capacity_exceeded2>` » ;
- « {doc}`semantic nest size </2_programmation/erreurs/t/tex_capacity_exceeded_nest>` ».

Il s'agit d'un reste de l'époque maintenant lointaine où {tttexlogo}`TeX` a été écrit. En ces temps lointains, les systèmes d'exploitation n'étaient en général pas capable d'allouer de la mémoire à un programme n'importe quand. Un programme devait donc demander une quantité de mémoire au début de son exécution, et s'en contenter durant tout le cours de son exécution. {tttexlogo}`TeX` a donc été écrit pour ne jamais redemander de mémoire au système. Il en prend une quantité fixée au démarrage et n'en redemandera plus jusqu'à la fin.

Cette façon de faire permet, en outre, de localiser certains problèmes très vite. Un document mal écrit, qui consomme trop de mémoire (une boucle infinie, ou quelque chose d'équivalent) va provoquer rapidement une erreur : au moment où il sera arrivé au bout de la mémoire demandée par {tttexlogo}`TeX` -- et non pas au moment où il sera arrivé au bout de la mémoire de la machine et de tout l'espace disque disponible comme mémoire virtuelle.

```{eval-rst}
.. todo:: Cette erreur est étudiée en détail à la section B.1.1 page 932 du *LaTeX Companion*.
```

## Le cas usuel

Bien que {tttexlogo}`TeX` suggère, comme toujours, qu'une augmentation de taille faite par un *magicien* (autrement dit un expert) pourrait aider à résoudre cette question de capacité dépassée, ce message résulte souvent d'autres causes :

- cela peut venir d'une commande mal écrite, d'une ligne trop longue, d'accolades mal appariées ou d'un mauvais paramètre donné à une commande fonctionnelle. Notez que des lignes trop longues sont souvent introduites lorsque les fichiers sont transférés de manière incorrecte entre les systèmes d'exploitation et que les fins de ligne ne sont pas bien traitées (le signe révélateur d'une erreur de ligne trop longue est la plainte que le valeur `buf_size` a été dépassée) ;
- cela peut également de documents bien écrits mais contenant des pages très compliquées ou de grandes quantités de fontes ou d'images. Par exemple, Si le message précise `[pool size=...]` et que le document comporte un grand nombre de flottants, les buffers de {tttexlogo}`TeX` sont saturés. Une solution consiste alors à vider régulièrement ces buffers en forçant la place des flottants (par des `\clearpage` par exemple pour laisser la place nécessaire aux figures...)

## Le cas rare

Si vous avez vraiment besoin d'étendre la capacité de {tttexlogo}`TeX`, la méthode appropriée dépend de votre installation. Il n'est pas nécessaire (avec les implémentations {tttexlogo}`TeX` modernes) de changer les valeurs par défaut dans la source WEB de Knuth. Cette modification est en effet une manipulation pour les experts : il faut utiliser un fichier de modification pour changer les valeurs définies dans le module 11, recompiler {tttexlogo}`TeX` et régénérer tous les fichiers de format.

Les implémentations modernes permettent de modifier de manière semi-dynamique les tailles des différents éléments de la mémoire de {tttexlogo}`TeX`. Celle-ci est en effet divisée en plusieurs zones, chacune de taille fixée au début de la compilation. Par exemple, la zone qui sert à stocker les fontes n'est pas la même que celle qui stocke les noms de macros ou les noms de fichiers, ou encore les contenus des pages, etc. Selon celle qui a saturé pendant la compilation, il faut adapter certaines tailles. Certaines distributions (comme emTeX) permettent de modifier les paramètres de mémoire dans les commutateurs de ligne de commande au démarrage de {tttexlogo}`TeX`. Le plus souvent, un fichier de configuration est lu pour spécifier la taille de la mémoire :

- sur les systèmes basés sur `web2c` (TeX Live, MikTeX), ce fichier s'appelle `texmf.cnf`. Ainsi, par exemple, si le message d'erreur précise `[main memory size=...]`, on peut augmenter la valeur des variables `extra_mem_top` et/ou `extra_mem_bot` (à la rigueur la valeur de `main_memory` mais il faudra alors refaire les formats) ;
- sur les autres distributions, il vous faudra lire la documentation de celle-ci pour trouver les fichiers à éditer).

Presque invariablement, après un tel changement, les fichiers de format doivent être régénérés après avoir changé les paramètres de la mémoire.

______________________________________________________________________

*Sources :*

- {faquk}`Enlarging TeX <FAQ-enlarge>`,
- <https://latex.developpez.com/faq/erreurs?page=T>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,taille mémoire de LaTeX,étendre la mémoire,augmenter la mémoire,imbrication
```
