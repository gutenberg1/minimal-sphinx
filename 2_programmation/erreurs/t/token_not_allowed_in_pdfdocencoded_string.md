```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Token not allowed in PDFDocEncoded string » ?

- **Message** : `Token not allowed in PDFDocEncoded string`
- **Origine** : extension *hyperref*.

L'extension {ctanpkg}`hyperref` produit cette erreur quand elle ne sait pas comment transformer quelque chose en un « caractère » qui ira dans l'un de ses champs de données du PDF. Par exemple, le code suivante provoquera une erreur :

```latex
% !TEX noedit
\newcommand{\filled}[2]{%
  #1%
  \hfil
  #2%
}
\section{\filled{foo}{bar}}
```

Ici, {ctanpkg}`hyperref` vous indique qu'il procéde à une modification :

```latex
% !TEX noedit
removing `\hfil' on input line ...
```

Cette suppression de la commande`\hfil` n'est pas surprenante : comment feriez-vous pour composer l'effet d'une commande `\hfil` dans un signet de PDF ?

L'extension vous permet de définir une méthode alternative pour de telles choses avec la commande `\texorpdfstring`. Elle prend deux arguments :

- le premier est ce qui est composé ;
- le second est ce qui est mis dans le signet.

Par exemple, ce que vous aimeriez probablement faire dans ce cas, c'est juste placer un seul espace dans le signet. Si tel est le cas, il faudrait alors écrire ceci :

```latex
% !TEX noedit
\newcommand{\filled}[2]{%
  #1%
  \texorpdfstring{\hfil}{\space}%
  #2%
}
\section{\filled{foo}{bar}}
```

Avec cette définition, l'exemple est fonctionnel ({ctanpkg}`hyperref` connait la commande `\space`).

______________________________________________________________________

*Source :* {faquk}`Token not allowed in PDFDocEncoded string <FAQ-texorpdf>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,errors
```
