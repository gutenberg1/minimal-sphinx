```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Rotation not supported » ?

- **Message** : `Rotation not supported`
- **Origine** : packages *graphics/graphicx*.

On a effectué une rotation avec `\rotatebox` ou une commande analogue, mais le pilote graphique sélectionné ne gère pas les rotations des objets. {latexlogo}`LaTeX` laissera l'espacement nécessaire, mais le document imprimé pourra montrer une image à un mauvais emplacement.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=R>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,pivoter une image,includegraphics,pilote graphique
```
