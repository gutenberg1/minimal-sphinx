```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « `\RequirePackage` or `\LoadClass` in Options Section » ?

- **Message** : `\RequirePackage or \LoadClass in Options Section`
- **Origine** : *LaTeX*.

{latexlogo}`LaTeX` a trouvé un `\RequirePackage` ou un `\LoadClass` à l'intérieur d'un fichier d'extension ou de classe entre des commandes `\DeclareOption` et `\ProcessOptions`. Le chargement d'extension ou de classe n'est pas permis dans cette partie puisqu'il démolirait la structure de données qui mémorise l'ensemble d'options en cours.
.. todo:: Voir section A.4 du *LaTeX Companion*.

Si l'on veut charger une extension lorsqu'une certaine option est spécifiée, il faut utiliser un drapeau pour indiquer que l'option a été sélectionnée et charger l'extension après que la commande `\ProcessOptions` a fait son travail.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=R>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,charger un package,charger une classe
```
