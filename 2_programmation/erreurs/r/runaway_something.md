```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Runaway ⟨quelque chose⟩ » ?

- **Message** : `Runaway ⟨quelque chose⟩`
- **Origine** : *TeX*.

{tttexlogo}`TeX` pense qu'il a analysé trop loin alors qu'il recherche la fin du `⟨quelque chose⟩`, où `⟨quelque chose⟩` peut être *argument*, *definition*, *preamble* ou *text*. Outre du code {tttexlogo}`TeX` de bas niveau fautif, le cas le plus fréquent est argument. On a, par exemple, oublié de refermer une accolade d'un argument et {tttexlogo}`TeX` recherche la fin de l'argument jusqu'à atteindre la fin du document ou remplir totalement sa mémoire. Des définitions incomplètes avec `\newcommand`, `\newenvironment`, et d'autres déclarations analogues, indiquent également que l'« `argument has run away` » (l'argument s'échappe). Seules les définitions de bas niveau, avec les primitives de {tttexlogo}`TeX` telles que `\def`, produisent une « `Runaway definition` ».

Un « `Runaway preamble` » signifie qu'une structure d'alignement a des problèmes (cela ne doit pas arriver dans des documents LaTeX normaux) et Runaway text indique généralement un problème sur une assignation de registre d'unité lexicale (cela ne doit normalement jamais arriver, à moins d'une sérieuse erreur d'implémentation dans une extension).

Contrairement aux situations avec des messages d'erreur normaux, il n'y a pas de numéro de ligne qui indique où l'erreur a été détectée (puisque {tttexlogo}`TeX` a souvent atteint la fin du fichier). À la place, on a le début du matériel qui a été absorbé. Par exemple, en cas de définition sans l'accolade fermante, comme ici :

```latex
% !TEX noedit
\newcommand\foo{bar
\begin{document}
Du texte
\end{document}
```

qui donne :

```
Runaway argument ?
{bar \begin {document} Du texte \end {document}
! File ended while scanning use of \@argdef.
<inserted text>
               \par
<*> samplefile.tex

?
```

L'insertion d'un `\par` par {tttexlogo}`TeX` afin de corriger l'erreur n'aide en rien dans ce cas précis, puisque la totalité du document a été avalée. Au lieu de « `File ended while...` », on peut voir un autre message tel que « `Paragraph ended before...` ».

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=R>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,truc qui s'échappe,accolade ouvrante,accolade fermante
```
