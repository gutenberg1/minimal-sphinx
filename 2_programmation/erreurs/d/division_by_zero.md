```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Division by 0 » ?

- **Message** : `Division by 0`
- **Origine** : package *graphics/graphicx*.

Cette erreur :

```latex
% !TEX noedit
! Package graphics Error : Division by 0.
```

peut apparaître parce que vous avez vraiment chargé une figure qui dit avoir une dimension nulle. Mais plus souvent, elle est causée par une rotation. Voici l'explication.

Dans TeX, les objets peuvent avoir à la fois une hauteur (mesure *au-dessus* de la ligne de base) et une profondeur (mesure *au-dessous* de la ligne de base). Si vous faites pivoter un objet de 180 degrés, vous convertissez sa hauteur en profondeur, et inversement ; si l'objet avait au départ une profondeur nulle (il ne s'étendait pas sous la ligne de base), vous l'aurez converti en un objet de hauteur nulle.

Supposons que vous incluiez votre graphique avec cette commande :

```latex
% !TEX noedit
\includegraphics[angle=180,height=5cm]{ma_figure.pdf}
```

Si le fichier `ma_figure.pdf` n'avait pas de profondeur au départ, les calculs de mise à l'échelle produiront une erreur de division par zéro.

Heureusement, l'extension {ctanpkg}`graphicx` propose aussi l'option `totalheight`, qui vous permet de spécifier la taille de l'image comme la somme de sa hauteur et de sa profondeur. Ainsi

```latex
% !TEX noedit
\includegraphics[angle=180,totalheight=5cm]{ma_figure.pdf}
```

résoudra l'erreur et se comportera comme vous pouvez l'espérer.

:::{note}
Si vous utilisez l'extension {ctanpkg}`graphics`, plus ancienne, utilisez la forme étoilée de la commande `\resizebox` (`\resizebox*`, donc) pour que ce soit `totalheight` qui soit utilisée :

```latex
% !TEX noedit
\resizebox*{!}{5cm}{%
  \rotatebox{180}{%
    \includegraphics{ma_figure.pdf}%
  }%
}
```
:::

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=D>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur,
- {faquk}`Graphics division by zero <FAQ-divzero>`.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,division par zéro,division par 0,dimension nulle,problème includegraphics,bug graphicx
```
