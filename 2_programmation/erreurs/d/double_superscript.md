```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Double superscript » ?

- **Message** : `Double superscript`
- **Origine** : *TeX*.

{latexlogo}`LaTeX` a trouvé deux exposants consécutifs. {doc}`Voir les explications pour les indices </2_programmation/erreurs/d/double_subscript>`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=D>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,exposant d'exposant,mettre en exposant,mode mathématique,puissance
```
