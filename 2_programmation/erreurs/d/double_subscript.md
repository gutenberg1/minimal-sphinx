```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Double subscript » ?

- **Message** : `Double subscript`
- **Origine** : *TeX*.

Deux indices apparaissent consécutivement (par exemple `x_i_2`) et {latexlogo}`LaTeX` ne sait pas ce que cela signifie :        $x_{i2}$ ou        $x_{i_2}$. Il faut donc ajouter des accolades autour de chaque indice : `x_{i2}` ou `x_{i_2}`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=D>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,indice d'indice,mettre en indice,mode mathématique,underscore,index
```
