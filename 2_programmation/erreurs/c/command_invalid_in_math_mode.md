```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Command ⟨nom⟩ invalid in math mode » ?

- **Message** : `Command ⟨nom⟩ invalid in math mode`

Il s'agit soit d'un avertissement, soit d'une erreur indiquant que l'on a utilisé une commande en mode mathématique qui ne doit être utilisée qu'en mode texte. Dans le cas du message d'erreur, utiliser `h` pour obtenir plus d'informations.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=C>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,warning en mode mathématique,commande mathématique invalide
```
