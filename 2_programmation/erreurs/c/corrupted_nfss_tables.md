```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Corrupted NFSS tables » ?

- **Message** : `Corrupted NFSS tables`

{latexlogo}`LaTeX` a essayé une certaine substitution de fontes [^footnote-1] et a détecté une incohérence dans ses tables internes. Cette erreur survient lorsqu'une substitution de fontes a été demandée et que les règles de substitution contiennent une boucle (c'est-à-dire l'existence de déclarations *sub* circulaires), ou lorsque les arguments de substitution par défaut pour le codage en cours pointent sur un groupe de formes de fontes inexistant.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=C>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,erreur de police,erreur de fonte,fichiers de polices
```

[^footnote-1]: **NFSS** signifie « *new font selection scheme*. »
