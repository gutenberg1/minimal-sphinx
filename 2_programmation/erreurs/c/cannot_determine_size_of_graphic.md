```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Cannot determine size of graphic in ⟨fichier⟩ » ?

- **Message** : `Cannot determine size of graphic in ⟨fichier⟩`
- **Origine** : package *graphics/graphicx*.

La taille explicite de l'image n'a pas été spécifiée et {latexlogo}`LaTeX` est incapable de déterminer cette taille directement à partir du `⟨fichier⟩` graphique. Normalement, il le fait automatiquement, par exemple avec les fichiers `.eps` en lisant l'information de la boîte englobante. Cependant, en fonction du pilote graphique, il peut être incapable d'extraire cette information à partir d'images *bitmap*, telles que les fichiers `.jpg`, `.gif` et `.png`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=C>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,taille des images,format d'images,includegraphics,fichier postscript,encapsulated postscript,fichier JPEG
```
