```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Command ⟨nom⟩ already defined » ?

- **Message** : `Command ⟨nom⟩ already defined`
- **Origine** : *LaTeX*.
- On a essayé de déclarer une commande, un environnement, un nouveau `savebox`, une longueur ou un compteur avec un `⟨nom⟩` qui a déjà une signification pour {latexlogo}`LaTeX`. Dans ce cas, cette déclaration est ignorée et il faut choisir un autre nom.
- Cette erreur est également déclenchée lors de l'utilisation de `\newcommand` avec un `⟨nom⟩` qui débute par `\end...`, même si `\renewcommand` affirme que ce `⟨nom⟩` est inutilisé.
- Elle survient enfin lorsqu'on essaie de définir un environnement `⟨nom⟩` et que les commandes `\⟨nom⟩` ou `\end⟨nom⟩` ont déjà une définition. Par exemple, on ne peut pas définir un environnement `graf`, car {tttexlogo}`TeX` définit une commande de bas niveau appelée `\endgraf`.

______________________________________________________________________

Vous avez chargé deux extensions et la seconde signale que l'une des commandes qu'elle définit est déjà présente.

Par exemple, {ctanpkg}`txfonts` et {ctanpkg}`amsmath` définissent toutes deux une commande `\iint` (et `\iiint` et ainsi de suite). Si vous saisissez cette séquence dans votre code :

```latex
% !TEX noedit
\usepackage{txfonts}
\usepackage{amsmath}
```

alors vous obtiendrez un message d'erreur de la forme :

```latex
% !TEX noedit
! LaTeX Error : Command \iint already defined.
               Or name \end... illegal, see p.192 of the manual.
```

En règle générale, les définitions de l'extension {ctanpkg}`amsmath` sont correctes. Cependant, il a ici matière à utiliser la version de la commande `\iint` de l'extension {ctanpkg}`txfonts` : ses polices `tx` ont un double symbole intégral qui n'a pas besoin d'être obtenu par « bidouille » comme le fait {ctanpkg}`amsmath`.

Ce cas se retrouve lorsque vous chargez plusieurs extensions de symboles et que chacune définit le même symbole (`\euro` est un cas fréquent).

Il existe des cas similaires où une extension redéfinit la commande d'une autre extension mais aucune erreur ne se produit car la redéfinition n'utilise pas `\newcommand`. Souvent, dans ce cas, vous ne remarquez le changement que parce que vous pensiez obtenir la définition donnée par la première extension. Le couple «  {ctanpkg}`amsmath` - {ctanpkg}`txfonts` » fonctionne ainsi : {ctanpkg}`txfonts` ne provoque pas d'erreurs s'il est chargé *après* mais il impose sa définition.

Vous pouvez résoudre le problème en enregistrant et en restaurant la commande. Les habitués de la programmation peuvent le faire mais il existe une extension pour traiter directement ce point : {ctanpkg}`savesym` qui se limite à définir deux commandes, à savoir `\savesymbol` et `\restoresymbol`. En voici un exemple :

```latex
% !TEX noedit
\usepackage{savesym}
\usepackage{amsmath}
\savesymbol{iint}
\usepackage{txfonts}
\restoresymbol{TXF}{iint}
```

Ce code va conserver la définition de `\iint` de l'extension {ctanpkg}`amsmath` et va créer une commande `\TXFiint` contenant la définition de l'extension {ctanpkg}`txfonts`.

______________________________________________________________________

*Sources :*

- {faquk}`Package reports "command already defined" <FAQ-alreadydef>`,
- <https://latex.developpez.com/faq/erreurs?page=C>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,définition d'une commande,erreur newcommand,erreur renewcommand,commandes liées aux environnements
```
