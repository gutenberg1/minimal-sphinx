```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Cannot define Unicode char value < 00A0 » ?

- **Message** : `Cannot define Unicode char value < 00A0`
- **Origine** : package *inputenc*.

Les valeurs strictement inférieures à `"00A0` (c'est de l'hexadécimal, et ça correspond à 160 en décimal) sont soit invalides en tant que valeurs Unicode pour des caractères de texte, soit elles doivent être redéfinies dans {latexlogo}`LaTeX`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=C>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,inputenc,nombre hexadécimal,code d'erreur
```
