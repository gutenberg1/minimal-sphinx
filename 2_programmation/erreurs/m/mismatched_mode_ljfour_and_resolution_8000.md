```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Mismatched mode ljfour and resolution 8000 » ?

- **Message** : `Mismatched mode ljfour and resolution 8000`
- **Origine** : dvips.

Vous exécutez `dvips` et vous rencontrez un flux de messages d'erreur, commençant par `Mismatched mode` (mode incompatible). Le mode en question est celui utilisé par défaut dans votre installation. Il est défini dans le fichier de configuration de `dvips`. De fait, `ljfour` est le plus courant (valeur par défaut dans la plupart des distributions) mais n'est pas invariable. Le problème est que `dvips` a rencontré une police pour laquelle il doit générer une image bitmap (car il ne peut pas trouver cette police au format Type 1) et il n'y a pas d'éléments disponibles pour fournir des instructions à MetaFont.

Alors que faire ? Le nombre 8000 vient de l'option `-Ppdf` de `dvips`, valeur que vous avez peut-être trouvée dans la {doc}`question traitant des mauvais type de polices </5_fichiers/fontes/mon_document_est_flou_a_cause_des_fontes_t3>`. La solution la plus simple est de passer au substitut trivial `-Pwww` qui sélectionne les polices de type 1 nécessaires pour la génération PDF, mais rien d'autre. Cependant, cela vous laissera avec des polices bitmap indésirables dans votre fichier PDF. La solution « appropriée » est de trouver un moyen d'exprimer ce que vous voulez faire, en utilisant des polices de type 1.

______________________________________________________________________

*Source :* {faquk}`Mismatched mode ljfour and resolution 8000 <FAQ-8000>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,errors
```
