```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Missing \$ inserted » ?

- **Message** : `Missing $ inserted`
- **Origine** : *TeX*.

{tttexlogo}`TeX` a rencontré quelque chose qui n'est autorisé qu'en {doc}`mode mathématique </4_domaines_specialises/mathematiques/passer_en_mode_mathematique>` (par exemple les commandes `\sum`, `\alpha`, `^`, `_`) alors qu'il était en mode texte, ou, à l'inverse, un élément interdit en mode mathématique (par exemple la commande `\par`) alors qu'il traitait une formule. Il a donc inséré un `$` pour passer en mode mathématique ou pour le quitter.

Par exemple si l'on essaie d'obtenir un trait de soulignement « _ » en saisissant `_` au lieu de la commande `\_`, {latexlogo}`LaTeX` composera le reste du paragraphe en mode mathématique, le plus souvent en produisant plusieurs erreurs au cours de ce traitement.

{latexlogo}`LaTeX` propose une commande `\ensuremath` qui met son argument en mode mathématique si nécessaire : si vous voulez donc un `\alpha` dans votre texte en cours, indiquez `\ensuremath{\alpha}`. Si le morceau de texte en cours d'exécution se voit placé dans des mathématiques, le `\ensuremath` deviendra inactif. Voir la question « [À quoi sert la commande « \\ensuremath » ?](/2_programmation/macros/commande_ensuremath) » pour plus d'informations.

______________________________________________________________________

*Sources :*

- {faquk}`"Missing \`$\` inserted" <FAQ-nodollar>`,
- <https://latex.developpez.com/faq/erreurs?page=M>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,dollar manquant,mode mathématique,problème avec une formule
```
