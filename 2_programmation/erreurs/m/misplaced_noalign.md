```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Misplaced `\noalign` » ?

- **Message** : `Misplaced \noalign`
- **Origine** : *TeX*.

La primitive {tttexlogo}`TeX` `\noalign` est utilisée en interne pour placer un matériel « non aligné » entre des lignes d'une structure alignée. Cependant, on ne peut l'utiliser qu'immédiatement après une commande qui termine une ligne. Par exemple, on obtient cette erreur lorsqu'on utilise `\hline` en dehors d'un `array` ou d'un `tabular`, ou bien pas immédiatement après un ```` \\ ```` dans ces environnements.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=M>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,array,tableau,tabular,matrice,alignement
```
