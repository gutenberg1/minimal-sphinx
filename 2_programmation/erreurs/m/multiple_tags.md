```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Multiple `\tag` » ?

- **Message** : `Multiple \tag`
- **Origine** : package *amsmath*.

On ne peut utiliser qu'une seule commande `\tag` par équation à l'intérieur des environnements hors-texte (`amsmath`). Mise à part la première, elles seront toutes ignorées.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=M>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,étiquettes,références
```
