```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Missing control sequence inserted » ?

- **Message** : `Missing control sequence inserted`
- **Origine** : *TeX*.

On a utilisé `\newcommand` ou `\renewcommand` sans fournir de nom de commande (commençant par une barre oblique inverse) comme premier argument.

## Plus précisément

Si vous souhaitez par exemple définir la macro `\L` pour écrire un « 𝐋 » gras, la syntaxe correcte est :

```latex
\documentclass{article}
  \usepackage{lmodern}
  \pagestyle{empty}
  \renewcommand{\L}{\textbf{L}}

\begin{document}
\Large L \L
\end{document}
```

Si vous oubliez la barre oblique dans la définition de la commande, en écrivant

```latex
\documentclass{article}
  \usepackage{lmodern}
  \pagestyle{empty}
  \def\L{\textbf{L}}

\begin{document}
\Large L \L
\end{document}
```

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=M>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.
- [DeclareDocumentEnvironment : missing control sequence inserted — \\inaccessible](https://tex.stackexchange.com/questions/121481/declaredocumentenvironment-missing-control-sequence-inserted-inaccessible).

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,définir une commande,définir une macro,nom de commande
```
