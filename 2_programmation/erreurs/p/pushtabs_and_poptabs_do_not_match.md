```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « `\pushtabs` and `\poptabs` don't match » ?

- **Message** : `\pushtabs and \poptabs don't match`
- **Origine** : *LaTeX*.

On a utilisé une commande `\poptabs` dans un environnement `tabbing`, mais il n'y avait aucune commande `\pushtabs` précédente.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=P>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,alignements,tabulations,push et pop
```
