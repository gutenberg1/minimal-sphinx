```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Page height already too large » ?

- **Message** : `Page height already too large`
- **Origine** : *LaTeX*.

On a utilisé `\enlargethispage` sur une page dont la hauteur est déjà supérieure à 8191.99998 points (pt), c'est-à-dire environ 2,88 mètres. {latexlogo}`LaTeX` pense qu'il est dangereux d'augmenter la taille de la page de manière aussi importante.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=P>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,dimension maximale d'une page
```
