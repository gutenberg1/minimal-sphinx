```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Extra `\endgroup` » ?

- **Message** : `Extra \endgroup`
- **Origine** : *TeX*.

{tttexlogo}`TeX` a vu un `\endgroup` sans `\begingroup` correspondant.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=E>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,begingroup,endgroup,erreur de compilation
```
