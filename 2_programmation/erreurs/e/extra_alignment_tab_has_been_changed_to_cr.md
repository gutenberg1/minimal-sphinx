```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Extra alignment tab has been changed to `\cr` » ?

- **Message** : `Extra alignment tab has been changed to \cr`
- **Origine** : *TeX*.

Lorsqu'on utilise une structure d'alignement, telle que `tabular`, ou l'un des environnements mathématiques hors-texte (par exemple, `eqnarray` ou `split` des extensions {ctanpkg}`mathtools` et {ctanpkg}`amsmath`), chaque ligne est alors divisée en un nombre défini de colonnes séparées par des signes `&`. L'erreur signifie que ces caractères sont trop nombreux, sans doute à cause d'un `&` surnuméraire ou de l'oubli d'un ```` \\ ```` indiquant la fin d'une ligne (le nom {tttexlogo}`TeX` pour cette fin de ligne est `\cr`, mais il n'est pas totalement équivalent à ```` \\ ````).

## Cas du « & » surnuméraire

Voici le cas classique :

```latex
% !TEX noedit
\begin{tabular}{ll}
  Il   & lui & manque \\
  une  & case
\end{tabular}
```

Ici, le deuxième `&` dans la première ligne du tableau dépasse ce que la spécification de colonne `ll`, demandant deux colonnes, peut accepter. Dans ce cas, un `l` supplémentaire résout le problème.

Cas du «  » manquant ----------

Voici un exemple :

```latex
% !TEX noedit
\begin{tabular}{ll}
  Il lui & manque
  une & case
\end{tabular}
```

Ici, le `\\` a été omis de la première ligne du tableau. Dans ce cas, si vous laissez passer l'erreur, vous constaterez que {latexlogo}`LaTeX` a créé une table équivalente à :

```latex
% !TEX noedit
\begin{tabular}{ll}
  Il lui   & manque une\\
  case
\end{tabular}
```

Autrement dit, la deuxième ligne du tableau n'a alors qu'une seule cellule.

L'erreur est plus difficile à repérer lorsque vous utilisez des instructions d'alignement dans une colonne de type `p` :

```latex
% !TEX noedit
\usepackage{array}
...
\begin{tabular}{l>{\raggedright}p{2in}}
Il lui & manque \\
une & case
\end{tabular}
```

Le problème ici est que la commande `\raggedright` dans la spécification de la colonne a écrasé la définition de `\\` dans l'environnement `tabular`, de sorte que le texte `une` apparait dans une nouvelle ligne de la deuxième colonne, et le `&` suivant est considéré comme le deuxième `&` dans le premier exemple ci-dessus. Cette erreur est détaillée dans la question « {doc}`Comment modifier la justification du texte dans une cellule ? </3_composition/tableaux/cellules/modifier_l_alignement_du_texte_dans_une_cellule>` », la solution consistant à utiliser la commande `\tabularnewline` explicitement.

## Cas des extensions « mathtools » et « amsmath »

L'extension {ctanpkg}`mathtools` (ou {ctanpkg}`amsmath`) ajoute ici sa propre subtilité. Lors de la composition d'une matrice (l'extension fournit de nombreux environnements matriciels), elle impose un nombre maximum de colonnes dans une matrice : dépassez ce maximum et l'erreur apparaîtra. Par défaut, le maximum est fixé à 10 mais cette valeur, stockée dans le compteur `MaxMatrixCols`, peut être modifiée tout comme n'importe autre compteur :

```latex
% !TEX noedit
\setcounter{MaxMatrixCols}{20}
```

______________________________________________________________________

*Sources :*

- {faquk}`Alignment tab changed to \\cr <FAQ-altabcr>`,
- <https://latex.developpez.com/faq/erreurs?page=E>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,tableaux,tabular,array,retour chariot,fin de ligne
```
