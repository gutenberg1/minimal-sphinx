```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Environment ⟨nom⟩ undefined » ?

- **Message** : `Environment ⟨nom⟩ undefined`
- **Origine** : *LaTeX*.

On obtient cette erreur lorsqu'on utilise `\renewenvironment` ou un nom d'environnement qui n'est pas connu de {latexlogo}`LaTeX`.

Soit le `⟨nom⟩` a été mal saisi, soit il faut utiliser `\newenvironment`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=E>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,environnement non défini,définir un nouvel environnement
```
