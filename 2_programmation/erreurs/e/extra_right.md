```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Extra `\right` » ?

- **Message** : `Extra \right`
- **Origine** : *TeX*.

Cette erreur indique que {tttexlogo}`TeX` a trouvé une commande `\right` sans `\left` correspondant dans une formule. Il faut se souvenir que les paires `\left...\right` doivent appartenir à la même « sous-formule ». Elles ne peuvent pas, par exemple, être séparées par un `&` dans un alignement ou apparaître dans des niveaux de groupes différents.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=E>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,délimiteurs mathématiques,accolades,parenthèses,mode mathématique
```
