```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « `\<` in mid line » ?

- **Message** : `\< in mid line`

Le `\<` défini à l'intérieur d'un environnement `tabbing` est situé au milieu d'une ligne. Il ne peut être utilisé qu'au début d'une ligne (par exemple, après `\\`).

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=-etoile-lt>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,environnement tabbing,alignement,tabulation,erreur dans un tableau
```
