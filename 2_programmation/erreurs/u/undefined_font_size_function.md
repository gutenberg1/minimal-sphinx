```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Undefined font size function ⟨nom⟩ » ?

- **Message** : `Undefined font size function ⟨nom⟩`
- **Origine** : *LaTeX*.

Une fonction de taille, utilisée dans `\DeclareFontShape`, a été mal saisie. Il faut vérifier l'entrée ou appeler l'administrateur du système.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=U>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,taille de fonte,taille de police,taille de caractère,déclaration de fonte
```
