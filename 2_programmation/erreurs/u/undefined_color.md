```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Undefined color ⟨nom⟩ » ?

- **Message** : `Undefined color ⟨nom⟩`
- **Origine** : package *color*.

On a demandé une couleur avec une commande `\color` (ou une commande analogue) de l'extension {ctanpkg}`color` sans l'avoir préalablement définie avec `\definecolor`.

Pour plus de détails, voir la référence \[60\] du *LaTeX Companion*

```{eval-rst}
.. todo:: Le précédent paragraphe appelle une révision.ou la :texdoc:`documentation de l'extension <color>` :ctanpkg:`color`.
```

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=U>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,nom de couleur,définir ses propres couleurs,couleur non définie
```
