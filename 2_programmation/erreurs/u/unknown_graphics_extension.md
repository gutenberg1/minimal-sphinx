```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Unknown graphics extension : ⟨ext⟩ » ?

- **Message** : `Unknown graphics extension : ⟨ext⟩`
- **Origine** : packages *graphics/graphicx*.

On obtient cette erreur lorsqu'on essaie de charger un fichier graphique (avec l'extension de nom de fichier `⟨ext⟩`), que le pilote graphique ne connaît pas cette extension et qu'il n'y a pas de règle par défaut pour les extensions.

Par exemple, le programme `dvips` interprète chaque extension inconnue comme `.eps`. On n'obtiendra donc jamais cette erreur (mais certainement d'autres) avec ce pilote.

______________________________________________________________________

Le package {ctanpkg}`graphicx` utilise plusieurs types de pilotes de sortie (DVI, PDF ou autres) ; chacun d'eux peut traiter une sélection différente de formats d'images. Le package doit donc être informé des types de fichiers graphiques que son pilote de sortie connaît ; jusqu'en 2020, cela se faisait généralement dans le fichier ⟨*pilote*⟩`.def` correspondant au pilote de sortie que vous utilisez, c'est maintenant intégré au noyau.

- Le message d'erreur apparaît si vous avez un fichier graphique dont l'extension ne correspond à aucune de celles que votre pilote connaît. Le plus souvent, c'est parce que vous avez été optimiste, en demandant à `dvips` de traiter un fichier PNG, ou à pdfTeX de traiter un fichier EPS. La solution est simplement de convertir le fichier graphique dans un format que votre pilote comprend, avec un outil externe, comme [ImageMagick](https://imagemagick.org/), [Gimp](https://www.gimp.org/), ou autre, en fonction des formats d'origine et de destination.
- Si vous êtes sûr que le pilote est censé comprendre le format de votre fichier, c'est peut-être que le package {ctanpkg}`graphicx` n'arrive pas à comprendre le *nom* de votre fichier d'image. Supposons que vous vouliez inclure un fichier `photo.paysage.eps` en utilisant le pilote `dvips` ; le package prendra en compte le premier point dans le nom du fichier et comprendra que l'extension de votre fichier est `paysage.eps`, et se plaindra.

Le package {ctanpkg}`grffile` peut aider dans ce dernier cas (et bien d'autres --- {texdoc}`voir sa documentation <grffile>`); ainsi, avec lui vous pouvez écrire :

```latex
% !TEX noedit
\usepackage{graphicx}
\usepackage{grffile}
...
\includegraphics{photo.paysage.eps}
```

or you may even write

```latex
% !TEX noedit
\includegraphics{photo.paysage}
```

et {ctanpkg}`graphicx` arrivera à trouver votre fichier `eps` ou `pdf` (ou autre), en fonction de la version de (La)TeX que vous utilisez.

Si pour une raison ou une autre vous ne pouvez pas utiliser {ctanpkg}`grffile`, vous avez quelques autres possibilités :

- Renommer le fichier : par exemple `photo.paysage.eps` \$\\rightarrow\$ `photo-paysage.eps`
- Cacher le premier point dans le nom du fichier en définissant une macro :

```latex
% !TEX noedit
\newcommand*{\DOT}{.}
\includegraphics{photo\DOT paysage.eps}
```

- Dire au package {ctanpkg}`graphicx` de quel type est le fichier, avec les options de la commande `\includegraphics` :

```latex
% !TEX noedit
\includegraphics[type=eps,ext=.eps,read=.eps]{photo.paysage}
```

______________________________________________________________________

*Sources :*

- {faquk}`"Unknown graphics extension" <FAQ-unkgrfextn>`,
- <https://latex.developpez.com/faq/erreurs?page=U>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,fichier JPEG,fichierPNG,includegraphics,formats d'images,fichiers graphiques,inclure une image
```
