```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Font shape ⟨forme-fonte⟩ not found » ?

- **Message** : `Font shape ⟨forme-fonte⟩ not found`

Ce message d'erreur survient lorsqu'il y a quelque chose qui ne va vraiment plus du tout avec la déclaration `\DeclareFontShape`, par exemple si elle ne contient aucune spécification de taille. Il faut dans ce cas vérifier l'initialisation pour le groupe de formes de fontes en question.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=F>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,police non trouvée,fonte non trouvée,forme non trouvée,déclarer une famille de fontes
```
