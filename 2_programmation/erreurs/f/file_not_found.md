```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « File \`⟨nom⟩' not found » ?

- **Message** : ```` File `⟨nom⟩' not found ````

{latexlogo}`LaTeX` a essayé de charger le fichier `⟨nom⟩` mais n'a pas pu le trouver, soit parce qu'il n'existe pas, soit parce que le programme {tttexlogo}`TeX` sous-jacent regarde au mauvais emplacement. Si le fichier existe et que {latexlogo}`LaTeX` indique qu'il n'est pas disponible, il est possible que l'installation {tttexlogo}`TeX` utilise un mécanisme de hachage pour accélérer l'accès au fichier. Dans ce cas, on doit lancer un programme spécial (par exemple, `mkTeXlsr`).

Cette erreur est déclenchée lorsque des commandes telles que `\input` et
`\usepackage` ne peuvent pas trouver le fichier demandé. On peut indiquer un
autre nom de fichier en réponse à l'erreur. Si le nouveau nom est spécifié sans
extension de nom de fichier, l'ancienne extension est réutilisée si elle est
connue de {latexlogo}`LaTeX`. Si l'on ne veut pas charger de fichier, il faut
appuyer sur *Entrée* ; pour quitter la compilation, il faut saisir `x` ou
`X`. Dans certains cas, on reçoit une erreur {tttexlogo}`TeX` de bas niveau
similaire (```` ! I can't find file `⟨nom⟩' ````) qui est un peu plus difficile à quitter. Voir l'entrée page 917 du *LaTeX Companion*

```{eval-rst}
.. todo:: Le précédent paragraphe appelle une révision.
```

Si un fichier graphique demandé avec `\includegraphics` est absent, il peut être judicieux de taper `h` pour en apprendre un peu plus sur les extensions testées lorsque le fichier a été recherché.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=F>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,fichier non trouvé,usepackage,input,includegraphics
```
