```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « OK » ?

- **Message** : `OK`
- **Origine** : *TeX*.

Il ne s'agit pas vraiment d'un message d'erreur. On a utilisé une commande de trace de {tttexlogo}`TeX`, telle que `\show` ou `\showthe`. Après avoir affiché les données, {latexlogo}`LaTeX` interrompt la compilation avec ce message pour permettre une interaction sur la ligne de commande (par exemple, pour saisir `i\show...` afin de voir d'autres valeurs). Ce message s'affiche également si `\tracingonline` est strictement positif et que des commandes, qui n'écrivent normalement que dans le fichier de trace, ont été utilisées. Voir {doc}`aussi ce message </2_programmation/erreurs/o/ok_see_the_transcript_file>`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=O>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,invite de commande LaTeX,compilation interactive
```
