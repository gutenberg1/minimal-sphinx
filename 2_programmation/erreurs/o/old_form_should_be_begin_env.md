```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Old form ⟨commande⟩ should be `\begin{⟨env⟩}` » ?

- **Message** : `Old form ⟨commande⟩ should be \begin{⟨env⟩}`
- **Origine** : package *amsmath*.

On a utilisé `cases` ou `matrix` avec leur ancienne syntaxe sous la forme d'une commande qui n'est plus celle d' {ctanpkg}`amsmath`. Il faut utiliser la nouvelle syntaxe : celle qui utilise un environnement.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=O>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,ancienne syntaxe d'amsmath,problème avec le package amsmath
```
