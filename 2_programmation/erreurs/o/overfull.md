```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie le message : « Overfull ⟨objet⟩ » ?

- **Message** : `Overfull ⟨objet⟩`
- **Origine** : TeX

Ce message est un avertissement et, la plupart du temps, ne prête pas à conséquence. Lorsque {latexlogo}`LaTeX` n'arrive pas à satisfaire tous ses critères de qualité de mise en forme, il peut avoir à dégrader un. Il signale alors cette entorse par ce message dans le fichier `.log`.

En voici un exemple utilisant le fait que la commande `\hbox` n'autorise pas un retour à la ligne :

```latex
\documentclass[draft]{article}
  \usepackage[width=6cm]{geometry}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
Pour obtenir un message Overfull, il suffit de
mettre \hbox{une phrase trop longue que \LaTeX{}
n'a pas le droit de couper}
\end{document}
```

Celui-ci précise le type de dépassement, sa valeur (160pt de longueur) et sa localisation :

```
Overfull \hbox (160.25319pt too wide) in paragraph at lines 7--9
\OT1/cmr/m/n/10 suf-fit de mettre [] |
```

:::{note}
L'option de classe `draft`, utilisée dans cet exemple, permet de visualiser ces dépassements par un carré noir dans la marge, pour éventuellement corriger le texte, par exemple en indiquant une césure possible non connue de {latexlogo}`LaTeX`.
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX,dépassement,mode brouillon
```
