```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « I can't find file \`⟨nom⟩' » ?

- **Message** : ''I can't find file \`⟨*nom*⟩' ''
- **Origine** : *TeX*.

Il s'agit d'une erreur {tttexlogo}`TeX` de bas niveau qui a lieu lorsque {tttexlogo}`TeX` ne peut pas trouver un fichier qui doit être chargé. Cette erreur ne peut être levée qu'en fournissant à {tttexlogo}`TeX` un fichier qu'il est capable de trouver, ou en stoppant la compilation (si le système d'exploitation le permet). Pour passer cette erreur, de nombreuses installations fournissent un fichier `null.tex` qui permet de répondre `null` à cette erreur. {latexlogo}`LaTeX` utilise habituellement le message d'erreur « `File`⟨nom⟩' not found\` » qui permet d'autres actions de la part de l'utilisateur. Cependant, selon le codage de l'extension, on peut obtenir plutôt cette erreur que l'erreur {latexlogo}`LaTeX`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=I>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,fichier non trouvé
```
