```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Improper `\hyphenation` » ?

- **Message** : `Improper \hyphenation`
- **Origine** : *TeX*.

Lorsqu'on souhaite spécifier une exception de coupure avec `\hyphenation`, il faut s'assurer que l'argument ne contient que des lettres, et des traits d'union (`-`) pour indiquer les points de coupure. Le problème est que, par exemple, les caractères accentués sont des glyphes dans certaines fontes (ils sont alors permis), mais que d'autres codages de fontes produisent des constructions complexes exigeant la primitive `\accent`. Par exemple, si l'on utilise le codage `T1`, `\"u` est un glyphe unique. Ainsi,

```latex
% !TEX noedit
\usepackage[T1]{fontenc}
\hyphenation{T\"ur-stop-per}
```

est valide. La même exception de coupure utilisée avec le codage par défaut `OT1` aurait produit cette erreur.

Voir page 463 du *LaTeX Companion*

```{eval-rst}
.. todo:: Ce dernier paragraphe appelle une révision : « pour une explication des différences de caractères dans les codages principaux. »
```

______________________________________________________________________

Par exemple le message d'erreur

```
! Improper \hyphenation will be flushed.
\leavevmode ->\unhbox
                      \voidb@x
<*> \hyphenation{Ji-m\'e
                        -nez}
```

en LaTeX, ou

```
! Improper \hyphenation will be flushed.
\'#1->{
       \accent 19 #1}
<*> \hyphenation{Ji-m\'e
                        -nez}
```

(en Plain TeX).

Comme mentionné dans la [question sur les échecs de césure](FAQ-nohyph), Les « mots » contenant un commande de type `\⟨accent⟩` ne peuvent pas être coupés en fin de ligne. Par conséquent, tout mot de ce type est considéré comme incorrect dans une commande `\hyphenation{}`.

La césure se fait au fur et à mesure de la mise en forme des paragraphes ; à ce moment, TeX sait quelle police est utilisée pour chaque glyphe ; il connaît donc le codage utilisé. La solution au problème consiste donc à utiliser une police de caractères qui contient le caractère accentué ; ainsi l'accent est « caché » aux mécanismes de césure et tout se passe bien.

Pour les utilisateurs de LaTeX, c'est très facile : il suffit d'utiliser `\usepackage[T1]{fontenc}`, et les commandes de lettres accentuées telles que le `\'e` dans `\\hyphenation{Ji-m\'e-nez}` deviennent automatiquement un seul caractère accentué avant que ne se déclenche l'algorithme de coupure de mots.

______________________________________________________________________

*Sources :*

- {faquk}`Improper "\\hyphenation" will be flushed <FAQ-badhyph>`,
- <https://latex.developpez.com/faq/erreurs?page=I>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur,
- <https://tex.stackexchange.com/questions/15344/what-is-accents-commands-behavior>.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,césure des mots,motifs de césure,hyphenation,coupure de mots
```
