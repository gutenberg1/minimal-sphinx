```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Illegal parameter number in definition of ⟨commande⟩ » ?

- **Message** : `Illegal parameter number in definition of ⟨commande⟩`
- **Origine** : *TeX*.

Cette erreur survient lorsqu'une commande ou un environnement (re)défini utilise `#⟨chiffre⟩` dans son texte de remplacement, avec un chiffre strictement supérieur au nombre de paramètres déclarés, surtout si ce nombre n'est pas indiqué comme dans l'exemple suivant (avec deux manières de définir une commande) :

```latex
% !TEX noedit
\newcommand{\mot}{Voici le mot #1 !} % Façon LaTeX

\def\terme{Voici le terme #1 !}      % Façon TeX
```

Dans l'un ou l'autre des cas ci-dessus, la définition utilise un argument, mais le programmeur n'a pas indiqué, à l'avance, qu'elle allait le faire. Le correctif est simple :

```latex
% !TEX noedit
\newcommand{\mot}[1]{Voici le mot #1 !} % Façon LaTeX

\def\terme#1{Voici le terme #1 !}       % Façon TeX
```

Cette erreur peut être provoquée implicitement en raison de commandes de déclarations emboîtées, comme `\newcommand`, en ayant oublié que les commandes intérieures se réfèrent à leurs arguments en doublant les caractères `#`. Voici un exemple de code générant cette anomalie :

```latex
% !TEX noedit
\newcommand{\ajout}{Définition de commande %  % Façon LaTeX
  \newcommand{\mot}[1]{Voici le mot #1 !}%
}

\def\enplus{Définition de commande %          % Façon TeX
  \def\terme#1{Voici le mot #1 !}%
}
```

Ce point est expliqué dans la question « {doc}`Comment définir des commandes dans des commandes ? </2_programmation/macros/definir_une_macro_a_l_interieur_d_une_autre_macro>` ». La bonne manière de l'écrire est ici :

```latex
% !TEX noedit
\newcommand{\ajout}{Définition de commande %  % Façon LaTeX
  \newcommand{\mot}[1]{Voici le mot ##1 !}%
}

\def\enplus{Définition de commande %          % Façon TeX
  \def\terme##1{Voici le mot ##1 !}%
}
```

Enfin, une autre cause est de se référer à des arguments d'un environnement dans le troisième paramètre obligatoire de `\newenvironment` ou `\renewenvironment` (celui donnant le code appliqué en fin d'environnement), comme expliqué à la question « [Que sont les environnements LaTeX ?](/2_programmation/macros/que_sont_les_environnements) » :

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=I>.
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.
- {faquk}`Illegal parameter number in definition <FAQ-errparnum>`.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,nombtre de paramètres,nombre d'arguments,trop d'arguments,définition des commandes,newcommand
```
