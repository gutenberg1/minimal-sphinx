```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Improper discretionary list » ?

- **Message** : `Improper discretionary list`
- **Origine** : *TeX*.

Cette erreur est produite par {tttexlogo}`TeX` lorsqu'il rencontre une commande `\discretionary` dont les arguments contiennent autre chose que des caractères, des boîtes ou des blancs fixes après développement.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=I>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,discretionary
```
