```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Incomplete ⟨condition⟩ ; all text was ignored after line ⟨numéro⟩ » ?

- **Message** : `Incomplete ⟨condition⟩ ; all text was ignored after line ⟨numéro⟩`
- **Origine** : *TeX*.

Une condition {tttexlogo}`TeX` de bas niveau ne se termine pas correctement (il n'y a pas de `\fi` correspondant) lorsque {latexlogo}`LaTeX` atteint la fin du fichier source.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=I>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,problème avec une condition,ifthen,ifthenelse
```
