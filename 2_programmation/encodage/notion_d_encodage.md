```{role} latexlogo
```
```{role} tttexlogo
```
# Que sont les encodages ?

Commençons par définir deux notions :

- un caractère est un « atome » d'une langue ou d'un dialecte : il peut donc s'agir d'une lettre dans une langue alphabétique, d'une syllabe dans une langue syllabique, ou d'un idéogramme dans une langue idéographique ;
- un glyphe est une marque créée sur écran ou papier qui représente un caractère.

Bien sûr, pour que la lecture soit possible, il doit y avoir une certaine relation entre le glyphe et le caractère. Ainsi, alors que la forme précise du glyphe peut être affectée par de nombreux autres facteurs, tels que les capacités du support d'écriture ou les choix graphiques du concepteur, l'essence du caractère sous-jacent doit être conservée.

Chaque fois qu'un ordinateur doit représenter des caractères, il faut définir la relation entre un ensemble de nombres (que manipule l'ordinateur) et les caractères qu'ils représentent. C'est là l'essence d'un [encodage](https://fr.wikipedia.org/wiki/Codage_des_caractères) : une correspondance entre un ensemble de nombres et un ensemble de choses à représenter.

Bien entendu, {tttexlogo}`TeX` traite des caractères encodés à longueur de temps : les caractères qui lui sont présentés en entrée sont encodés et il émet des caractères encodés dans sa sortie DVI ou PDF. Dans le détail, ces encodages ont des propriétés assez différentes.

## L'encodage en entrée

Le flux d'entrée de {tttexlogo}`TeX` était assez chaotique à l'époque où Knuth a implémenté {tttexlogo}`TeX` pour la première fois. Knuth lui-même a préparé des documents sur des terminaux qui produisaient toutes sortes de caractères étranges. Aussi, {tttexlogo}`TeX` contient des dispositions pour traduire son entrée (même encodée) en quelque chose de régulier.

De nos jours, le système d'exploitation traduit les saisies de l'utilisateur en un code approprié à sa langue : l'encodage utilisé se rattache généralement une norme nationale ou internationale, bien que certains systèmes d'exploitation utilisent des « pages de code » (*code pages* telles que définies par Microsoft). Ces normes et pages de codes contiennent souvent des caractères qui peuvent ne pas apparaître dans le flux d'entrée du système {tttexlogo}`TeX`. D'une manière ou d'une autre, ces caractères doivent être traités --- donc un caractère d'entrée comme « é » doit être interprété par {tttexlogo}`TeX` d'une manière qui imite au moins la façon dont il interprète la commande `\'e`.

## L'encodage en sortie (ou encodage de police)

Le flux de sortie de {tttexlogo}`TeX` diffère quelque peu : les caractères qu'il contient doivent être utilisés pour sélectionner les glyphes des polices à utiliser. Ainsi, l'encodage du flux de sortie est théoriquement un encodage de police (bien que la police en question puisse être une {doc}`police virtuelle </5_fichiers/fontes/que_sont_les_fontes_virtuelles>`). En principe, une bonne partie de ce qui apparaît dans le flux de sortie peut être une transcription directe de ce qui est arrivé dans l'entrée, mais le flux de sortie contient également le produit de commandes dans l'entrée et de traductions de l'entrée telles que des [ligatures](/3_composition/texte/mots/desactiver_une_ligature) (par exemple la syllabe « fi »).

Les encodages de polices sont devenus un sujet important lorsque l {doc}`encodage de Cork </5_fichiers/fontes/que_sont_les_fontes_ec>` est apparu, en raison de la possibilité de supprimer les commandes `\accent` dans le flux de sortie (et donc d'améliorer la qualité de la césure du texte dans les [langues flexionnelles](https://fr.wikipedia.org/wiki/Langue_flexionnelle) comme montré à la question « {doc}`Comment fonctionne la césure en TeX ? </3_composition/langues/cesure/comment_fonctionne_la_cesure>`) ». Pour tirer parti des caractères diacritiques présents dans les polices, il est nécessaire de faire en sorte qu'à chaque fois que la séquence de commandes `\'e` a été saisie (explicitement, ou implicitement via la correspondance d'entrée mentionnée ci-dessus), le caractère qui code la position du glyphe « é » est utilisé.

Ainsi, nous nous en présence d'un mécanisme en deux temps :

- un caractère [diacritique](https://fr.wikipedia.org/wiki/Diacritique) dans le flux d'entrée {tttexlogo}`TeX` est traduit en commandes {tttexlogo}`TeX` qui génèrent quelque chose ressemblant au caractère d'entrée ;
- cette séquence de commandes {tttexlogo}`TeX` est ensuite à nouveau traduite en un seul glyphe diacritique au fur et à mesure de la création de la sortie.

Ceci correspond précisément à ce que font les extensions {latexlogo}`LaTeX` {ctanpkg}`inputenc` et {ctanpkg}`fontenc` qui fonctionnent souvent en tandem avec l'encodage d'entrée ISO Latin-1 et l'encodage de sortie T1. À première vue, il semble excentrique que la première extension fasse une chose tandis que la seconde l'annule. Toutefois, cela ne se passe pas toujours ainsi : la plupart des encodages de sortie (ou encodages de police) ne correspondent pas aussi bien à l'encodage d'entrée correspondant. D'où l'intérêt de ces extensions qui rétablissent les correctifs de correspondances dont {latexlogo}`LaTeX` a besoin.

______________________________________________________________________

*Source :* {faquk}`What are encodings? <FAQ-whatenc>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,encodage,codage,représentation des caractères,T1,OT1,fontes
```
