```{role} latexlogo
```
```{role} tttexlogo
```
# Comment détecter si la compilation va donner un PDF ?

La plupart des installations modernes utilisent le moteur `pdfTeX` pour toutes les compilations, y compris en mode `dvi`. Ce dernier fournit un moyen simple de déterminer au moment de la compilation si celle-ci a lieu en mode `pdf` ou `dvi`, afin par exemple de n'inclure que dans la version `pdf` certaines commandes spécifiques.

```latex
% !TEX noedit
\ifnum\pdfoutput>0
Texte pour pdf
\else
Texte pour dvi
\fi
```

Vous pouvez bien sûr omettre le `\else` et passer immédiatement au `\fi`. Ce test se rencontre parfois sous la forme `\ifnum\pdfoutput=1`, ce qui marche souvent en pratique, mais le manuel de `pdfTeX` dit seulement que les valeurs négatives indiquent le mode `dvi` sans préciser de valeur précise pour le mode `pdf`.

Certains commandes détectent seules le mode de compilation et en tirent les conclusion appropriées. C'est le cas par exemple de la commande `\includegraphics{⟨fichier⟩}` de l'extension {ctanpkg}`graphicx`. Si vous prenez la précaution d'écrire le nom de fichier sans extension, et que deux versions (`eps` et `pdf`) sont trouvées, {ctanpkg}`graphicx` déterminera automatiquement quel type de fichier il convient d'insérer.

______________________________________________________________________

*Source :* <https://web.archive.org/web/20170314080827/https://elzevir.fr/imj/latex/tips.html#pdfornot>

```{eval-rst}
.. meta::
   :keywords: LaTeX,TeX,compilation,DVI,PDF
```
