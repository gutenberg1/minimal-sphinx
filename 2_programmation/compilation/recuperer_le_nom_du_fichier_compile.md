```{role} latexlogo
```
```{role} tttexlogo
```
# Comment récupérer le nom du fichier compilé ?

- La commande `\jobname` retourne le nom (sans son extension) du fichier maître en cours de traitement.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
