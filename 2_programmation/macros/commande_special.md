```{role} latexlogo
```
```{role} tttexlogo
```
# À quoi sert la commande « `\special` » ?

Cette commande permet à {tttexlogo}`TeX` d'envoyer des instructions particulières (non {tttexlogo}`TeX`) à un pilote (*driver*) sans les interpréter (par exemple un {doc}`pilote DVI </5_fichiers/dvi/qu_est_qu_un_pilote_dvi>`). Les instructions ainsi passées sont généralement dépendantes du pilote qui, lui, saura les interpréter. L'utilisateur n'a en général pas à se soucier de cette commande (`\special`). Elle rend par ailleurs le document dépendant de la plate-forme de travail.

Un exemple d'extension utilisant intensivement ce mécanisme : {ctanpkg}`PSTricks <pstricks-base>`. Ses macros de dessin encapsulent du code PostScript dans des commandes `\special`, pour les faire parvenir au [logiciel de visualisation](/6_distributions/visualisateurs/visualisateurs_postscript) ([Ghostview](https://fr.wikipedia.org/wiki/Ghostview) ou autre) ou à l'imprimante.

```{eval-rst}
.. meta::
   :keywords: LaTeX,format DVI,pilote DVI,instructions spéciales
```
