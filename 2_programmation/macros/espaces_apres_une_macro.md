```{role} latexlogo
```
```{role} tttexlogo
```
# Comment mieux gérer l'espace après une commande ?

Une solution consiste à utiliser l'extension {ctanpkg}`xspace`. Son utilisation est décrite à la question : « {doc}`Comment gérer l'espace après une commande ? </3_composition/texte/mots/une_commande_avale_l_espace_qui_la_suit>` » développée dans la partie de cette FAQ traitant du texte.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
