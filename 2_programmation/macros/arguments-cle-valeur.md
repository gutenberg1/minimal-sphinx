```{role} latexlogo
```
```{role} tttexlogo
```
# Comment définir des arguments clé-valeur ?

## Avec l'extension « keyval »

L'extension {ctanpkg}`keyval` permet d'écrire des commandes assez sophistiquées, dont l'appel pourrait ressembler à :

```latex
% !TEX noedit
\instancefleur{espece=Primula veris,
  famille=Primulaceae,
  localisation=Coldham's Common,
  typeemplacement=Paturage,
  date=24/04/1995,
  nombre=50,
  typesol=alkaline
}
```
