```{role} latexlogo
```
```{role} tttexlogo
```
# Comment définir des arguments optionnels comme ceux de `\section` ?

On présentera quelques méthodes traditionnelles, avant de se tourner vers la commande `\NewDocumentCommand` désormais incluse dans le noyau de {latexlogo}`LaTeX`, qui fournit une syntaxe *ad hoc*.

## Avec `\newcommand`

Les arguments optionnels des macros définies avec `\newcommand` ne fonctionnent pas vraiment comme l'argument optionnel de `\section`. En effet, la valeur par défaut de l'argument optionnel de `\section` est celle de l'argument obligatoire donnée par l'utilisateur ; au contraire, `\newcommand` exige que vous déterminiez la valeur de l'argument par défaut au moment où vous définissez la commande.

L'astuce requise consiste à utiliser une macro dans l'argument optionnel :

```latex
% !TEX noedit
\documentclass{article}

\newcommand\thing[2][\DefaultOpt]{%
  \def\DefaultOpt{#2}%
  optional arg : #1,  mandatory arg : #2%
}

\begin{document}
\thing{manda}% #1=#2

\thing[opti]{manda}% #1="opti"
\end{document}
```

```latex
\documentclass{article}

\newcommand\thing[2][\DefaultOpt]{%
  \def\DefaultOpt{#2}%
  optional arg : #1,  mandatory arg : #2%
}

\begin{document}
\thispagestyle{empty}
\thing{manda}% #1=#2

\thing[opti]{manda}% #1="opti"
\end{document}
```

Le code source de \\LaTeX utilise une méthode plus subtile : il utilise une macro `\@dblarg`. Voici comment l'exemple précédent aurait été écrit dans LaTeX :

```latex
% !TEX noedit
\makeatletter
\newcommand\thing{\@dblarg\@thing}
\newcommand\@thing[2][\@error]{%
  optional arg : #1,  mandatory arg : #2%
}
\makeatother
```

Dans ce code, `\@thing` n'est jamais appelé qu'avec un argument optionnel et un argument obligatoire ; si la valeur par défaut de la `\newcommand` est appelée, un bug apparaît dans le code utilisateur...

typ o

## Avec `\NewDocumentCommand`

La macro `\NewDocumentCommand`, autrefois fournie par l'extension {ctanpkg}`xparse` et intégrée dans le cœur de {latexlogo}`LaTeX` depuis 2020, fournit une syntaxe qui permet de définir les commandes de manière flexible. On peut ainsi redéfinir la commande `\thing` ci-dessus :

```latex
% !TEX noedit
\NewDocumentCommand\thing{o m}{%
  argument optionnel entre crochets : #1,  argument obligatoire : #2%
}
```

`o` indique un argument optionnel et `m` un argument obligatoire (*mandatory*). Chaque argument peut être rendu long en préfixant le caractère d'un+''.

On peut indiquer une valeur par défaut de la manière suivante :

```latex
% !TEX noedit
\NewDocumentCommand\thing{O{valeur par défaut} m}{%
  argument optionnel entre crochets : #1,  argument obligatoire : #2%
}
```

Il est même possible d'indiquer plus d'un argument entre crochets, auquel cas l'utilisateur ne pourra pas définir le second sans avoir défini le premier (noter que `[]` produit un argument vide, pas l'argument par défaut) :

```latex
% !TEX noedit
\NewDocumentCommand\thing{
  O{valeur par défaut}
  O{un autre défaut}
  m
}{%
  premier argument optionnel entre crochets : \textbf{#1}\\
  second argument optionnel entre crochets : \textbf{#2}\\
  argument obligatoire : \textbf{#3}
}

\thing[le premier argument]{%
  j'ai défini le premier argument entre crochets mais
  pas le second}

\thing[le premier argument][le second argument]{%
  j'ai défini les deux arguments optionnels}

\thing[][le second argument]{le premier argument
  est vide}

\thing[valeur par défaut][le second argument]{%
  pour obtenir la valeur par défaut du premier
  argument et définir le second argument, j'ai
  dû indiquer explicitement la valeur par défaut}
```

```latex
\documentclass{article}
\thispagestyle{empty}
\setlength{\parskip}{10pt}
\setlength{\parindent}{0pt}
\def\thing[#1][#2]#3{%
  premier argument optionnel entre crochets : \textbf{#1}\\
  second argument optionnel entre crochets : \textbf{#2}\\
  argument obligatoire : \textbf{#3} }

\begin{document}

\thing[le premier argument][un autre défaut]{j'ai défini le premier argument entre crochets mais pas le second}

\thing[le premier argument][le second argument]{j'ai défini les deux arguments optionnels}

\thing[][le second argument]{le premier argument est vide}

\thing[valeur par défaut][le second argument]{pour obtenir la valeur par défaut du premier argument et définir le second argument, j'ai dû indiquer explicitement la valeur par défaut}

\end{document}
```

______________________________________________________________________

*Sources :*

- {faquk}`Optional arguments like « \\section. <FAQ-oarglikesect>`
- Joseph Wright, [\\NewDocumentCommand versus \\newcommand versus …](https://www.tug.org/TUGboat/tb42-1/tb130wright-newdoccmd.pdf), *TUGboat* 42-1, 2021.

```{eval-rst}
.. meta::
   :keywords: LaTeX,latex,macros,programmation,plusieurs arguments optionnels
```
