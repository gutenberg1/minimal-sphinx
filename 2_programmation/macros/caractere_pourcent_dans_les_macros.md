```{role} latexlogo
```
```{role} tttexlogo
```
# Pourquoi y a-t-il tant de « % » dans le code des macros ?

- De façon générale, **un saut de ligne est considéré comme une espace**, y compris dans la définition d'une commande. Regardez ici avant le point :

```latex
\documentclass{article}
\usepackage{lmodern}
\pagestyle{empty}

\begin{document}
\large
\newcommand{\gras}[1]{
  \textbf{#1}
}

Je mets du texte en \gras{gras}.
\end{document}
```

Pour éviter l'apparition de ces espaces en trop, on pourrait bien sûr écrire le code de la commande sur une seule ligne :

```latex
\documentclass{article}
\usepackage{lmodern}
\pagestyle{empty}

\begin{document}
\large
\newcommand{\gras}[1]{\textbf{#1}}

Je mets du texte en \gras{gras}.
\end{document}
```

mais ça deviendrait vite illisible pour une commande plus complexe. Pour pouvoir proprement découper et indenter le code, on préfère souvent mettre les sauts de ligne en commentaire, en les précédant par le caractère pourcent (`%`) :

```latex
\documentclass{article}
\usepackage{lmodern}
\pagestyle{empty}

\begin{document}
\large
\newcommand{\gras}[1]{%
  \textbf{#1}%
}

Je mets du texte en \gras{gras}.
\end{document}
```

Notez que les espaces en début de ligne sont ignorées dans tous les cas, et peuvent donc être librement ajoutées pour mettre en évidence la structure du code.

:::{note}
Ce caractère `%` peut être omis à la fin de certaines lignes, parce qu'elles se terminent par un nom de commande. Or toutes les espaces après un nom de commande sont ignorées.

Inversement, les espaces après certains mots-clefs {tttexlogo}`TeX`, les nombres et les unités (`pt`, `cm`...) sont importantes pour que le moteur puisse les distinguer du mot suivant. Dans ce cas, le signe `%` peut poser problème ([explication détaillée en anglais ici](https://tex.stackexchange.com/questions/34844/when-is-it-harmful-to-add-percent-character-at-end-of-lines-in-a-newcommand-or)).
:::

**Autre cas:** on a parfois besoin de terminer une ligne par une espace, notamment si la ligne se termine par « ''\\ '' » (antislash-espace). Dans ce cas, le caractère pourcent garantit que l'espace finale sera interprétée par {tttexlogo}`TeX`, et en même temps vue par les humains :

```latex
% !TEX noedit
\show\
\show\ %
```

______________________________________________________________________

*Source :*

- [What is the use of percent signs (%) at the end of lines ? (Why is my macro creating extra space?)](https://tex.stackexchange.com/questions/7453/what-is-the-use-of-percent-signs-at-the-end-of-lines-why-is-my-macro-creat)
- [When is it harmful to add percent character at end of lines in a « \\newcommand », or similar?](https://tex.stackexchange.com/questions/34844/when-is-it-harmful-to-add-percent-character-at-end-of-lines-in-a-newcommand-or)

```{eval-rst}
.. meta::
   :keywords: LaTeX,programmation,symbole pourcent,mise en commentaire,fins de lignes en commentaire
```
