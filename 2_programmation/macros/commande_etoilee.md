```{role} latexlogo
```
```{role} tttexlogo
```
# Comment définir une commande étoilée ?

```{eval-rst}
.. todo:: Fusionner avec :doc:`Comment définir une commande étoilée ? </2_programmation/macros/commande_etoilee3>`
```

La commande `\@ifstar` permet d'appeler une commande ou sa version étoilée en fonction du contexte. Elle figure au début de la définition de la commande, et est suivie immédiatement par deux noms de commande : celle implémentant la version étoilée, puis celle implémentant la version non étoilée. Voici un exemple illustrant cette utilisation :

```latex
% !TEX noedit
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\newcommand{\starmacro}[1]{J'ai une étoile et le paramètre #1.}
\newcommand{\nostarmacro}[1]{Je n'ai pas d'étoile et le paramètre #1.}
\makeatletter
\newcommand{\macro}{\@ifstar\starmacro\nostarmacro}
\makeatother

\begin{document}
\macro{42}
\macro*{7*9}
\end{document}
```

```latex
\documentclass[french]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\newcommand{\starmacro}[1]{J'ai une étoile et le paramètre #1.}
\newcommand{\nostarmacro}[1]{Je n'ai pas d'étoile et le paramètre #1.}
\makeatletter
\newcommand{\macro}{\@ifstar\starmacro\nostarmacro}
\makeatother

\pagestyle{empty}
\begin{document}
\macro{42}
\macro*{7*9}
\end{document}
```

:::{warning}
La plupart du temps, les commandes sont définies dans un fichier `sty`, mais si jamais vous avez besoin d'utiliser `\@ifstar` dans un fichier `tex`, comme pour le cas ci-dessus, il ne faut pas oublier les commandes `\makeatother` et `\makeatletter`. Voir la question « [À quoi servent \\makeatletter et \\makeatother ?](/2_programmation/macros/makeatletter_et_makeatother) » pour plus de précisions.
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX,commande,étoile,ifstar,makeatletter,makeatother
```
