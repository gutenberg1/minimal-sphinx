```{role} latexlogo
```
```{role} tttexlogo
```
# Que sont les environnements LaTeX ?

Les *environnements* ont été mis en place avec {latexlogo}`LaTeX` : ils effectuent une action sur la totalité d'un bloc délimité dans le document plutôt que d'agir en un point donné de votre document.

## Environnement sans paramètre

Un environnement simple pourrait changer la police utilisée pour une partie du texte. En voici un exemple de définition :

```latex
% !TEX noedit
\newenvironment{chassefixe}%
  {% Code exécuté au début de l'environnement
   \ttfamily}%
  {% Code exécuté en fin d'environnement
  }
```

L'environnement `chassefixe` ainsi défini peut ensuite être utilisé de la manière suivante :

```latex
\documentclass{article}
  \usepackage{lmodern}
  \pagestyle{empty}

\newenvironment{chassefixe}%
  {\ttfamily}%
  {}

\begin{document}
Voici un exemple%
\begin{chassefixe}
de texte à chasse fixe.
\end{chassefixe}
\end{document}
```

Certains environnements s'avèrent autrement plus complexes tel `document`. Simple d'usage, il n'en appelle pas moins des codes {tttexlogo}`TeX` peu évidents. De fait, la plupart des environnements sont plus élaborés que `chassefixe` et *beaucoup* plus simples que `document`.

Un environnement place son contenu dans un *groupe* {tttexlogo}`TeX`, de sorte que les commandes utilisées à l'intérieur de l'environnement « ne s'en échappent pas ». Ainsi, l'environnement `chassefixe`, ci-dessus, restreint son effet à son propre contenu (ce qui figure entre `\begin{chassefixe}` et `\end{chassefixe}`), ce qui correspond exactement à notre besoin.

:::{tip}
Certains environnements simples ne nous font pas gagner beaucoup de temps en matière de saisie. De fait, notre environnement `chassefixe` pourrait être remplacé par :

```latex
% !TEX noedit
Voici un exemple {\ttfamily de texte à chasse fixe.}
```

Cependant, ces environnements :

- peuvent rendre votre code plus lisible ;
- permettent de mieux gérer la mise en forme de votre document. Ainsi, modifier la définition de votre environnement modifiera la mise en forme de tous les environnements présents dans votre document.
:::

## Environnement avec paramètres

{latexlogo}`LaTeX` permet également d'utiliser des paramètres avec les environnements. Voici un exemple d'environnement demandant un paramètre :

```latex
% !TEX noedit
\newenvironment{blocfonte}[1]%
  {#1\selectfont}%
  {}
```

Cet environnement s'utilise alors ainsi (et produit ici le même résultat que l'environnement `chassefixe` vu plus haut) :

```latex
% !TEX noedit
Voici un exemple%
\begin{blocfonte}{\ttfamily}
de texte à chasse fixe.
\end{blocfonte}
```

### Les paramètres optionnels

Les environnements peuvent également avoir des paramètres optionnels, à l'image des commandes. Voici un exemple :

```latex
% !TEX noedit
\newenvironment{textespecial}[1][\itshape]%
  {#1}%
  {}
```

En temps normal, cet environnement compose son contenu en italique mais, dès qu'un paramètre lui est transmis, il modifie la mise en forme (ici, une nouvelle fois pour restituer la mise en forme obtenue avec `chassefixe`) :

```latex
% !TEX noedit
Voici un exemple%
\begin{textespecial}{\ttfamily}
de texte à chasse fixe.
\end{textespecial}
```

### La gestion des paramètres en fin d'environnement

Les paramètres d'environnement *ne sont pas transmis* au code en fin d'environnement. Dès lors, le code suivant pose problème :

```latex
% !TEX noedit
\newenvironment{textespecial}[1][extrait]%
  {Ici commence notre #1.}%
  {Ici finit notre #1.}
```

Il se produit ici l'erreur suivante (détaillée à la question « [Que signifie l'erreur : « Illegal parameter number in definition of ⟨commande⟩ » ?](/2_programmation/erreurs/i/illegal_parameter_number_in_definition_of_command) » :

```text
! Illegal parameter number in definition of \endtextespecial.
```

Aussi, si vous devez passer un paramètre d'environnement dans le code de fin d'environnement, vous devez le transmettre par le biais d'une commande intermédiaire contenant votre paramètre placée dans le code de début d'environnement :

```latex
% !TEX noedit
\newenvironment{textespecial}[1][extrait]%
  {Ici commence notre #1.%
   \newcommand{\transfert}{#1}}%
  {Ici finit notre \transfert.}
```

______________________________________________________________________

*Source :* {faquk}`What are LaTeX « environments » <FAQ-whatenv>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,programmation,environnements,environnement,environnements personnalisés
```
