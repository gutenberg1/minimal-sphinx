```{role} latexlogo
```
```{role} tttexlogo
```
# Comment comparer `\jobname` à quelque chose ?

La commande `\jobname`, {doc}`qui donne le nom de la tâche en cours </3_composition/document/utiliser_le_nom_de_fichier_dans_le_document>` (*job* en anglais) basé sur le nom du fichier, produit une séquence de caractères dont le {doc}`catcode </2_programmation/syntaxe/catcodes/que_sont_les_catcodes>` est 12 (« autre »), indépendamment de la nature réelle des caractères.

Comme une comparaison d'une commande ne peut se faire qu'avec le contenu d'une autre commande (utilisant `\ifx` quelque part), pour comparer le `\jobname` à quelque chose, il faut créer une commande dont le développement ressemble à celle de `\jobname`. Ceci peut s'obtenir avec `\meaning`, en retirant le préfixe ajouté par `\show`.

Voici le code complet de la commande :

```latex
% !TEX noedit
\def\StripPrefix#1>{}
\def\jobis#1{FF\fi
  \def\predicate{#1}%
  \edef\predicate{\expandafter\StripPrefix\meaning\predicate}%
  \edef\job{\jobname}%
  \ifx\job\predicate
}
```

qui s'utilise ainsi :

```latex
% !TEX noedit
\if\jobis{mainfile}%
  \message{YES}%
\else
  \message{NO}%
\fi
```

:::{note}
La commande `\StripPrefix` n'a pas besoin d'être définie si vous utilisez {latexlogo}`LaTeX` : il existe déjà une {doc}`commande interne </2_programmation/macros/makeatletter_et_makeatother>` `\strip@prefix` que vous pouvez utiliser.
:::

______________________________________________________________________

*Sources :*

- {faquk}`Comparing the "job name" <FAQ-compjobnam>`,
- [Compilation conditionnelle en LaTeX](https://lespetitesnotesdecamille.wordpress.com/2016/01/14/latex-compilation-conditionnelle/),
- [Conditional formatting based upon filename (\\jobname)](https://latex.org/forum/viewtopic.php?t=3155),
- [What is the meaning of "\\meaning"?](https://tex.stackexchange.com/questions/338302/what-is-the-meaning-of-meaning).

```{eval-rst}
.. meta::
   :keywords: LaTeX,programmation,nom de la tâche,job,compilation conditonnelle
```
