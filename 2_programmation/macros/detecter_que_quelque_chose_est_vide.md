```{role} latexlogo
```
```{role} tttexlogo
```
# Comment détecter un argument vide ?

## Argument réellement vide

Si vous avez besoin de savoir quand l'argument de votre commande est vide, autrement dit distinguer `\commande{}` de `\commande{truc}`, voici une solution relativement simple :

```latex
% !TEX noedit
\def\commande#1{%
  \def\tempa{}%
  \def\tempb{#1}%
  \ifx\tempa\tempb
    Cas où l'argument est vide
  \else
    Cas où l'argument est non vide
  \fi
}
```

## Argument uniquement composé d'espaces

Le cas où vous voulez ignorer un argument qui ne consiste en rien d'autre que des espaces est plus délicat. Il est résolu par l'extension {ctanpkg}`ifmtarg`, qui définit les commandes `\@ifmtarg` et `\@ifnotmtarg`, dont la fonction est d'examiner leur premier argument et, selon sa valeur, de sélectionner (dans des directions opposées) leur second ou troisième argument. La documentation de l'extension, en anglais, propose des exemples simples et visuels de ce fonctionnement. Par ailleurs, le code de cette extension est également utilisé par la classe {latexlogo}`LaTeX` {ctanpkg}`memoir`.

Le document {texdoc}`Around the bend <around-the-bend>`, de Michael Downes, développe ce sujet dans sa section 2.

______________________________________________________________________

*Sources :*

- {faquk}`Detecting that something is empty <FAQ-empty>`,
- [How to test if a string is empty](https://tex.stackexchange.com/questions/179964/how-to-test-if-a-string-is-empty).

```{eval-rst}
.. meta::
   :keywords: LaTeX,programmation,syntaxe,comparer à la chaîne vide
```
