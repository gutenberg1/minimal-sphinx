```{role} latexlogo
```
```{role} tttexlogo
```
# Comment écrire des nombres en toutes lettres ?

Malgré la moindre lisibilité, les documents administratifs demandent fréquemment que les nombres soient systématiquement écrits en toutes lettres : chèques, contrats, actes notariés...

Pour le français, il y a de nombreuses règles à respecter pour cette écriture, souvent peu intuitives, mais applicables de façon automatique. Il existe (au moins) deux extensions capables de vous aider :

## Avec l'extension « numspell »

```{eval-rst}
.. todo:: À rédiger.
```

## Avec l'extension « fmtcount »

L'extension {ctanpkg}`fmtcount`, de Nicola Talbot & Vincent Belaïche, faisait initialement partie de {ctanpkg}`datetime`, mais elle est maintenant distribuée indépendamment. Elle fournit des commandes pour afficher la valeur d'un compteur LaTeX dans divers formats (ordinal, texte, hexadécimal, décimal, octal, binaire, etc). Elle offre un certain support multilingue, avec la possibilité d'écire en anglais (britannique ou américain), français (y compris les variantes belge et suisse), allemand, italien, portugais et espagnol.

- Pour écrire un nombre en toutes lettres, utilisez la commande `\numberstringnum`, qui prend directement un nombre en argument.
- Pour écrire la valeur d'un compteur en toutes lettres, utilisez `\numberstring`, et le nom du compteur.
- Pour faire les mêmes choses sous forme ordinale (*premier*, *deuxième*...), vous avez `\ordinalstringnum` or `\ordinalstring`, respectivement.

```latex
\documentclass[french]{article}
  \usepackage[width=7cm]{geometry}
  \usepackage{fmtcount}
  \usepackage{babel}
  \pagestyle{empty}

\begin{document}
Lorsqu'il sera complet, ce document aura
\numberstringnum{42}~pages.
Vous êtes à la page \numberstring{page},
dans la \ordinalstring{section} section.
\end{document}
```

Pour avoir une majuscule en début de nombre, il suffit d'utiliser les variantes `\Numberstringnum`, `\Ordinalstringnum` (avec la première lettre en majuscule) et ainsi de suite :

```latex
\documentclass[french]{article}
  \usepackage[width=7cm]{geometry}
  \usepackage{fmtcount}
  \usepackage{babel}
  \pagestyle{empty}

\begin{document}
\Numberstringnum{1978} est une bonne année.
\end{document}
```

Pour avoir les formes féminines des nombres, on peut ajouter un argument optionnel indiquant le genre : `[f]`, `[m]` ou `[n]` pour, respectivement, féminin, masculin ou neutre.

```latex
\documentclass[french]{article}
  \usepackage[width=7cm]{geometry}
  \usepackage{fmtcount}
  \usepackage{babel}
  \pagestyle{empty}

\begin{document}
\Numberstringnum{1}[f] hirondelle ne fait pas le printemps,
mais la \ordinalstringnum{2}[f] est un bon indice.
\end{document}
```

### Comment écrire « septante » et « nonante » ?

Pour avoir les variantes suisses ou belges des nombres, il est possible d'utiliser des options de l'extension, passées avec la commande `\fmtcountsetoptions` :

```latex
\documentclass[french]{article}
  \usepackage[width=7cm]{geometry}
  \usepackage{fmtcount}
  \usepackage{babel}
  \pagestyle{empty}

  \fmtcountsetoptions{french=belgian}

\begin{document}
Baudouin a régné jusqu'en \numberstringnum{1993}.
\end{document}
```

```latex
\documentclass[french]{article}
  \usepackage[width=7cm]{geometry}
  \usepackage{fmtcount}
  \usepackage{babel}
  \pagestyle{empty}

  \fmtcountsetoptions{french=swiss}

\begin{document}
En Suisse, on mange \numberstringnum{88}~kilos
de chocolat pour 10~personnes, en moyenne.
\end{document}
```

Voici les termes utilisés dans les différentes variantes :

| Option    | 70           | 80            | 90               |
| --------- | ------------ | ------------- | ---------------- |
| `france`  | soixante-dix | quatre-vingts | quatre-vingt-dix |
| `belgian` | septante     | quatre-vingts | nonante          |
| `swiss`   | septante     | huitante      | nonante          |

:::{important}
Note de décembre 2021 : ces variantes locales ont encore quelques petits bugs qui sont en cours de correction par les auteurs de l'extension.
:::

______________________________________________________________________

*Sources :*

- [Chapter numbers as words in LaTeX](https://texblog.org/2017/04/11/chapter-numbers-as-words-in-latex/),
- [Soixante, septante, huitante, nonante… logique !](https://www.swissinfo.ch/fre/soixante--septante--huitante--nonante--logique-/25525018)

```{eval-rst}
.. meta::
   :keywords: LaTeX,nombres en lettres,chiffre en lettre,écrire chèque,écrire date en toutes lettres,documents administratifs
```
