```{role} latexlogo
```
```{role} tttexlogo
```
# Comment projeter ma présentation Beamer avec deux écrans ?

- En combinant des options de {ctanpkg}`Beamer` et un logiciel de visualisation, vous pouvez simuler le comportement de Microsoft Powerpoint en mode deux-écrans :
- projeter le contenu de vos diapositives sur grand écran pour votre audience,
- utiliser l'écran interne de votre ordinateur portable pour garder des notes sous les yeux, et éventuellement une horloge.

## Le mode présentateur de Beamer

Utiliser le package {ctanpkg}`pgfpages` (il n'y a pas d'erreur, il ne s'agit pas de `pdfpages`, qui existe aussi), et l'option de Beamer `show notes on second screen`.

```latex
% !TEX noedit
\documentclass{beamer}
  \usepackage{pgfpages}

\setbeameroption{show notes on second screen}

\begin{document}
\begin{frame}{Ma première diapositive}

Ici commence mon exposé.

\note{%
Ne pas oublier de dire bonjour, puis remercier :
  \begin{itemize}
     \item Le jury,
     \item Mes parents.
 \end{itemize}\par
}

\end{frame}
\end{document}
```

Le fichier PDF résultant aura des diapos de la largeur de deux écrans, et il vous faudra un visualisateur spécial (voir ci-dessous) :

```{image} /4_domaines_specialises/diaporama/presentation_beamer_avec_notes.png
:alt: image
:class: align-middle
```

## Le logiciel de présentation

Des visualisateurs ont été développés spécialement pour les présentations {ctanpkg}`Beamer`. Ils permettent d'afficher les dapositives sur un vidéoprojecteur, et des notes de présentation et une horloge sur un second écran :

- [Présentation.app](http://iihm.imag.fr/blanch/software/osx-presentation/) et [SlidePilot](https://slidepilotapp.com/) sous MacOS,
- [PDF presenter console](https://pdfpc.github.io/) sous Linux (des instructions sont [disponibles pour l'utiliser sous Windows](https://github.com/pdfpc/pdfpc)),
- [Pympress](https://pypi.org/project/pympress/) sous Linux (instructions disponibles pour MacOS et Windows sur la même page).

______________________________________________________________________

*Sources :*

- <https://tex.stackexchange.com/questions/21777/is-there-a-nice-solution-to-get-a-presenter-mode-for-latex-presentations>

```{eval-rst}
.. meta::
   :keywords: Diapositives avec LaTeX,présentation avec LaTeX, deux écrans,notes,projecteur,vidéoprojecteur
```
