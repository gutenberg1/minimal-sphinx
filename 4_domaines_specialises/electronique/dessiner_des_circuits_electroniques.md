```{role} latexlogo
```
```{role} tttexlogo
```
# Comment dessiner des circuits électroniques ?

Le choix se fera en fonction de l'outil de dessin que vous préférez (Metafont, PStricks ou TikZ) et des besoins de votre projet. Toutes les extensions ne sont pas maintenues au même niveau.

## Avec l'extension circ

L'extension {ctanpkg}`circ` permet de dessiner des circuits électroniques grâce à {ctanpkg}`Metafont <metafont>`.

## Avec l'extension MakeCirc

L'extension {ctanpkg}`MakeCirc` fait, elle, appel à {ctanpkg}`Metapost <metapost>`.

## Avec l'extension circuit-macro

L'extension {ctanpkg}`circuit-macros` est beaucoup plus évoluée et plus puissante. Par défaut, elle produit du code [pic](https://en.wikipedia.org/wiki/Pic_language), mais ses versions récentes peuvent également produire du code PStricks, TikZ ou SVG. Elle nécessite l'utilisation de [m4](<https://fr.wikipedia.org/wiki/M4_(langage)>), qui n'est disponible que sous les environnement de type Unix.

## Avec l'extension pst-circ

L'extension {ctanpkg}`pst-circ` permet de dessiner des circuits électroniques grâce à {ctanpkg}`PStricks <pstricks>`.

## Avec l'extension CircuiTikZ

L'extension {ctanpkg}`CircuiTikZ` permet de dessiner des circuits électroniques grâce à {ctanpkg}`TikZ`.

## Avec le programme xfig

Si vous préférez dessiner dans une interface graphique, le programme {ctanpkg}`xfig` possède [des librairies permettant de tracer des circuits électroniques.](http://www.oocities.org/eqys/simple/simple.html)

```{eval-rst}
.. meta::
   :keywords: LaTeX,schéma,composants électroniques,symboles,dessin technique
```
