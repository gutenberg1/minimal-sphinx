```{role} latexlogo
```
```{role} tttexlogo
```
# Comment mettre en page des exercices dont les solutions sont reportées dans un autre paragraphe ?

- L'objectif ici est de pouvoir saisir dans le fichier source les textes des exercices suivis de leurs solutions, alors qu'au niveau de la mise en page du document, les solutions apparaissent groupées dans un autre paragraphe/chapitre.

Le package {ctanpkg}`answers` permet de réaliser ce genre d'exercice. Il permet entre autres :

- d'avoir plusieurs types de solutions (par exemple : réponse numérique seule ou détail);
- d'inclure les solutions (l'une, l'autre ou les deux dans le cas précité) dans le texte (après une marque spécifique si désirée);
- mettre les solutions à la fin;
- de ne pas mettre les solutions.

Voici un exemple d'utilisation du package {ctanpkg}`answers` :

```latex
%%
%% This is file `ansexam2.tex',
%% generated with the docstrip utility.
%%
%% The original source files were :
%%
%% answers.dtx  (with options : `ex2')
%%
\documentclass[12pt,a4paper]{article}
\usepackage{answers}
%\usepackage[nosolutionfiles]{answers}
% def d'un environnement Exercise numerote
\newtheorem{Exc}{Exercise}
\newenvironment{Ex}{\begin{Exc}\normalfont}%
                   {\end{Exc}}
% Trois types de solutions sont proposes
\Newassociation{solution}{Soln}{test}
\Newassociation{hint}{Hint}{test}
\Newassociation{Solution}{sSol}{testtwo}
\newcommand{\prehint}{~[Hint]}
\newcommand{\presolution}{~[Solution]}
\newcommand{\preSolution}{~[Homework]}
% test
\newcommand{\Opentesthook}[2]%
   {\Writetofile{#1}%
    {\protect\section{#1 : #2}}}
% introduction de la solution
\renewcommand{\Solnlabel}[1]{\emph{Solution #1}}
\renewcommand{\Hintlabel}[1]{\emph{Hint #1}}
\renewcommand{\sSollabel}[1]{\emph{Solution to #1}}

\begin{document}
% gestion des fichiers contenant les solutions
   \Opensolutionfile{test}[ans2]{Solutions}
   \Writetofile{test}%
               {\protect\subsection
            {Some Solutions}}
   \Opensolutionfile{testtwo}[ans2x]
   \Writetofile{testtwo}{%
      \protect\subsection{Extra Solutions}}

   % Exercices
   \section{Exercises}
   \begin{Ex}
      An exercise with a solution.
      \begin{solution}
         This is a solution.
         \relax{}
      \end{solution}
   \end{Ex}
   \begin{Ex}
      An exercise with a hint and a secret
      solution.
      \begin{hint}
         This is a hint.
      \end{hint}
      \begin{Solution}
         This is a secret solution.
      \end{Solution}
   \end{Ex}
   \begin{Ex}
      An exercise with a hint.
      \begin{hint}
         This is a hint.
      \end{hint}
   \end{Ex}
   % gestion des fichiers contenant les solutions
   \Closesolutionfile{test}
   \Readsolutionfile{test}
   % \clearpage
   \hrule
   \Closesolutionfile{testtwo}
   \Readsolutionfile{testtwo}
\end{document}
%%
%% End of file `ansexam2.tex'.
```

- On peut également trouver la classe {ctanpkg}`exam`.

```{eval-rst}
.. todo:: Ajouter un exemple d'utilisation.
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,enseignant,professeur,enseignement,examen,contrôle,QCM,séparer questions et corrections,corrigé,solutions
```
