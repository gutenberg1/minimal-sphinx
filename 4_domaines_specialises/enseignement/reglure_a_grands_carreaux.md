```{role} latexlogo
```
```{role} tttexlogo
```
# Comment écrire sur des grands carreaux ?

## Dessiner la réglure

-

```{eval-rst}
.. todo:: Ce dernier paragraphe appelle une révision : « À rédiger. »
```

### Exemple avec « TikZ »

Avec la classe {ctanpkg}`sesamanuel` et {texdoc}`sa documentation <sesamath-doc-fr>`, page 68 (en français).

### Exemple avec « PStricks »

Tiré de [LaTeX pour le prof de maths](http://math.univ-lyon1.fr/irem/IMG/pdf/LatexPourLeProfDeMaths.pdf), page 105 :

```latex
% !TEX noedit
\newcommand\quadrillage[1]{%
  \psset{unit=.8cm}
  \begin{pspicture}(0,-1)(20,#1)
  \multirput(0,1){#1}{%
    \psline[linewidth=0.15pt](0,0.25)(20,0.25)
    \psline[linewidth=0.15pt](0,0.5)(20,0.5)
    \psline[linewidth=0.15pt](0,0.75)(20,0.75)}
    \psgrid[subgriddiv=1,gridlabels=0,%
            gridwidth=1pt,gridcolor=darkgray,%
            subgridwidth=0.1pt, subgridcolor=gray,%
            labels=none](20,#1)%
    \psline[linewidth=1.2pt](2,0)(2,#1)
  \end{pspicture}%
  \smallbreak%
}
```

## Écriture cursive sur les lignes

L'extension {ctanpkg}`frcursive` :

French Cursive is a cursive hand-writing font family. Its design is based on the French academic tradition for running-hand. The base shape is upright with lightly contrasted stems and hairlines. All lowercase letters are connected, but most uppercase are independent.

```latex
\documentclass{article}
  \usepackage{frcursive}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
\cursive\seyes{Mon tailleur est riche\dots}
\end{document}
```

______________________________________________________________________

*Sources :*

- [Jean-Alexandre Seyès](https://fr.wikipedia.org/wiki/Jean-Alexandre_Seyès), inventeur de la [réglure à grands carreaux](https://fr.wikipedia.org/wiki/Réglure),
- [Papier seyes en pdf à télécharger](https://pi.ac3j.fr/papier-seyes-en-pdf-telecharger/), réalisé avec PStricks par Fabrice Arnaud.

```{eval-rst}
.. meta::
   :keywords: LaTeX,enseignement de l'écriture,réglure Seyès,grands carreaux,petits carreaux
```
