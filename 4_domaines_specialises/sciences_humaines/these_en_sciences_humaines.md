```{role} latexlogo
```
```{role} tttexlogo
```
# Je commence une thèse en sciences humaines. Est-ce que LaTeX est fait pour moi ?

- Oui, bien sûr ! {latexlogo}`LaTeX` a été initalement développé dans la communauté mathématique/informatique, mais son domaine d'excellence est très étendu.

## Pourquoi choisir LaTeX ?

Quelques bonnes raisons de choisir cet outil :

- **Support de très gros documents structurés.** Les thèses de sciences humaines font souvent plusieurs centaines de pages, mais ça ne posera aucun problème à LaTeX. Et de votre côté, en tant que rédacteur, vous pourrez découper votre document en fichiers de taille humainement gérable, ce qui vous permettra de modifier facilement le plan de votre document aussi souvent que nécessaire.
- **Construction de la bibliographie.**
- **Gestion automatique des références internes.**

## Par où commencer ?

```{eval-rst}
.. todo:: Ajouter du texte de description de chaque référence.
```

<https://cipanglo.hypotheses.org/343>

<http://barthes.enssib.fr/cours/informatique-pour-litteraires/>

<https://www.atramenta.net/books/latex-sciences-humaines/79> (commentaire : <https://journals.openedition.org/lectures/9324> )

## Classes et packages spécialisés

- Plusieurs classes se veulent adaptées aux « sciences humaines » en général. Mais le domaine est vaste, et nous vous laissons faire votre choix. Cette liste n'est pas exhaustive :
- La [classe Bredele](https://www.overleaf.com/latex/templates/bredele/wyhhrmqjbdbv), de Christophe Masutti, non (encore) disponible sur le CTAN, mais [développée sur Framagit](https://framagit.org/Framatophe/Bredele).
- Le [fichier de style de l'INALCO](https://cipanglo.hypotheses.org/340), de Thomas Pellard, [disponible sur Bitbucket](https://bitbucket.org/tpellard/theseinalco/src/master/)

Pour des usages plus précis :

- {ctanpkg}`reledmac`
- {ctanpkg}`Technica`

Styles de bibliographie :

- {ctanpkg}`jurabib`
- {ctanpkg}`biblatex-enc`

______________________________________________________________________

*Sources :*

- [Blog de Maieul Rouquette](http://geekographie.maieul.net/),
- [Blog de Thomas Pellard](https://cipanglo.hypotheses.org/tag/latex),
- [LaTeX ninja'ing in the digital humanities](https://latex-ninja.com/), blog de Sarah Lang portant notamment sur l'enseignement de LaTeX. En anglais mais l'auteur peut répondre aux questions en français,
- [LaTeX appliqué aux Sciences humaines](https://www.laurentbloch.net/MySpip3/LaTeX-applique-aux-Sciences-humaines) sur le blog de Laurent Bloch.

```{eval-rst}
.. meta::
   :keywords: LaTeX,sciences humaines,humanités,thèse,rapport,mémoire,linguistique,théologie,histoire,sociologie
```
