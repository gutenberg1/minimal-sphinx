```{role} latexlogo
```
```{role} tttexlogo
```
# Comment ajouter un carré en fin de démonstration ?

Ce symbole est utilisé pour remplacer l'expression latine « *quod erat demonstrandum* » (QED), autrement dit notre [CQFD](<https://fr.wikipedia.org/wiki/CQFD_(mathématiques)>). Il est parfois appelé « halmos », du nom du mathématicien [Paul Halmos](https://fr.wikipedia.org/wiki/Paul_Halmos).

L'obtention de ce symbole n'est pas compliquée, il s'agit du symbole {latexlogo}`LaTeX` mathématique `\square`. La problématique tient plus à son positionnement sur la ligne du document où elle vient conclure la démonstration.

## Avec l'extension « amsthm »

L'extension {ctanpkg}`amsthm` fournit un environnement `proof` qui ajoute automatiquement ce symbole en fin de démonstration :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{amsthm}
  \usepackage[french]{babel}

\begin{document}
\begin{proof}
(...) Et, par récurrence :
\[ (X+Y)^{n} = \sum_{{k=0}}^{n} {n \choose k} X^{{n-k}} Y^{k} \]
Ce qui généralise l'\emph{identité polynomiale}.
\end{proof}
\end{document}
```

```latex
\documentclass{article}
  \usepackage{amsthm}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
\begin{proof}
(...) Et, par récurrence :
\[ (X+Y)^{n} = \sum_{{k=0}}^{n} {n \choose k} X^{{n-k}} Y^{k} \]
Ce qui généralise l'\emph{identité polynomiale}.
\end{proof}
\end{document}
```

Cependant, si la démonstration se termine par une formule en exergue, le symbole CQFD risque d'apparaître trop bas. Son placement pourra être corrigé en utilisant la commande 

`\qedhere`

 comme étiquette de formule, avec la commande 

`\tag`

 :

**Sans correction:**

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{amsthm,amsmath}
  \usepackage[french]{babel}

\begin{document}
\begin{proof}
  Texte...
  \begin{equation*}
    maths...
  \end{equation*}
\end{proof}
\end{document}
```

```latex
\documentclass{article}
  \usepackage[width=6cm]{geometry}
  \usepackage{amsthm,amsmath}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
\begin{proof}
  Texte...
  \begin{equation*}
    maths...
  \end{equation*}
\end{proof}
\end{document}
```

**Avec correction:**

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{amsthm,amsmath}
  \usepackage[french]{babel}

\begin{document}
\begin{proof}
  Texte...
  \begin{equation*}
    maths... \tag*{\qedhere}
  \end{equation*}
\end{proof}
\end{document}
```

```latex
\documentclass{article}
  \usepackage[width=6cm]{geometry}
  \usepackage{amsthm,amsmath}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
\begin{proof}
  Texte...
  \begin{equation*}
    maths... \tag*{\qedhere}
  \end{equation*}
\end{proof}
\end{document}
```

### Comment obtenir un carré noir ?

Il est très simple de changer le symbole de fin de démonstration, en redéfinissant la commande `\qedsymbol` :

```latex
\documentclass{article}
  \usepackage[width=6cm]{geometry}
  \usepackage{amsthm,amssymb}
  \usepackage[french]{babel}
  \pagestyle{empty}

\renewcommand{\qedsymbol}{$\blacksquare$}

\begin{document}
\begin{proof}
  Et nous y voilà.
\end{proof}
\end{document}
```

De la même façon, il est très simple de faire disparaître le carré, en redéfinissant `\qedsymbol` pour qu'elle ne fasse rien :

```latex
\documentclass{article}
  \usepackage[width=6cm]{geometry}
  \usepackage{amsthm}
  \usepackage[french]{babel}
  \pagestyle{empty}

\renewcommand{\qedsymbol}{}

\begin{document}
\begin{proof}
  Et nous y voilà.
\end{proof}
\end{document}
```

## Avec l'extension « ntheorem »

L'extension {ctanpkg}`ntheorem` vous permet de définir de nouveaux styles de théorèmes, de lemme, de démonstration, etc.

Avec l'option `standard`, elle vous fournit un ensemble d'environnements prédéfinis, dont `Proof` pour les démonstrations. En ajoutant l'option `thmmarks`, on fait apparaître des marques de fin de démonstration :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage[standard,thmmarks]{ntheorem}
  \usepackage[french]{babel}

\begin{document}
\begin{Proof}
(...) Et, par récurrence :
\[ (X+Y)^{n} = \sum_{{k=0}}^{n} {n \choose k} X^{{n-k}} Y^{k} \]
Ce qui généralise l'\emph{identité polynomiale}.
\end{Proof}
\end{document}
```

```latex
\documentclass{article}
  \usepackage[standard,thmmarks]{ntheorem}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
\begin{Proof}
(...) Et, par récurrence :
\[ (X+Y)^{n} = \sum_{{k=0}}^{n} {n \choose k} X^{{n-k}} Y^{k} \]
Ce qui généralise l'\emph{identité polynomiale}.
\end{Proof}
\end{document}
```

La {texdoc}`documentation de « ntheorem » <ntheorem>` reste un peu théorique. Vous trouverez [ici des exemples pédagogique et en français](https://zestedesavoir.com/tutoriels/1126/des-theoremes-personnalises-en-latex/).

## Avec l'extension « QED »

L'extension {ctanpkg}`QED`, de Paul Taylor, définit un environnement de preuve, `Proof`, et une commande `\qed`. Vous devrez charger manuellement l'extension {ctanpkg}`amssymb` pour disposer du symbole carré.

Vous aurez également à disposition ces variantes :

| Commande | Rendu        | Signification                                          |
| -------- | ------------ | ------------------------------------------------------ |
| `\qed`   | \$\\square\$ | *Quod erat demonstrandum* / Ce qu'il fallait démontrer |
| `\QED`   | Q.E.D.       | *Quod erat demonstrandum* / Ce qu'il fallait démontrer |
| `\QEI`   | Q.E.I.       | *Quod erat inveniendum* / Ce qu'il fallait trouver     |
| `\QEF`   | Q.E.F.       | *Quod erat faciendum* / Ce qu'il fallait faire         |

:::{important}
Ces commandes (`\qed`, `\QED`, etc) n'ont pas d'effet si elles ne suivent pas une commande qui démarre une preuve (`\begin{Proof}` ou `\Proof`).
:::

:::{warning}
 L'extension 

{ctanpkg}`QED`

 a été développée en 1993-1995. Avec elle, Paul Taylor a apporté une solution automatique à un problème délicat : en effet, certaines preuves se terminent par une équation en exergue, d'autres non. Si le fichier d'entrée contient 

`...\] \end{proof}`

, LaTeX termine de composer les maths, puis se prépare immédiatement pour une nouvelle ligne, avant même de lire le code de la fin de démonstration.

Cette extention a donc un intérêt historique, et son code est très intéressant. Mais les autres solutions proposées sont préférables si vous travaillez sur un document récent.
:::

## Avec des modifications manuelles

Si vos besoins sont simples, une commande `\qed` peut être définie manuellement :

```latex
% !TEX noedit
\def\myhfill{
  \parfillskip=0pt
  \widowpenalty=10000
  \displaywidowpenalty=10000
  \finalhyphendemerits=0
  \unskip\nobreak\null\hfil\penalty50
  \hskip2em\null\hfill
}

\def\qedsymbol{\ensuremath\square}
\def\qed{\myhfill\qedsymbol\par}
```

Le symbole de fin de démonstration sera placé à droite, sur la ligne s'il reste de la place, sur la ligne d'en-dessous dans le cas contraire.

______________________________________________________________________

*Sources :*

- [Signe de fin d'article](https://fr.wikipedia.org/wiki/Signe_de_fin_d'article),
- [QED symbol in latex](https://stackoverflow.com/questions/1910493/qed-symbol-in-latex),
- [Solid black box in the proof environment](https://tex.stackexchange.com/questions/98382/solid-black-box-in-the-proof-environment),
- {faquk}`Proof environment <FAQ-proof>`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,ce qu'il fallait démontrer,ὅπερ ἔδει δεῖξαι,carré blanc,carré noir,signe de fin de démonstration
```
