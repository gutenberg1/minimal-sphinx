```{role} latexlogo
```
```{role} tttexlogo
```
# Comment écrire les bra-kets de Dirac ou une spécification d'ensemble ?

**Ou « comment obtenir une barre verticale dans une expression mathématique ? »**

L'une des rares omissions flagrantes des capacités de composition mathématique de TeX est une façon de placer des séparateurs *au milieu* d'expressions mathématiques.

TeX fournit bien des primitives appelées `\left` et `\right`, qui peuvent être utilisées pour modifier les parenthèses (ou délimiteurs de toutes sortes) autour d'une expression mathématique, comme dans : `\left( ⟨expression⟩ \right)`, ce qui fera que la taille des parenthèses sera adaptée à la hauteur de leur contenu. Cependant, dans certaines expressions mathématiques, on peut avoir besoin d'une commande `\middle`, comme ici :

```latex
\documentclass[12pt]{article}
  \usepackage{lmodern}
  \usepackage{braket}
  \pagestyle{empty}

\begin{document}
\[\left\{ x \in \mathbb{N} \middle| x \mbox{ even} \right\}\]
\end{document}
```

pour désigner l'ensemble des entiers naturels pairs.

Le {doc}`système ε-TeX </1_generalites/glossaire/qu_est_ce_que_etex>` fournit une telle commande, mais les utilisateurs du TeX original de Knuth n'ont pas la même chance. L'extension {ctanpkg}`braket` de Donald Arseneau résout le problème, en fournissant des commandes pour les spécifications d'ensembles (comme ci-dessus) et pour les [parenthèses de Dirac (et les fameux « bras » et « kets »)](https://fr.wikipedia.org/wiki/Notation_bra-ket).

:::{note}
L'extension {ctanpkg}`braket` utilise la commande intégrée à ε-TeX s'il est chargé sous ε-TeX.
:::

______________________________________________________________________

*Source :* {faquk}`Set specifications and Dirac brackets <FAQ-braket>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,grands délimiteurs,délimiteur interne,crochets,parenthèses,notation Bayesienne
```
