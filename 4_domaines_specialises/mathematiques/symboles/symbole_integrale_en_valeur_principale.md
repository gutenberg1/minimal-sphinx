```{role} latexlogo
```
```{role} tttexlogo
```
# Comment obtenir le symbole d'intégrale de la valeur principale de Cauchy ?

Également nommé « intégrale impropre », ce symbole est un signe d'intégrale « barré ». Il n'apparaît dans aucune des polices habituellement disponibles pour les utilisateurs de {latexlogo}`LaTeX`, mais il peut être créé à l'aide des macros suivantes :

```latex
\documentclass{article}
  \usepackage{amsmath}
  \pagestyle{empty}

\def\Xint#1{\mathchoice
   {\XXint\displaystyle\textstyle{#1}}%
   {\XXint\textstyle\scriptstyle{#1}}%
   {\XXint\scriptstyle\scriptscriptstyle{#1}}%
   {\XXint\scriptscriptstyle\scriptscriptstyle{#1}}%
   \!\int}
\def\XXint#1#2#3{{\setbox0=\hbox{$#1{#2#3}{\int}$}
     \vcenter{\hbox{$#2#3$}}\kern-.5\wd0}}
\def\ddashint{\Xint=}
\def\dashint{\Xint-}

\begin{document}
\Large
\[ \dashint{f(x) \mathrm{d} x} \]

\[ \ddashint{f(y) \mathrm{d} y} \]

\end{document}
```

La commande `\dashint` donne un symbole d'intégrale barré d'un simple trait, et `\ddashint` donne un symbole barré d'un double trait.

:::{tip}
L'exemple ci-dessus charge l'extension {ctanpkg}`amsmath` pour autoriser [l'ajustement de la taille du symbole d'intégrale](/4_domaines_specialises/mathematiques/tailles_des_symboles_en_mode_mathematique) si besoin. Ce n'est pas obligatoire.
:::

______________________________________________________________________

*Source :*

- {faquk}`The Principal Value Integral symbol <FAQ-prinvalint>`,
- [Valeur principale de Cauchy](https://fr.wikipedia.org/wiki/Valeur_principale_de_Cauchy).

```{eval-rst}
.. meta::
   :keywords: LaTeX,symbole mathématique,intégrale barrée,intégrale de Cauchy,symbole intégrale impropre
```
