```{role} latexlogo
```
```{role} tttexlogo
```
# Comment utiliser des lettres grecques grasses en mode mathématique ?

La question est rendue non-triviale par le fait que `\mathbf` (la commande qui met du texte en gras en mode mathématique) affecte différemment les lettres grecques minuscules et majuscules : les lettres grecques minuscules sont dans les polices mathématiques, tandis que les lettres majuscules sont dans les polices de texte originales (encodées en OT1).

```latex
\Large
gras : $\mathbf{\theta\Theta}$

maigre : $\theta\Theta$
```

- La solution Plain TeX fonctionne, mais a ses inconvénients. Il s'agit de passer en style mathématique gras, avant de commencer l'équation :

```latex
\Large
gras : {\boldmath$\theta\Theta$}

maigre : {$\theta\Theta$}
```

Ça marche, mais `\boldmath` ne peut pas être utilisé en mode mathématique. Donc pour s'en servir au sein d'une formule, il faut en fait inclure une autre formule à l'intérieur, dans une boîte :

```latex
% !TEX noedit
$... \mbox{\boldmath$\theta$} ...$
```

ce qui cause d'autres problèmes, par exemple avec les indices et les exposants.

Si on charge le package {ctanpkg}`amsmath`, on peut écrire

```latex
% !TEX noedit
$... \text{\boldmath$\theta$} ...$
```

qui pose moins de problèmes avec les indices et exposants, mais reste peu satisfaisant.

- Tous ces problèmes peuvent être résolus avec des packages spécifiques :
- le package {ctanpkg}`bm` (*bold math*), qui définit une commande `\bm` utilisable partout en mode mathématique;
- le package {ctanpkg}`amsbsy` (qui fait partie de l'ensemble {ctanpkg}`AMS-math <latex-amsmath>`) définit une commande `\boldsymbol`, qui couvre quasiment tous les cas d'usage, même si elle est un peu moins complète que `\bm`.

Ces solutions s'appliquent à tous les symboles mathématiques, et pas seulement aux lettres grecques.

**Avec {ctanpkg}`bm`:**

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{bm}

\begin{document}
gras : $\bm{\theta + \Theta}$

maigre : $\theta + \Theta$
\end{document}
```

```latex
\documentclass[14pt]{extarticle}
  \usepackage{lmodern}
  \usepackage{bm}
  \pagestyle{empty}

\begin{document}
gras : $\bm{\theta + \Theta}$

maigre : $\theta + \Theta$
\end{document}
```

**Avec {ctanpkg}`amsbsy`:**

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{amsbsy}

\begin{document}
gras : $\boldsymbol{\theta + \Theta}$

maigre : $\theta + \Theta$
\end{document}
```

```latex
\documentclass[14pt]{extarticle}
  \usepackage{lmodern}
  \usepackage{amsbsy}
  \pagestyle{empty}

\begin{document}
gras : $\boldsymbol{\theta + \Theta}$

maigre : $\theta + \Theta$
\end{document}
```

______________________________________________________________________

*Source :* {faquk}`Setting bold Greek letters in LaTeX maths <FAQ-boldgreek>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,mise en forme des mathématiques,lettres grecques en gras,lettres grecques grasses,caractères grecs gras
```
