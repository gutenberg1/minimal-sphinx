```{role} latexlogo
```
```{role} tttexlogo
```
# Comment changer la taille des polices mathématiques ?

- Cet exemple donne une solution de T. Bouche et S.A. Zaimi :

```latex
\documentclass{article}
\def\mathtitre#1{
\font\tenrm=cmr10   scaled \magstep#1
\font\sevenrm=cmr7  scaled \magstep#1
\font\fiverm=cmr5   scaled \magstep#1
\font\teni=cmmi10   scaled \magstep#1
\font\seveni=cmmi7  scaled \magstep#1
\font\fivei=cmmi5   scaled \magstep#1
\font\tensy=cmsy10  scaled \magstep#1
\font\sevensy=cmsy7 scaled \magstep#1
\font\fivesy=cmsy5  scaled \magstep#1
\font\tenex=cmex10  scaled \magstep#1
\textfont0=\tenrm \scriptfont0=\sevenrm
  \scriptscriptfont0=\fiverm
\textfont1=\teni  \scriptfont1=\seveni
  \scriptscriptfont1=\fivei
\textfont2=\tensy \scriptfont2=\sevensy
  \scriptscriptfont2=\fivesy
\textfont3=\tenex \scriptfont3=\tenex
  \scriptscriptfont3=\tenex
}
\pagestyle{empty}
\begin{document}
  {\mathtitre0 \[A+B=C^{B^A}\]}
  {\mathtitre1 \[A+B=C^{B^A}\]}
  {\mathtitre2 \[A+B=C^{B^A}\]}
  {\mathtitre3 \[A+B=C^{B^A}\]}
  {\mathtitre4 \[A+B=C^{B^A}\]}
  {\mathtitre5 \[A+B=C^{B^A}\]}
\end{document}
```

:::{warning}
Il ne faut pas que le bloc en police `\mathtitreXX` se trouve à cheval sur deux pages... cela mettrait le numéro de page (et les en-têtes-pieds de page) dans ladite police...
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX,texte trop petit,mode mathématique,taille de police,agrandir les formules,agrandir les équations
```
