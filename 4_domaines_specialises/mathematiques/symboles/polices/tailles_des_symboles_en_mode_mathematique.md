```{role} latexlogo
```
```{role} tttexlogo
```
# Comment agrandir les symboles mathématiques ?

Par défaut, les « grands » symboles mathématiques restent à la même taille quelle que soit la taille de la police du texte environnant. Il y a une bonne raison à cela : les polices `cmex` ne sont pas vraiment conçues pour être mises à l'échelle, avec pour conséquence que, si les polices étaient mises à l'échelle, les algorithmes de placement des symboles mathématiques de TeX conduiraient à un résultat médiocre.

Cependant, ce choix, en plus de dérouter l'utilisateur, peut conduire à des documents à l'aspect un peu étrange. Si vous voulez que les polices soient mises à l'échelle, malgré l'avertissement ci-dessus, utilisez soit l'extension {ctanpkg}`exscale`, soit {ctanpkg}`amsmath`. Il suffit de les charger pour qu'elles opèrent.

Exemple en taille normale :

```latex
\documentclass{article}
  \pagestyle{empty}

\begin{document}
\[ \sum_{i=0}^{\infty} x^i \]
\end{document}
```

Exemple en grande taille (sans extension particulière) :

```latex
\documentclass{article}
  \pagestyle{empty}

\begin{document}
\Huge
\[ \sum_{i=0}^{\infty} x^i \]
\end{document}
```

Exemple en grande taille avec {ctanpkg}`exscale` :

```latex
\documentclass{article}
  \usepackage{exscale}
  \pagestyle{empty}

\begin{document}
\Huge
\[ \sum_{i=0}^{\infty} x^i \]
\end{document}
```

Exemple en grande taille avec {ctanpkg}`amsmath` :

```latex
\documentclass{article}
  \usepackage{amsmath}
  \pagestyle{empty}

\begin{document}
\Huge
\[ \sum_{i=0}^{\infty} x^i \]
\end{document}
```

______________________________________________________________________

*Source :*

- {faquk}`Maths symbols don't scale up <FAQ-exscale>`,
- [Should one always use exscale when using lmodern?](https://tex.stackexchange.com/questions/198219/should-one-always-use-exscale-when-using-lmodern)
- [Compatibility between amsmath and exscale](https://tex.stackexchange.com/questions/80088/compatibility-between-amsmath-and-exscale).

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,formules mathématiques,équations,taille des symboles
```
