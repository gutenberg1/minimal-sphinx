```{role} latexlogo
```
```{role} tttexlogo
```
# Comment ajuster la longueur d'une flèche par rapport à celle d'un texte ?

## Avec l'extension « mathtools »

### Flèche simple

La commande `\xrightarrow` de l'extension {ctanpkg}`mathtools` (qui appelle {ctanpkg}`amsmath`) permet de générer des flèches dont la longueur est fonction de celles du texte qui est placé au-dessus et au-dessous (et de la chaîne la plus longue lorsqu'il y a à la fois un texte au-dessus et un autre au-dessous). En voici un exemple :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\usepackage{mathtools}
\pagestyle{empty}
\begin{document}
\Large
$\xrightarrow[\text{au-dessous
  (plus long)}]{\text{au-dessus}}
\xrightarrow[\text{au-dessous}]{%
  \text{au-dessus (pas plus%
  court)}}$
\end{document}
```

Comme vous pouvez l'imaginer, il y a une commande `\xleftarrow` équivalente et même une commande `\xleftrightarrow` (cette dernière n'existant pas dans {ctanpkg}`amsmath`) :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\usepackage{mathtools}
\pagestyle{empty}
\begin{document}
\Large
$\xleftarrow[\text{au-dessous}]{%
  \text{au-dessus}}
\xleftrightarrow[\text{%
  au-dessous}]{\text{au-dessus}}$
\end{document}
```

### Flèche double

De la même façon qu'il existe `\rightarrow` pour la flèche simple (\$\\rightarrow\$) et `\Rightarrow`, avec une majuscule, pour la flèche double (\$\\Rightarrow\$), vous pouvez imaginer appeler `\xRightarrow` pour avoir une flèche double longue. Ceci n'est cependant vrai que pour l'extension {ctanpkg}`mathtools` et pas {ctanpkg}`amsmath` :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\usepackage{mathtools}
\pagestyle{empty}
\begin{document}
\Large
$\xLeftarrow[\text{au-dessous}]{%
  \text{au-dessus}}
\xRightarrow[\text{au-dessous}]{%
  \text{au-dessus}}
\xLeftrightarrow[\text{au-dessous}]{%
  \text{au-dessus}}$
\end{document}
```

### Flèche à deux pointes

Pour une flèche à deux pointes, vous devrez écrire vos propres commandes :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\usepackage{mathtools}
\newcommand\dhrightarrow{%
  \mathrel{\ooalign{$\rightarrow$%
    \cr$\mkern3.5mu\rightarrow$}}
}
\newcommand\dhxrightarrow[2][]{%
  \mathrel{\ooalign{$\xrightarrow[%
    #1\mkern4mu]{#2\mkern4mu}$\cr%
    \hidewidth$\rightarrow%
    \mkern4mu$}}
}
\pagestyle{empty}
\begin{document}
\begin{alignat*}{3}
  A \rightarrow   B  &&\qquad
  A \xrightarrow[C]{D} B    &&\qquad
  A \xrightarrow{\text{Long texte%
  \dots}} B
  & \\
  A \dhrightarrow B  &&\qquad
  A \dhxrightarrow[C]{D} B  &&\qquad
  A \dhxrightarrow{\text{Long texte%
  \dots}} B
  & \\
\end{alignat*}
\end{document}
```

______________________________________________________________________

*Source :* [Two-headed version of \\xrightarrow](https://tex.stackexchange.com/questions/260554/two-headed-version-of-xrightarrow).

```{eval-rst}
.. meta::
   :keywords: LaTeX,mode mathématique,flèches réactionnelles,texte sur une flèche,texte sous une flèche,flèche double longue
```
