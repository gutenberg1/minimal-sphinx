```{role} latexlogo
```
```{role} tttexlogo
```
# Comment composer un tableau en mode mathématique ?

En mode mathématique, un tableau se compose dans un environnement `array`, comme dans cet exemple :

```latex
\documentclass{article}
  \usepackage[width=6cm]{geometry}
  \usepackage{lmodern}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
\large Résultats :
\[
\begin{array}{|c @=c|}
   \hline
   \alpha + \beta & 12 \\
   \hline
   \sqrt{\gamma} & 36 \\
   \hline
\end{array}
\]
\end{document}
```

## Quelle est la différence entre « array » et « tabular » ?

Dans un environnement `array`, le contenu des cellules est composé en mode mathématique (comme s'il y avait des `$...$` autour de chaque cellule), alors que dans un environnement `tabular`, le contenu des cellules est composé en mode texte.

Il est parfaitement possible d'insérer un environnement `tabular` en mode mathématique, mais ses cellules resteront composées en mode texte. Par contre, il faut être en mode mathématique pour insérer un `array`.

Les environnements `array` et `tabular` partagent beaucoup de choses, mais utilisent aussi chacun des paramètres spécifiques. Par exemple l'espace séparant deux colonnes est défini de façon distincte, par `\arraycolsep` pour `array` et par `\tabcolsep` pour `tabular`.

:::{tip}
Notez que même dans un `array`, les {doc}`spécificateurs de colonnes </3_composition/tableaux/construire_un_tableau>` `p`, `m` et `b` basculent le contenu des cellules en mode texte.
:::

:::{warning}
Il existe une extension {ctanpkg}`array`, très utile pour mettre en forme les tableaux. Malgré son nom, elle modifie le comportement à la fois des environnements `array` et `tabular`. Voir par exemple « {doc}`Comment améliorer l'espacement entre les lignes d'un tableau ? </3_composition/tableaux/lignes/augmenter_la_largeur_des_lignes_d_un_tableau>` ».
:::

______________________________________________________________________

*Source :* [Difference between tabular and array environment](https://tex.stackexchange.com/questions/204838/difference-between-tabular-and-array-environment).

```{eval-rst}
.. meta::
   :keywords: LaTeX,package array,matrice vs tableau
```
