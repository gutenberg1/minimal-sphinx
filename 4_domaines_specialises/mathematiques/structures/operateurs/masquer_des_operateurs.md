```{role} latexlogo
```
```{role} tttexlogo
```
# Comment masquer un opérateur dans une expression mathématique ?

La commande `\phantom` permet de masquer du texte, tout en disposant le reste du document comme si le texte masqué occupait normalement sa place (les traits rouges aident à visualiser les alignements) :

```latex
\documentclass[12pt]{article}
  \usepackage{tikz}
  \pagestyle{empty}
  \setlength{\parindent}{0pt}

\begin{document}
Le chat\tikz[overlay]\draw[red] (0pt,1.5ex) -- ++(0ex,-5ex); du Cheshire \tikz[overlay]\draw[red] (0pt,1.5ex) -- ++(0ex,-5ex);est dans l'arbre.

Le chat \phantom{du Cheshire} est dans l'arbre.
\end{document}
```

Mais si vous essayez avec des opérateurs mathématiques, vous obtiendrez un résultat décevant :

```latex
\documentclass[12pt]{article}
  \pagestyle{empty}
  \setlength{\parindent}{0pt}

\begin{document}
  $aa = bb$

  $aa \phantom{=} bb$

\medskip
ou
\medskip

  $x = a + b$

  $x = a \phantom{=} b$
\end{document}
```

En effet, à l'intérieur de `\phantom`, les opérateurs `=` et+'' n'ont pas leurs propriétés de « relation » ou d'« opérateur binaire » (respectivement).

- On peut leur restaurer de deux façons :
- soit en utilisant `\mathrel` `\mathbin` pour forcer la classe des opérateurs :

```latex
\documentclass[12pt]{article}
  \usepackage{tikz}
  \pagestyle{empty}
  \setlength{\parindent}{0pt}

\begin{document}
$aa = bb$

$aa\tikz[overlay]\draw[red] (0pt,4.5ex) -- ++(0ex,-5ex); \mathrel{\phantom{=}} \tikz[overlay]\draw[red] (0pt,4.5ex) -- ++(0ex,-5ex);bb$

\bigskip

$x = a + b$

$x = a\tikz[overlay]\draw[red] (0pt,4.5ex) -- ++(0ex,-5ex); \mathbin{\phantom{+}} \tikz[overlay]\draw[red] (0pt,4.5ex) -- ++(0ex,-5ex);b$

\end{document}
```

- soit forçant manuellement un contexte de relation ou d'opérateur binaire avec des termes vides, `{}`, pour que LaTeX identifie correctement la classe des symboles en question :

```latex
\documentclass[12pt]{article}
  \usepackage{tikz}
  \pagestyle{empty}
  \setlength{\parindent}{0pt}

\begin{document}
$aa = bb$

$aa\tikz[overlay]\draw[red] (0pt,4.5ex) -- ++(0ex,-5ex); \phantom{{}={}} \tikz[overlay]\draw[red] (0pt,4.5ex) -- ++(0ex,-5ex);bb$

\bigskip

$x = a + b$

$x = a\tikz[overlay]\draw[red] (0pt,4.5ex) -- ++(0ex,-5ex); \phantom{{}+{}} \tikz[overlay]\draw[red] (0pt,4.5ex) -- ++(0ex,-5ex);b$
\end{document}
```

## Comment masquer des parenthèses dans une expression mathématique ?

Comme précédemment, si vous essayez d'utiliser `\phantom` dans un tel cas, vous obtiendrez un alignement incorrect :

```latex
\documentclass[12pt]{article}
  \usepackage{tikz}
  \pagestyle{empty}
  \setlength{\parindent}{0pt}

\begin{document}
$ N = (-9) + (+5) $

$ N = \phantom{(}-9\phantom{)} + \phantom{(+}5\phantom{)} $
\end{document}
```

Dans ce cas, vous pouvez avoir l'impression que le problème vient des parenthèses, mais il vient surtout des        $+$ et        $-$ qui, suivant le contexte, peuvent être des opérateurs binaires (symboles de l'addition et de la soustraction), ou des opérateurs unaires (signes de nombres). Suivant le cas, les espacements autour d'eux ne sont pas les mêmes. LaTeX se débrouille en général bien pour identifier le contexte, mais la macro `\phantom` l'en empêche ici.

- Vous avez de nouveau deux solutions :
- utiliser la macro `\mathord` pour forcer manuellement la classe des opérateurs unaires (si besoin, il existe aussi `\mathopen` et `\mathclose` pour forcer la classe des délimiteurs ouvrants et fermants. Il se trouve que les parenthèses de cet exemple n'en ont pas besoin ici),
- utiliser des accolades `{}` pour aider LaTeX à deviner les bons contextes (sûrement la solution la plus simple).

```latex
\documentclass[12pt]{article}
  \usepackage{tikz}
  \pagestyle{empty}
  \setlength{\parindent}{0pt}

\begin{document}
$ N = (-\tikz[overlay]\draw[red] (0pt,2ex) -- ++(0ex,-8.5ex);9) + (+\tikz[overlay]\draw[red] (0pt,2ex) -- ++(0ex,-8.5ex);5) $

$ N = \phantom{(}\mathord{-}9\phantom{)} + \phantom{(}\mathord{\phantom{+}}5\phantom{)} $

$ N = \phantom{(}{-}9\phantom{)} + \phantom{({+}}5\phantom{)} $
\end{document}
```

______________________________________________________________________

*Source :*

- [\[gut\] « \\phantom{=} » en mode mathématique](https://groups.google.com/g/gut_fr/c/jyZGu8OECMQ/m/HLodbWoQAgAJ),
- [Phantom width of binary operator](https://tex.stackexchange.com/questions/28075/phantom-width-of-binary-operator),
- [How to add phantom space in math mode without losing « natural » spacing?](https://tex.stackexchange.com/questions/330845/how-to-add-phantom-space-in-math-mode-without-losing-natural-spacing)

```{eval-rst}
.. meta::
   :keywords: LaTeX,faire disparaître un opérateur,simplifier une équation,\\phantom et espacement,\\phantom et alignement,classes de symboles mathématiques
```
