```{role} latexlogo
```
```{role} tttexlogo
```
# Comment obtenir des opérateurs mathématiques textuels comme « log » ?

Il existe un certain nombre de fonctions définies par défaut :

| Opérateur    | Commandes | Opérateur    | Commandes |
| ------------ | --------- | ------------ | --------- |
| \$\\arccos\$ | `\arccos` | \$\\cos\$    | `\cos`    |
| \$\\arcsin\$ | `\arcsin` | \$\\sin\$    | `\sin`    |
| \$\\arctan\$ | `\arctan` | \$\\tan\$    | `\tan`    |
| \$\\inf\$    | `\inf`    | \$\\sup\$    | `\sup`    |
| \$\\min\$    | `\min`    | \$\\max\$    | `\max`    |
| \$\\liminf\$ | `\liminf` | \$\\limsup\$ | `\limsup` |
| \$\\cosh\$   | `\cosh`   | \$\\sinh\$   | `\sinh`   |
| \$\\cot\$    | `\cot`    | \$\\coth\$   | `\coth`   |
| \$\\exp\$    | `\exph`   | \$\\log\$    | `\log`    |
| \$\\ln\$     | `\ln`     | \$\\lg\$     | `\lg`     |
| \$\\csc\$    | `\csc`    | \$\\ker\$    | `\ker`    |
| \$\\deg\$    | `\deg`    | \$\\gcd\$    | `\gcd`    |
| \$\\Pr\$     | `\Pr`     | \$\\det\$    | `\det`    |
| \$\\hom\$    | `\hom`    | \$\\lim\$    | `\lim`    |
| \$\\sec\$    | `\sec`    | \$\\arg\$    | `\arg`    |
| \$\\dim\$    | `\dim`    | \$\\tanh\$   | `\tanh`   |

S'il vous manque des fonctions, vous pouvez en définir vous-même avec la méthode indiquée à la question « [Comment déclarer de nouveaux opérateurs mathématiques ?](/4_domaines_specialises/mathematiques/structures/operateurs/definir_un_nouvel_operateur) ».

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,structures mathématiques,opérateurs mathématiques,fonctions
```
