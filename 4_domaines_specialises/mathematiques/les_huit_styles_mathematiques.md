```{role} latexlogo
```
```{role} tttexlogo
```
# Quels sont les huit styles mathématiques ?

TeX, et donc LateX, possède fondamentalement quatre styles mathématiques, nommés :

- `\displaystyle`,
- `\textstyle`,
- `\scriptstyle`
- et `\scriptscriptstyle`,

chacun décliné en deux versions : normale et tassée. Il ne faut pas confondre les styles avec les modes (en-ligne ou hors-texte) même si ceux-ci sont reliés : le style par défaut du mode hors-texte est `\displaystyle`, celui du mode en-ligne étant `\textstyle`.

Ces styles déterminent de nombreux éléments de composition mathématiques, comme la taille des symboles, leur espacement, et la place des indices et exposants. On peut à tout moment changer de style par les commandes ci-dessus, qui agissent jusqu'à la fin du groupe ou jusqu'au prochain changement de style.

On contrôle indépendamment le placement des « limites » avec `\limits` et `\nolimits`. Comparons ainsi

- `$\sum_a^b$`,
- `\[\sum_a^b\]`,
- `$\displaystyle \sum_a^b$`,
- `$\displaystyle \sum\nolimits_a^b$`
- et `$\sum\limits_a^b$` :

```latex
\documentclass{article}
  \usepackage{graphicx}
  \pagestyle{empty}

\begin{document}
\resizebox{6cm}{!}{
    $\sum_a^b$
    \begin{minipage}[b]{2.5ex}\[\sum_a^b\]\end{minipage}
    $\displaystyle \sum_a^b$
    $\displaystyle \sum\nolimits_a^b$
    $\sum\limits_a^b$
}
\end{document}
```

Pour accéder manuellement à la variante tassée, on utilise la commande `\cramped` de {ctanpkg}`mathtools`, qui compose son argument en style tassé : cela change principalement la hauteur des indices et exposants. Il peut être utile de les compresser, par exemple pour préserver l'interlignage dans un paragraphe. L'effet est assez subtil, mais clair en comparant `$2^{2^2}$` à `$\cramped{2^{2^2}}$` :

```latex
\documentclass{article}
  \usepackage{graphicx}
  \pagestyle{empty}

\begin{document}
\resizebox{3cm}{!}{
    $2^{2^2}$
    $\cramped{2^{2^2}}$
}
\end{document}
```

## Comment écrire ses propres commandes mathématiques ?

Lors de la définition d'une commande mathématique, vous pouvez prévoir des variantes selon de style mathématique courant avec la commande `\mathchoice`, qui prend quatre arguments correspondants aux quatre styles mathématiques. Exemple :

```latex
% !TEX noedit
\newcommand\undemi{\mathchoice
{\frac{1}{2}}
{1/2}{1/2}{1/2}
}
```

En style `\displaystyle` (par exemple une équation hors-texte) `\undemi` utilisera `\frac{1}{2}`, et `1/2` dans tous les autres cas (par exemple une équation en-ligne ou un exposant en mode hors-texte).

______________________________________________________________________

*Archived copy:* <https://web.archive.org/web/20170314080827/https://elzevir.fr/imj/latex/tips.html#mathstyles>

```{eval-rst}
.. meta::
   :keywords: LaTeX,mode mathématique hors texte,équation hors texte,mathématiques dans un paragraphe,mathématiques et lignes,limites d'intégrales,grands opérateurs
```
