```{role} latexlogo
```
```{role} tttexlogo
```
# Comment gérer les espaces en mode mathématique ?

Le tableau suivant donne les différentes commandes d'espacement mathématique :

| Commande | Effet d'espacement               | Commentaire                                         |
| -------- | -------------------------------- | --------------------------------------------------- |
| `\!`     | $\blacksquare\!\blacksquare$     | Réduction de l'espace                               |
| *rien*   | $\blacksquare\blacksquare$       | Affichage en l'absence de commande                  |
| `\,`     | $\blacksquare\,\blacksquare$     | Espace fine                                         |
| `\;`     | $\blacksquare\;\blacksquare$     | Espace moyenne                                      |
| ''\\ ''  | $\blacksquare\ \blacksquare$     | Espace normale (la commande est suivie d'un espace) |
| `\quad`  | $\blacksquare\quad\blacksquare$  | Espace cadratin                                     |
| `\qquad` | $\blacksquare\qquad\blacksquare$ | Espace double cadratin                              |

Voici un exemple d'utilisation de ces commandes :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{mathtools}
%\usepackage{amssymb}
\pagestyle{empty}
\begin{document}
\[
\forall \lambda \in \mathbb{R}
\quad
\int _{a}^{b}\lambda \, f(x)
\, \mathrm{d} x
=
\lambda \, \int _{a}^{b}f(x)
\, \mathrm{d} x
\]
\end{document}
```
