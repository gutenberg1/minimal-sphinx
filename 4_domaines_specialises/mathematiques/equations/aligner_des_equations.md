```{role} latexlogo
```
```{role} tttexlogo
```
# Comment aligner des équations ?

## Avec l'extension « mathtools »

Pour aligner des équations sur un signe de relation, il suffit sous {latexlogo}`LaTeX` d'appeler l'environnement `eqnarray` avec ou sans la commande `lefteqn`. Cependant, cette méthode n'est {doc}`pas recommandée </4_domaines_specialises/mathematiques/equations/arguments_contre_eqnarray>` et il vaut mieux utiliser l'extension {ctanpkg}`mathtools` (ou {ctanpkg}`amsmath`)

### Les environnements classiques

Les environnements couramment utilisés sont `split`, `multline`, `align` ou `flalign`.

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{mathtools}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
Voici quelques exemples :
\begin{align}
   f(x) & = x^2 + 8x + 16 \\
   & = (x+4)^2
\end{align}

\begin{eqnarray}
   x + 3y - 6z & = & -4a + 5b -7b \\
               & = & -4a - 2b
\end{eqnarray}

\begin{align}
   \begin{split}
      A &= B + C + D + E + F + G \\
      &\quad + H + I
   \end{split} \\
   F &= G + H
\end{align}
\end{document}
```

### L'environnement « alignat »

Pour produire des systèmes d'équations, on peut utiliser l'environnement `alignat`. Toutefois, il faut noter que cet environnement est alors détourné de son utilisation normale. En effet, ce dernier est prévu pour aligner des objets différents sur des colonnes alternativement justifiées à droite puis à gauche. L'utilisation de colonnes vides permet alors de choisir la justification voulue. De plus, l'utilisation de `{}` permet d'obtenir des espacements « normaux ».

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{mathtools}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
Voici quelques exemples :
\begin{alignat}{2}
   x   & = y   &&+ z   \\
   x_1 & = y_1 &&+ z_1
\end{alignat}

\begin{alignat}{5}
    10a& ={}&  3x&& 3y& +{}& 18z&&  2w&  \\
     6a& ={}& 17x&&   & +{}&  5z&& 19w&
\end{alignat}
\end{document}
```

## Avec l'extension « eqnarray »

L'environnement `equationarray` de l'extension {ctanpkg}`eqnarray` associe les avantages des environnements `eqnarray` et `array`. Il n'est pas limité en nombre de colonnes.

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{eqnarray}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
Voici quelques exemples :
\begin{equationarray}{ccccc}
  10a & = & 3x  3y & + & 18z  2w \\
   6a & = & 17x    & + &  5z 19w
\end{equationarray}
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
