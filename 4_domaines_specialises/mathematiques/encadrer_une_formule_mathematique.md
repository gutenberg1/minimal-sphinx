```{role} latexlogo
```
```{role} tttexlogo
```
# Comment encadrer des formules mathématiques ?

## Avec l'extension « mathtools » (ou « amsmath »)

L'extension {ctanpkg}`mathtools` (ou {ctanpkg}`amsmath`) offre la commande `\boxed`, présentée dans l'exemple ci-dessous :

```latex
\[\boxed{a=b}\]
```

:::{note}
Il peut être utile d'encapsuler les environnements multilignes de {ctanpkg}`mathtools` (ou (comme {ctanpkg}`amsmath`), tels `multline` ou `split`, dans un `math` ou dans un `displaymath`.
:::

Il existe également la commande `\fbox`, comme ici :

```latex
\begin{equation}
   \fbox{$
   \begin{array}{rcl}
      x + y + z & = & 0 \\
      2x + 2y + 2z & = & 0
   \end{array}
   $}
\end{equation}
```

## Avec l'extension « fancybox »

L'extension {ctanpkg}`fancybox` peut également être utile :

```latex
\documentclass{article}
  \usepackage{fancybox}
  \pagestyle{empty}

\begin{document}
\linethickness{3pt}
\Ovalbox{
\begin{Beqnarray}
ds^2&\,=\,&
\displaystyle{\frac{\epsilon^{\prime 2}}{L^2}
\frac{12N}{(N+1)(N+2)}}
\end{Beqnarray}
}
\end{document}
```

## Avec une méthode moins simple

Une dernière solution consiste à mettre la formule dans un tableau d'une seule cellule.

```{eval-rst}
.. meta::
   :keywords: LaTeX,cadre autour des formules,encadrer une formule,mettre une formule en valeur
```
