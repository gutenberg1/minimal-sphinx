```{role} latexlogo
```
```{role} tttexlogo
```
# Comment faire son CV avec LaTeX ?

:::{tip}
Un CV doit refléter votre personnalité. C'est un document sur lequel vous allez passer du temps, pour que le fond et la forme attirent un recruteur et lui donnent envie de vous rencontrer.

Dans certains domaines, un CV fait avec LaTeX sera très bien vu, notamment les domaines scientifiques et l'ingénierie. Mais ne vous reposez pas entièrement sur LaTeX, ses extensions ou des modèles que vous aurez trouvés sur internet pour composer votre CV. **Personnalisez-le !** Évitez que votre CV ressemble à celui de tous les autres candidats qui ont utilisé la même classe !

Enfin, il est parfaitement possible de composer votre CV sans classe particulière, mais là encore ne vous contentez pas des macros de base. Profitez de la souplesse de LaTeX pour montrer votre personnalité et votre motivation !
:::

## Solutions modernes et élégantes

Deux classes se détachent du lot en 2020 pour composer des CV à l'allure propre, moderne et dynamique : {ctanpkg}`moderncv` et {ctanpkg}`Europass CV <europasscv>`.

### « ModernCV », pour une mise en forme dynamique

- Xavier Danaux a écrit une classe {ctanpkg}`moderncv` qui permet de composer des CV modernes, de style soit classique soit plus décontracté. Elle est assez personnalisable, et vous permet de définir votre propre style en changeant les couleurs, les polices, etc. Un exemple d'utilisation de la classe `moderncv` : <https://blog.madrzejewski.com/creer-cv-elegant-latex-moderncv/>. En complément, l'extension {ctanpkg}`moderntimeline` permet d'ajouter une ligne de temps à un CV conçu avec l'extension {ctanpkg}`moderncv`.
- Des exemples de CV (avec leurs fichiers sources) sont disponibles sur :
- [Dépôt GitHub de modernCV](https://github.com/xdanaux/moderncv/tree/master/examples),
- [Overleaf](https://www.overleaf.com/latex/templates/tagged/cv),
- et [Template CV LaTeX : les 11 meilleurs exemples de CV LaTeX](https://zety.fr/blog/modeles-cv-latex).

Quelques autres URL inspirantes :

- [SMart fancy LaTeX CV](https://github.com/neoben/smart-fancy-latex-cv) (en anglais),
- [Twenty Seconds Resume/CV](http://www.latextemplates.com/template/twenty-seconds-resumecv) (en anglais).

### CV type Europass

- La Commission européenne recommande l'utilisation d'un [format unifié pour les CV en Europe](https://europa.eu/europass/fr). Les extensions {ctanpkg}`europasscv` et {ctanpkg}`EuropeCV` sont dédiées à la mise en page d'un CV selon le format recommandé par la Commission européenne.

Si (de l'aveu de Nicola Vita, son auteur) la classe {ctanpkg}`europecv` ne résout pas tous les problèmes, elle semble bien pensée et prend en charge toutes les langues officielles actuelles de l'Union européenne (ainsi que quelques langues non officielles, comme le catalan, le galicien et le serbe).

## Solutions plus anciennes

- La classe {ctanpkg}`vita` d'Andrej Brodnik offre tout ce qu'il faut pour la production d'un *curriculum vitae*. La classe peut être personnalisée à la fois pour le domaine (les exemples fournis avec la classe concernent informaticiens et chanteurs, notamment), et pour la langue (les exemples sont proposés en anglais et en slovène). Sa configuration peut se faire au moyen de fichiers `.clo` (fichiers d'options de classe), ou en utilisant les macros définies dans la classe pour créer de nouveaux types d'entrées, etc.

Voici un exemple d'utilisation :

```latex
% !TEX noedit
\documentclass[ComputerScience]{vita}
\title{\hfill Vita}
\begin{document}
 \name{Andrej Brodnik}
 \businessAddress{%
     First line \\
     second line of bussines address}
 \homeAddress{%
     Again \\
     multiline address \\
     perhaps with phone number}
\begin{vita}
\begin{Degrees}
   \item B.S. etc.
   \item Ph.D. ...
\end{Degrees}

\begin{Publications}
   \begin{Books}
     \item My First One
     \item My Last One
   \end{Books}

   \begin{Papers in Refereed Journals}
     \item My First One
     \item My Last One
   \end{Papers in Refereed Journals}

\end{Publications}

\begin{References}
    The first person
\\ his address
\\ in multiple lines

\and

    The second person
\\ again, multiple lines
\end{References}

\end{vita}
\end{document}
```

- La classe {ctanpkg}`curve` de Didier Verna est basée sur un modèle dans lequel le CV est constitué d'un ensemble de *rubriques* (telles que « éducation », « expérience professionnelle », etc). La documentation de la classe propose quelques fichiers d'exemple, et un mode Emacs est fourni.

## Sans classe spécialisée

- L'alternative à l'utilisation d'une classe spécifique est l'utilisation d'une extension à l'une des classes standards. Par exemple, l'extension {ctanpkg}`currvita` d'Axel Reichert a été recommandée à l'équipe de la FAQ. Son résultat est certainement bon, mais la dernière version de cette extension date de 1999.
- L'extension {ctanpkg}`ESIEEcv` offre un certain nombre d'environnements qui facilitent la mise en page type d'un CV :

```latex
% !TEX noedit
\documentclass[a4paper,10pt]{article}
\usepackage{ESIEEcv}

\begin{document}
\begin{rubrique}{Titre de la rubrique}
   \begin{sousrubrique}
      \Date{deb-fin}
      \Duree{longue}
      \Lieu{Cela s'est passé ici}
      \Titre{Ce que j'ai fait}
      \Descr{Quelques détails}
      \Apport{Ce que cela m'a apporté}
      \Apport{et ça aussi}
   \end{sousrubrique}
   \begin{sousrubrique}
      \Competence{Parlote}
      \Descr{Bonne maîtrise}
   \end{sousrubrique}
\end{rubrique}
\end{document}
```

:::{note}
La largeur de la première colonne peut être modifiée par :

```latex
% !TEX noedit
\setlength{\largeurcolonne}{2.5cm}
```
:::

- Voici en outre quelques macros qui peuvent être utiles :

```latex
% !TEX noedit
% Definition des categories de rubriques.
\newcommand{\categorie}[1]{%
   \vspace*{1cm}%
   \noindent
   {\large\textsl{#1}\par}%
   \vspace*{2pt}%
   \hrule
   \vspace*{.5cm}}
% On definit une colonne pour les dates
\newlength{\duree}
\settowidth{\duree}{\textbf{1985-1987}}
% La seconde colonnes doit occuper le reste de
% la page
\setlength{\duree}{-\duree}
\addtolength{\duree}{0.9\textwidth}

% Definition des rubriques.
\newcommand{\rubrique}[2]{%
   \noindent{\textbf{#1\ \ }}%
   \parbox[t]{\duree}{#2}}

\categorie{Formation :}
\rubrique{1990--1992}{%
  \'Etudiant à l'école Machin. Cette école
  propose un programme...

  Major de promo.}

\rubrique{1992--1993}%
         {Spécialisation dans le domaine...}

\categorie{Expérience :}
\rubrique{1987-1988}{%
  Stage ouvrier dans l'usine de carton de La Ville
  aux Cartons.}
```

{octicon}`alert;1em;sd-text-warning` *Information obsolète. Ce qui suit est informatif.*

- On trouvera les macros de J.M. Lasgouttes à l'URL <http://www-rocq.inria.fr/~lasgoutt/lyx/cv-1.5.tar.gz> . Ces macros sont distribuées avec LyX, et donc parfaitement intégrées à LyX.

{octicon}`alert;1em;sd-text-warning` *Information obsolète. Ce qui suit est informatif.*

- Il existe également une très ancienne extension [resume](https://ctan.org/tex-archive/obsolete/macros/latex209/contrib/resume) de Michael DeCorte, pour LaTeX 2.09, datant de 1989, qui vient avec peu de documentation, si ce n'est le conseil **ne pas** l'utiliser.

______________________________________________________________________

*Source :*

- {faquk}`Curriculum Vitae (Résumé) <FAQ-cv>`,
- [Template CV LaTeX : les 11 meilleurs exemples de CV LaTeX](https://zety.fr/blog/modeles-cv-latex),
- [Créer un CV élégant avec Latex et moderncv](https://blog.madrzejewski.com/creer-cv-elegant-latex-moderncv/).

```{eval-rst}
.. meta::
   :keywords: CV en LaTeX,trouver un travail grâce à LaTeX,postuler pour un emploi,lettre de motivation,EuroPass,CV européen
```
