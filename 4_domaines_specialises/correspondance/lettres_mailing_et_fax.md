```{role} latexlogo
```
```{role} tttexlogo
```
# Lettres, mailing et fax

## Comment structurer une lettre ?

- Il existe pour cela la classe {ctanpkg}`letter`. Celle-ci définit les commandes :
- `\address` qui définit l'adresse de l'expéditeur placée, en haut à gauche de la première page si l'on utilise une mise en page à la française, ou en haut à droite dans le cas d'une mise en page anglo-saxonne (solution par défaut),
- `\signature` prépare la signature de l'auteur de la lettre. Celle-ci sera placée en bas à droite de la dernière page de la lettre,
- `\begin{letter}` prend en argument le nom du destinataire,
- `\opening` et `\closing` permettent respectivement d'ouvrir et de fermer la lettre généralement par des formules de politesse.
- `\makelabels` est utilisé dans le préambule et permet de générer une liste des adresses des destinataires pour préparer un mailing.

Un certain nombre d'autres commandes permet de générer des champs spécifiques tels que : pièces jointes, post-scriptum... De manière générale, le positionnement des différents champs dépend de la langue utilisée dans la lettre.

:::{note}
Lorsque plusieurs environnements `letter` sont utilisés dans un même source, ils produiront chacun une lettre. Toutes ces lettres auront la même adresse d'expéditeur et la même signature.
:::

Lettre avec la classe {ctanpkg}`letter`

```latex
% !TEX noedit
\documentclass[11pt]{letter}

\name{expediteur}
\address{adresse \\ de l'expediteur}
\signature{signature \\ de l'expediteur}

\begin{document}
\begin{letter}{le destinataire}
   \opening{Cher destinataire,}

   texte -- texte -- texte -- texte -- texte --
   texte -- texte -- texte -- texte -- texte --
   texte -- texte -- texte -- texte -- texte --
   texte -- texte -- texte -- texte -- texte --
   texte -- texte -- texte -- texte -- texte --
   texte -- texte -- texte -- texte -- texte --
   texte -- texte -- texte -- texte -- texte --

   \closing{Formule de politesse}
   \ps{PS : j'ai oublie...}
   \cc{Monsieur Truc.}
   \encl{pieces jointes}
\end{letter}
\end{document}
```

:::{warning}
Si l'on ajoute le package {ctanpkg}`french`, la disposition des champs utilisés change puisque les lettres anglo-saxonnes ne respectent pas la même typographie que les françaises.
:::

- La classe {ctanpkg}`lettre` de D. Megevand est un outil très bien fait et adaptable à toutes les situations (voir l'exemple ci-dessous). Elle est disponible sur <https://www.ctan.org/macros/latex/contrib/lettre/> .

```latex
% !TEX noedit
\documentclass[11pt]{lettre}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\begin{document}
\begin{letter}{Mme Marie-Paule \textsc{Kluth}\\
   Alcatel Alsthom Recherche\\
   Route de Nozay\\
   \textbf{F-91460 Marcoussis}}

\address{Denis Mégevand\\Genève}
\notelephone\nofax\nolieu\nodate
\signature{Denis Mégevand}
\def\concname{Objet :~}
\conc{FAQ de \TeX{}}

\opening{Chère Madame,}
Je vous remercie de citer ma classe lettre dans le
\S20.1 de votre FAQ sur \TeX{}. Il est exact que
l'on peut faire beaucoup de choses avec cette
classe dans le domaine de la correspondance,
notamment des lettres et des télécopies (fax).
On peut également obtenir une page d'étiquettes
avec les adresses des destinataires des lettres
contenues dans le document.

Tous les paramètres de mise-en-page, ainsi que les
chaînes "Concerne", "Cc", "PS", etc. peuvent être
facilement modifiés.

\closing{Meilleures salutations}
\cc{À personne !}
\end{letter}
\end{document}
```

- La classe {ctanpkg}`fribrief` permet également de mettre en page des lettres.
- La classe {ctanpkg}`akletter` est une extension de la classe lettre classique. Elle permet notamment de définir son propre en-tête et de gérer la position de l'adresse pour des enveloppes à fenêtre.

## Comment préparer un mailing ?

- Le package {ctanpkg}`mailing`, disponible sur <https://www.ctan.org/%7Bmacros/latex/contrib/mailing/%7D> permet de créer plusieurs documents similaires avec des valeurs définies dans des bases externes (fichiers ou autres). Il ne fonctionne qu'avec la classe {ctanpkg}`letter`.
- Il existe également le package {ctanpkg}`envlab` disponible sur <https://www.ctan.org/%7Bmacros/latex/contrib/envlab/%7D>. Il est aujourd'hui aux standards américains mais doit être étendu à d'autres pays.
- Le package {ctanpkg}`labels` est également disponible sur <https://www.ctan.org/%7Bmacros/latex/contrib/labels/%7D>.

```latex
% !TEX noedit
\documentclass[12pt]{book}
\usepackage{labels}
\LabelCols=3
\LabelRows=11
\LeftBorder=8mm
\RightBorder=4mm
\TopBorder=2mm
\BottomBorder=4mm
%\LabelInfotrue

\begin{document}
%\footnotesize\sf
\numberoflabels=3

\addresslabel[\small\sf]
{Me, Myself \& I\\
SomePlace\\
SomeCompany\\
SomeStreet\\
SomeTown, SomeZip}
\end{document}
```

- Le package {ctanpkg}`adrlist` permet de gérer des listes d'adresses.
- Le package {ctanpkg}`envbig` permet d'imprimer des adresses sur des enveloppes.
- Le package {ctanpkg}`formlett` permet de gérer des mailings.

# Comment faire des références dans une lettre ?

- Dans la classe {ctanpkg}`lettre`, il existe les commandes `\Nref`, pour les références de l'expéditeur et `\Vref` pour celles du destinataire :

```latex
% !TEX noedit
\documentclass[11pt]{lettre}

\begin{document}
\begin{letter}{le destinataire}

\address{Mme Marie-Paule \textsc{Kluth}}
\notelephone\nofax\nolieu\nodate
\signature{MPK}
\Vref{A VOUS}
\Nref{DE MOI}

 \opening{Cher ami,}

   Texte....

   \closing{Sincèrement.}
   \encl{Pièces jointes :}
   \cc{copie à Totor}
\end{letter}
\end{document}
```

- Le shareware {ctanpkg}`french` (French Pro) offre également les commandes `\yourref` et `\ourref` :

```latex
% !TEX noedit
\documentclass[a4paper]{letter}
\usepackage{french}
\signature{ma signature}

\begin{document}
\yourref{mon texte}
\ourref{mon autre texte}
\begin{letter}{le destinataire}
   \opening{Cher Monsieur,}

   Texte...
   \closing{Salutations}
   \encl{pieces jointes}
   \cc{copie a M. Totor}
\end{letter}
\end{document}
```

## Comment mettre en page un fax ?

- Le package {ctanpkg}`fax` propose un ensemble de commandes pour préparer un fax.

## Comment positionner une adresse pour une enveloppe à fenêtre ?

- La classe `scrlettr.cls` du package {ctanpkg}`koma-script` permet de faire cela.

## Comment suppprimer la date sur une lettre ?

- Sous la classe {ctanpkg}`lettre` disponible sur <https://www.ctan.org/macros/>, il existe la commande `\nodate`.

## Comment inclure une figure dans une lettre ?

- Il faut utiliser le package {ctanpkg}`float` et déclarer le type figure :

```latex
% !TEX noedit
\usepackage{float}
\newfloat{figure}{htbp}{lof}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
