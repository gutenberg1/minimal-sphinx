```{role} latexlogo
```
```{role} tttexlogo
```
# Comment écrire des lettres et mémos ?

LaTeX lui-même fournit une classe {ctanpkg}`letter`, généralement peu appréciée; même son auteur a depuis longtemps renoncé à l'utiliser. Si vous voulez néanmoins l'essayer, mais que sa façon de positionner verticalement le texte vous irrite, essayez le hack suivant :

```latex
% !TEX noedit
\makeatletter
\let\@texttop\relax
\makeatother
```

dans le préambule de votre fichier.

Une stratégie courante est de faire les choses soi-même; Knuth (pour une utilisation avec Plain TeX, dans le {tttexlogo}`TeX`​book), et Kopka et Daly (dans leur {isbn}`Guide to LaTeX <978-0-3211-7385-0>`) offrent des exemples.

:::{note}
La dernière version des macros de Knuth apparaît dans {ctanpkg}`sa bibliothèque locale <knuth-local>` sur le CTAN. Elle est mise à jour en même temps que {tttexlogo}`TeX` lui-même, donc pas très souvent, mais la dernière version date de février 2021.
:::

Néanmoins, il existe *plein* d'alternatives toutes faites. Par nécessité, la liste suivante ne propose qu'une courte sélection.

La classe la plus importante et la plus complète est {ctanpkg}`newlfm`; le `lfm` de son nom suggère que la classe peut créer des **l**ettres, des **f**ax et des **m**émos. Sa documentation est volumineuse, et le paquetage semble très flexible.

D'autres classes recommandées sont {ctanpkg}`akletter` et {ctanpkg}`isodoc`.

La classe {ctanpkg}`dinbrief`, bien que souvent recommandée, n'est documentée qu'en allemand.

Il existe des classes pour les lettres dans les excellentes extensions :

- {ctanpkg}`KOMA-script`, avec la classe {ctanpkg}`scrlttr2` dont la documentation est disponible en anglais {doc}`et en français </1_generalites/documentation/documents/documents_extensions/koma-script_en_francais>`,
- et {ctanpkg}`ntgclass`, avec la classe {ctanpkg}`brief <ntgclass>` dont la documentation est {texdoc}`en néerlandais seulement <brief>`.

Elles sont probablement bonnes (puisque les extensions elles-mêmes inspirent confiance), mais elles n'ont pas été spécialement recommandées par les utilisateurs.

______________________________________________________________________

*Sources :*

- {faquk}`Letters and the like <FAQ-letterclass>`,
- [Écrire des lettres en LaTeX](https://zestedesavoir.com/tutoriels/508/ecrire-des-lettres-en-latex/) sur *Zeste de savoir*.

```{eval-rst}
.. meta::
   :keywords: LaTeX,mise en page,lettres,courrier,correspondance,publipostage,mailing,fax,télécopie,mémos,circulaires
```
