```{role} latexlogo
```
```{role} tttexlogo
```
# Comment dessiner un automate ?

- Le package {ctanpkg}`pst-node`, qui est un package de la famille de {ctanpkg}`PStricks <pstricks>`, permet de faire des *nœuds* de toutes formes, et de les relier par des arêtes, éventuellement étiquetées.

Exemple d'automate avec {ctanpkg}`pst-node` :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{pstricks,pst-node}
  \usepackage{stmaryrd}

\begin{document}
\begin{pspicture}(4,4)
\rput(1,1){\Circlenode{A}{$s$}}
\rput(3,0){\Circlenode{B}{$t$}}
\rput(3,1){\Circlenode{C}{$u$}}
\rput(3,2){\Circlenode{D}{$v$}}

\psset{radius=.15,arrows=->,labelsep=2pt,nrot=:U}

\everymath{\scriptscriptstyle}
\nccurve[angleA=10,angleB=170]{A}{C}\naput{$0$}
\nccurve[angleA=-170,angleB=-10]{C}{A}\naput{$1$}
\ncline{A}{B}\nbput{$\llbracket 2,4\rrbracket$}
\ncline{A}{D}\naput{$\llbracket 3,5\rrbracket$}
\nccurve[angleA=30,angleB=80,ncurv=3]{D}{D}
\end{pspicture}
\end{document}
```

- Le package {ctanpkg}`gastex` est spécialement destiné aux graphes et automates. Il est disponible, avec sa documentation et de nombreux exemples, à l'adresse <http://www.liafa.jussieu.fr/~gastin/gastex/gastex.html>.

Il existe par ailleurs une interface graphique associée à {ctanpkg}`gastex`.

Cet exemple donne un aperçu de ce que peut faire {ctanpkg}`gastex` :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{gastex}
  \usepackage{amsmath}

\begin{document}
\setlength{\unitlength}{1.0mm}
\begin{picture}(54,53)(-27,-25)
\gasset{linewidth=0.18,Nw=6,Nh=6,AHnb=1}
\gasset{Nmr=999,loopdiam=5,ELdist=0.5}
\gasset{AHLength=2,AHlength=1.8856}

\node(s0)(0,25){\large $0$}
\node(s1)(23.776413,7.725425){\large $1$}
\node(s2)(14.6946313,-20.225429){\large $2$}
\node(s3)(-14.6946313,-20.225429){\large $3$}
\node(s4)(-23.776413,7.725425){\large $4$}

\imark[iangle=135](s0)
\fmark[fangle=18](s1)
\gasset{linewidth=0.20}
\drawloop[loopangle=0](s0){\large $a$}
\drawedge[ELpos=80,curvedepth=-4](s1,s3)
        {\large $a$}
\drawedge[ELpos=80,curvedepth=4,ELside=r](s3,s1)
        {\large $a$}
\drawedge[ELpos=80,curvedepth=-4](s2,s4)
        {\large $a$}
\drawedge[ELpos=80,curvedepth=4,ELside=r](s4,s2)
        {\large $a$}

\drawedge[ELpos=80,ELside=r](s0,s3){\large $b$}
\drawedge[ELpos=50,ELside=l](s1,s4){\large $b$}
\drawedge[ELpos=50,ELside=r](s2,s1){\large $b$}
\drawedge[ELpos=50,ELside=l](s3,s2){\large $b$}
\drawedge[ELpos=50,ELside=r](s4,s0){\large $b$}

\gasset{dash={0.6 0.45}0,linewidth=0.25}
\drawedge[curvedepth=-4,ELside=r](s0,s4)
        {\large $c$}
\drawedge[ELside=r,ELpos=80](s2,s0)
        {\large $c$}
\drawbpedge[ELside=r,ELpos=25](s4,250,55,s2,-120,5)
        {\large $c$}
\drawloop(s1){\large $c$}
\drawloop[loopangle=180,ELpos=75](s3){\large $c$}
\end{picture}

\end{document}
```

- Le package {ctanpkg}`vaucanson-g` est une autre possibilité basée sur {ctanpkg}`PStricks <pstricks>`. Cette extension est disponible à l'adresse <http://www.liafa.jussieu.fr/~lombardy/Vaucanson-G/>.

Voici un exemple d'un automate représenté avec {ctanpkg}`vaucanson-g` :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage[pstricks1-10]{vaucanson-g}

\begin{document}
\begin{VCPicture}[]{(-10,-5)(0,2)}
\PlainState
\LargeState
\StateIF[p,q]{(-10,-1)}{AB}
\StateIF[q,r]{(-6,-1)}{BC}
\StateIF[p,r]{(-8,-4.464)}{AC}
\VCPut{(-5,-5)}{\Large $\kappa=[2,0,0]$}

\ChgStateLabelScale{0.7}
\StateIF[p,qr]{(0,-1.464)}{ABc}
\StateIF[q,pr]{(-2,2)}{BAc}
\StateIF[r,pq]{(2,2)}{CAb}
\VCPut{(3,-2)}{\Large $\kappa=[1,1,0]$}

\DimEdge
\ChgEdgeLineStyle{dotted}
\RstEdgeLineWidth

\EdgeR{ABc}{AB}{}
\EdgeR{ABc}{AC}{}
\EdgeR{BAc}{AB}{}
\EdgeR{BAc}{BC}{}
\EdgeR{CAb}{AC}{}
\EdgeR{CAb}{BC}{}

\RstEdge
\Initial{ABc}
\Initial{BAc}
\Initial[s]{CAb}
\Final[s]{BAc}
\Final{CAb}
%
\Initial{AB}
\Initial{AC}
\Initial[s]{BC}
\Final{BC}
\LoopN{AB}{b}
%
\EdgeL{ABc}{BAc}{a}
\EdgeL{BAc}{CAb}{a}
\EdgeL{CAb}{ABc}{a}
\EdgeL{AB}{BC}{a,b}
\EdgeL{BC}{AC}{a}
\EdgeL{AC}{AB}{a}
\end{VCPicture}
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,PStricks
```
