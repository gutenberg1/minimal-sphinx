```{role} latexlogo
```
```{role} tttexlogo
```
# Comment mettre en forme du code informatique ?

Les logiciels qui affichent du code informatique utilisent des couleurs pour rendre les programmes plus lisibles. Ceci permet de repérer visuellement les mots-clefs du langage, les noms de variables, etc. Il est possible de faire la même chose avec LaTeX, pour que la structure du code soit soulignée par la mise en forme. C'est très utile pour les livres sur la programmation, mais aussi pour les travaux des étudiants en informatique.

:::{important}
{doc}`L'inclusion de code « verbatim » </4_domaines_specialises/informatique/inclure_un_fichier_en_mode_verbatim>` est traitée ailleurs, ainsi que {doc}`le problème de la mise en forme des algorithmes </4_domaines_specialises/informatique/ecrire_du_pseudocode>`.
:::

## Avec l'extension « listings »

L'extension {ctanpkg}`listings` est souvent considérée comme la meilleure solution pour une sortie mise en forme (elle se charge d'analyser les sources des programmes et d'utiliser différentes polices de caractères pour en mettre la structure en évidence), mais il existe plusieurs autres extensions bien établies qui s'appuient sur une sorte de pré-compilateur. Vous pouvez utiliser {ctanpkg}`listings` pour mettre en page des extraits que vous incluez dans votre source :

```latex
\documentclass{article}
  \usepackage{lmodern}
  \usepackage{listings}
  \lstset{language=C}
  \pagestyle{empty}

\begin{document}
\begin{lstlisting}
#include <stdio.h>

int main(int argc, char ** argv)
{
  printf("Bonjour tout le monde !\n");
  return 0;
}
\end{lstlisting}
\end{document}
```

mais vous pouvez aussi l'utiliser pour mettre en forme des fichiers entiers :

```latex
\documentclass{article}
  \usepackage[latin1]{inputenc}
  \usepackage[T1]{fontenc}
  \usepackage{listings}

\begin{document}
\thispagestyle{empty}

\lstset{language=Pascal,literate={:=}{{$\gets$}}1
  {<=}{{$\leq$}}1 {>=}{{$\geq$}}1 {<>}{{$\neq$}}1}

\begin{lstlisting}
program Toto;
  var i : integer;
begin
  if (i<=0) then i := 1;
  if (i>=0) then i := 0;
  if (i<>0) then i := 0;
end.
\end{lstlisting}
\end{document}
```

:::{note}
Moyennant quelques paramètres à ajouter, il est tout à fait possible de faire en sorte que ces packages reconnaissent les fontes 8 bits. On peut lui faire comprendre par exemple que, lorsqu'il rencontre la chaîne « `mathonesuperior` », il doit la transformer en `$\mathonesuperior$`.
:::

Pendant longtemps, l'extension {ctanpkg}`listings` a été vue comme la panacée en la matière. Mais ces dernières années, des alternatives viables sont apparues.

## Avec l'extension « highlight »

{ctanpkg}`Highlight <highlight>` est intéressante si vous avez besoin de plus d'un format de sortie pour votre programme. En effet, en plus de la sortie (La)TeX, {ctanpkg}`highlight` pourra produire des représentations (X)HTML, RTF et XSL-FO de votre code-source. [Le manuel (en anglais)](http://www.andre-simon.de/doku/highlight/en/highlight.php) vous sera d'une grande aide si vous devez écrire le fichier de paramètres pour un nouveau langage, ou modifier la mise en forme d'un langage déjà défini.

## Avec l'extension « minted »

L'extension {ctanpkg}`minted` est une autre alternative qui permet créer de nouvelles définitions de langage. Il nécessite que le code soit traité à l'aide d'un script externe, écrit en Python, [`Pygments`](https://pygments.org/). Pygments, à son tour, a besoin d'un [analyseur lexical](https://fr.wikipedia.org/wiki/Analyse_lexicale) qui connaît le langage que vous voulez utiliser; il en existe de nombreux, pour les langages les plus utilisés, et il y a aussi des conseils pour écrire le vôtre sur le [site web de `Pygments`](https://pygments.org/docs/lexerdevelopment/).

L'usage de {ctanpkg}`minted` peut être aussi simple que ceci :

```latex
% !TEX noedit
\begin{minted}{⟨language⟩}
...
\end{minted}
```

qui traite le code du programme à la volée, au moment de la compilation.

:::{important}
Cela demande que {doc}`l'appel aux programmes externes </2_programmation/compilation/write18>` soit autorisé.
:::

- Le package {ctanpkg}`minted` est sans doute la solution la plus moderne et flexible. Il fait appel au package {ctanpkg}`fancyvrb`, mais aussi au programme externe [`Pygment`](https://pygments.org/) que vous devrez installer séparément.

```{eval-rst}
.. todo:: Un exemple d'utilisation est donné sur `https:bioinfo-fr.net/]], et un [[https:zestedesavoir.com/tutoriels/1848/presenter-du-code-source-dans-un-document-latex/|tutoriel très détaillé <https://bioinfo-fr.net/latex-la-mise-en-forme-du-texte-et-des-paragraphes#crayon-5f0e18eb47226674652384>`__ existe aussi.
```

## Avec l'extension « showexpl »

Dans un autre ordre d'idées, le paquetage {ctanpkg}`showexpl` prend en charge la mise en forme de code (La)TeX et de sa sortie compilée, dans des « volets » situés en regard l'un de l'autre. C'est notamment utile pour les manuels (La)TeX, ou pour les articles sur (La)TeX. Le paquetage utilise des {ctanpkg}`listings` pour le volet (La)TeX, et insère le résultat de la compilation dans une simple boîte, pour l'autre volet.

## Autres solutions

- L'extension {ctanpkg}`lgrind` (qui fournit un exécutable et un fichier de style, `lgrind.sty`) permet, entre autres, de formater du code source d'un langage donné en {latexlogo}`LaTeX`. Parmi les langages reconnus, on trouve : `Ada`, `assembleur`, `BASIC`, `Batch`, `C`, `C++`, `FORTRAN`, `GnuPlot`, `Icon`, `IDL`, `ISP`, `Java`, `Kimwitu++`, `ABAP`, `LaTeX`, `LDL`, `Lex`, `Linda`, `Lisp`, `MATLAB`, `ML`, `Mercury`, `model`, `Modula-2`, `Pascal`, `Perl`, scripts shell, `PostScript`, `Prolog`, `RATFOR`, `RLaB`, `Russell`, `SAS`, `Scheme`, `SICStus`, `src`, `SQL`, `Tcl/Tk`, `VisualBasic`, `yacc`.

Le programme `lgrind` permet à partir du code source de générer du code {latexlogo}`LaTeX` respectant l'indentation. Il transforme le source en question, par exemple `monfichier.c`, en `monfichier.tex`, que l'on inclut directement dans son fichier {latexlogo}`LaTeX`, à l'aide d'une commande appropriée (voir «[Comment découper un document en plusieurs fichiers?](/3_composition/texte/document/decouper_un_document_en_plusieurs_fichiers) »). L'inconvénient est qu'évidemment, il y a un fichier `.tex` qui est généré en plus.

L'exemple ci-dessous présente le code {latexlogo}`LaTeX` produit par `lgrind` pour le même code souce Pascal que dans l'exemple précédent (aux changements de ligne près).

:::{note}
- Utiliser au-moins la version 3.6;
- on peut paramétrer {ctanpkg}`lgrind` avec le fichier `lgrindef`.
- Par défaut, l'auteur a jugé utile de transformer la lettre « à » en \$\\alpha\$. Il suffit donc de commenter cette option à la fin de ce fichier pour éviter cela.
:::

Résultat produit par `lgrind` :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage[latin1]{inputenc}
  \usepackage[T1]{fontenc}

  \usepackage[procnames,noindent]{lgrind}
  \usepackage{fancyhdr,a4wide}
  \usepackage{german}
  \usepackage{makeidx}
  \pagestyle{fancy}

\makeindex

\begin{document}
\thispagestyle{empty}

\renewcommand{\footrulewidth}{0.4pt}
\fancyhead[C]{\lgrindhead}
\fancyhead[LO,RE]{\lgrindfilesize~Bytes\\%
\lgrindmodtime}
\fancyhead[RO,LE]{\bfseries \lgrindfilename\\%
\lgrindmodday.\lgrindmodmonth.\lgrindmodyear}
\fancyfoot[C]{\bfseries\thepage}
\setlength{\headheight}{24pt}
\begin{lgrind}
\BGfont
\File{toto.p}{2004}{2}{18}{16:08}{119}
\L{\LB{\K{program}_\V{Toto};}}
\L{\LB{}\Tab{2}{\K{var}_\V{i}_:_\V{integer};}}
\L{\LB{\K{begin}}}
\L{\LB{}\Tab{2}%
{\K{if}_(\V{i}\<=\N{0})_\K{then}_\V{i}_:=_\N{1};}}
\L{\LB{}\Tab{2}%
{\K{if}_(\V{i}\>=\N{0})_\K{then}_\V{i}_:=_\N{0};}}
\L{\LB{}\Tab{2}%
{\K{if}_(\V{i}\<\>\N{0})_\K{then}_\V{i}_:=_\N{0};}}
\L{\LB{\K{end.}}}
\end{lgrind}
\printindex
\end{document}
```

- Le package {ctanpkg}`tinyc2l` est un convertisseur de code `C`, `C++` ou `Java` en {latexlogo}`LaTeX`. Il numérote les lignes, traduit `!=` en \$\\neq\$, gère les commentaires, les en-têtes de procédures, etc. Il supporte plusieurs fichiers d'entrée et gère automatiquement les changements de section et la génération d'index.

## Solutions anciennes

- Le package {ctanpkg}`c++2latex`, sous licence GPL, est capable de convertir des fichiers C, C++ ou JAVA en {latexlogo}`LaTeX`. Les lignes peuvent être numérotées. Une ancienne version est disponible sur le CTAN : <http://tug.ctan.org/support/C++2LaTeX-1_1pl1/>, et ses mises à jour sont [sur le site web de l'auteur](http://www.arnoldarts.de/cpp2latex/).
- `cvt2ltx`, disponible sur <ftp://axp3.sv.fh-mannheim.de/cvt2latex/cvt2ltx.zip> est une famille de convertisseurs code source vers {latexlogo}`LaTeX` pour les langages C, C++, IDL et Perl.

```{eval-rst}
.. todo:: Est-il intéressant de parler de cvt2ltx ? Je n'ai même pas réussi à le compiler.
```

Voici quelques autres systèmes plus anciens (et moins « puissants ») :

- Le système {ctanpkg}`lgrind` est un précompilateur bien établi, avec toutes les facilités dont on peut avoir besoin et un vaste répertoire de langages supportés; il est dérivé du système {ctanpkg}`tgrind`, encore plus ancien, dont la sortie était basée sur Plain TeX.
- Le système {ctanpkg}`tinyc2l` est un peu plus récent : les utilisateurs sont encore encouragés à écrire leurs propres pilotes pour les langages qu'il ne supporte pas encore, mais le « *tiny* » dans son nom indique à juste titre qu'il ne s'agit pas d'un système spécialement élaboré.
- [C++2LaTeX](http://www.arnoldarts.de/cpp2latex/) est clairement orienté vers une utilisation avec `C` et `C++`.
- Un système extrêmement simple est {ctanpkg}`c2latex`, avec lequel vous écrivez votre source LaTeX dans les commentaires de votre programme `C` (un peu dans la logique du {doc}`literate programming </5_fichiers/web/literate_programming>`). Le programme convertit ensuite votre programme en un fichier LaTeX prêt à être compilé. Le programme est censé être "auto-documenté".

______________________________________________________________________

*Source :* {faquk}`Code listings in LaTeX <FAQ-codelist>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,langages de programmation,mise en forme de code,coloration syntaxiquenformater un programme,formater du code,verbatim,insérer du code-source
```
