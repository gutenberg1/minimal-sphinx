```{role} latexlogo
```
```{role} tttexlogo
```
# Où trouver un format pour mon article scientifique ?

- Des classes et packages de format de publication dans les revues scientifiques telles que IEEE, IFAC, SIAM, SIGGRAPH, etc., sont disponibles sur le CTAN. Regardez notamment les packages qui concernent :
- [les actes de conférences](https://ctan.org/topic/confproc),
- [les journaux](https://ctan.org/topic/confproc).

Pour plus de renseignements sur les documents IEEE, consulter la page <http://www.ieee.org/pubs/authors.html> qui donne des références de style pour {latexlogo}`LaTeX` 2.09 et {latexlogo}`LaTeX`.

- La classe {ctanpkg}`paper` est dérivée de la classe {ctanpkg}`article`. Elle est mieux adaptée pour la publication, notamment en termes de présentation de la page, le choix des fontes. Elle définit un certain nombre de macros utiles.
- De même, le package {ctanpkg}`elsevier` est disponible sur CTAN et fournit la classe `elsart.cls`. Cette classe est en outre très bien documentée.
- On trouve également une classe pour journaux d'Academic Press, que l'on peut trouver sur <http://www.academicpress.com/www/journal/latex_a.htm>.
- Kluwer propose une classe sur <http://www.wkap.nl/authors/jrnlstylefiles/>.
- *Annual Reviews* fait de même sur <http://www.annualreviews.org/authors/help_latex.asp>.
- L'*American Chemical Society* ne fournit, quant à elle, que des conseils aux auteurs sur <http://pubs.acs.org/instruct/texguide.html>.

```{eval-rst}
.. meta::
   :keywords: LaTeX,journaux scientifiques,templates,canevas,classes pour articles
```
