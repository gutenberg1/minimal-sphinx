```{role} latexlogo
```
```{role} tttexlogo
```
# Comment mettre en forme une publication pour un journal ou une revue ?

Les éditeurs de revues ont souvent de nombreuses exigences pour la présentation des articles. Bien que nombre d'entre eux acceptent les soumissions électroniques en {latexlogo}`LaTeX`, ils ne proposent pas souvent les groupes de commandes associées à ces présentations dans des archives publiques.

Néanmoins, il existe un nombre considérable de groupes de commandes disponibles sur le [catalogue du CTAN](/1_generalites/documentation/le_catalogue_du_ctan). Aussi, rechercher le nom de votre revue dans le catalogue CTAN peut très bien vous donner le résultat souhaité. Vous pouvez consulter ici la question « [Comment trouver un fichier (La)TeX ?](5_fichiers/comment_trouver_un_fichier) ».

À défaut, vous serez peut-être bien avisé de contacter l'éditeur potentiel de votre article ; de nombreux éditeurs disposent de commandes ou de styles sur leurs propres sites web ou les rendent disponibles uniquement sur demande. Dans ce cas, pensez à vérifier que l'éditeur vous propose des commandes adaptées à un environnement que vous pouvez utiliser : quelques éditeurs ne sont toujours pas passés aux dernières versions de {latexlogo}`LaTeX`, en prétextant par exemple que telle ou telle version un peu datée de {latexlogo}`LaTeX` est suffisante.

Mais tous les éditeurs ne sont pas aussi exigeants :

- certains retraitent tout ce qui leur a été envoyé, de sorte que les commandes que vous utilisez n'ont pas vraiment d'importance ;
- d'autres vous encouragent simplement à utiliser le moins d'extensions possible, afin qu'ils puissent facilement transformer votre document.

______________________________________________________________________

*Source :* {faquk}`Setting papers for journals <FAQ-journalpaper>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,journal,revue,publication
```
