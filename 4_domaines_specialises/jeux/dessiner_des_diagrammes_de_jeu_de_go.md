```{role} latexlogo
```
```{role} tttexlogo
```
# Peut-on réaliser des diagrammes de jeu de go ?

- Il est possible de faire des diagrammes de go avec {latexlogo}`LaTeX`. L'extension {ctanpkg}`psgo` facilite cette tâche, comme le montre l'exemple :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{psgo}

\newcommand{\SW}{\stone{white}}
\newcommand{\SB}{\stone{black}}

\begin{document}
\begin{psgoboard}[9]
\SW a9\SW b9\SW c9
\SB a8\SB b8\SW c8
\SB a7\SB b7\SW c7\SW d7
      \SW b6\SB c6\SW d6
      \SB b5\SB c5
      \SW b4\SB c4      \SW e4
\SW a3\SB b3\SB c3\SW d3
      \SW b2\SW c2\SW d2
\end{psgoboard}

\vspace{2ex}
Les noirs jouent et vivent
\end{document}
```

## Comment convertir un fichier « sgf » en TeX ?

- Ceux qui ne souhaitent pas écrire un script eux-mêmes peuvent utiliser le programme `sgf2tex`. Ce programme convertit des fichiers `sgf` (*smart-go format*) en format {tttexlogo}`TeX`.

Il était disponible à l'adresse <http://match.stanford.edu/~bump/sgf2tex.html>, qui n'existe plus. Tentez votre chances du côté de [Archive.org](https://web.archive.org/web/20060813132355/http://match.stanford.edu/~bump/sgf2tex.html) ou sur [CPAN](https://metacpan.org/release/Games-Go-Sgf2Dg)...

:::{warning}
`sgf2tex` produit des fichiers Plain{tttexlogo}`TeX` et non {latexlogo}`LaTeX`, il faudra donc compiler le résultat avec `tex` au lieu de `latex`.
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
