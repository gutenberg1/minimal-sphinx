```{role} latexlogo
```
```{role} tttexlogo
```
# Comment gérer un herbier ?

- Le package {ctanpkg}`nanicolle` permet de préparer des étiquettes pour les spécimens biologiques, à partir d'une base de données de terrain (type tableau Excel/LibreOffice). Il produit à la fois des étiquettes de collecte et des étiquettes d'identification.

L'astuce de ce package consiste à utiliser la tabulation (`↹`) comme séparateur dans la syntaxe de ses macros, ce qui permet de copier-coller très simplement les données depuis le tableau vers {latexlogo}`LaTeX` pour produire des étiquettes prêtes à être imprimées.

## Et si je veux mettre en forme une clef de détermination ?

- Le package {ctanpkg}`edichokey` permet de mettre en page une clef de détermination (dans le style de la *Flora Europaea*, par exemple).

```{eval-rst}
.. todo:: Ajouter un exemple de code.
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,herbarium,botanique,herbier,collection de spécimens,base de données,étiquettes,flore,classification dichotomique
```
