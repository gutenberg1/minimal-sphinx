```{role} latexlogo
```
```{role} tttexlogo
```
# Comment mettre en forme un calendrier ?

- Le package {ctanpkg}`termcal` permet de mettre en page un calendrier. Il permet de préciser quels jours doivent apparaître et d'insérer du texte soit régulièrement à certaines dates, soit à des dates particulières.
- Le package {ctanpkg}`calendar` est également fait pour ça.
- Enfin, il existe aussi le package {ctanpkg}`yplan`.

```{eval-rst}
.. todo:: Ajouter des exmples pour ces différents packages.
```

```{eval-rst}
.. meta::
   :keywords: LaTeX, calendrier annuel, calendrier mensuel, agenda, planning
```
