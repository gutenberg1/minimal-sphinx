```{role} latexlogo
```
```{role} tttexlogo
```
# Comment écrire des recettes de cuisine ?

- Le package {ctanpkg}`cuisine` permet de décrire le déroulement d'une préparation, avec, en regard, la liste des ingrédients à utiliser à chaque étape.

```{eval-rst}
.. todo:: Ajouter un exemple.
```

______________________________________________________________________

*Sources :*

- [Ginette Mathiot](https://fr.wikipedia.org/wiki/Ginette_Mathiot)

```{eval-rst}
.. meta::
   :keywords: LaTeX,recettes de cuisine,enseignement ménager,recettes,ingrédients,bon d'économat
```
