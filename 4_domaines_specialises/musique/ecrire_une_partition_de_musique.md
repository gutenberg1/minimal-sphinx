```{role} latexlogo
```
```{role} tttexlogo
```
# Comment saisir une partition de musique ?

- [NoteEdit](https://www.berlios.de/software/noteedit/) est un outil graphique permettant la saisie des partitions de musique, l'import et l'export de fichiers `midi`, permet la saisie à partir d'un clavier Midi, etc. Vous pourrez également saisir les schémas d'accord de guitare (tablatures) et les afficher sur la partition. Et bien sûr, il permet d'exporter des partitions pour [MusiXTeX](https://fr.wikipedia.org/wiki/MusiXTeX) ({ctanpkg}`voir sur CTAN <musixtex>`) et [LilyPond](https://lilypond.org/), sinon on n'en parlerait pas ici... Son développement est [actuellement arrêté](https://directory.fsf.org/wiki/Note_Editor).

Voir aussi la question « {doc}`Comment écrire de la musique sous LaTeX? </4_domaines_specialises/musique/ecrire_de_la_musique>` ».

- [Rosegarden](https://www.rosegardenmusic.com/) est un séquenceur ainsi qu'un éditeur de partitions. C'est un équivalent du logiciel commercial [Cubase](https://fr.wikipedia.org/wiki/Cubase). Il permet également d'exporter des fichiers au format LilyPond.
- [BRAHMS](https://web.archive.org/web/20070203115023/http://brahms.sourceforge.net/) était un séquenceur MIDI, décrit comme un « laboratoire de musique » par son auteur, Jan Wuethner. Il n'est plus développé depuis 2001 et sa [page d'accueil](http://brahms.sourceforge.net/) a été récupérée pour un projet homonyme.

```{eval-rst}
.. meta::
   :keywords: LaTeX,musique,partition musicale
```
