```{role} latexlogo
```
```{role} tttexlogo
```
# Existe-t-il une liste de discussion de musique ?

- La liste de discussion {tttexlogo}`TeX`-music (anciennement mutex) est consacrée à l'écriture de musique en {tttexlogo}`TeX`. On peut s'y inscrire à la page suivante : <http://icking-music-archive.org/mailman/listinfo/tex-music>

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
