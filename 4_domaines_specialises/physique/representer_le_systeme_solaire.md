```{role} latexlogo
```
```{role} tttexlogo
```
# Comment représenter le système solaire

- L'extension {ctanpkg}`pst-solarsystem` permet de représenter la position des planètes du système solaire dans le plan de l'écliptique à une date donnée. Par exemple :

```latex
\documentclass{article}
\usepackage[width=4.5cm]{geometry}
\usepackage{pst-solarsystem}
\pagestyle{empty}

\begin{document}
\SolarSystem[Day=31,Month=06,Year=2001,Hour=23,Minute=59,Second=59]
\end{document}
```

```{eval-rst}
.. todo:: L'exemple ne compile pas sur le site de la FAQ
```
