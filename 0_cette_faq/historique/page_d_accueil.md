```{role} latexlogo
```
```{role} tttexlogo
```
# Les dessins de la page d'accueil de la FAQ

## Les dés

```{image} /0_cette_faq/dices_manuel_luque.png
:alt: dices_manuel_luque.png
:class: align-middle
:width: 400px
```

Le dessin des trois dés jetés sur un tapis de jeu vient d'un travail de [Manuel Luque,](http://pstricks.blogspot.com/) réalisé avec {ctanpkg}`PStricks, <pstricks>` et réutilisé avec son aimable autorisation.

- [Version originale,](http://www.tug.org/pipermail/pstricks/2013.txt)
- [Une version plus récente est disponible sur son site web,](http://pstricks.blogspot.com/2013/12/jeu-de-des-avec-pst-solides3d.html) utilisant {ctanpkg}`pst-solides3d`.

```latex
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -*- Mode : Latex -*- %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% pnode3D.tex--- Manuel Luque <Mluque5130@aol.com> (June 2000 - April 2002)
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass{article}
\usepackage[a4paper,margin=2cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}%
\usepackage{pstricks}
\usepackage{pst-node}
\usepackage{pst-plot}
% fp est de Michael Mehlich
\usepackage[nomessages]{fp}
% Dés en 3D
% 31 mars 2002 - 27 mai 2002(documentation version 0.2)
% Avec les remarques et les modifications de Denis Girou de 27 mars 2002
% Le générateur de nombres aléatoires est de
% Donald Arseneau
% http://ctan.mines-albi.fr/macros/generic/misc/random.tex
\input random
\makeatletter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\pst@addfams{pst-die}
\define@key[psset]{pst-die}{THETA}{\edef\psk@IIID@Theta{#1}}
\define@key[psset]{pst-die}{PHI}{\edef\psk@IIID@Phi{#1}}
\define@key[psset]{pst-die}{Dobs}{\edef\psk@IIID@Dobs{#1}}
\define@key[psset]{pst-die}{Decran}{\edef\psk@IIID@Ecran{#1}}
\define@key[psset]{pst-die}{Side}{\edef\psk@Die@Side{#1}}
\psset{THETA=60,PHI=60,Dobs=30,Decran=10,Side=2}
%
\define@key[psset]{pst-die}{thetax}{\edef\psk@Die@Thetax{#1}}
\define@key[psset]{pst-die}{thetay}{\edef\psk@Die@Thetay{#1}}
\define@key[psset]{pst-die}{thetaz}{\edef\psk@Die@Thetaz{#1}}
\define@key[psset]{pst-die}{CX}{\edef\psk@Die@CX{#1}}
\define@key[psset]{pst-die}{CY}{\edef\psk@Die@CY{#1}}
\define@key[psset]{pst-die}{CZ}{\edef\psk@Die@CZ{#1}}
\define@key[psset]{pst-die}{K}{\edef\psk@Die@K{#1}}
%
\psset{thetax=0,thetay=0,thetaz=0,CX=0,CY=0,CZ=0,K=0.75,linejoin=1}
%
\newif\ifPst@Axes%
\define@key[psset]{pst-die}{axes}[true]{\@nameuse{Pst@Axes#1}}%
\psset{axes=false}
%
\newif\ifPst@carpet%
\define@key[psset]{pst-die}{carpet}[true]{\@nameuse{Pst@carpet#1}}%
\psset{carpet=false}
%
\newpsstyle{sol}{fillstyle=vlines,hatchcolor=lightgray,hatchwidth=0.2\pslinewidth,hatchsep=1\pslinewidth}
\newpsstyle{Side}{fillstyle=solid,fillcolor=yellow}%grandes faces
\newpsstyle{side}{fillstyle=solid,fillcolor=magenta}%coins facettes
\newpsstyle{points}{fillstyle=solid,fillcolor=black,linestyle=none}% la couleur des points
\definecolor{MonGris}{rgb}{0.8,0.8,0.8}
\definecolor{MonRouge}{rgb}{0.8,0,0}
\SpecialCoor


\def\pnodeIIID{\def\pst@par{}\pst@object{pnodeIIID}}
\def\pnodeIIID@i(#1,#2,#3)#4{{%
  \begin@SpecialObj
\pstVerb{%
 /THETA \psk@IIID@Theta\space def
 /PHI \psk@IIID@Phi\space def
 /Dobs \psk@IIID@Dobs\space def
 /DScreen \psk@IIID@Ecran\space def
 /Sin1 THETA sin def
 /Sin2 PHI sin def
 /Cos1 THETA cos def
 /Cos2 PHI cos def
 /Cos1Sin2 Cos1 Sin2 mul def
 /Sin1Sin2 Sin1 Sin2 mul def
 /Cos1Cos2 Cos1 Cos2 mul def
 /Sin1Cos2 Sin1 Cos2 mul def
 /XpointVue Dobs Cos1Cos2 mul def
 /YpointVue Dobs Sin1Cos2 mul def
 /ZpointVue Dobs Sin2 mul def
}
 %
\pnode(!
 /xObservateur #1 Sin1 mul neg #2 Cos1 mul add def
 /yObservateur #1 Cos1Sin2 mul neg #2 Sin1Sin2 mul sub #3 Cos2 mul add def
 /zObservateur #1 neg Cos1Cos2 mul #2 Sin1Cos2 mul sub #3 Sin2 mul sub Dobs add def
 /xScreen DScreen xObservateur mul zObservateur div def
 /yScreen DScreen yObservateur mul zObservateur div def
  xScreen yScreen){#4}
\end@SpecialObj}}
%
\def\Die{\def\pst@par{}\pst@object{Die}}
\def\Die@i{{%
\pst@killglue
  \begin@SpecialObj
\pstVerb{%
 /CX \psk@Die@CX\space def
 /CY \psk@Die@CY\space def
 /CZ \psk@Die@CZ\space def
 /K \psk@Die@K\space def
 /thetax \psk@Die@Thetax\space def
 /thetay \psk@Die@Thetay\space def
 /thetaz \psk@Die@Thetaz\space def
 /sinthetax thetax sin def
 /costhetax thetax cos def
 /sinthetay thetay sin def
 /costhetay thetay cos def
 /sinthetaz thetaz sin def
 /costhetaz thetaz cos def
 /M11 costhetaz costhetay mul def
 /M12 costhetaz sinthetay mul sinthetax mul sinthetaz costhetax
 mul sub def
 /M13 costhetaz sinthetay mul costhetax mul sinthetaz sinthetax
 mul add def
 /M21 sinthetaz costhetay mul def
 /M22 sinthetaz sinthetay mul sinthetax mul costhetaz costhetax
 mul add def
 /M23 sinthetaz sinthetay mul costhetax mul costhetaz sinthetax
 mul sub def
 /M31 sinthetay neg def
 /M32 sinthetax costhetay mul def
 /M33 costhetax costhetay mul def
 /Side \psk@Die@Side\space def
 /Point Side 4 div def} % rayon des points
 \CalculsTeX
 \MatriceRotTeX
 \CoordinatesFaces
 \CoordinatesSummit
 \TestVisibilitySide
 \TestVisibilitySummit
 \pnodeIIID(0,0,0){fictif}
 \ifPst@carpet
    \Sol
 \fi
 \Cube
 \ifPst@Axes
    \Axes
 \fi
\end@SpecialObj

%
\def\MatriceRot{% postscript
    /abscisse M11 abscisse1 mul M12 ordonnee1 mul add M13 cote1 mul add CX add def
    /ordonnee M21 abscisse1 mul M22 ordonnee1 mul add M23 cote1 mul add CY add def
    /cote M31 abscisse1 mul M32 ordonnee1 mul add M33 cote1 mul add CZ add def}
%%%%%%%%
\def\MatriceRotTeX{%
        \FPeval\Thetax{(\psk@Die@Thetax) * \FPpi/180}
        \FPeval\Thetay{(\psk@Die@Thetay) * \FPpi/180}
        \FPeval\Thetaz{(\psk@Die@Thetaz) * \FPpi/180}
        \FPcos\CosThetax{\Thetax}
        \FPsin{\SinThetax}{\Thetax}
        \FPcos\CosThetay{\Thetay}
        \FPsin{\SinThetay}{\Thetay}
        \FPcos\CosThetaz{\Thetaz}
        \FPsin{\SinThetaz}{\Thetaz}
        \FPeval\Moneone{(\CosThetaz)*(\CosThetay)}
        \FPeval\Monetwo{(\CosThetaz)*(\SinThetay)*(\SinThetax) -
        (\SinThetaz)*(\CosThetax)}
        \FPeval\Monethree{(\CosThetaz)*(\SinThetay)*(\CosThetax) +
        (\SinThetaz)*(\SinThetax)}
        \FPeval\Mtwoone{(\SinThetaz)*(\CosThetay)}
        \FPeval\Mtwotwo{(\SinThetaz)*(\SinThetay)*(\SinThetax) +
        (\CosThetaz)*(\CosThetax)}
        \FPeval\Mtwothree{(\SinThetaz)*(\SinThetay)*(\CosThetax) -
        (\CosThetaz)*(\SinThetax)}
        \FPeval\Mthreeone{(\SinThetay)*(-1)}
        \FPeval\Mthreetwo{(\SinThetax)*(\CosThetay)}
        \FPeval\Mthreethree{(\CosThetax)*(\CosThetay)}
        }
%
\def\CoordinatesFaces{%
    \FPeval\abscisseOne{(\psk@Die@Side)*(\Moneone)+(\psk@Die@CX)}
    \FPeval\ordonneeOne{(\psk@Die@Side)*(\Mtwoone)+(\psk@Die@CY)}
    \FPeval\coteOne{(\psk@Die@Side)*(\Mthreeone)+(\psk@Die@CZ)}
    \FPeval\abscisseTwo{(\psk@Die@Side)*(\Monetwo)+(\psk@Die@CX)}
    \FPeval\ordonneeTwo{(\psk@Die@Side)*(\Mtwotwo)+(\psk@Die@CY)}
    \FPeval\coteTwo{(\psk@Die@Side)* (\Mthreetwo)+(\psk@Die@CZ)}% ?
    \FPeval\abscisseThree{(-\psk@Die@Side)*(\Moneone)+(\psk@Die@CX)}
    \FPeval\ordonneeThree{(-\psk@Die@Side)*(\Mtwoone)+(\psk@Die@CY)}
    \FPeval\coteThree{(-\psk@Die@Side)*(\Mthreeone)+(\psk@Die@CZ)}
    \FPeval\abscisseFour{(-\psk@Die@Side)*(\Monetwo)+(\psk@Die@CX)}
    \FPeval\ordonneeFour{(-\psk@Die@Side)*(\Mtwotwo)+(\psk@Die@CY)}
    \FPeval\coteFour{(-\psk@Die@Side)* (\Mthreetwo)+(\psk@Die@CZ)}% ?
    \FPeval\abscisseFive{(-\psk@Die@Side)*(\Monethree)+(\psk@Die@CX)}
    \FPeval\ordonneeFive{(-\psk@Die@Side)*(\Mtwothree)+(\psk@Die@CY)}
    \FPeval\coteFive{(-\psk@Die@Side)*(\Mthreethree)+(\psk@Die@CZ)}
    \FPeval\abscisseSix{(\psk@Die@Side)*(\Monethree)+(\psk@Die@CX)}
    \FPeval\ordonneeSix{(\psk@Die@Side)*(\Mtwothree)+(\psk@Die@CY)}
    \FPeval\coteSix{(\psk@Die@Side)*(\Mthreethree)+(\psk@Die@CZ)}
    }
%
\def\CoordinatesSummit{%
    \FPeval\abscisseA{((\psk@Die@Side)*(\Moneone)) +
    ((\psk@Die@Side)*(\Monetwo)) +
    ((\psk@Die@Side)*(\Monethree))+(\psk@Die@CX)}
    \FPeval\ordonneeA{((\psk@Die@Side)*(\Mtwoone)) +
    ((\psk@Die@Side)*(\Mtwotwo)) +
    ((\psk@Die@Side)*(\Mtwothree))+(\psk@Die@CY)}
    \FPeval\coteA{((\psk@Die@Side)*(\Mthreeone)) +
    ((\psk@Die@Side)*(\Mthreetwo)) +
    ((\psk@Die@Side)*(\Mthreethree))+(\psk@Die@CZ)}
    %
    \FPeval\abscisseB{(\psk@Die@Side)*(\Moneone)+
    (-\psk@Die@Side)*(\Monetwo) +
    (\psk@Die@Side)*(\Monethree)+(\psk@Die@CX)}
    \FPeval\ordonneeB{(\psk@Die@Side)*(\Mtwoone) +
    (-\psk@Die@Side)*(\Mtwotwo) +
    (\psk@Die@Side)*(\Mtwothree)+(\psk@Die@CY)}
    \FPeval\coteB{(\psk@Die@Side)*(\Mthreeone) +
    (-\psk@Die@Side)*(\Mthreetwo) +
    (\psk@Die@Side)*(\Mthreethree)+(\psk@Die@CZ)}
    %
    \FPeval\abscisseC{(\psk@Die@Side)*(\Moneone) +
    (-\psk@Die@Side)*(\Monetwo) +
    (-\psk@Die@Side)*(\Monethree)+(\psk@Die@CX)}
    \FPeval\ordonneeC{(\psk@Die@Side)*(\Mtwoone) +
    (-\psk@Die@Side)*(\Mtwotwo)+
    (-\psk@Die@Side)*(\Mtwothree)+(\psk@Die@CY)}
    \FPeval\coteC{(\psk@Die@Side)*(\Mthreeone) +
    (-\psk@Die@Side)*(\Mthreetwo) +
    (-\psk@Die@Side)*(\Mthreethree)+(\psk@Die@CZ)}
    %
    \FPeval\abscisseD{(\psk@Die@Side)*(\Moneone) +
    (\psk@Die@Side)*(\Monetwo) +
    (-\psk@Die@Side)*(\Monethree)+(\psk@Die@CX)}
    \FPeval\ordonneeD{(\psk@Die@Side)*(\Mtwoone) +
    (\psk@Die@Side)*(\Mtwotwo) +
    (-\psk@Die@Side)*(\Mtwothree)+(\psk@Die@CY)}
    \FPeval\coteD{(\psk@Die@Side)*(\Mthreeone) +
    (\psk@Die@Side)*(\Mthreetwo) +
    (-\psk@Die@Side)*(\Mthreethree)+(\psk@Die@CZ)}
    %
    \FPeval\abscisseE{(-\psk@Die@Side)*(\Moneone) +
    (\psk@Die@Side)*(\Monetwo)+
    (\psk@Die@Side)*(\Monethree)+(\psk@Die@CX)}
    \FPeval\ordonneeE{(-\psk@Die@Side)*(\Mtwoone) +
    (\psk@Die@Side)*(\Mtwotwo)+
    (\psk@Die@Side)*(\Mtwothree)+(\psk@Die@CY)}
    \FPeval\coteE{(-\psk@Die@Side)*(\Mthreeone) +
    (\psk@Die@Side)*(\Mthreetwo) +
    (\psk@Die@Side)*(\Mthreethree)+(\psk@Die@CZ)}
    %
    \FPeval\abscisseF{(-\psk@Die@Side)*(\Moneone) +
    (-\psk@Die@Side)*(\Monetwo) +
    (\psk@Die@Side)*(\Monethree)+(\psk@Die@CX)}
    \FPeval\ordonneeF{(-\psk@Die@Side)*(\Mtwoone) +
    (-\psk@Die@Side)*(\Mtwotwo) +
    (\psk@Die@Side)*(\Mtwothree)+(\psk@Die@CY)}
    \FPeval\coteF{(-\psk@Die@Side)*(\Mthreeone) +
    (-\psk@Die@Side)*(\Mthreetwo) +
    (\psk@Die@Side)*(\Mthreethree)+(\psk@Die@CZ)}
    %
    \FPeval\abscisseG{(-\psk@Die@Side)*(\Moneone) +
    (-\psk@Die@Side)*(\Monetwo) +
    (-\psk@Die@Side)*(\Monethree)+(\psk@Die@CX)}
    \FPeval\ordonneeG{(-\psk@Die@Side)*(\Mtwoone) +
    (-\psk@Die@Side)*(\Mtwotwo) +
    (-\psk@Die@Side)*(\Mtwothree)+(\psk@Die@CY)}
    \FPeval\coteG{(-\psk@Die@Side)*(\Mthreeone) +
    (-\psk@Die@Side)*(\Mthreetwo) +
    (-\psk@Die@Side)*(\Mthreethree)+(\psk@Die@CZ)}
    %
    \FPeval\abscisseH{(-\psk@Die@Side)*(\Moneone) +
    (\psk@Die@Side)*(\Monetwo) +
    (-\psk@Die@Side)*(\Monethree)+(\psk@Die@CX)}
    \FPeval\ordonneeH{(-\psk@Die@Side)*(\Mtwoone) +
    (\psk@Die@Side)*(\Mtwotwo) +
    (-\psk@Die@Side)*(\Mtwothree)+(\psk@Die@CY)}
    \FPeval\coteH{(-\psk@Die@Side)*(\Mthreeone) +
    (\psk@Die@Side)*(\Mthreetwo) +
    (-\psk@Die@Side)*(\Mthreethree)+(\psk@Die@CZ)}
}
\def\TestVisibilitySide{%
    \FPeval\VisibilityFaceOne{((\abscisseOne)-(\psk@Die@CX))*((\xObs)-(\abscisseOne))
    + ((\ordonneeOne)-(\psk@Die@CY))*((\yObs)-(\ordonneeOne)) +
    ((\coteOne)-(\psk@Die@CZ))*((\zObs)-(\coteOne))}
    \FPeval\VisibilityFaceTwo{((\abscisseTwo)-(\psk@Die@CX))*((\xObs)-(\abscisseTwo))
    + ((\ordonneeTwo)-(\psk@Die@CY))*((\yObs)-(\ordonneeTwo)) +
    ((\coteTwo)-(\psk@Die@CZ))*((\zObs)-(\coteTwo))}
    \FPeval\VisibilityFaceThree{((\abscisseThree)-(\psk@Die@CX))*((\xObs)-(\abscisseThree))
    + ((\ordonneeThree)-(\psk@Die@CY))*((\yObs)-(\ordonneeThree)) +
    ((\coteThree)-(\psk@Die@CZ))*((\zObs)-(\coteThree))}
    \FPeval\VisibilityFaceFour{((\abscisseFour)-(\psk@Die@CX))*((\xObs)-(\abscisseFour))
    + ((\ordonneeFour)-(\psk@Die@CY))*((\yObs)-(\ordonneeFour)) +
    ((\coteFour)-(\psk@Die@CZ))*((\zObs)-(\coteFour))}
    \FPeval\VisibilityFaceFive{((\abscisseFive)-(\psk@Die@CX))*((\xObs)-(\abscisseFive))
    + ((\ordonneeFive)-(\psk@Die@CY))*((\yObs)-(\ordonneeFive)) +
    ((\coteFive)-(\psk@Die@CZ))*((\zObs)-(\coteFive))}
    \FPeval\VisibilityFaceSix{((\abscisseSix)-(\psk@Die@CX))*((\xObs)-(\abscisseSix))
    + ((\ordonneeSix)-(\psk@Die@CY))*((\yObs)-(\ordonneeSix)) +
    ((\coteSix)-(\psk@Die@CZ))*((\zObs)-(\coteSix))}}

%
\def\TestVisibilitySummit{%
    \FPeval\VisibilitySummitA{((\abscisseA)-(\psk@Die@CX))*((\xObs)-(\abscisseA))
    + ((\ordonneeA)-(\psk@Die@CY))*((\yObs)-(\ordonneeA)) +
    ((\coteA)-(\psk@Die@CZ))*((\zObs)-(\coteA))}
    \FPeval\VisibilitySummitB{((\abscisseB)-(\psk@Die@CX))*((\xObs)-(\abscisseB))
    + ((\ordonneeB)-(\psk@Die@CY))*((\yObs)-(\ordonneeB)) +
    ((\coteB)-(\psk@Die@CZ))*((\zObs)-(\coteB))}
    \FPeval\VisibilitySummitC{((\abscisseC)-(\psk@Die@CX))*((\xObs)-(\abscisseC))
    + ((\ordonneeC)-(\psk@Die@CY))*((\yObs)-(\ordonneeC)) +
    ((\coteC)-(\psk@Die@CZ))*((\zObs)-(\coteC))}
    \FPeval\VisibilitySummitD{((\abscisseD)-(\psk@Die@CX))*((\xObs)-(\abscisseD))
    + ((\ordonneeD)-(\psk@Die@CY))*((\yObs)-(\ordonneeD)) +
    ((\coteD)-(\psk@Die@CZ))*((\zObs)-(\coteD))}
    \FPeval\VisibilitySummitE{((\abscisseE)-(\psk@Die@CX))*((\xObs)-(\abscisseE))
    + ((\ordonneeE)-(\psk@Die@CY))*((\yObs)-(\ordonneeE)) +
    ((\coteE)-(\psk@Die@CZ))*((\zObs)-(\coteE))}
    \FPeval\VisibilitySummitF{((\abscisseF)-(\psk@Die@CX))*((\xObs)-(\abscisseF))
    + ((\ordonneeF)-(\psk@Die@CY))*((\yObs)-(\ordonneeF)) +
    ((\coteF)-(\psk@Die@CZ))*((\zObs)-(\coteF))}
    \FPeval\VisibilitySummitG{((\abscisseG)-(\psk@Die@CX))*((\xObs)-(\abscisseG))
    + ((\ordonneeG)-(\psk@Die@CY))*((\yObs)-(\ordonneeG)) +
    ((\coteG)-(\psk@Die@CZ))*((\zObs)-(\coteG))}
    \FPeval\VisibilitySummitH{((\abscisseH)-(\psk@Die@CX))*((\xObs)-(\abscisseH))
    + ((\ordonneeH)-(\psk@Die@CY))*((\yObs)-(\ordonneeH)) +
    ((\coteH)-(\psk@Die@CZ))*((\zObs)-(\coteH))}}
%
\def\formulesIIID{%
    /xObservateur abscisse Sin1 mul neg ordonnee Cos1 mul add def
    /yObservateur abscisse Cos1Sin2 mul neg ordonnee Sin1Sin2 mul sub cote Cos2 mul add def
    /zObservateur abscisse neg Cos1Cos2 mul ordonnee Sin1Cos2 mul sub cote Sin2 mul sub Dobs add def
    /xScreen DScreen xObservateur mul zObservateur div def
    /yScreen DScreen yObservateur mul zObservateur div def
  xScreen yScreen}

% les sommets
\def\sommets{
    \pnode(! /abscisse1 Side def
        /ordonnee1 Side K mul def
        /cote1 Side def
        \MatriceRot
        \formulesIIID){A1}
    \pnode(! /abscisse1 Side def
        /ordonnee1 Side K neg mul def
        /cote1 Side def
        \MatriceRot
        \formulesIIID){B1}
    \pnode(! /abscisse1 Side def
        /ordonnee1 Side neg def
        /cote1 Side K mul def
        \MatriceRot
        \formulesIIID){C1}
    \pnode(! /abscisse1 Side def
        /ordonnee1 Side neg def
        /cote1 Side K neg mul def
        \MatriceRot
        \formulesIIID){D1}
    \pnode(! /abscisse1 Side def
        /ordonnee1 Side K mul neg def
        /cote1 Side neg def
        \MatriceRot
        \formulesIIID){E1}
    \pnode(! /abscisse1 Side def
        /ordonnee1 Side K mul def
        /cote1 Side neg def
        \MatriceRot
        \formulesIIID){F1}
    \pnode(! /abscisse1 Side def
        /ordonnee1 Side def
        /cote1 Side K mul neg def
        \MatriceRot
        \formulesIIID){G1}
    \pnode(! /abscisse1 Side def
        /ordonnee1 Side def
        /cote1 Side K mul def
        \MatriceRot
        \formulesIIID){H1}
    \pnode(! /abscisse1 Side neg def
        /ordonnee1 Side K mul def
        /cote1 Side def
        \MatriceRot
        \formulesIIID){A2}
    \pnode(! /abscisse1 Side neg def
        /ordonnee1 Side K neg mul def
        /cote1 Side def
        \MatriceRot
        \formulesIIID){B2}
    \pnode(! /abscisse1 Side neg def
        /ordonnee1 Side neg def
        /cote1 Side K mul def
        \MatriceRot
        \formulesIIID){C2}
    \pnode(! /abscisse1 Side neg def
        /ordonnee1 Side neg def
        /cote1 Side K neg mul def
        \MatriceRot
        \formulesIIID){D2}
    \pnode(! /abscisse1 Side neg def
        /ordonnee1 Side K mul neg def
        /cote1 Side neg def
        \MatriceRot
        \formulesIIID){E2}
    \pnode(! /abscisse1 Side neg def
        /ordonnee1 Side K mul def
        /cote1 Side neg def
        \MatriceRot
        \formulesIIID){F2}
    \pnode(! /abscisse1 Side neg def
        /ordonnee1 Side def
        /cote1 Side K mul neg def
        \MatriceRot
        \formulesIIID){G2}
    \pnode(! /abscisse1 Side neg def
        /ordonnee1 Side def
        /cote1 Side K mul def
        \MatriceRot
        \formulesIIID){H2}
    \pnode(! /abscisse1 Side K mul def
        /ordonnee1 Side def
        /cote1 Side def
        \MatriceRot
        \formulesIIID){I1}
    \pnode(! /abscisse1 Side K neg mul def
        /ordonnee1 Side def
        /cote1 Side def
        \MatriceRot
        \formulesIIID){J1}
    \pnode(! /abscisse1 Side K mul neg def
        /ordonnee1 Side neg def
        /cote1 Side def
        \MatriceRot
        \formulesIIID){K1}
    \pnode(! /abscisse1 Side K mul def
        /ordonnee1 Side neg def
        /cote1 Side def
        \MatriceRot
        \formulesIIID){L1}
    \pnode(! /abscisse1 Side K mul def
        /ordonnee1 Side def
        /cote1 Side neg def
        \MatriceRot
        \formulesIIID){I2}
    \pnode(! /abscisse1 Side K neg mul def
        /ordonnee1 Side def
        /cote1 Side neg def
        \MatriceRot
        \formulesIIID){J2}
    \pnode(! /abscisse1 Side K mul neg def
        /ordonnee1 Side neg def
        /cote1 Side neg def
        \MatriceRot
        \formulesIIID){K2}
    \pnode(! /abscisse1 Side K mul def
        /ordonnee1 Side neg def
        /cote1 Side neg def
        \MatriceRot
        \formulesIIID){L2}
% les sommets du cube initial
    \pnode(! /abscisse1 Side def
        /ordonnee1 Side def
        /cote1 Side def
        \MatriceRot
        \formulesIIID){A}
    \pnode(! /abscisse1 Side def
        /ordonnee1 Side neg def
        /cote1 Side def
        \MatriceRot
        \formulesIIID){B}
    \pnode(! /abscisse1 Side def
        /ordonnee1 Side neg def
        /cote1 Side neg def
        \MatriceRot
        \formulesIIID){C}
    \pnode(! /abscisse1 Side def
        /ordonnee1 Side def
        /cote1 Side neg def
        \MatriceRot
        \formulesIIID){D}
    \pnode(! /abscisse1 Side neg def
        /ordonnee1 Side def
        /cote1 Side def
        \MatriceRot
        \formulesIIID){E}
    \pnode(! /abscisse1 Side neg def
        /ordonnee1 Side neg def
        /cote1 Side def
        \MatriceRot
        \formulesIIID){F}
    \pnode(! /abscisse1 Side neg def
        /ordonnee1 Side neg def
        /cote1 Side neg def
        \MatriceRot
        \formulesIIID){G}
    \pnode(! /abscisse1 Side neg def
        /ordonnee1 Side def
        /cote1 Side neg def
        \MatriceRot
        \formulesIIID){H}
        }
% les coins
\def\coinA{%
\pscustom[style=side]{% Denis Girou
    \pspolygon[fillstyle=solid,fillcolor=yellow](A1)(H1)(I1)}}
\def\coinE{%
\pscustom[style=side]{% D.G.
    \pspolygon[fillstyle=solid,fillcolor=yellow](J1)(A2)(H2)}}
\def\coinH{%
\pscustom[style=side]{% D.G.
    \pspolygon[fillstyle=solid,fillcolor=yellow](J2)(F2)(G2)}}
\def\coinD{%
\pscustom[style=side]{% D.G.
    \pspolygon[fillstyle=solid,fillcolor=yellow](F1)(I2)(G1)}}
\def\coinB{%
\pscustom[style=side]{% D.G.
    \pspolygon[fillstyle=solid,fillcolor=yellow](L1)(B1)(C1)}}
\def\coinF{%
\pscustom[style=side]{% D.G.
    \pspolygon[fillstyle=solid,fillcolor=yellow](B2)(K1)(C2)}}
\def\coinG{%
\pscustom[style=side]{% D.G.
    \pspolygon[fillstyle=solid,fillcolor=yellow](D2)(E2)(K2)}}
\def\coinC{%
\pscustom[style=side]{% D.G.
    \pspolygon[fillstyle=solid,fillcolor=yellow](D1)(L2)(E1)}}
%
%
\def\Cube{%
\sommets
% face 1
\FPifpos\VisibilityFaceOne
\pscustom[style=Side]{% Denis Girou
        \pspolygon[style=Side](A1)(B1)(C1)(D1)(E1)(F1)(G1)(H1)}
        \parametricplot[style=points]{0}{360}{%
        /ordonnee1 t cos Point mul def
        /cote1 t sin Point mul def
        /abscisse1 Side def
        \MatriceRot
        \formulesIIID}
   \fi
% face number 3
\FPifpos\VisibilityFaceTwo
\pscustom[style=Side]{% D.G.
        \pspolygon[style=Side](H1)(I1)(J1)(H2)(G2)(J2)(I2)(G1)}
    \parametricplot[style=points]{0}{360}{%
        /abscisse1 t cos Point mul def
        /cote1 t sin Point mul def
        /ordonnee1 Side def
        \MatriceRot
        \formulesIIID}
    \parametricplot[style=points]{0}{360}{%
        /abscisse1 t cos Point mul Side 2 div sub def
        /cote1 t sin Point mul Side 2 div add def
        /ordonnee1 Side def
        \MatriceRot
        \formulesIIID}
    \parametricplot[style=points]{0}{360}{%
        /abscisse1 t cos Point mul Side 2 div add def
        /cote1 t sin Point mul Side 2 div sub def
        /ordonnee1 Side def
        \MatriceRot
        \formulesIIID}
\fi
% face number 4
\FPifpos\VisibilityFaceFour
\pscustom[style=Side]{% D.G.
        \pspolygon[style=Side](L1)(C1)(D1)(L2)(K2)(D2)(C2)(K1)}
    \parametricplot[style=points]{0}{360}{%
        /abscisse1 t cos Point mul Side 2 div sub def
        /cote1 t sin Point mul Side 2 div add def
        /ordonnee1 Side neg def
        \MatriceRot
        \formulesIIID}
    \parametricplot[style=points]{0}{360}{%
        /abscisse1 t cos Point mul Side 2 div add def
        /cote1 t sin Point mul Side 2 div sub def
        /ordonnee1 Side neg def
        \MatriceRot
        \formulesIIID}
    \parametricplot[style=points]{0}{360}{%
        /abscisse1 t cos Point mul Side 2 div sub def
        /cote1 t sin Point mul Side 2 div sub def
        /ordonnee1 Side neg def
        \MatriceRot
        \formulesIIID}
    \parametricplot[style=points]{0}{360}{%
        /abscisse1 t cos Point mul Side 2 div add def
        /cote1 t sin Point mul Side 2 div add def
        /ordonnee1 Side neg def
        \MatriceRot
        \formulesIIID}
\fi
% face 6
\FPifpos\VisibilityFaceThree
\pscustom[style=Side]{% D.G.
\pspolygon[style=Side](A2)(B2)(C2)(D2)(E2)(F2)(G2)(H2)}
    \parametricplot[style=points]{0}{360}{%
        /ordonnee1 t cos Point mul Side 2 div add def
        /cote1 t sin Point mul Side 2 div add def
        /abscisse1 Side neg def
        \MatriceRot
        \formulesIIID}
    \parametricplot[style=points]{0}{360}{%
        /ordonnee1 t cos Point mul Side 2 div add def
        /cote1 t sin Point mul Side 2 div sub def
        /abscisse1 Side neg def
        \MatriceRot
        \formulesIIID}
    \parametricplot[style=points]{0}{360}{%
        /ordonnee1 t cos Point mul Side 2 div sub def
        /cote1 t sin Point mul Side 2 div add def
        /abscisse1 Side neg def
        \MatriceRot
        \formulesIIID}
    \parametricplot[style=points]{0}{360}{%
        /ordonnee1 t cos Point mul Side 2 div sub def
        /cote1 t sin Point mul Side 2 div sub def
        /abscisse1 Side neg def
        \MatriceRot
        \formulesIIID}
    \parametricplot[style=points]{0}{360}{%
        /ordonnee1 t cos Point mul def
        /cote1 t sin Point mul Side 2 div sub def
        /abscisse1 Side neg def
        \MatriceRot
        \formulesIIID}
    \parametricplot[style=points]{0}{360}{%
        /ordonnee1 t cos Point mul def
        /cote1 t sin Point mul Side 2 div add def
        /abscisse1 Side neg def
        \MatriceRot
        \formulesIIID}
\fi
% face 2
\FPifpos\VisibilityFaceSix
\pscustom[style=Side]{% D.G.
        \pspolygon[style=Side](A1)(I1)(J1)(A2)(B2)(K1)(L1)(B1)}
    \parametricplot[style=points]{0}{360}{%
        /cote1  Side def
        /ordonnee1 t sin Point mul Side 2 div sub def
        /abscisse1 t cos Point mul Side 2 div sub def
        \MatriceRot
        \formulesIIID}
    \parametricplot[style=points]{0}{360}{%
        /cote1 Side def
        /ordonnee1 t sin Point mul Side 2 div add def
        /abscisse1 t cos Point mul Side 2 div add def
        \MatriceRot
        \formulesIIID}
   \fi
% face number 3
\FPifpos\VisibilityFaceTwo
\pscustom[style=Side]{% D.G.
        \pspolygon[style=Side](H1)(I1)(J1)(H2)(G2)(J2)(I2)(G1)
        }
   \parametricplot[style=points]{0}{360}{%
        /abscisse1 t cos Point mul def
        /cote1 t sin Point mul def
        /ordonnee1 Side def
        \MatriceRot
        \formulesIIID}
    \parametricplot[style=points]{0}{360}{%
        /abscisse1 t cos Point mul Side 2 div sub def
        /cote1 t sin Point mul Side 2 div add def
        /ordonnee1 Side def
        \MatriceRot
        \formulesIIID}
    \parametricplot[style=points]{0}{360}{%
        /abscisse1 t cos Point mul Side 2 div add def
        /cote1 t sin Point mul Side 2 div sub def
        /ordonnee1 Side def
        \MatriceRot
        \formulesIIID}
\fi
% face 5
\FPifpos\VisibilityFaceFive
\pscustom[style=Side]{% D.G.
        \pspolygon[style=Side](F1)(I2)(J2)(F2)(E2)(K2)(L2)(E1)}
    \parametricplot[style=points]{0}{360}{%
        /abscisse1 t cos Point mul Side 2 div sub def
        /ordonnee1 t sin Point mul Side 2 div add def
        /cote1 Side neg def
        \MatriceRot
        \formulesIIID}
    \parametricplot[style=points]{0}{360}{%
        /abscisse1 t cos Point mul Side 2 div add def
        /ordonnee1 t sin Point mul Side 2 div sub def
        /cote1 Side neg def
        \MatriceRot
        \formulesIIID}
    \parametricplot[style=points]{0}{360}{%
        /abscisse1 t cos Point mul Side 2 div sub def
        /ordonnee1 t sin Point mul Side 2 div sub def
        /cote1 Side neg def
        \MatriceRot
        \formulesIIID}
    \parametricplot[style=points]{0}{360}{%
        /abscisse1 t cos Point mul Side 2 div add def
        /ordonnee1 t sin Point mul Side 2 div add def
        /cote1 Side neg def
        \MatriceRot
        \formulesIIID}
    \parametricplot[style=points]{0}{360}{%
        /abscisse1 t cos Point mul def
        /ordonnee1 t sin Point mul def
        /cote1 Side neg def
        \MatriceRot
        \formulesIIID}
\fi
\FPifpos\VisibilitySummitA
    \coinA
\fi
\FPifpos\VisibilitySummitB
    \coinB
\fi
\FPifpos\VisibilitySummitC
    \coinC
\fi
\FPifpos\VisibilitySummitD
    \coinD
\fi
\FPifpos\VisibilitySummitE
    \coinE
\fi
\FPifpos\VisibilitySummitF
    \coinF
\fi
\FPifpos\VisibilitySummitG
    \coinG
\fi
\FPifpos\VisibilitySummitH
    \coinH
\fi
}
%%%%%%%%%%%%
%
%%%%%%%%%%%
%
\def\Sol{\def\pst@par{}\pst@object{Sol}}
\def\Sol@i{{%
\begin@SpecialObj
    \pnodeIIID(-10,-10,-\psk@Die@Side){A}
    \pnodeIIID(-10,10,-\psk@Die@Side){B}
    \pnodeIIID(10,10,-\psk@Die@Side){C}
    \pnodeIIID(10,-10,-\psk@Die@Side){D}
    \pspolygon[style=sol](A)(B)(C)(D)
\multido{\iY=-10+2,\iX=-10+2}{10}{
    \pnodeIIID(-10,\iY,-\psk@Die@Side){A}
    \pnodeIIID(10,\iY,-\psk@Die@Side){B}
    \psline[linecolor=red](A)(B)
    \pnodeIIID(\iX,-10,-\psk@Die@Side){C}
    \pnodeIIID(\iX,10,-\psk@Die@Side){D}
    \psline[linecolor=red](C)(D)
    \pnodeIIID(-10,\iY,-\psk@Die@Side){C}
    \pnodeIIID(10,\iY,-\psk@Die@Side){D}
    \psline[linecolor=blue](C)(D)
    }
\end@SpecialObj

%
%
\def\CalculsTeX{%
        \FPeval{\ThetaObsRadian}{(\psk@IIID@Theta) * (\FPpi/180)}
        \FPeval{\PhiObsRadian}{(\psk@IIID@Phi) * \FPpi/180}
        \FPcos{\CosThetaObs}{\ThetaObsRadian}
        \FPsin{\SinThetaObs}{\ThetaObsRadian}
        \FPcos{\CosPhiObs}{\PhiObsRadian}
        \FPsin{\SinPhiObs}{\PhiObsRadian}
        \FPeval{\xObs}{\psk@IIID@Dobs*\CosThetaObs*\CosPhiObs}
        \FPeval{\yObs}{\psk@IIID@Dobs*\SinThetaObs*\CosPhiObs}
        \FPeval{\zObs}{\psk@IIID@Dobs*\SinPhiObs}
        }
%
\def\Axes{%
    \pnodeIIID(0,0,-\psk@Die@Side){O}
    \pnodeIIID(0,10,-\psk@Die@Side){Y}
    \pnodeIIID(10,0,-\psk@Die@Side){X}
    \pnodeIIID(0,0,10){Z}
    \bgroup
        \psset{linestyle=dashed,arrowsize=0.2}
        \psline{->}(O)(X)
        \psline{->}(O)(Y)
        \psline{->}(O)(Z)
    \egroup
    \uput[90](X){$x$}
    \uput[90](Y){$y$}
    \uput[90](Z){$z$}
    \uput[270](O){O}
    }
\makeatother
\newpsstyle{sol}{fillstyle=vlines,hatchcolor=green,hatchwidth=0.2\pslinewidth,hatchsep=1\pslinewidth}
\pagestyle{empty}
\psset{dimen=middle}
\newcount\diex
\newcount\diey
\newdimen{\diez}
\newcount\CY
\newcount\CX
\newdimen{\vuePhi}
\newdimen{\vueTheta}
%
\makeatletter
%
\define@key[psset]{pst-die}{randomi1}{\edef\psk@ThreeDies@randomiDieA{#1}}
\define@key[psset]{pst-die}{randomi2}{\edef\psk@ThreeDies@randomiDieB{#1}}
\define@key[psset]{pst-die}{randomi3}{\edef\psk@ThreeDies@randomiDieC{#1}}
%
\psset{randomi1=182048360,randomi2=17483647,randomi3=568003}

\def\ThreeDies{\def\pst@par{}\pst@object{ThreeDies}}
\def\ThreeDies@i{{%
  \begin@SpecialObj
\begin{pspicture}(-5,-5)(5,5)
\randomi=\psk@ThreeDies@randomiDieA
\setrannum{\diex}{-2}{2}
\setrannum{\diey}{-2}{2}
\setrannum{\CX}{-8}{8}
\setrannum{\CY}{-8}{-6}
\setrandim{\diez}{0pt}{360pt}
\setrandim{\vuePhi}{10pt}{80pt}
\setrandim{\vueTheta}{0pt}{90pt}
\FPeval\ithetax{(\the\diex)*(90)}
\FPeval\ithetay{(\the\diey)*(90)}
\psset{thetax=\ithetax,thetay=\ithetay,thetaz={\pointless\diez},%
    PHI={\pointless\vuePhi},THETA={\pointless\vueTheta}}
\Die[carpet=true,CX={\the\CX},CY={\the\CY}]
\randomi=\psk@ThreeDies@randomiDieB
\setrannum{\diex}{-2}{2}
\setrannum{\diey}{-2}{2}
\setrannum{\CX}{-8}{-2}
\setrannum{\CY}{-3}{8}
\setrandim{\diez}{0pt}{360pt}
\FPeval\ithetax{(\the\diex)*(90)}
\FPeval\ithetay{(\the\diey)*(90)}
\psset{thetax=\ithetax,thetay=\ithetay,thetaz={\pointless\diez}}
\Die[CX={\the\CX},CY={\the\CY}]
\randomi=\psk@ThreeDies@randomiDieC
\setrannum{\diex}{-2}{2}
\setrannum{\diey}{-2}{2}
\setrannum{\CX}{2}{8}
\setrannum{\CY}{2}{8}
\setrandim{\diez}{0pt}{360pt}
\FPeval\ithetax{(\the\diex)*(90)}
\FPeval\ithetay{(\the\diey)*(90)}
\psset{thetax=\ithetax,thetay=\ithetay,thetaz={\pointless\diez}}
\Die[CX={\the\CX},CY={\the\CY}]
\end{pspicture}
\end@SpecialObj

\makeatother

\begin{document}
\input{random_dices.tex}
\end{document}
```

Un script Perl produit une nouvelle image à intervalles aléatoires sur notre site web :

```perl
#!/usr/bin/perl

use strict ;
use warnings ;

## Ne pas dépasser 2^31-1=2147483647, pour chaque valeur,
## voir la doc de ramdom.tex
my $seed1 = int rand(2**31) ;
my $seed2 = int rand(2**31) ;
my $seed3 = int rand(2**31) ;

open( RANDOM_DICES, '>random_dices.tex' ) or die "Cannot create file random_dices.tex" ;
print RANDOM_DICES qq#\\ThreeDies[randomi1=${seed1},randomi2=${seed2},randomi3=${seed3}]\n# ;
close( RANDOM_DICES ) or die "Cannot write file random_dices.tex" ;

system( 'latex dices.tex > dices.latex_out 2> dices.latex_err' ) ;
system( 'dvips dices.dvi > dices.dvips_out 2> dices.dvips_err' ) ;
system( '/usr/bin/convert -density 60 -trim -transparent "#FFFFFF"  dices.ps dices.png > dices.convert_out 2> dices.convert_err' ) ;

exit ;
```

## Les canards

Nous employons une équipe d'une trentaine de canards pour assurer la sécurité du site web. Ils utilisent le package {ctanpkg}`TikZducks <tikzducks>`. Un script Perl assure leur rotation aléatoire.

```latex
\documentclass[14pt,oneside]{extarticle}
  \usepackage[latin1]{inputenc}
  \usepackage[T1]{fontenc}
  \usepackage[upright]{fourier}

  \usepackage{tikz}
    \usetikzlibrary{ducks}

  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
\definecolor{carrot}{HTML}{f4661b}

\begin{tikzpicture}
 \draw (0,0) pic[duck/water=carrot,
                 duck/laughing,
                 duck/signpost=\TeX{}
                ] {duck} ;
\end{tikzpicture}
\end{document}
```

```{image} /0_cette_faq/duck_connect.png
:alt: duck_connect.png
:class: align-middle
:width: 209px
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,canard,caneton,dessin aléatoire,PStricks,TikZ,dessin en LaTeX,générateur aléatoire,Perl
```
