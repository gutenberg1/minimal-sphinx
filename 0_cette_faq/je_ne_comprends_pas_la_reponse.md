```{role} latexlogo
```
```{role} tttexlogo
```
# Je ne comprends pas la réponse!

Cette FAQ se veut relativement complète, et surtout **compréhensible**. Mais il est toujours possible que certaines réponses soient écrites de façon trop technique ou manquent de contexte pour que tout le monde les comprenne facilement.

**Vous avez un rôle à jouer pour améliorer ça !**

D'abord, vous pouvez chercher une meilleure réponse ailleurs :

- sur internet, avec votre moteur de recherche préféré,

- sur la liste de diffusion [gut@ens.fr](mailto:gut@ens.fr) ([instructions pour s'abonner](https://www.gutenberg.eu.org/listes)),

- sur le groupe Usenet `fr.comp.text.tex` ([consultation possible par le web](https://groups.google.com/g/fr.comp.text.tex), mais il vaut mieux utiliser un client Usenet),

- sur les sites d'entraide :

  - [TeX.StackExchange](https://tex.stackexchange.com/) (en anglais),
  - [TopAnswers](https://topanswers.xyz/tex) (en anglais),
  - [OSQA](https://texnique.fr/) (en français).

Ensuite, modifiez simplement la réponse sur la FAQ ! Si vous n'avez pas encore de compte, [vous pouvez vous en créer un immédiatement](start?do=register). Cette FAQ, maintenue sur des [ressources associatives](http://www.gutenberg.eu.org/faq), est volontairement ouverte à tous.

Si vous n'êtes pas sûr de votre réponse, vous pouvez écrire « `FIX``ME` » dans la page, ce qui attirera l'attention des autres contributeurs par un logo

```{eval-rst}
.. todo:: Le précédent paragraphe appelle une révision.
```

Ainsi, avec un peu de chance, nous pourrons ensemble améliorer le contenu de cette documentation pour l'ensemble de la communauté.

Enfin, si vous avez une question ou une remarque sur le fonctionnement de la FAQ, vous pouvez [envoyer un mail à ses mainteneurs](mailto:faq@gutenberg.eu.org).

**Très bonne lecture !** 8-)

```{eval-rst}
.. meta::
   :keywords: LaTeX,trouver de l'aide,répondre aux questions,modifier la FAQ,foire aux questions,problème avec LaTeX,contribuer à la FAQ
```
