```{role} latexlogo
```
```{role} tttexlogo
```
# Quelles sont les conventions typographiques de la FAQ ?

Le code informatique court (que ce soit du {latexlogo}`LaTeX` ou autre) est intégré au texte `et écrit de cette façon`. Lorsque des éléments du code sont variables, ils seront en italique entre signes `⟨` et `⟩`, comme ici l'argument *titre* : `\title{⟨titre⟩}`. Ces caractères (unicodes `U+27E8` et `U+27E9`) peuvent être obtenus :

- par des raccourcis différents selon les systèmes d'exploitation :

| Système d'exploitation | Raccourci pour `⟨`   | Raccourci pour `⟩`   |
| ---------------------- | -------------------- | -------------------- |
| GNU/Linux              | Ctrl + Shift + u27e8 | Ctrl + Shift + u27e9 |
| macOS                  | ⌥ + 27e8             | ⌥ + 27e9             |
| Windows                | Alt + 10216          | Alt + 10217          |

- par copiés-collés depuis la présente page.

Le code plus long sera composé en-dehors du texte et mis en couleurs pour une bonne lisibilité :

```latex
% !TEX noedit
\documentclass{article}

\begin{document}
  Hello world!
\end{document}
```

Pour la plupart des exemples {latexlogo}`LaTeX`, le résultat de la compilation est présenté à côté du code, ou juste au-dessous :

```latex
\documentclass{article}
  \usepackage{lmodern}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
  Bonjour tout le monde!
\end{document}
```

La FAQ contient beaucoup de liens vers d'autres sites web, pour vous permettre d'approndir vos connaissances. Ces liens sont signalés par un petit logo :

- lien vers un package LaTeX : {ctanpkg}`graphics` ;
- lien vers la documentation d'un package LaTeX : {texdoc}`documentation du package <graphics>` ;
- lien vers la FAQ anglophone : {faquk}`Where to find FAQs? <FAQ-whereFAQ>` ;
- lien vers Wikipedia : [Wikipedia francophone](https://fr.wikipedia.org/wiki/Wikipedia) ou [Wikipedia anglophone](https://en.wikipedia.org/wiki/Wikipedia) ;
- lien vers la notice d'un livre : {isbn}`LaTeX Companion <978-2744071331>`.

:::{note}
Les liens vers les packages et leurs documentations s'appuient sur les outils de la communauté {latexlogo}`LaTeX` : `CTAN` et `texdoc`, respectivement. Ces outils vous orienteront vers les différents serveurs où sont hébergées les ressources demandées.
:::

## Sources

En bas des pages de la FAQ, vous trouverez souvent une liste de pointeurs vers d'autres ressources. Ces « *sources* » peuvent être à la fois des ressources qui ont servi à la rédaction de la page (et que nous tenons à créditer pour leur aide), ou des documents qui permettent d'approfondir le sujet.

Si une page vous semble incomplète ou imprécise (voire fausse), vous avez la possibilité de la corriger vous-même. Si vous n'avez pas encore de compte, [vous pouvez en demander un](start?do=register), c'est immédiat. Ensuite, cliquez sur « Modifier cette page » et utilisez l'éditeur de texte qui apparaît. La syntaxe du wiki {doc}`est très simple pour commencer </wiki/antiseche>` (vous pourrez {doc}`aller plus loin dans un second temps </wiki/syntax>`).

Enfin, si vous rencontrez un problème ou si vous avez des questions ou des suggestions, [n'hésitez pas à nous contacter.](mailto:faq@gutenberg.eu.org)

```{eval-rst}
.. meta::
   :keywords: LaTeX,conventions de la FAQ,sources de la FAQ,contribuer à la FAQ
```
