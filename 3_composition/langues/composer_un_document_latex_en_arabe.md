```{role} latexlogo
```
```{role} tttexlogo
```
# Comment composer du texte en caractères arabes ?

## Cas simple

Avec un {doc}`encodage en UFT-8 </2_programmation/encodage/pourquoi_m_embeter_avec_inputenc_et_fontenc>`, vous pouvez taper votre texte directement en caractères arabes et il sera reproduit tel quel.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{fontspec}
\usepackage{polyglossia}
\setmainlanguage{arabic}
\setmainfont{Amiri}

\begin{document}

أخذ مكانه فخرج هارباً على وجهه حتى انتهى إلى الساحل.

إذا وصل إليك فتمارضي.

\end{document}
```

## Fonctionnalités avancées

Les extensions {ctanpkg}`arabtex`, {ctanpkg}`arabxetex` et {ctanpkg}`arabluatex`
permettent de générer automatiquement l'affichage du texte arabe à partir d'une
saisie complète (avec les voyelles) en caractères latins (p. ex. ```` .hurUf-u
'l-mu`jam-i ```` pour حروف المعجم). Cette méthode présente plusieurs avantages,
notamment :

- Possibilité de changer de conventions pour la mise en forme du texte arabe
sans avoir à modifier la saisie : à partir de ```` .hurUf-u 'l-mu`jam-i ````, on
peut obtenir حروف المعجم ou حُرُوفُ المُعجَم en arabe, ou encore *ḥurūf al-muʿǧam* ou
*ḥurūf al-muʿjam* en translittération ;
- Garantit l'application de conventions uniformes (par exemple, la *waṣla* est toujours indiquée si on demande une vocalisation complète et jamais si on demande une vocalisation partielle) ;
- Lors de la saisie du document, évite les problèmes liés au mélange de mots écrits de droite à gauche et de gauche à droite (par exemple lorsque l'on utilise des commandes LaTeX dans du texte arabe ou que l'on cite des termes arabes dans un texte en français).

Ces trois extensions ne supportent pas les mêmes langues, fonctionnalités et moteurs voir leurs documentations respectives.

```{eval-rst}
.. meta::
   :keywords: LaTeX,langues orientales,arabe,écrire en arabe avec LaTeX, translittération
```
