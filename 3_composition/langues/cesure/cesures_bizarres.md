```{role} latexlogo
```
```{role} tttexlogo
```
# Comment gérer des règles de césures spécifiques ?

Vous avez peut-être noté que la césure automatique des mots par {tttexlogo}`TeX` ne produit pas les césures recommandées par votre dictionnaire. Cela peut être dû au fait que {tttexlogo}`TeX` est configuré pour l'anglais américain, dont les règles de division des mots (telles que spécifiées, par exemple, dans le dictionnaire Webster) sont complètement différentes de celles d'autres langues, ne serait-ce que les règles britanniques (telles que spécifiées, par exemple, dans les dictionnaires Oxford).

Dans le cas général, vous pouvez vous tourner vers la question « [Comment utiliser une nouvelle langue avec l'extension babel?](/3_composition/langues/utiliser_une_nouvelle_langue_avec_babel) ».

Dans le cas particulier de l'anglais britannique, le problème a été étudié en 1998 par la communauté britannique des utilisateurs de {tttexlogo}`TeX` dans le [Numéro 4.4](https://ctan.org/tex-archive/usergrps/uktug/baskervi/bask4_4.pdf) de [Baskerville](https://ctan.org/tex-archive/usergrps/uktug/baskervi/). Une solution entièrement satisfaisante demandera du temps.

______________________________________________________________________

*Source :* {faquk}`(Merely) peculiar hyphenation <FAQ-oddhyphen>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,TeX,césure,langues,anglais
```
