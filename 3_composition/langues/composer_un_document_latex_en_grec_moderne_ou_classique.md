```{role} latexlogo
```
```{role} tttexlogo
```
# Comment composer du texte en grec moderne ou classique ?

## Grec moderne

Pour le grec moderne (avec un seul accent), il suffit en théorie  d'utiliser l'option `[greek]` avec {ctanpkg}`babel` ou {ctanpkg}`polyglossia` :

```latex
\documentclass{article}
  \usepackage[utf8x]{inputenc}
  \usepackage[LGR]{fontenc}
  \usepackage[width=6cm,height=6cm]{geometry}
  \usepackage{fontspec}
  \setmainfont{GFS Artemisia}
  \usepackage{microtype}
  \usepackage[greek]{babel}
  \pagestyle{empty}
\begin{document}
\textsc{Άρθρο 1} Όλοι οι άνθρωποι γεννιούνται \textbf{ελεύθεροι και ίσοι} στην αξιοπρέπεια και τα δικαιώματα. Είναι προικισμένοι με λογική και συνείδηση, και οφείλουν να συμπεριφέρονται μεταξύ τους με πνεύμα \textit{αδελφοσύνης}.
\end{document}
```

## Grec ancien

En ce qui concerne le grec ancien (avec tous les accents, iota souscrit, etc.), il faut utiliser :

- `[polutonikogreek]` avec {ctanpkg}`babel`
- `[ancient]` avec {ctanpkg}`polyglossia`

### Alterner entre grec ancien et français

#### Avec babel

```latex
% !TEX noedit
\usepackage[polutonikogreek,french]{babel}
```

puis on pourra basculer d'une langue à l'autre avec

```latex
% !TEX noedit
\selectlanguage{polutonikogreek}
```

et

```latex
% !TEX noedit
\selectlanguage{french}
```

Quand on rédige un document en français ne contenant que de courts exemples en grec, il peut être pratique de se faire une macro :

```latex
% !TEX noedit
\newcommand{\Gk}[1]{%
\selectlanguage{polutonikogreek}%
#1%
\selectlanguage{frenchb}%
}
```

#### Avec polyglossia

On utilisera l'environnement `greek` pour un long texte ou la commande `\textgreek{...}` pour quelques mots.

### Correspondance des lettres pour pdflatex

```{eval-rst}
.. todo:: À compléter
```

Pour l'instant, voir : <https://hiko-seijuro.developpez.com/articles/latexgrec/>

Pour les lettres ayant plusieurs formes suivant leur position dans le mot (début, milieu ou fin), comme le sigma, la forme adéquate est sélectionnée de façon automatique.

### Exemple complet de saisie en beta code avec pdflatex

```latex
\documentclass[a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage[polutonikogreek,french]{babel}

\newcommand{\Gk}[1]{%
\selectlanguage{polutonikogreek}%
#1%
\selectlanguage{frenchb}%
}

\begin{document}
  \thispagestyle{empty}
\Gk{<Hrod'otou <Alikarnhss'eoc <istor'ihc >ap'odexic <'hde,
<wc m'hte t`a gen'omena >ex >anjr'wpwn t~w| qr'onw|  >ex'ithla
g'enhtai, m'hte >'erga meg'ala te ka`i jwmast'a, t`a m`en
<'Ellhsi, t`a d'e barb'aroisi >apodeqj'enta, >akle~a g'enhtai,
t'a te >'alla ka`i di'' <`hn a>it'ihn >epol'emhsan >all'hloisi.

Pers'ewn m'en nun o<i l'ogioi Fo'inikac a>it'iouc fas`i gen'esjai
t~hc diafor~hc;  to'utouc g`ar >ap`o t~hc >Erujr~hc kaleom'enhc
jal'asshc >apikom'enouc >ep`i t'hnde t`hn j'alassan ka`i o>ik'hsantac
to~uton t`on q~wron t`on ka`i n~un o>ik'eousi, a>ut'ika nautil'ih|si
makr~h|si >epij'esjai, >apagin'eontac d`e fort'ia A>ig'upti'a te ka`i
>Ass'uria t~h| te >'allh| >esapikn'eesjai ka`i d`h ka`i >ec >'Argoc.
t`o d`e >'Argoc to~uton t`on qr'onon proe~iqe <'apasi t~wn >en t~h|
n~un <Ell'adi kaleom'enh| q'wrh|.
 <Hrod'otou <Alikarnhss'eoc <istor'i}
\end{document}
```

______________________________________________________________________

*Sources :*

- <https://hiko-seijuro.developpez.com/articles/latexgrec/>,
- <https://r12a.github.io/scripts/greek/>.

```{eval-rst}
.. meta::
   :keywords: LaTeX,langues anciennes,grec ancien,grec classique,grec moderne,iota souscrit,alphabet grec
```
