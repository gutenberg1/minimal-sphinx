```{role} latexlogo
```
```{role} tttexlogo
```
# Comment utiliser un nouveau langage avec « Babel » ?

L'extension {ctanpkg}`babel` est capable de travailler avec un large éventail de langues. Parfois, un utilisateur souhaite utiliser une langue pour laquelle son installation {tttexlogo}`TeX` n'est pas configurée. Il insérera dans son préambule de document la commande suivante :

```latex
% !TEX noedit
  \usepackage[catalan]{babel}
```

Cette commande va toutefois générer le message suivant :

```text
Package babel Warning : No hyphenation patterns were loaded for
(babel)                the language `Catalan'
(babel)                I will use the patterns loaded for \language=0 instead.
```

Ce qui veut dire « Aucun motif de césure n'a été chargé pour la langue « catalan ». J'utiliserai plutôt les modèles chargés pour \\language=0 ». De fait, votre système {tttexlogo}`TeX` ne sait pas comment couper un texte catalan et vous devez le lui dire pour que {ctanpkg}`babel` puisse faire son travail correctement. Pour cela, pour les installations {latexlogo}`LaTeX`, il faut modifier le fichier `language.dat` (qui fait partie de l'installation de {ctanpkg}`babel`) qui contient une ligne :

```latex
% !TEX noedit
%catalan         cahyphen.tex
```

Si vous supprimez la marque de commentaire, cette ligne est censée indiquer à {latexlogo}`LaTeX` de charger les modèles de césure catalane lorsque vous lui demandez de créer un nouveau format.

Malheureusement, dans certaines versions de {ctanpkg}`babel`, la ligne n'est parfois pas correcte : vous devez vérifier le nom du fichier contenant les motifs que vous allez utiliser. Comme vous pouvez le voir dans notre exemple, le nom est ici censé être `cahyphen.tex` alors que le fichier réellement présent sur le système s'avère être `cahyph.tex`. Heureusement, la correction est ici rapide. Si jamais vous devez récupérer un nouveau fichier, assurez-vous qu'il est correctement installé (voir pour cela la question « {doc}`Comment installer des fichiers « là où (La)TeX peut les trouver » ? </5_fichiers/installer_des_fichiers_pour_latex>` »).

Enfin, vous devez régénérer les formats utilisés.

## With teTeX

Il est possible de faire toute l'opération en une seule fois, en utilisant la commande `texconfig` :

```bash
texconfig hyphen latex
```

Un éditeur est alors lancé sur le fichier `language.dat`. Une fois édité, le format que vous spécifiez (`latex` dans ce cas) est régénéré.

Sinon, pour regénérer tous les formats, indiquez :

```bash
fmtutil --all
```

Des choses plus fines peuvent être faites en sélectionnant une séquence de formats et, pour chacun, en exécutant (mais ce n'est pas une méthode pas pour les *âmes sensibles*) la commande suivante dans laquelle `nom format` peut valoir par exemple `latex` :

```bash
fmtutil --byfmt ⟨nom format⟩
```

Un résultat similaire pourra être obtenu avec la commande suivante où `fichier césures` est le fichier spécifiant la césure du format (généralement `language.dat`) :

```bash
fmtutil --byhyphen ⟨fichier césures⟩
```

## With MiKTeX

Sur une distribution {ctanpkg}`MiKTeX` antérieure à la v2.0, faites :

- `Démarrer` \$\\rightarrow\$ `Programmes` \$\\rightarrow\$ `MiKTeX` \$\\rightarrow\$ `Maintenance` \$\\rightarrow\$ `Create all format files`

Vous pouvez aussi ouvrir une fênetre de commandes DOS et lancer :

```bash
initexmf --dump
```

Sur une distribution {ctanpkg}`MiKTeX` v2.0 ou ultérieure, toute la procédure peut être effectuée via l'interface graphique. Pour sélectionner la nouvelle langue, faites :

- `Démarrer` \$\\rightarrow\$ `Programmes` \$\\rightarrow\$ `MiKTeX 2` \$\\rightarrow\$ `MiKTeX Options` ;
- puis sélectionner l'onglet `Languages` ;
- sélectionnez votre langue dans la liste, appuyez sur le bouton `Apply`, puis sur le bouton `OK` ;
- sélectionnez ensuite l'onglet `General` et appuyez sur le bouton `Update Now`.

Sinon, éditez le fichier `language.dat` (comme indiqué ci-dessus), puis exécutez à l'image de ce qui est fait pour un système pré-v2.0 :

```bash
initexmf --dump
```

:::{note}
Il est possible que votre système {tttexlogo}`TeX` manque de « mémoire de modèle » lors de la génération du nouveau format. La plupart des implémentations de {tttexlogo}`TeX` ont des tableaux de taille fixe pour stocker les détails des modèles de césure, mais bien que la taille soit ajustable dans la plupart des distributions modernes, changer cette taille est en réalité un casse-tête. Si vous *constatez* que vous manquez de mémoire, cela peut valoir la peine de parcourir la liste des langues dans votre `language.dat` pour voir si certaines peuvent raisonnablement être supprimées.
:::

______________________________________________________________________

*Source :* {faquk}`Using a new language with Babel <FAQ-newlang>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,document multilingue,support des langues,langues étrangères
```
