```{role} latexlogo
```
```{role} tttexlogo
```
# Comment superposer du texte sur des figures ?

- La réponse est évidente lorsque la figure est composée directement dans le document {latexlogo}`LaTeX` », en utilisant l'environnement `picture` de {latexlogo}`LaTeX`, ou les environnements de {ctanpkg}`TikZ <tikz>` ou {ctanpkg}`PStricks <pstricks>` : il suffit d'utiliser les commandes du package utilisé pour positionner le texte sur la figure :
- `\put` avec {latexlogo}`LaTeX`,
- ''\\path ... node {}; avec Ti*k*Z,
- `\rput` avec PStricks.

La même méthode peut être utilisée pour annoter une image importée avec `\includegraphics`. Par exemple avec Ti*k*Z, on peut faire ceci :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{tikz}

\begin{document}
\begin{tikzpicture}
\path (0,0) node {\includegraphics[width=80mm]{image-externe}} ;
\draw[<-,line width=1pt] (10mm,0mm) -- ++(20mm,20mm) node {Regardez ici!} ;
\end{tikzpicture}
\end{document}
```

- `xfig` permet d'exporter une figure en deux parties : l'une contient la figure en elle-même, et est exportée en `eps` ; l'autre partie est un fichier {latexlogo}`LaTeX` qui inclut l'image `eps` ci-dessus, et y ajoute les commandes {latexlogo}`LaTeX` idoines pour que le texte de la figure soit ajouté au bon endroit.

```{eval-rst}
.. meta::
   :keywords: LaTeX,PStricks,TikZ,inclure une image,annoter une image
```
