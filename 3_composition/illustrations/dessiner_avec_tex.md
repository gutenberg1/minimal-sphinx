```{role} latexlogo
```
```{role} tttexlogo
```
# Comment dessiner avec TeX ?

Il existe de nombreux logiciels permettant de faire des dessins directement dans (La)TeX (plutôt que d'importer des graphiques créés avec un autre outil), allant de la simple utilisation de l'environnement `picture` de {latexlogo}`LaTeX` au dessin sophistiqué avec {ctanpkg}`PSTricks` ou {ctanpkg}`TikZ <tikz>`. En fonction de votre type de dessin et de votre configuration, voici quelques systèmes à envisager.

## Avec les commandes de base

L'environnement `picture` offre des capacités de dessin plutôt basiques : tout ce qui nécessite plus que de simples calculs linéaires est exclu, à moins qu'une police de caractères ne puisse vous venir en aide. Cet environnement dispose de sa propre définition de la commande `\unitlength` comme unité de mesure de base. Cette fonctionnalité pouvant être gênante, l'extension {ctanpkg}`picture` permet de l'éviter en détectant si une longueur est citée sous forme de nombre ou de longueur et agit en conséquence.

Voici un exemple d'utilisation :

```latex
\documentclass{article}
  \pagestyle{empty}

\begin{document}
\setlength\unitlength{1mm}
\begin{picture}(32,40)(-2,-2)
\put(0,0){\circle{1}}
\put(25,0){\line(-1,2){10}}
\put(12,26){\circle*{15}}
\qbezier(5,5)(30,40)(25,0)
\end{picture}
\end{document}
```

## Avec les extensions « epic », « eepic » et `eepicemu`

L'extension {ctanpkg}`epic` a été conçue pour utiliser l'environnement `picture` de manière plus efficace. L'extension {ctanpkg}`eepic` l'étend et est capable d'utiliser les commandes `\special` de `tpic` pour améliorer les performances d'impression. Si les commandes `\special` ne sont pas disponibles, l'extension {ctanpkg}`eepicemu <eepic>` pourra effectuer le traitement mais bien moins efficacement.

## Avec l'extension « pict2e »

L'extension {ctanpkg}`pict2e` comble les lacunes de l'environnement `picture` de {latexlogo}`LaTeX`. Elle définit un ensemble de commandes permettant de tracer des figures dont des cercles de tout diamètres des droites de toutes pentes. Elle a moins de commandes que {ctanpkg}`PStricks <pstricks>` mais présente le gros avantage de fournir des pilotes permettant la compilation avec {latexlogo}`LaTeX` *et* pdfLaTeX, comme le montre l'exemple ci-dessous. De plus, elle ne nécessite pas de compilation externe.

Voici des courbes de Bézier (quadratique et cubique) avec {ctanpkg}`pict2e` :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{ifpdf}
  \ifpdf
    \usepackage[pdftex]{pict2e}
  \else
    \usepackage[dvips]{pict2e}
  \fi
  \usepackage{color}

\newcommand*\FPOINT{
  \begingroup
    \setlength\unitlength{.8pt}
    \circle*{5}
  \endgroup
}

\newcommand*\OPOINT{
  \begingroup
    \setlength\unitlength{.8pt}
    \circle{5}
  \endgroup
}

\begin{document}
\setlength\unitlength{.007\linewidth}

\begin{picture}(100,120)(-50,-60)
  \put(-50,-60){\framebox(100,120){}}
  \color{green}
  \qbezier[0](-40,-30)(-30,10)(0,10)
  \qbezier[0](0,10)(30,10)(40,50)
  \put(-40,-30){\FPOINT}\put(-30,10){\OPOINT}
  \put(0,10){\FPOINT}
  \put(30,10){\OPOINT}\put(40,50){\FPOINT}
  \color{black}
  \cbezier[0](-40,-50)(-20,30)(20,-50)(40,30)
  \put(-40,-50){\FPOINT}\put(-20,30){\OPOINT}
  \put(20,-50){\OPOINT}\put(40,30){\FPOINT}
\end{picture}
\end{document}
```

## Avec l'extension « PICTeX »

L'extension {ctanpkg}`PICTeX <pictex>` est un système ancien et assez puissant qui dessine en plaçant des points sur la page pour donner l'effet d'une ligne ou d'une courbe. Bien qu'il y ait ici beaucoup de potentiel, cette extention est beaucoup plus lente que n'importe laquelle des autres extensions établies. De plus, sa {doc}`documentation </1_generalites/documentation/documents/documents_extensions/manuel_de_pictex>` pose des difficultés.

Vous avez aimé PicTeX mais il a été trop gourmand en mémoire ou en temps ? Testez l'extension {ctanpkg}`dratex` d'Eitan Gurari : elle est tout aussi puissante, plus économe en mémoire et beaucoup plus lisible.

## Avec l'extension « XYpic »

L'extension {ctanpkg}`XYpic <xypic>`, développée par Kristoffer Rose, permet de dessiner des graphes et des diagrammes. Elle n'est pas évidente de prime abord mais permet néanmoins de faire de très belles choses, une fois qu'on a pris un peu l'habitude.

En voici un exemple :

```latex
% !TEX noedit
\documentclass{report}
\usepackage{xy}

\begin{document}
\begin{xy}
(0,0)*+{A}; (20,20)*+{B} **\dir{-};
(0,20)*+{C} **\dir2{~}
\end{xy}
\end{document}
```

## Avec l'extension « PSTricks »

{ctanpkg}`PStricks <pstricks>`, développé essentiellement par Timothy Van Zandt, est un ensemble d'extensions permettant de faire à peu près tout ce que permet de faire le langage [PostScript](https://fr.wikipedia.org/wiki/Postscript). Le principe est de convertir des commandes {latexlogo}`LaTeX` (utilisant des commandes [\\special](/5_fichiers/postscript/commandes_special)) en commandes Postscript, qui seront interprétées lors de la visualisation de Postscript.

Les commandes `\special` de {ctanpkg}`PSTricks` sont par défaut spécifiques à `dvips`, mais il existe un pilote {ctanpkg}`PSTricks` qui lui permet de fonctionner sous `XeTeX`. Les utilisateurs de `pdfTeX` peuvent utiliser l'extension {ctanpkg}`pst-pdf` qui, comme l'extension {ctanpkg}`epstopdf` présentée à la question « [Comment insérer des images avec « pdfLaTeX » ?](/3_composition/illustrations/inclure_une_image/inclure_un_fichier_pdf2) », génère des fichiers PDF à l'aide d'un programme auxiliaire à partir des commandes {ctanpkg}`PSTricks`. L'extension {ctanpkg}`pst-pdf` nécessite également une version récente de l'extension {ctanpkg}`preview`.

Il existe une liste de diffusion en anglais traitant de {ctanpkg}`PSTricks` à laquelle vous pouvez vous [abonner](https://tug.org/mailman/listinfo/pstricks). Vous pouvez aussi en parcourir ses [archives](https://tug.org/pipermail/pstricks/).

Voici un exemple de dessin réalisé avec {ctanpkg}`PStricks <pstricks>` :

```latex
% !TEX noedit
\documentclass{report}
\usepackage{pstricks}

\begin{document}
\psset{unit=3pt}
\begin{pspicture}(-10,-10)(50,50)
\psline[linewidth=1,linecolor=red,linearc=5]
                {*->}(0,0)(10,0)(10,30)(40,40)
\psline[linewidth=1pt,linearc=5]
                (-10,0)(10,0)(10,30)(40,40)
\pspolygon(30,10)(30,30)(10,20)
\pscircle*[linewidth=5pt,linecolor=gray](40,5){5}
\pswedge*[linecolor=blue](40,40){10}{-45}{60}
\psarc*[linecolor=green](40,40){8}{-45}{60}
\psbezier{*-*}(0,20)(10,10)(25,10)(35,-10)
\psellipse(40,20)(5,10)
\end{pspicture}
\end{document}
```

## Avec l'extension « TikZ »

Alors que l'extension {ctanpkg}`PStricks <pstricks>` est très puissante et pratique avec les moteurs historiques de {tttexlogo}`TeX`, son utilisation avec `pdfLaTeX` est assez fastidieuse. Dès lors, l'extension {ctanpkg}`PGF <pgf>` est un outil très intéressant à étudier pour faire des dessins. Il est conçu avec des *frontends* (interfaces frontales) et des *backends* (interfaces dorsales) qui lui donnent une grande souplesse. L'interface frontale {ctanpkg}`TikZ <tikz>` fournie avec cette extension est assez conviviale à utiliser. Mais il est possible d'ajouter d'autres syntaxes, par exemple celle de {ctanpkg}`PStricks <pstricks>`. Grâce aux interfaces dorsales, il fonctionne avec {latexlogo}`LaTeX`, `pdfLaTeX`, mais permet également de générer du SVG avec {ctanpkg}`TeX4ht` (c'est le seul qui le permet à notre connaissance). Par ailleurs, {ctanpkg}`PGF <Pgf>` a un support mathématique étendu, ce qui lui permet de rivaliser avec l'utilisation du moteur de calcul de {ctanpkg}`PSTricks`.

L'exemple ci-dessous est celui la page de garde du manuel de {ctanpkg}`PGF <pgf>`, qui présente comment faire un arbre de type « soleil » :

```latex
\documentclass{article}
  \usepackage{tikz}
  \usetikzlibrary{snakes}
  \usetikzlibrary{trees}
  \pagestyle{empty}
\begin{document}
\tikzstyle{level 1}=[sibling angle=120]
\tikzstyle{level 2}=[sibling angle=60]
\tikzstyle{level 3}=[sibling angle=30]
\tikzstyle{every node}=[fill]
\tikzstyle{edge from parent}=[snake=expanding waves,
           segment length=1mm,segment angle=10,draw]

\tikz [grow cyclic,shape=circle,very thick,
       level distance=13mm,cap=round]
  \node {} child [color=\A]
    foreach \A in {red,green,blue}
    { node {} child [color=\A!50!\B]
      foreach \B in {red,green,blue}
       { node {} child [color=\A!50!\B!50!\C]
         foreach \C in {black,gray,white}
         { node {} }
       }
    };
\end{document}
```

Si le {texdoc}`manuel de TikZ <TikZ>` est énorme, son introduction permet à l'utilisateur d'avoir une idée des capacités du système (cette dernière est disponible en français dans le [cahier GUTenberg n°48](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_2007___48_23_0.pdf)). D'autres documents proposent des présentations :

- le [Cahier GUTenberg n°50](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_2008___50_5_0.pdf) : *Manuel de prise en main pour TikZ*,
- [TikZ pour l'impatient](http://math.et.info.free.fr/TikZ/index.html), de Gérard Tisseau et Jacques Duma,
- [A very minimal introduction to TikZ](http://cremeronline.com/LaTeX/minimaltikz.pdf) de Jacques Crémer (en anglais).

## Avec Metapost

À la différence des solutions précédentes, {doc}`MetaPost </1_generalites/glossaire/qu_est_ce_que_metapost>` n'est pas une extension mais un programme et un langage développé par John B. Hobby. Il met à votre disposition toute la puissance de `MetaFont` pour générer des graphiques `PostScript`. Notez que vous pouvez intégrer du code `Metapost` dans votre code {latexlogo}`LaTeX` comme évoqué dans la question «[Quels langages de description graphique peut-on utiliser avec LaTeX ?](/3_composition/illustrations/dessiner_avec_tex2) ».

De nombreuses documentations et exemples sont disponibles sur Internet :

- la [page Metapost](https://www.tug.org/metapost.html) (en anglais) du [TUG](/1_generalites/gutenberg) ;
- la [page Metapost](http://melusine.eu.org/syracuse/metapost/) du site [Syracuse](https://melusine.eu.org/syracuse/) ;
- le [cahier GUTenberg n°41](http://cahiers.gutenberg.eu.org/cg-bin/feuilleter?id=CG_2001___41) : [Metapost, le dessin sous \*TeX](http://cahiers.gutenberg.eu.org/cg-bin/feuilleter?id=CG_2001___41), contenant le [manuel de Metapost](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_2001___41_17_0.pdf) de John B. Hobby traduit en français ;
- le [cahier GUTenberg n°52-53](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_2009___52-53_5_0.pdf) : [Metapost raconté aux piétons](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_2009___52-53_5_0.pdf).

## Avec l'extension « Mfpic »

L'extension {ctanpkg}`Mfpic <mfpic>` permet de faciliter l'utilisation de [Metafont](https://fr.wikipedia.org/wiki/Metafont). Il s'agit tout d'abord d'écrire du code {latexlogo}`LaTeX` à la manière de {ctanpkg}`PStricks <pstricks>`. Puis à la compilation, un fichier `mf` est produit. Après avoir compilé le fichier obtenu avec Metafont (`mf`), il suffit de recompiler le fichier initial avec {latexlogo}`LaTeX` pour admirer les figures. Voici un exemple tiré de la {texdoc}`documentation Mfpic <mfpic>` :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{mfpic}
\begin{document}
\opengraphsfile{pics}
\begin{mfpic}[20]{-3}{3}{-3}{3}
\axes
\function{-2,2,0.1}{((x**3)-x)/3}
\tcaption{\raggedright{\it Fig. :}
Fonction dans un repère orthonormal.}
\end{mfpic}
\closegraphsfile
\end{document}
```

{ctanpkg}`MFpic <mfpic>`, même s'il n'a pas la puissance de {ctanpkg}`PStricks <pstricks>`, a l'avantage de fonctionner à la fois avec {latexlogo}`LaTeX` et `pdfLaTeX` et de permettre de tracer très facilement des courbes.

## Avec du code d'autres langages

Il existe plusieurs moyens de générer du code pour votre application graphique (`asymptote`, `gnuplot` et `MetaPost`, au moins) avec du code placé dans votre document {tttexlogo}`TeX`. Pour plus de détails, voir la question «{doc}`Quels langages de description graphique peut-on utiliser avec LaTeX ? </3_composition/illustrations/dessiner_avec_tex2>` ».

______________________________________________________________________

*Source :* {faquk}`Drawing with TeX <FAQ-drawing>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,graphiques, dessins,figures,packages pour dessiner avec LaTeX
```
