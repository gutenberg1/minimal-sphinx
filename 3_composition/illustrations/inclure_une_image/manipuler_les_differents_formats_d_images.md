```{role} latexlogo
```
```{role} tttexlogo
```
# Comment gérer différents formats de figures ?

Il existe de nombreux convertisseurs entre tous les formats, et cette FAQ n'est pas le lieu pour en donner une liste complète. Citons les incontournables du logiciel libre :

- [ImageMagick](https://imagemagick.org/index.php) (disponible pour Windows, Mac et Unix) qui fournit la commande `convert` permettant la conversion entre une multitude de formats ;
- [GIMP](https://www.gimp.org/) (disponible pour plusieurs plate-formes également) ;
- quelques logiciels dédiés tels `imgtops` ou `jpeg2ps` (pour Linux, qui sont plus spécifiques et plus efficaces)...

```{eval-rst}
.. meta::
   :keywords: LaTeX,conversion d'image,logiciel
```
