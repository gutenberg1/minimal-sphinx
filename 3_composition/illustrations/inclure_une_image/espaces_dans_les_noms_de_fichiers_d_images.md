```{role} latexlogo
```
```{role} tttexlogo
```
# Comment utiliser des noms de fichiers modernes pour les images ?

TeX a été conçu dans un monde où les noms de fichiers étaient en effet très simples, strictement limités en termes de jeu de caractères et de longueur. Sur les systèmes modernes, la plupart de ces restrictions ont disparu, les noms de fichiers peuvent utiliser quasiment tous les caractères et être longs de 200 lettres si l'utilisateur le veut. TeX ayant gardé une partie des anciennes limitations, on peut se retrouver dans des situations frustrantes... Notamment, des problèmes particuliers se posent avec les espaces dans les noms de fichiers, et, pire, la présence de plusieurs points (`.`) dans les noms de fichiers perturbent sérieusement l'extension {ctanpkg}`graphics`.

Les spécifications de TeX laissent quand même une certaine marge de manœuvre aux distributions pour adapter l'accès aux fichiers par rapport au système d'exploitation, et bien des progrès ont été réalisés. De nombreuses distributions modernes vous permettent de spécifier un nom de fichier comme `"nom du fichier.tex"` (avec des guillemets), ce qui est déjà pas mal, mais si cela nous permet d'écrire

```latex
% !TEX noedit
\input "autre chapitre.tex"
```

l'utilisation analogue

```latex
% !TEX noedit
\includegraphics{"autre image.eps"}
```

pourrait provoquer des problèmes avec `xdvi` et `dvips` en LaTeX « ordinaire » , même si la compilation proprement dite fonctionne.

À une époque, il a été conseillé de charger l'extension {ctanpkg}`grffile` en plus de l'extension {ctanpkg}`graphics`. Elle offre plusieurs options, dont les plus simples sont `multidot` (qui permet d'avoir plusieurs points dans un nom de fichier) et `space` (qui permet d'avoir des espaces dans un nom de fichier). Des limitation restaient pour les fichiers MetaPost.

## Et maintenant ?

Depuis 2017, {ctanpkg}`grffile` n'est plus utile. Il suffit d'utiliser l'extension {ctanpkg}`graphicx` (avec un X à la fin de son nom, comme *eXtended*), et vous ne devriez plus rencontrer de problèmes avec les espaces et les points multiples dans les noms de fichiers.

Ceci :

```latex
% !TEX noedit
\includegraphics{"autre image.nouvelle version.pdf"}
```

fonctionne donc, en général, avec une version de pdfTeX relativement récente.

Il reste une limitation : comme TeX remplace les espaces multiples par une seule espace, vous devrez remplacer le nom de fichier `fichier 2.pdf` par `fichier\space\space\space2.pdf` dans votre code LaTeX.

:::{tip}
Malgré tous ces progrès, si vous rencontrez des problèmes bizarres pour inclure des fichiers dans votre document LaTeX, notamment sous Windows, commencez par retirer les espaces dans les chemins d'accès, on ne sait jamais...
:::

______________________________________________________________________

*Sources :*

- {faquk}`Modern graphics file names <FAQ-grffilenames>`,
- [Include figures with multiple spaces in the filename](https://stackoverflow.com/questions/57467173/include-figures-with-multiple-spaces-in-the-filename).

```{eval-rst}
.. meta::
   :keywords: LaTeX,graphics
```
