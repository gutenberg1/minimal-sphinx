```{role} latexlogo
```
```{role} tttexlogo
```
# Où trouver un logiciel de dessin ?

Les logiciels de dessin sont très nombreux et sont séparables ici en deux grandes catégories : les outils « extérieurs » à {latexlogo}`LaTeX`, qui nécessitent de passer par un fichier intermédiaire, et ceux qui s'intègrent directement dans {latexlogo}`LaTeX`. Dans ce dernier cas, la question « [Comment dessiner avec TeX ?](/3_composition/illustrations/dessiner_avec_tex) » présente les différentes solutions.

Pour ce qui est des logiciels extérieurs, on peut encore distinguer deux sous-catégories : les logiciels de dessin vectoriel qui décrivent la figure à l'aide d'objets tels les cercles et les segments, et les logiciels de dessin matriciel (ou *bitmap*) pour qui une image n'est qu'une matrice de pixels.

## Pour du dessin vectoriel

### Le programme xfig

Le logiciel {ctanpkg}`xfig` (voir aussi [son site web](http://www.xfig.org/)) est disponible pour Linux et Windows. Il est très simple à utiliser, permet d'inclure des commandes {latexlogo}`LaTeX` dans le dessin (ces commandes seront donc [exécutées à la compilation du document](/3_composition/illustrations/inclure_des_formules_latex_dans_xfig)) et d'exporter l'image en [PostScript encapsulé (EPS)](5_fichiers/postscript/postscript_encapsule).

Il en existe plusieurs portages :

- une version pour Windows, [WinFIG](http://user.cs.tu-berlin.de/~huluvu/WinFIG.htm) ;
- une version java (donc fonctionnant sur de nombreuses plate-formes), [jfig](http://tech-www.informatik.uni-hamburg.de/applets/jfig/).

### Le programme eukleides

Le logiciel [eukleides](http://eukleides.free.fr/), disponible pour Linux et Windows, est adapté pour les figures de géométrie euclidienne. Il fonctionne en ligne de commande et produit du code {ctanpkg}`PStricks <pstricks>`.

- **Inconvénients** : il n'est disponible qu'en ligne de commande. De plus, une fois la figure compilée, les légendes sont légèrement décalées par rapport à la fenêtre `eukleides` de départ. Enfin, il est long à maîtriser.
- **Avantages :** il est bien adapté pour la géométrie euclidienne et produit de jolies figures. Son code {ctanpkg}`PStricks <pstricks>` est éditable par la suite dans le document.

### Le programme declic

Le logiciel [declic](http://emmanuel.ostenne.free.fr/declic.htm), disponible pour Linux et Windows, exporte son résultat en EPS. Bien adapté à la géométrie, ses polices de caractère ne sont cependant pas très esthétiques dans le document final.

### Le programme jpicedt

Le logiciel [jpicedt](http://jpicedt.sourceforge.net/) est un programme java générant directement du code {latexlogo}`LaTeX` et possédant une interface graphique. S'il est très bien pour les figures simples, il nécessite un peu d'habitude pour les figures complexes.

### Le programme TeXgraph

Le logiciel [TeXgraph](https://texgraph.tuxfamily.org/) génère du code {latexlogo}`LaTeX` et/ou {ctanpkg}`PStricks <pstricks>`.

### Le programme Tikzedt

Le logiciel [TikZedt](http://www.tikzedt.org/) est un éditeur semi-graphique pour le code {ctanpkg}`TikZ/PGF <tikz>`.

## Pour du dessin matriciel

En ce qui concerne les logiciels de dessin bitmap, on peut citer [The Gimp](https://www.gimp.org/), [POV-ray](http://www.povray.org/), et plus généralement, tout logiciel de dessin avec interface graphique.

Certains de ces logiciels n'exportent pas directement au format [EPS](/5_fichiers/postscript/postscript_encapsule) (qui peut être utile dans certaines chaînes de compilation). Mais à partir d'une image au format JPEG, par exemple, il est possible d'obtenir la même image en EPS, avec les convertisseurs `imgtops`, `jpeg2ps`, ou `convert` d'[ImageMagick](https://imagemagick.org/). La qualité et la taille du résultat peut varier d'un convertisseur à l'autre.

```{eval-rst}
.. meta::
   :keywords: Format DVI,LaTeX,Postscript,dessin,figure,dessin vectoriel,image,haute résolution,logiciel de dessin,PDF,SVG,tutoriel pour dessiner
```
