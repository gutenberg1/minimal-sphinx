```{role} latexlogo
```
```{role} tttexlogo
```
# Comment dessiner en style 8-bits ?

Vous en avez marre de toutes ces [courbes de Bézier](https://fr.wikipedia.org/wiki/Metafont), et vous voulez retrouver le bon vieux style graphique de votre GameBoy ?

## En deux couleurs : « PixelArt »

- Le package {ctanpkg}`PixelArt <pixelart>` est fait pour vous. Sa commande `\bwpixelart` prend une matrice en paramètre, et affiche une image bitmap à gros pixels.

```latex
\documentclass{article}
  \usepackage{pixelart}
  \pagestyle{empty}

\begin{document}
\bwpixelart[color=red, scale=.2]{%
001101100
011111110
111111111
111111111
111111111
011111110
001111100
000111000
000010000
}
\end{document}
```

La commande prend également une option `raise` (par exemple `raise=-1ex`), qui aide à aligner le dessin avec le texte, si besoin.

:::{tip}
Le nom des macros qui commence par `\bw...` suggère que ça ne fonctionne qu'en noir et blanc (*black and white*) mais ce n'est pas vrai (voir exemple ci-dessus), et la {texdoc}`documentation du package <pixelart>` propose même une façon d'obtenir un dessin en plusieurs couleurs.
:::

## Package « pxpic »

- Le package {ctanpkg}`pxpic` s'inspire de PixelArt et propose de faire des dessins en pixels de toutes les couleurs.

{texdoc}`Sa documentation <pxpic>` est richement illustrée.

______________________________________________________________________

*Source :* [Style 8 bits](https://fr.wikipedia.org/wiki/Style_8_bits)

```{eval-rst}
.. meta::
   :keywords: LaTeX,dessin,post-it battle,style 8 bits,dessin pixelisé
```
