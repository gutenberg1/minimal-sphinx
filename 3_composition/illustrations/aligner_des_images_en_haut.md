```{role} latexlogo
```
```{role} tttexlogo
```
# Comment aligner le haut de graphiques importés ?

Lorsque TeX compose du texte, il s'assure que la ligne de base de chaque objet de la ligne courante est au même niveau que la ligne de base de la ligne elle-même. (sauf, bien sûr, si vous jouez avec des commandes `\raisebox`...)

Lorsque vous importez un graphique, sa ligne de base est fixée au bas de l'image. Mais si vous utilisez une extension comme {ctanpkg}`subfig`, vous souhaiterez généralement que les images s'alignent entre elles par leur sommet. Cet exemple de code fait exactement cela :

```latex
% !TEX noedit
\vtop{%
  \vskip0pt
  \hbox{%
    \includegraphics{figure}%
  }%
}
```

La primitive `\vtop` fixe la ligne de base de l'objet résultant à celle de la première ligne recontrée à l'intérieur de celui-ci; la primitive `\vskip` crée l'illusion d'une ligne vide, donc `\vtop` fait du sommet de la boîte la ligne de base.

Dans les cas où les graphiques doivent être alignés avec le texte, il est préférable de placer la ligne de base à une hauteur un peu supérieure à celle du haut de la boîte, comme dans cet exemple :

```latex
% !TEX noedit
\vtop{%
  \vskip-1ex
  \hbox{%
    \includegraphics{figure}%
  }%
}
```

Une façon plus LaTeXienne de faire le travail utilise l'extension {ctanpkg}`calc` :

```latex
% !TEX noedit
\usepackage{calc}
...
\raisebox{1ex-\height}{\includegraphics{figure}}
```

(ceci a le même effet que la version qui aligne sur le texte, ci-dessus).

C'est à vous de décider où vous voulez placer la ligne de base. Cette réponse vous montre simplement quelques choix possibles.

______________________________________________________________________

*Sources :*

- {faquk}`Top-aligning imported graphics <FAQ-topgraph>`,
- [What are the different kinds of boxes in (La)TeX?](https://tex.stackexchange.com/questions/83930/what-are-the-different-kinds-of-boxes-in-latex)

```{eval-rst}
.. meta::
   :keywords: LaTeX,images,figures,aligner le haut,aligner le bas,graphiques de hauteurs différentes
```
