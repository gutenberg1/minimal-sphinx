```{role} latexlogo
```
```{role} tttexlogo
```
# Comment changer le style d'une ligne entière dans un tableau ?

Chaque cellule d'un tableau est {doc}`placée dans une boîte </2_programmation/syntaxe/boites/comprendre_le_modele_de_boites>`, de sorte qu'un changement de style (par exemple un chgangement de police) ne dure que jusqu'à la fin de la cellule. Un tableau peut contenir de nombreuses cellules, et placer une commande de changement de style de police dans chacune serait extrêmement fastidieux.

L'extension {ctanpkg}`array` permet de définir des modificateurs de colonne qui changeront le style de la *colonne* entière. Cependant, avec un peu d'astuce, on peut faire en sorte que ces modificateurs affectent les lignes plutôt que les colonnes. Notre préambule contiendra ceci :

```latex
% !TEX noedit
\usepackage{array}

\newcolumntype{$}{>{\global\let\currentrowstyle\relax}}
\newcolumntype{^}{>{\currentrowstyle}}
\newcommand{\rowstyle}[1]{\gdef\currentrowstyle{#1}%
  #1\ignorespaces
}
```

Maintenant, il nous reste à mettre `$` devant le premier spécificateur de colonne, puis `^` devant les spécificateur des colonnes suivantes. Enfin, il faudra utiliser la commande `\rowstyle` au début de chaque ligne dont on veut modifier le style :

```latex
% !TEX noedit
\begin{tabular}{|$l|^l|^l|}   \hline
  \rowstyle{\bfseries}
  Heading & Big and & Bold \\ \hline
  Meek & mild & entry      \\
  Meek & mild & entry      \\
  \rowstyle{\itshape}
  Strange & and & italic   \\
  Meek & mild & entry      \\ \hline
\end{tabular}
```

```latex
\documentclass{article}
  \usepackage{array}
  \usepackage[french]{babel}
  \pagestyle{empty}

\newcolumntype{$}{>{\global\let\currentrowstyle\relax}}
\newcolumntype{^}{>{\currentrowstyle}}
\newcommand{\rowstyle}[1]{\gdef\currentrowstyle{#1}%
  #1\ignorespaces
}

\begin{document}
\begin{tabular}{|$l|^l|^l|}   \hline
  \rowstyle{\bfseries}
  Heading & Big and & Bold \\ \hline
  Meek & mild & entry      \\
  Meek & mild & entry      \\
  \rowstyle{\itshape}
  Strange & and & italic   \\
  Meek & mild & entry      \\ \hline
\end{tabular}
\end{document}
```

```{eval-rst}
.. todo:: L'exemple ne compile pas sur le serveur.
```

:::{warning}
L'extension {ctanpkg}`array` fonctionne avec plusieurs autres environnements similaires à `tabular`, par exemples ceux fournis par {ctanpkg}`longtable`, mais malheureusement cette astuce ne fonctionnera pas toujours.
:::

______________________________________________________________________

*Source :* {faquk}`How to change a whole row of a table <FAQ-wholerow>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,flotants,mise en forme des tableaux,mise en forme d'une ligne,changer la police d'une ligne
```
