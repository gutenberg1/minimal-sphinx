```{role} latexlogo
```
```{role} tttexlogo
```
# Comment écrire un texte sur plusieurs lignes ?

## Avec l'extension « multirow »

L'extension {ctanpkg}`multirow` permet d'effectuer ce genre de manipulation. L'idée est simplement de décaler le texte d'une cellule vers le bas, et de ne rien mettre dans les cellules au-dessous. Il faut aussi penser à ne pas placer de filet de séparation horizontal sur toute la longueur.

La commande `\cline` permet de tracer des filets horizontaux {doc}`ne remplissant pas toute la largeur du tableau </3_composition/tableaux/filets/dessiner_des_lignes_partielles_dans_un_tableau>` ; elle prend en arguments les numéros de cellule sous lesquelles doit être tracé le filet. Voici un exemple :

```latex
\documentclass[11pt]{article}
\usepackage[francais]{babel}
\usepackage{multirow}
\usepackage{mathtools}
\pagestyle{empty}
\begin{document}

\[
\renewcommand\arraystretch{1.4}
%
\begin{array}{|c|c||c|c|}
 \hline
 \multirow{2}{0.5cm}{$k$} &
 \multirow{2}{0.5cm}{$p_G$} &
 \multicolumn{2}{c|}{test} \\
 \cline{3-4}
 & & \text{DADWRD} & \text{RARWRD} \\
 \hline
 2 & 1     & 90n   & 228 n \\
 3 & p_d   & 202 n & 449 n \\
 4 & p_d^2 & 424 n & 891 n \\
 5 & p_d^3 & 866 n & 1774 n \\
 \hline
\end{array}
\]
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,tableaux,lignes,filets
```
