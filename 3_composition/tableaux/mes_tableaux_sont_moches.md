```{role} latexlogo
```
```{role} tttexlogo
```
# Pourquoi les tableaux en LaTeX sont laids ?

Depuis longtemps, plusieurs auteurs notent que les exemples de tableaux donnés par [Leslie Lamport](https://fr.wikipedia.org/wiki/Leslie_Lamport) dans son {doc}`manuel LaTeX </1_generalites/documentation/livres/documents_sur_latex>` ne sont pas terribles, qu'ils ont mal inspiré les utilisateurs et ont généralisé une mise en forme assez médiocre des tableaux en {latexlogo}`LaTeX`. Il est parfois même difficile de comprendre le « sens » d'exemples du livre de Lamport.

De fait, des tableaux générés avec {latexlogo}`LaTeX` peuvent paraître laids si leurs auteurs n'y consacrent pas un minimum d'attention. Voici ici quelques pistes pour éviter cet écueil.

## L'espacement vertical

Le problème de l'espacement vertical est évident pour tous et est abordé dans plusieurs extensions. La question « {doc}`Comment améliorer l'espacement entre les lignes d'un tableau ? </3_composition/tableaux/lignes/augmenter_la_largeur_des_lignes_d_un_tableau>` » traite de ce sujet.

## Le piège des filets

L'utilisation des filets (traits horizontaux et verticaux présents dans les tableaux) est souvent abusive. L'{texdoc}`introduction de la documentation <booktabs-fr>` (en français) de l'extension {ctanpkg}`booktabs` de Simon Fear précise ce point (la {texdoc}`version anglaise de la documentation <booktabs>` pouvant être plus récente). Voir la question « [Comment présenter un tableau comme dans les livres ?](/3_composition/tableaux/presentation_professionnelle_d_un_tableau) » pour un exemple.

Les mêmes règles sont également implémentées dans la classe {ctanpkg}`memoir` [^footnote-1].

## La position de la légende

Par défaut, {latexlogo}`LaTeX` a également tort de mettre les légendes **sous** le tableau. Étant donné qu'un tableau peut s'étendre sur plusieurs pages, les règles traditionnelles de typographie placent la légende **au-dessus** du tableau. Si vous cherchez à mettre la légende au-dessus du tableau avec la commande `\caption`, la position finale sera décalée de 10pt vers le bas, et sera trop proche du tableau. Heureusement, l'extension {ctanpkg}`topcapt` résout le problème (ci-dessous à droite).

**Légende standard**

```latex
% !TEX noedit
\begin{table}
  \caption{Tableau de maîtres}
  \begin{tabular}{...}
    ...
  \end{tabular}
\end{table}
```

**Légende avec {ctanpkg}`topcapt`**

```latex
% !TEX noedit
\usepackage{topcapt}
...
\begin{table}
  \topcaption{Tableau de maîtres}
  \begin{tabular}{...}
    ...
  \end{tabular}
\end{table}
```

```latex
\documentclass{article}
  \usepackage[width=7cm]{geometry}
  \usepackage[french]{babel}
  \pagestyle{empty}
\begin{document}
\begin{table}
  \caption{Tableau de maîtres}
  \begin{tabular}{lc}
    Manet  & 1832--1883 \\
    Ingres & 1780--1867 \\
    Goya   & 1746--1828 \\
  \end{tabular}
\end{table}
\end{document}
```

```latex
\documentclass{article}
  \usepackage[width=7cm]{geometry}
  \usepackage{topcapt}
  \usepackage[french]{babel}
  \pagestyle{empty}
\begin{document}
\begin{table}
  \topcaption{Tableau de maîtres}
  \begin{tabular}{lc}
    Manet  & 1832--1883 \\
    Ingres & 1780--1867 \\
    Goya   & 1746--1828 \\
  \end{tabular}
\end{table}
\end{document}
```

Les classes {ctanpkg}`KOMA-script` disposent d'une commande similaire, `\captionabove` ; elles ont également une option de classe `tablecaptionabove` qui fait en sorte que `\caption` signifie `\captionabove` dans les environnements de tableau. L'extension {ctanpkg}`caption` peut être chargée avec une option qui a le même effet :

```latex
% !TEX noedit
\usepackage[tableposition=top]{caption}
```

Cela peut aussi être changé après le chargement de l'extension, avec `\captionsetup` :

```latex
% !TEX noedit
\usepackage{caption}
\captionsetup[table]{position=above}
```

Notez que les deux « options de position » sont différentes : `top` (en haut) dans un cas, et `above` (au-dessus) dans l'autre, mais dans ce contexte, elles signifient la même chose.

:::{tip}
Faire soi-même le travail de {ctanpkg}`topcapt` peut être assez facile : celle-ci se contente en fait de permuter les valeurs des paramètres {latexlogo}`LaTeX` `\abovecaptionskip` (valeur par défaut `10pt`) et `\belowcaptionskip` (valeur par défaut : `0pt`). Voici donc un exemple de modification... supposant que les valeurs n'ont pas été modifiées par ailleurs :

```latex
% !TEX noedit
\begin{table}
  \setlength{\abovecaptionskip}{0pt}
  \setlength{\belowcaptionskip}{10pt}
  \caption{Example table}
  \begin{tabular}{...}
    ...
  \end{tabular}
\end{table}
```
:::

______________________________________________________________________

*Source :* {faquk}`The design of tables <FAQ-destable>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,tableaux,tableaux en imprimerie,filets,mettre en forme un tableau,composer un tableau
```

[^footnote-1]: Il existe un [projet de traduction](https://github.com/jejust/memoir-fr) de la documentation de `Memoir`.
