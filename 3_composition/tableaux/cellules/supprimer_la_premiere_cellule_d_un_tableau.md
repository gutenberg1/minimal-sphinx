```{role} latexlogo
```
```{role} tttexlogo
```
# Comment supprimer la cellule supérieure gauche ?

- Avec les commandes `\multicolumn` et `\cline` :

```latex
% !TEX noedit
\begin{tabular}{|c|*{9}{c}|}
\cline{2-10}
\multicolumn{1}{c|}{}
  & 1 & 2 & 3 &  4 &  5 &  6 &  7 &  8 &  9 \\
\hline
1 & 1 & 2 & 3 &  4 &  5 &  6 &  7 &  8 &  9 \\
2 & 2 & 4 & 6 &  8 & 10 & 12 & 14 & 16 & 18 \\
3 & 3 & 6 & 9 & 12 & 15 & 18 & 21 & 24 & 27 \\
  ...     ...     ...     ...     ...     ...
\hline
\end{tabular}
```

```latex
\begin{tabular}{|c|*{9}{c}|}
\cline{2-10}
\multicolumn{1}{c|}{}
  & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 \\
\hline
1 & 1 &  2 &  3 &  4 &  5 &  6 &  7 &  8 &  9 \\
2 & 2 &  4 &  6 &  8 & 10 & 12 & 14 & 16 & 18 \\
3 & 3 &  6 &  9 & 12 & 15 & 18 & 21 & 24 & 27 \\
4 & 4 &  8 & 12 & 16 & 20 & 24 & 28 & 32 & 36 \\
5 & 5 & 10 & 15 & 20 & 25 & 30 & 35 & 40 & 45 \\
6 & 6 & 12 & 18 & 24 & 30 & 36 & 42 & 48 & 54 \\
7 & 7 & 14 & 21 & 28 & 35 & 42 & 49 & 56 & 63 \\
8 & 8 & 16 & 24 & 32 & 40 & 48 & 56 & 64 & 72 \\
9 & 9 & 18 & 27 & 36 & 45 & 54 & 63 & 72 & 81 \\
\hline
\end{tabular}
```

:::{note}
- `\hline` trace une ligne horizontale sur toute la largeur du tableau,
- `\cline{⟨col1⟩-⟨col2⟩}` trace une ligne horizontale de la colonne numéro ⟨*col1*⟩ à la colonne numéro ⟨*col2*⟩.
:::

Bien sûr, pour supprimer une autre cellule, c'est pareil...

______________________________________________________________________

*Sources :*

- [Making stats table with « \\multicolumn » and « \\cline »](https://tex.stackexchange.com/questions/314025/making-stats-table-with-multicolumn-and-cline),
- [Faire des tableaux sous LaTeX](https://www.tuteurs.ens.fr/logiciels/latex/tableaux.html).

```{eval-rst}
.. meta::
   :keywords: LaTeX,tableau,mise en forme,faire disparaître une cellule,lignes partielles
```
