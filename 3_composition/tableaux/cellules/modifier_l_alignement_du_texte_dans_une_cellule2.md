```{role} latexlogo
```
```{role} tttexlogo
```
# How to alter the alignment of tabular cells

One often needs to alter the alignment of a tabular `p` ("paragraph") cell, but problems at the end of a table row are common. With a `p` cell that looks like :

```latex
% !TEX noedit
... & \centering blah ... \\
```

one is liable to encounter errors that complain about a "misplaced `\noalign`" or "[extra alignment tab](FAQ-altabcr)", or the like. The problem is that the command `\\` means different things in different circumstances : the `tabular` environment switches the meaning to a value for use in the table, and `\centering`, `\raggedright` and `\raggedleft` all change the meaning to something incompatible. Note that the problem only arises in the last cell of a row : since each cell is set into a box, its settings are lost at the `&` (or `\\`) that terminates it.

In the old days, the actual value of `\\` that the `tabular` environment uses was only available as an internal command. Nowadays, the value is a public command, and you can in principle use it explicitly :

```latex
% !TEX noedit
... & \centering blah ... \tabularnewline
```

(but that's a rather verbose way of doing things).

The {ctanpkg}`array` package provides a command `\arraybackslash` which restores `\\` to its correct (within table) meaning; the command may be used in {ctanpkg}`array`'s "field format" preamble specifications :

```latex
% !TEX noedit
\begin{tabular}{... >{\centering\arraybackslash}p{50mm}}
...
```

The `\tabularnewline` and `\arraybackslash` commands are (somewhat) modern additions to LaTeX and the {ctanpkg}`array` package, respectively. In the unlikely event that neither is available, the user may try the (old) solution which preserves the meaning of `\\` :

```latex
% !TEX noedit
\newcommand\PBS[1]{\let\temp=\\%
  #1%
  \let\\=\temp
}
```

which one uses within a table as :

```latex
% !TEX noedit
... & \PBS\centering blah ... \\
```

or in the preamble as :

```latex
% !TEX noedit
\begin{tabular}{...>{\PBS\centering}p{5cm}}
```

______________________________________________________________________

*Source :* {faquk}`How to alter the alignment of tabular cells <FAQ-tabcellalign>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,figures,floats,tables
```
