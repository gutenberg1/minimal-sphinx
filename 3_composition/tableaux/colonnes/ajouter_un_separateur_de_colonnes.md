```{role} latexlogo
```
```{role} tttexlogo
```
# Comment définir un séparateur de colonne ?

- Le séparateur@'' permet de remplacer la séparation `|` par ce qu'on veut. Voir l'exemple ci-dessous. Noter que l'espacement entre les colonnes est supprimé dans ce cas.

```latex
% !TEX noedit
\begin{center}
\begin{tabular}{r@{ est un}l}
La poire &{} fruit \\
La carotte &{} légume \\
La rose &e fleur \\
\end{tabular}
\end{center}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
