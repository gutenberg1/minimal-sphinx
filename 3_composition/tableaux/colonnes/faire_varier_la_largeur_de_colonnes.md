```{role} latexlogo
```
```{role} tttexlogo
```
# Comment avoir des colonnes de largeur variable dans un tableau ?

Il s'agit d'une version légèrement différente du problème abordé dans « [Comment fixer la largeur d'un tableau?](/3_composition/tableaux/fixer_la_largeur_d_un_tableau) ». Ici, nous avons une colonne dont nous ne pouvons pas prévoir la taille au moment de l'écriture du document.

Si les techniques de base sont les mêmes pour ce problème que pour celui des tableaux à largeur déterminée, avec les extensions {ctanpkg}`tabularx`, {ctanpkg}`tabulary` et {ctanpkg}`ltxtable`, il existe un outil supplémentaire que nous pouvons appeler à notre secours et est préférable dans certaines situations.

Supposons que le contenu d'une colonne soit lu à partir d'une source externe, et que la source elle-même ne soit pas entièrement prévisible. Ce contenu est parfois étroit, mais d'autres fois tellement large que le tableau déborde de la page; cependant, nous ne voulons pas rendre la colonne aussi large que possible « juste au cas où », en donnant une taille fixe au tableau. Nous aimerions que la colonne soit aussi petite que possible, mais qu'elle ait la possibilité de s'étendre jusqu'à une largeur maximale et, si cette largeur en question est dépassée, de se transformer en une colonne de style `p` pour que son contenu se répartisse sur plusieurs lignes.

L'extension {ctanpkg}`varwidth`, dont il a été question dans « {doc}`Comment optimiser la largeur d'une minipage? </3_composition/texte/paragraphes/ajuster_la_taille_d_une_minipage>` », offre une solution. Si vous la chargez en même temps que l'incontournable extension {ctanpkg}`array`, elle définira un nouveau type de colonne, `V`, que vous pouvez utiliser comme ceci :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{array}
  \usepackage{varwidth}

\begin{document}
\begin{tabular}{l V{3.5cm} r}
  foo & blah      & bar \\
  foo & blah blah & bar \\
\end{tabular}
\end{document}
```

```latex
\documentclass{article}
  \usepackage{array}
  \usepackage{varwidth}
  \pagestyle{empty}

\begin{document}
\begin{tabular}{l V{3.5cm} r}
  foo & blah      & bar \\
  foo & blah blah & bar \\
\end{tabular}
\end{document}
```

Dans l'exemple précédent, la colonne centrale avait une largeur inférieure à 3,5 cm. Voyons ce qui se passe quand son contenu s'étend :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{array}
  \usepackage{varwidth}

\begin{document}
\begin{tabular}{l V{3.5cm} r}
  foo & blah      & bar \\
  foo & blah blah & bar \\
  foo & blah blah blah blah blah blah
                  & bar \\
\end{tabular}
\end{document}
```

```latex
\documentclass{article}
  \usepackage{array}
  \usepackage{varwidth}
  \pagestyle{empty}

\begin{document}
\begin{tabular}{l V{3.5cm} r}
  foo & blah      & bar \\
  foo & blah blah & bar \\
  foo & blah blah blah blah blah blah
                  & bar \\
\end{tabular}
\end{document}
```

Ici, le contenu de la colonne centrale « s'enroule » sur une deuxième ligne au lieu d'élargir démesurément la colonne qui le contient.

______________________________________________________________________

*Source :* {faquk}`Variable-width columns in tables <FAQ-varwidcol>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,table,tableau,mise en forme des tableaux
```
