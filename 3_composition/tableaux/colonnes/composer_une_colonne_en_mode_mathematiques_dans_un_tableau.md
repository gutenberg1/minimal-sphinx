```{role} latexlogo
```
```{role} tttexlogo
```
# Comment définir une colonne en mode mathématique dans un tableau ?

- De la même façon qu'à la question~\\vref\{colfont}, on utilise les « descripteurs » de colonnes `>` et `<`. Voir l'exemple~\\vref{ex=supinf}, dans lequel la colonne centrale est en mode mathématique.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
