```{role} latexlogo
```
```{role} tttexlogo
```
# Comment passer à la ligne dans une cellule ?

- Dans les colonnes de type paragraphe ('p' dans la définition), on dispose de toutes les commandes usuelles de retour à la ligne, sauf évidemment de `\char92`, qui indique la fin de la **ligne de cellules** du tableau. Selon l'effet désiré, on pourra utiliser : `\newline`, `\par`, ou la classique ligne blanche de séparation de paragraphes.
- Les colonnes de type `c`, `l` ou `r` ne sont pas conçues pour contenir plus d'une ligne. Si l'on tient à la mise en forme (justification, centrage, ...), il faut utiliser une colonne de type `p`, dans laquelle on introduira la commande de formattage désirée. Le problème est alors que les commandes `\raggedleft`, `\raggedright` et `\centering` redéfinissent `\char92`. On se retrouve alors dans la situation inverse de celle de la réponse précédente : lorsque l'on utilise ces commandes (ou au moins leur définition de `\char92`) dans un tableau, la commande `\char92` n'indique plus un changement de ligne dans le tableau, mais un passage à la ligne dans la cellule. La commande `\tabularnewline`, qui est équivalente à `\char92` habituellement, devra alors remplacer celle-ci pour indiquer la fin d'une ligne du tableau.

En combinant tout ceci avec le package {ctanpkg}`array`, qui permet de spécifier dans la définition d'une colonne une commande qui sera systématique appliquée au contenu de chacune des cellules de cette colonne (même technique que {doc}`pour changer la fonte d'une colonne </3_composition/tableaux/colonnes/changer_la_fonte_d_une_colonne>`), on peut écrire ceci :

```latex
% !TEX noedit
\makeatletter
\newcommand\justify{%
  \let\\\@centercr
  \rightskip\z@skip
  \leftskip\z@skip}
\makeatother
\begin{center}
\begin{tabular}{|l>{\justify}p{5cm}|}
\hline
LTL & Logique du temps linéaire : cette logique
      permet d'exprimer des propriétés sur une
      exécution du système. \\
      Le model checking et la satisfaisabilité
      sont alors PSPACE-complets.
      \tabularnewline[3mm]
CTL & Logique du temps arborescent : cette logique
      exprime des propriétés sur l'arbre de
      toutes les exécutions possibles. \\
      Le model checking est P-complet, mais la
      satisfaisabilité est EXPTIME-complète.
      \tabularnewline
\hline
\end{tabular}
\end{center}
```

qui donne après compilation :

```latex
\documentclass[french]{article}
\usepackage{lmodern}
\usepackage{array}
\usepackage{babel}

\makeatletter
\newcommand\justify{%
  \let\\\@centercr
  \rightskip\z@skip
  \leftskip\z@skip}
\makeatother

\begin{document}
\thispagestyle{empty}

\begin{tabular}{|l>{\justify}p{5cm}|}
\hline
LTL & Logique du temps linéaire : cette logique
      permet d'exprimer des propriétés sur une
      exécution du système. \\
      Le model checking et la satisfaisabilité
      sont alors PSPACE-complets.
      \tabularnewline[3mm]
CTL & Logique du temps arborescent : cette logique
      exprime des propriétés sur l'arbre de
      toutes les exécutions possibles. \\
      Le model checking est P-complet, mais la
      satisfaisabilité est EXPTIME-complète.
      \tabularnewline
\hline
\end{tabular}
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,mise en forme de tableaux,saut de ligne dans un tableau
```
