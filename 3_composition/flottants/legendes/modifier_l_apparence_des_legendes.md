```{role} latexlogo
```
```{role} tttexlogo
```
# Comment modifier la commande « `\caption` » ?

S'il s'agit juste de modifier les mots « Figure » et « Table » qui sont affichés avant le titre de la légende, il suffit de redéfinir les commandes `\figurename` et `\tablename`, respectivement.

:::{warning}
Lorsqu'on utilise le package {ctanpkg}`babel`, cependant, c'est un peu plus compliqué : en effet, {ctanpkg}`babel` redéfinit lui-même ces deux commandes *à chaque changement de langue*, et les modifications risquent donc d'être perdues.

Pour contourner ce problème, on pourra écrire :

```latex
% !TEX noedit
\renewcommand*\frenchfigurename{%
    {\scshape Figure}%
}
\renewcommand*\frenchtablename{%
    {\scshape Tableau}%
}
```
:::

S'il s'agit de changer la mise en page de la légende, plusieurs solutions existent, suivant ce que l'on veut faire :

- L'extension {ctanpkg}`hangcaption` définit deux commandes équivalentes, `\hangcaption` et `\isucaption`, qui permettent de limiter la largeur de la légende (en redéfinissant `\captionwidth`), et de composer le texte de la légende en retrait, pour l'aligner sur le numéro de la figure ou du tableau. Notez qu'il n'est intégré dans aucune distribution, car l'extension suivante a les mêmes fonctionnalités ;
- L'extension {ctanpkg}`caption` permet de redéfinir beaucoup plus de choses. Avec l'option `format=hang` et la longueur `\captionwidth`, elle permet de faire ce que fait l'extension {ctanpkg}`hangcaption`. Les commandes `\captionfont`, `\captionlabeldelim`, `\captionlabelsep`... permettent de configurer très précisément le comportement de `\caption` ;
- L'extension {ctanpkg}`topcapt` permet de placer la légende au-dessus de la figure, grâce à la commande `\topcaption`. L'option `plaintop` du package {ctanpkg}`float` permet également d'obtenir ce comportement ;
- Les utilisateurs de classes {ctanpkg}`koma-script` trouveront les commandes concernant les tableaux dans la section 3.20 de la documentation (2.20 de la [traduction française](https://framabook.org/koma-script/)).

Exemples de redéfinitions de légendes :

```{eval-rst}
.. todo:: *Trouver pourquoi le code ne compile pas sur le serveur alors qu'il compile sur mon ordinateur*
```

```latex
% !TEX noedit
\documentclass[french]{article}
\usepackage[T1]{fontenc}
\usepackage{hangcaption}
\usepackage{caption}
\usepackage{topcapt}
\usepackage{babel}

\begin{document}
Du texte, du texte, du texte, du texte,
du texte, du texte, du texte, du texte,
du texte, et encore du texte...
\begin{figure}[!ht]
\centerline{\fbox{Une figure, ici}}
\setlength\captionwidth{6cm}
\hangcaption{Ça, c'est la première figure,
 qui utilise la commande \texttt{\string\hangcaption}.}
\end{figure}

Du texte, du texte, du texte, du texte,
du texte, du texte, du texte, du texte,
du texte, et encore du texte...
\begin{figure}[!ht]
\centerline{\fbox{Une figure, ici}}
\captionsetup{width=9cm,font=it}
\caption{Ça, c'est la deuxième figure,
 qui utilise la commande \texttt{\string\caption}
 définie par l'extension \textsf{caption}.}
\end{figure}

Du texte, du texte, du texte, du texte,
du texte, du texte, du texte, du texte,
du texte, et encore du texte...

\begin{figure}[!ht]
\topcaption{Ça, c'est la troisième figure,
 qui utilise la commande \texttt{\string\topcaption}
 définie dans l'extension \textsf{topcapt}.}
\centerline{\fbox{Une figure, ici}}
\end{figure}

Du texte, du texte, du texte, du texte,
du texte, du texte, du texte, du texte,
du texte, et encore du texte...
\end{document}
```

```latex
\documentclass[french]{article}
\usepackage[T1]{fontenc}
\usepackage[width=9cm]{geometry}
\usepackage{caption}
\usepackage{topcapt}
\usepackage{babel}

\pagestyle{empty}

\begin{document}
Du texte, du texte, du texte, du texte,
du texte, du texte, du texte, du texte,
du texte, et encore du texte...
\begin{figure}[!ht]
\centerline{\fbox{Une figure, ici}}
\setlength\captionwidth{6cm}
\hangcaption{Ça, c'est la première figure,
 qui utilise la commande \texttt{\string\hangcaption}.}
\end{figure}

Du texte, du texte, du texte, du texte,
du texte, du texte, du texte, du texte,
du texte, et encore du texte...
\begin{figure}[!ht]
\centerline{\fbox{Une figure, ici}}
\captionsetup{width=9cm,font=it}
\caption{Ça, c'est la deuxième figure,
 qui utilise la commande \texttt{\string\caption}
 définie par l'extension \textsf{caption}.}
\end{figure}

Du texte, du texte, du texte, du texte,
du texte, du texte, du texte, du texte,
du texte, et encore du texte...

\begin{figure}[!ht]
\topcaption{Ça, c'est la troisième figure,
 qui utilise la commande \texttt{\string\topcaption}
 définie dans l'extension \textsf{topcapt}.}
\centerline{\fbox{Une figure, ici}}
\end{figure}

Du texte, du texte, du texte, du texte,
du texte, du texte, du texte, du texte,
du texte, et encore du texte...
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,légendes des figures,légendes des tableaux,mise en forme des légendes
```
