```{role} latexlogo
```
```{role} tttexlogo
```
# Comment utiliser la commande « `\caption` » hors d'un environnement flottant ?

La commande `\caption` ne peut pas être utilisée en dehors d'un environnement `figure` ou `table`, pour la simple raison qu'elle a besoin de savoir à quel type de flottant elle s'applique (pour choisir entre `\figurename` ou `\tablename`).

L'extension {ctanpkg}`caption` définit une commande, `\captionof`, qui prend deux arguments :

- le premier indique le type (`figure` ou `table`) à utiliser ;
- le deuxième contient la légende elle-même.

Voici un exemple de cette commande :

```latex
\documentclass{report}
  \usepackage[width=8cm]{geometry}
  \usepackage[francais]{babel}
  \usepackage{caption}
  \pagestyle{empty}

\begin{document}
Du texte... Du texte... Du texte...
Du texte... Du texte... Du texte...
Du texte... Du texte... Du texte...

\begin{center}
\begin{tabular}{|c|}
\hline
Ce tableau n'est pas un flottant. \\
\hline
\end{tabular}
\captionof{table}{Tableau non flottant.}
\label{montableau}
\end{center}

Du texte... Du texte... Du texte...
Du texte... Du texte... Du texte...
Du texte... Du texte... Du texte...
Et une référence au tableau~1.
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,légende d'un tableau,légende d'une figure,mise en forme
```
