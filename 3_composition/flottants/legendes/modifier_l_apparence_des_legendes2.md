```{role} latexlogo
```
```{role} tttexlogo
```
# Comment changer la mise en forme des légendes ?

Les changements de style des légendes peuvent être effectués directement, en redéfinissant les commandes qui produisent la légende. Ainsi, par exemple, `\fnum@figure` (qui produit le numéro de flottant des figures) peut être redéfini, dans un fichier `.sty` qui vous sera propre, ou entre les commandes makeatletter--makeatother[2_programmation/macros/makeatletter_et_makeatother](/2_programmation/macros/makeatletter_et_makeatother) :

```latex
% !TEX noedit
\renewcommand{\fnum@figure}{\textbf{Fig.~\thefigure}}
```

ce qui aura pour effet de faire apparaître le nombre en caractères gras (la définition d'origine utilise [\\figurename](/3_composition/langues/traduire_le_titre_de_table_des_matieres_ou_bibliographie)). Des changements plus élaborés peuvent être effectués en modifiant la commande`\caption`, mais ce travail devient vite délicat, et il est plutôt recommandé de se servir des extensions développées à cet effet.

L'extension {ctanpkg}`float` permet de contrôler l'apparence des légendes, bien qu'elle soit principalement conçue pour la création de types de flottants non standards. Les extensions {ctanpkg}`caption` et {ctanpkg}`ccaption` (avec deux `c`) fournissent diverses options de formatage.

> {ctanpkg}`ccaption` fournit également des légendes « qui se continuent » et des légendes qui peuvent être placées en dehors des environnements de flottants. L'extension (très simple) {ctanpkg}`capt-of` permet également des légendes en dehors d'un environnement de flottant. Notez qu'il faut être prudent lorsque vous faites des choses qui supposent que les flottants apparaissent dans un certain ordre (par exemple quand une légende est la suite d'une légende précédente), notamment si vous mélangez des légendes associées à des flottants et des légendes non flottantes.

La classe {ctanpkg}`memoir` inclut les fonctionnalités du paquetage {ctanpkg}`ccaption`; les classes {ctanpkg}`KOMA-script` fournissent également une vaste palette de commandes pour formater les légendes.

:::{note}
L'extension {ctanpkg}`caption2` a été recommandée à un moment, étant plus avancée que {ctanpkg}`caption`; mais le développement de {ctanpkg}`caption` a repris, et son utilisation est de nouveau conseillée.

L'extension {ctanpkg}`caption2` ne reste disponible que pour permettre la compilation d'anciens documents.
:::

______________________________________________________________________

*Source :* {faquk}`The style of captions <FAQ-captsty>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,légendes des flottants,mise en forme des légendes,style des captions
```
