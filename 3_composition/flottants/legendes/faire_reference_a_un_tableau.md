```{role} latexlogo
```
```{role} tttexlogo
```
# Comment nommer un tableau ?

La méthode est identique à celle présentée pour la question « {doc}`Comment nommer une figure ? </3_composition/flottants/legendes/faire_reference_a_une_figure>` » : la commande `\caption` a exactement le même comportement dans un environnement `table` que dans un environnement `figure`, si ce n'est qu'elle écrira « Table » au lieu de « Figure » devant le numéro de tableau. On peut cependant redéfinir cela, voir la question « [Comment modifier la commande « \\caption » ?](/3_composition/flottants/legendes/modifier_l_apparence_des_legendes) ».

```{eval-rst}
.. meta::
   :keywords: LaTeX,tableau,table,flottant,mise en forme,légende
```
