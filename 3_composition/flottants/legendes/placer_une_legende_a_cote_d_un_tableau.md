```{role} latexlogo
```
```{role} tttexlogo
```
# Comment placer une légende à côté d'une figure ?

## Avec un changement d'orientation de la légende

Cette première solution est détaillée à la question « {doc}`Comment changer l'orientation d'une légende ? </3_composition/flottants/legendes/changer_l_orientation_d_une_legende>` ».

## Sans changement d'orientation de la légende

Dans ce cas, il est possible de s'inspirer de la référence ci-dessus, et utiliser `minipage`. La commande `\caption` est placée à l'intérieur d'une `minipage`. Par exemple :

```latex
\documentclass{article}
  \usepackage[width=10cm]{geometry}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
Du texte... Du texte... Du texte...
Du texte... Du texte... Du texte...
Du texte... Du texte... Du texte...

\begin{table}[!ht]
\begin{minipage}{3cm}
  \caption{La légende, placée à côté du
  tableau, avec une largeur fixée.}\label{test}
\end{minipage}
\centerline{%
\begin{tabular}{|c|}
\hline
Un petit tableau \\
Avec quelques lignes \\
Pour voir comment \\
est placée la légende.\\
\hline
\end{tabular}}
\end{table}

Du texte... Du texte... Du texte...
Du texte... Du texte... Du texte...
Du texte... Du texte... Du texte...

Et pour finir, une référence au tableau 1.%~\ref{test}.
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,tableaux,tables,légende,position de la légende
```
