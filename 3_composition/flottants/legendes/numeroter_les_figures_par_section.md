```{role} latexlogo
```
```{role} tttexlogo
```
# Comment numéroter les figures en fonction des sections ?

Par défaut, les numéros des figures (et des tableaux) sont remis à zéro aux changements de chapitre dans la classe {ctanpkg}`book`.

## En étendant le comportement de la classe « book »

Afin d'étendre ce comportement aux sections, par exemple dans la classe {ctanpkg}`article`, il faut insérer les lignes suivantes dans le préambule du document :

```latex
% !TEX noedit
\makeatletter
\@addtoreset{figure}{section}
\makeatother
\renewcommand{\thefigure}{\ifnum\value{section}>0
    \thesection.\fi\arabic{figure}}
```

## Avec l'extension « remreset »

Inversement, pour que les figures soient numérotées continûment et non remises à zéro à chaque chapitre, il faut utiliser l'extension {ctanpkg}`remreset` et les lignes suivantes en préambule :

```latex
% !TEX noedit
\makeatletter
\@removefromreset{figure}{section}
\makeatother
\renewcommand{\thefigure}{\arabic{figure}}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,légendes,numérotation
```
