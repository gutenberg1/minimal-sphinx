```{role} latexlogo
```
```{role} tttexlogo
```
# Pourquoi ma figure (ou mon tableau) n'est-elle pas centrée ?

Vous vouliez un flottant centré horizontalement, mais {latexlogo}`LaTeX` ignore votre environnement `center` ? Vous avez probablement écrit quelque chose comme ça :

```latex
% !TEX noedit
\begin{center}
  \begin{figure}
    ...
  \end{figure}
\end{center}
```

Quand il a rencontré ce code, LaTeX a pris la figure en charge, et il va s'occuper de l'afficher là où il en a envie (ce serait la même chose avec un tableau, car c'est le principe même des flottants) ; du coup, la figure n'est plus à l'intérieur de l'environnement `center` que vous pensiez utiliser. Par conséquent, cet environnement n'a plus de raison d'être... sauf {doc}`mettre le bazar dans votre espacement vertical </3_composition/flottants/trop_d_espace_dans_un_flottant>`.

La solution est la même que celle proposée {doc}`dans cette réponse </3_composition/flottants/trop_d_espace_dans_un_flottant>`, à savoir que tout les commandes contrôlant le rendu d'un flottant tel que `figure` ou `table` doivent se trouver **à l'intérieur** de l'environnement du flottant. Vous devrez donc plutôt écrire :

```latex
% !TEX noedit
\begin{figure}
  \centering
  ...
\end{figure}
```

(ce sera le même principe pour un environnement `table`).

______________________________________________________________________

*Source :* {faquk}`Why is my table/figure/... not centred? <FAQ-centre-flt>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,mise en page,tableaux,figures,environnement centré,comment centrer un tableau,comment centrer une figure
```
