```{role} latexlogo
```
```{role} tttexlogo
```
# Comment est géré le positionnement des flottants ?

## Avec les commandes de base

Les environnements `figure` et `table` admettent un argument optionnel, qui indique les placements *acceptés* pour la figure ou le tableau. Cet argument est composé des symboles suivants :

- « `h` » (pour *here*, ici) qui désigne l'emplacement courant dans le texte ;
- « `t` » (pour *top*, en haut) qui désigne le haut de la page courante ;
- « `b` » (pour *bottom*, en bas) qui désigne le bas de la page courante ;
- « `p` » (pour page) qui désigne une page dédiée pour la figure ou la table.
- « `!` » afin d'indiquer à {latexlogo}`LaTeX` de ne pas tenir compte des limites imposées sur le nombre de flottants par page (voir la question « {doc}`Comment modifier le nombre de figures par page ? </3_composition/flottants/positionnement/modifier_le_nombre_de_flottants_par_page>` »).

Voici un exemple de code où l'utilisateur souhaite avoir sa figure placée de préférence en bas de page ou sur une page dédiée, sans être trop limité par les contraintes de nombre de flottants par page :

```latex
% !TEX noedit
\begin{figure}[bp!] % L'argument détaillé ci-dessus
\centering % Pour centrer la figure
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH !
\caption{Beaucoup de bruit pour rien}
\end{figure}
```

Suivant les différents placements possibles, {latexlogo}`LaTeX` s'arrange pour satisfaire la demande (l'ordre des lettres `h`, `t`, `b` ou `p` n'a pas d'importance). L'algorithme de placement des flottants est très compliqué : la réalité ne reflète pas exactement ce qui est décrit ci-dessus.

## Avec l'extension « float »

L'extension {ctanpkg}`float` propose une option de placement supplémentaire, « `H` » qui *force* le placement du flottant à l'endroit où il est inséré dans le source du document. Cette option est décrite à la question « [Comment imposer un emplacement à un flottant ?](/3_composition/flottants/positionnement/forcer_la_position_d_un_flottant2) ».

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,positionnement,float
```
