```{role} latexlogo
```
```{role} tttexlogo
```
# Comment placer des figures face à face en recto-verso ?

## Avec l'extension « dpfloat »

L'extension {ctanpkg}`dpfloat` permet de s'assurer qu'un flottant sera placé sur une page « de gauche », c'est-à-dire une page de numéro pair, ou sur une page « de droite ».

L'exemple fournit dans la {texdoc}`documentation <dpfloat>` de l'extension est le suivant :

```latex
% !TEX noedit

  \begin{figure}[p]% sera sur une page de gauche
     \begin{leftfullpage}
       ...
     \end{leftfullpage}
  \end{figure}
  \begin{figure}[p]% sera sur une page de droite
     \begin{fullpage}
       ...
     \end{fullpage}
  \end{figure}
```

L'environnement `leftfullpage` indiquera à {latexlogo}`LaTeX` que, s'il se trouve sur une page impaire, il doit repousser le flottant à la page suivante.

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,positionnement,gauche,droite
```
