```{role} latexlogo
```
```{role} tttexlogo
```
# Comment gérer des sous-figures sur plusieurs pages ?

Rien n'est prévu, semble-t-il, pour traiter ce problème. Il est cependant assez simple à résoudre « à la main », en modifiant les valeurs des compteurs. L'exemple ci-dessous indique comment procéder.

```latex
% !TEX noedit
\begin{page}
\documentclass{article}
\usepackage{graphicx,subfigure}
\usepackage{boxedminipage}
\begin{document}

Ici, je mets le texte qui précède les
figures~\ref{fig+graphics+a}
et~\ref{fig+graphics+b}.
\end{page}
\begin{page}
  \begin{figure}
    \centering
    \subfigure[Première figure]{%
      \label{fig+graphics+a}% label for subfigure
      \begin{boxedminipage}{\linewidth}
       \rule[-.5\textheight]{0pt}{.8\textheight}
       \centerline{Un premier flottant}
      \end{boxedminipage}}
    \caption{Deux grandes figures}%
    \label{fig+graphics}% label for figure
  \end{figure}
\end{page}
\begin{page}
  \addtocounter{figure}{-1}
  \begin{figure}
    \addtocounter{subfigure}{1}
    \centering
    \subfigure[Deuxième figure]{%
      \label{fig+graphics+b}% label for subfigure
      \begin{boxedminipage}{\linewidth}
       \rule[-.5\textheight]{0pt}{.8\textheight}
       \centerline{Un deuxième flottant}
      \end{boxedminipage}}
    \caption{Deux grandes figures (suite)}%
  \end{figure}
\end{document}
\end{page}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,sous-figures,plusieurs pages
```
