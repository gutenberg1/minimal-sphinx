```{role} latexlogo
```
```{role} tttexlogo
```
# Comment modifier l'orientation de flottants ?

Si la présente question traite du cas général des flottants, une autre question traite le cas un peu plus spécifique des tableaux « [Comment modifier l'orientation d'un tableau ?](/3_composition/tableaux/changer_l_orientation_d_un_tableau) ».

## Avec l'extension « rotating »

L'extension {ctanpkg}`rotating` permet de réaliser ce genre de choses. Cependant, il est à noter que le format DVI ne supporte pas ces manipulations, qui se font au niveau du PS. Certains interpréteurs DVI montreront alors la figure dans son orientation normale.

Les environnement `sidewaystable` et `sidewaysfigure` permettent de changer l'orientation d'un flottant. Ils créent des flottants, qui seront nécessairement inclus sur une page à part, du fait du changement d'orientation. Il n'est donc pas possible d'indiquer des préférences quant à la position de ce flottant. La légende sera également automatiquement tournée.

Dans l'exemple ci-dessous, l'utilisation de l'extension {ctanpkg}`fancyhdr` supprime le numéro des pages ne contenant que des flottants :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage[french]{babel}
  \usepackage{fancyhdr}
  \usepackage{rotating}

\fancyfoot[C]{\iffloatpage{}{\thepage}}
\fancyfoot[LO,RO]{}
\fancyhead[LO,RO,C]{}
\renewcommand\headrulewidth{0pt}
\pagestyle{fancy}

\begin{document}

\section{Une section}
Un peu de texte pour commencer...
Un peu de texte pour commencer...
Un peu de texte pour commencer...
Un peu de texte pour commencer...

\begin{sidewaysfigure}
\framebox{\Huge Une figure tournée\\
à $90$~degrés.}
\caption{Une figure tournée}
\end{sidewaysfigure}

\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,tableaux,flottants,mise en forme des tableaux,faire tourner un tableau,pivoter un tableau,pivoter une figure
```
