```{role} latexlogo
```
```{role} tttexlogo
```
# Comment mettre un commentaire à côté d'un flottant ?

Il y a plusieurs façons de comprendre la question :

- s'il s'agit de placer du texte à côté de la figure, de telle sorte que la figure soit *plongée* dans le texte, les réponses à la question « {doc}`Comment fondre une image dans du texte ? </3_composition/flottants/habiller_une_image_avec_du_texte>` » devraient convenir ;
- s'il s'agit de placer une légende à côté d'une figure, c'est l'objet de la question « {doc}`Comment placer une légende à côté d'une figure ? </3_composition/flottants/legendes/placer_une_legende_a_cote_d_un_tableau>` » ;
- si, enfin, il s'agit de mettre du texte à côté d'une figure, deux solutions existent : mettre la figure dans un tableau à deux colonnes, l'autre colonne étant destinée à recevoir le commentaire ; ou mettre la figure dans une `minipage` et le commentaire dans une autre, en s'arrangeant pour que la largeur cumulée ne soit pas trop grande.

L'exemple ci-dessous montre ces deux dernières possibilités :

```latex
% !TEX noedit
\documentclass{report}
  \usepackage[french]{babel}

\begin{document}
Un peu de texte autour, pour faire bien...
Un peu de texte autour, pour faire bien...
Un peu de texte autour, pour faire bien...
Un peu de texte autour, pour faire bien...
\begin{figure}[!ht]
  \centerline{%
  \begin{tabular}{lc}
    Un commentaire &
    \parbox[c]{4cm}{\framebox{\Huge Une figure}}
  \end{tabular}}
  \caption{Premier exemple}
\end{figure}
Un peu de texte autour, pour faire bien...
Un peu de texte autour, pour faire bien...
Un peu de texte autour, pour faire bien...
\begin{figure}[!ht]
  \centerline{%
  \begin{minipage}{3cm}
    Un commentaire très long, qui sera coupé comme il faut à une largeur fixée.
  \end{minipage}\hspace{1cm}
  \begin{minipage}{4cm}
    \centerline{\framebox{\Huge Une figure}}
  \end{minipage}}
  \caption{Deuxième exemple}
\end{figure}
Un peu de texte autour, pour faire bien...
Un peu de texte autour, pour faire bien...
Un peu de texte autour, pour faire bien...
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,mise en page
```
