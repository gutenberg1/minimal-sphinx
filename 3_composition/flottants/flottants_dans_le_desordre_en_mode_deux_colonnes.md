```{role} latexlogo
```
```{role} tttexlogo
```
# Pourquoi les flottants sont-ils numérotés dans le désordre en mode deux colonnes ?

Lorsque {latexlogo}`LaTeX` ne peut pas placer un flottant immédiatement dans le document, il l'enregistre dans l'une des nombreuses « listes d'attente ». Si un autre flottant du même type se présente et que la liste d'attente pour ce type n'est pas vide, le nouveau flottant doit attendre que tout ce qui se trouve devant lui dans la liste trouve sa place.

Avant sa version 2015, {latexlogo}`LaTeX` avait des listes distinctes pour les flottants en mode simple colonne et les flottants en mode deux colonnes; cela impliquait que les figures à une colonne pouvaient passer devant les figures à deux colonnes (ou l'inverse), et on pouvait donc voir les figures apparaître dans le désordre par rapport au code-source. De même, bien sûr, pour les tableaux, ou pour tout autre type de flottant.

Les développeurs de {latexlogo}`LaTeX` ont reconnu le problème, et ont proposé l'extension {ctanpkg}`fixltx2e` [^footnote-1], pour le résoudre en fusionnant les deux listes d'attente, de sorte que les flottants ne se retrouvent plus dans le désordre.

:::{note}
En 2015, toutes les corrections fournies par {ctanpkg}`fixltx2e` ont été intégrées à {latexlogo}`LaTeX`, donc :

- ce problème de flottant ne devrait plus apparaître ;
- il n'y a plus besoin de charger l'extension {ctanpkg}`fixltx2e`. Si un document fait encore appel à elle, cela n'empêche pas la compilation, mais vous verrez cet avertissement dans le `log` :

```text
Package fixltx2e Warning : fixltx2e is not required with releases after 2015
(fixltx2e)                All fixes are now in the LaTeX kernel.
(fixltx2e)                See the latexrelease package for details.
```
:::

Pour ceux qui utiliseraient encore une vieille distribution de {latexlogo}`LaTeX`, en plus de l'extension générale {ctanpkg}`fixltx2e`, les extensions {ctanpkg}`fix2col` et {ctanpkg}`dblfloatfix` s'occupent également de ce problème. Cette dernière contient également du code pour placer les flottants pleine largeur lorsque {doc}`l'option de placement « [b] » est choisie </3_composition/flottants/placer_les_flottants_en_bas_de_page_en_mode_deux_colonnes>`.

______________________________________________________________________

*Sources :*

- {faquk}`Two-column float numbers out of order <FAQ-2colfltorder>`,
- [How to use « fixltx2e » only when necessary?](https://tex.stackexchange.com/questions/287146/how-to-use-fixltx2e-only-when-necessary)

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,document en colonnes,mode deux-colonnes,mise en forme,ordre des flottants
```

[^footnote-1]: Le nom «  {ctanpkg}`fixltx2e` » vient de « *fix LaTeX2e* » (= « réparer {latexlogo}`LaTeX` »).
