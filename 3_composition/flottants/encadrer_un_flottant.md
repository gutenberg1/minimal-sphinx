```{role} latexlogo
```
```{role} tttexlogo
```
# Comment encadrer un objet flottant ?

## Avec les commandes de base

S'il s'agit juste d'encadrer la figure, et pas la légende, la commande `\framebox` peut suffire.

## Avec l'extension « boxedminipage »

De façon similaire à la commande `\framebox`, l'environnement `boxedminipage` de l'extension {ctanpkg}`boxedminipage` peut encadrer la figure sans la légende.

## Avec l'extension « float »

L'extension {ctanpkg}`float` permet de définir des styles de flottants. En particulier, le style `framed` permet d'obtenir des flottants encadrés. Le style `ruled` permet d'avoir un filet vertical au dessus et au-dessous du flottant. Un exemple est donné dans la question « [Comment définir de nouveaux flottants ?](/3_composition/flottants/definir_de_nouveaux_flottants) ».

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,encadrer
```
