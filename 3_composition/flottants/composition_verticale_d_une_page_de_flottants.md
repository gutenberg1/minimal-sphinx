```{role} latexlogo
```
```{role} tttexlogo
```
# Comment changer l'agencement vertical des pages de flottants ?

Lorsque {latexlogo}`LaTeX` remplit une page avec des flottants, il les centre verticalement sur la page. Certains auteurs n'aiment pas cette disposition. Malheureusement, le contrôle de ce positionnement est caché dans les profondeurs des commandes internes de {latexlogo}`LaTeX`, et il faut donc faire attention pour modifier ce comportement.

## Paramètres de mise en forme

Les pages de flottants utilisent trois longueurs {latexlogo}`LaTeX` pour définir leur mise en forme :

- `\@fptop` définit la distance entre le haut de la page et le haut du premier flottant,
- `\@fpsep` définit la distance entre les flottants, et
- `\@fpbot` définit la distance entre le bas du dernier flottant de la page et le bas de la page.

:::{note}
En fait, la routine de sortie place un saut de hauteur `\@fpsep` au-dessus de chaque flottant, mais les sauts de hauteur `\@fptop` sont toujours suivis d'une correction pour compenser cela.
:::

Les valeurs par défaut de {latexlogo}`LaTeX` sont :

```latex
% !TEX noedit
\@fptop = 0pt + 1fil
\@fpsep = 8pt + 2fil
\@fpbot = 0pt + 1fil
```

de sorte que les espaces s'étendent pour remplir l'espace non occupé par les flottants. S'il y a plus d'un flottant sur la page, les espaces entre eux s'étendront deux fois plus que les espaces en haut et en bas.

## Modification des paramètres

Une fois ceci compris, des modifications élaborées peuvent être faites. Le besoin le plus courante consiste à obtenir des flottants commençant en haut de la page. C'est assez simple à obtenir :

```latex
% !TEX noedit
\makeatletter
\setlength{\@fptop}{0pt}
\makeatother
```

Vous serez peut-être surpris de constater que ce paramètre positionne vos flottants trop haut sur la page. Vous pourrez donc préférer une valeur de `5pt` (à la place de `0pt`) --- c'est à peu près la différence entre `\topskip` et la hauteur du texte normal (`10pt`).

Notez qu'il s'agit d'un paramètre « global ». Le mieux est de le définir dans un fichier de style, ou au moins dans le préambule du document. Effectuer le changement pour une seule page de flottants s'avère assez délicat.

______________________________________________________________________

*Source :* {faquk}`Vertical layout of float pages <FAQ-vertposfp>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,flottants,espacement vertical,page de flottants,tableaux,figures,espacement des figures,espacement des tableaux
```
