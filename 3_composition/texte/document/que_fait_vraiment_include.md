```{role} latexlogo
```
```{role} tttexlogo
```
# Que fait la commande « `\include` » ?

À ses débuts, {latexlogo}`LaTeX` fournissait la commande `\include` pour résoudre le problème des documents longs. Avec les ordinateurs relativement lents de l'époque, la commande associée ''\\includeonly '' représentait une véritable aubaine. Même si, de nos jours, `\includeonly` semble moins intéressant, elle reste utile à certains grands projets. Toutefois, certains utilisateurs sont surpris par le comportement de ces deux commandes : elles sont en fait plus élaborées qu'on le pense.

Pour que `\includeonly` fonctionne, `\include` crée un fichier `aux` pour chaque fichier inclus et constitue un « point de contrôle » des paramètres importants (tels que les numéros de page, figure, tableau et note de bas de page). Or, {doc}`évaluer un numéro de page </3_composition/texte/pages/numerotation_des_pages/le_numero_de_page_est_incorrect>` peut être délicat. Aussi, en conséquence, la commande `\include` *efface* la page en cours avant et après elle. Par ailleurs, ce mécanisme d'inclusion ne fonctionne pas si une commande `\include` apparaît dans un fichier qui est lui-même objet d'une commande `\include` : {latexlogo}`LaTeX` le diagnostique comme une erreur.

Ces principes précisés, nous pouvons maintenant répondre aux deux questions les plus courantes sur `\include` :

- Pourquoi {latexlogo}`LaTeX` place-t-il une page avant et après les commandes `\include` ? Parce qu'il le faut, comme vu plus haut. Si cela vous pose difficulté, remplacez la commande `\include` par `\input`. Vous ne pourrez plus utiliser `\includeonly` mais il est probable que vous n'en ayez pas besoin.
- Pourquoi ne puis-je pas imbriquer les fichiers avec `\include` puisque j'ai toujours pu le faire sous LaTeX 2.09 ? En fait, vous ne pouviez pas, même sous LaTeX 2.09, mais l'échec n'était pas diagnostiqué. Cependant, puisque vous étiez satisfait du comportement sous LaTeX 2.09, remplacez une nouvelle fois les commandes `\include` par des commandes `\input` (avec, selon le cas des commandes `\clearpage` en plus).

______________________________________________________________________

*Source :* {faquk}`What's going on in my « \\include » commands? <FAQ-include>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,commande \\input,command \\include,inclure un document LaTeX,document en plusieurs parties,document maître
```
