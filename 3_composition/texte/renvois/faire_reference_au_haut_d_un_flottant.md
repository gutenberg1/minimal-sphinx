```{role} latexlogo
```
```{role} tttexlogo
```
# Comment faire référence au début d'un flottant et pas au titre ?

Lors de l'utilisation de références avec liens hypertextes, le lien est placé à l'endroit où la commande `\label` est exécutée, donc au niveau de la commande `\caption` pour un objet flottant.

Pour le `\label` apparaisse en haut du flottant, il est possible d'utiliser une solution, de Michel Bovani :

```latex
% !TEX noedit
\begin{figure}[!htbp]
    \refstepcounter{figure}        % Incrémentation du compteur des figures (de 1)
    \label{fig-numero13}           % Récupération du numéro de figure avec \label
    \addtocounter{figure}{-1}      % Décrémentation du compteur de figure (de 1)
    \includegraphics{Figure.pdf}
    \caption{C'est ma figure.}     % Le numéro de figure sera à nouveau incrémenté ici.
\end{figure}
```

L'utilisation de la commande `\refstepcounter` est explicitée à la question « {doc}`Comment faire référence à ses propres compteurs ? </3_composition/texte/renvois/faire_reference_a_ses_propres_compteurs>` ».

```{eval-rst}
.. meta::
   :keywords: LaTeX,références croisées,titre,flottant
```
