```{role} latexlogo
```
```{role} tttexlogo
```
# Comment savoir si une étiquette n'est pas définie ?

Au moment de l'exécution de votre code {latexlogo}`LaTeX`, vous pourriez souhaiter savoir si une étiquette n'est pas définie (même si c'est un état transitoire et que {latexlogo}`LaTeX` gère assez bien cette situation).

## Avec les commandes de base

Une étiquette définie est simplement une commande : `\r@`*nom-étiquette*. Notre question initiale revient donc à vérifier si la commande existe. Avec {latexlogo}`LaTeX`, cela se fait généralement avec la commande de test `\@ifundefined{r@`*nom-étiquette*`}{`*cas-non-déf*`}{`*cas-déf*`}` pour laquelle :

- `nom-étiquette` est le nom d'étiquette que vous avez utilisé dans la commande `\label` ;
- et les deux autres arguments sont les commandes qui seront exécutées si l'étiquette est non définie (`cas-non-déf`) ou si elle est définie (`cas-déf`).

Notez que toute commande qui intègre `\@ifundefined` est naturellement {doc}`fragile </2_programmation/syntaxe/c_est_quoi_la_protection>`. Pour la protéger, consultez la question « {doc}`À quoi sert la « protection » ? </2_programmation/syntaxe/c_est_quoi_la_protection>` ».

Si vous suivez cette piste, vous ne soucierez peut-être pas de l'avertissement de {latexlogo}`LaTeX` concernant les étiquettes non définies à la fin du document. Cependant, si vous y trouvez un intérêt, ajoutez la commande `\G@refundefinedtrue` dans l'argument `cas-non-déf`.

Enfin, bien sûr, rappelez-vous que vous avez affaire à des commandes internes. En cas de doute, consultez la question : « [À quoi servent \\makeatletter et \\makeatother ?](/2_programmation/macros/makeatletter_et_makeatother) ».

## Avec l'extension « labelcas »

Tout ce qui précède peut être évité en utilisant l'extension {ctanpkg}`labelcas` : elle fournit des commandes permettant de d'agir différemment selon l'état d'une étiquette, ou des états d'une liste d'étiquettes. Cette extension, un peu compliquée, s'avère assez puissante.

______________________________________________________________________

*Source :* {faquk}`Finding if a label is undefined <FAQ-labundef>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,commande,macro,programmation,étiquette,définition
```
