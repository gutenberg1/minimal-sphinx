```{role} latexlogo
```
```{role} tttexlogo
```
# Comment obtenir un symbole «fin de paragraphe» ?

Le [pied-de-mouche](https://fr.wikipedia.org/wiki/Pied-de-mouche), « ¶ », marque souvent une fin de paragraphe. Il s'obtient avec la commande `\P` ou la commande `\textparagraph`.

```latex
% !TEX noedit
\documentclass{article}

\begin{document}
\P{} Auparavant, le pied-de-mouche, << \textparagraph{} >>, marquait le début d'un paragraphe.
\end{document}
```

```latex
\documentclass{article}
  \pagestyle{empty}

\begin{document}
\P{} Auparavant, le pied-de-mouche, << \textparagraph{} >>, marquait le début d'un paragraphe.
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,caractère "fin de paragraphe",symbole de paragraphe,symbole de saut de paragraphe,symbole de nouveau paragraphe,pi
```
