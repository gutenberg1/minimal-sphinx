```{role} latexlogo
```
```{role} tttexlogo
```
# Comment obtenir des chiffres elzéviriens ?

Les [chiffres elzéviriens](https://fr.wikipedia.org/wiki/Chiffres_elz%C3%A9viriens), aussi appelés chiffres minuscules, sont des caractères qui ne sont pas présents dans toutes les polices de caractères.

## Pour (La)TeX, avec les fontes par défaut

### Avec la commande « \\oldstylenums »

{latexlogo}`LaTeX` propose une commande `\oldstylenums{⟨chiffres⟩}` qui utilise par défaut un ensemble de chiffres minuscules présents dans la fonte de « mathématique italique » de Donald Knuth. Cette commande est uniquement sensible à la graisse de fonte au moment où elle est employée : les glyphes disponibles correspondent aux versions de graisses « normale » (*medium*) et « grasse » (*bold*) des fontes *Computer Modern Roman*.

```latex
\documentclass{article}
  \pagestyle{empty}

\begin{document}
En \oldstylenums{1452}, Gutenberg commence à
imprimer sa fameuse <<\,Bible à 42 lignes\,>>.
\end{document}
```

### Avec l'extension « textcomp »

{octicon}`alert;1em;sd-text-warning` *L’extension* {ctanpkg}`textcomp` *est classée comme* {doc}`obsolète </1_generalites/histoire/liste_des_packages_obsoletes>`*. Ce qui suit est informatif.*

L'extension {ctanpkg}`textcomp` modifie `\oldstylenums` pour lui faire utiliser les glyphes de la police *Text Companion* (encodage LaTeX TS1) en mode texte. Elle rend également disponible ces chiffres en utilisant des commandes de la forme `\text⟨numéro⟩oldstyle`, par exemple, `\textzerooldstyle`. Il faut toujours noter que toutes les familles de polices ne peuvent pas fournir cette fonctionnalité.

```latex
\documentclass{article}
  \usepackage{textcomp}
  \pagestyle{empty}

\begin{document}
En \oldstylenums{14}\textfiveoldstyle%
\texttwooldstyle, Gutenberg commence à imprimer
sa fameuse <<\,Bible à 42 lignes\,>>.
\end{document}
```

## Pour (La)TeX, avec d'autres fontes

Certaines extensions proposant des polices de caractères (par exemple, {ctanpkg}`mathpazo`) rendent disponibles des chiffres minuscules et proposent des options d'extension pour en faire les chiffres par défaut dans un document comme :

```latex
% !TEX noedit
\usepackage[osf]{mathpazo}  % "osf" signifie "old style figures"
```

L'extension {ctanpkg}`fontinst` générera automatiquement des « versions elzéviriennes » des familles de polices commerciales Adobe Type 1 disposant de versions pour expert.

Il est également possible de créer des polices virtuelles, qui offrent des chiffres minuscules, à partir d'extension de polices. L'extension {ctanpkg}`cmolddig` fournit une version virtuelle des polices originales de Knuth, et les extensions {ctanpkg}`eco` ou {ctanpkg}`hfoldsty` fournissent toutes deux des versions des polices EC. La famille `lm` propose des chiffres minuscules aux utilisateurs d'OpenType (voir ci-dessous), mais nous n'avons pas de correspondance stable pour `lm` avec les chiffres minuscules des versions Adobe Type 1 des polices.

## Pour Xe(La)TeX et Lua(La)TeX

À l'origine, les chiffres minuscules ne se trouvaient que dans les versions expertes de polices commerciales, mais maintenant ils sont de plus en plus largement disponibles. Par exemple, la police *Georgia* de Matthew Carter propose des chiffres minuscules par défaut (cette police a été créée pour être incluse avec certains produits Microsoft et est destinée aux écrans).

Les polices OpenType disposent de deux critères de détermination de la forme des nombres (le critère proportionnel/tabulaire et le critère minuscule/majuscule) et sont couramment utilisées. Un accès complet aux fonctionnalités des polices OpenType est déjà pris en charge par {doc}`XeTeX </1_generalites/glossaire/qu_est_ce_que_xetex>` en utilisant, par exemple, l'extension {ctanpkg}`fontspec`. Un support similaire est également en cours de développement pour {doc}`LuaTeX </1_generalites/glossaire/qu_est_ce_que_luatex>`.

______________________________________________________________________

*Source :* {faquk}`Using "old-style" figures <FAQ-osf>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,chiffre minuscule,chiffre elzévirien,chiffres anciens,chiffres que descendent,chiffres sous la ligne de base
```
