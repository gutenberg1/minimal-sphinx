```{role} latexlogo
```
```{role} tttexlogo
```
# Comment obtenir un « degré » ?

Par défaut, {latexlogo}`LaTeX` fournit une commande `\textdegree`. L'exemple suivant illustre ce que cela donne en montrant également l'utilisation directe du caractère « ° » présent sur le clavier. Il montre aussi le besoin d'une espace insécable devant ce symbole lorsqu'il s'agit de désigner une température.

```latex
% !TEX noedit
\documentclass{article}

\begin{document}
Lorsqu'il fait 20~\textdegree C ou 10~°C, on n'a pas envie de boire du rhum à 45\textdegree.
\end{document}
```

```latex
\documentclass{article}
\pagestyle{empty}

\begin{document}
Lorsqu'il fait 20~\textdegree C ou 10~°C, on n'a pas envie de boire du rhum à 45\textdegree.
\end{document}
```

## Avec l'extension « babel »

Avec l’option `french` de l’extension `babel` sont mises à disposition la commande `\degres` et la commande `\degre`. La seconde est considérée comme à éviter car elle gère moins bien l'espacement.

```latex
% !TEX noedit
\documentclass{article}
\usepackage[french]{babel}

\begin{document}
Lorsqu'il fait 20~\degres C ou 10~\degre C, on n'a pas envie de boire du rhum à 45\degres.
\end{document}
```

```latex
\documentclass[french]{article}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\pagestyle{empty}

\begin{document}
Lorsqu'il fait 20~\degres C ou 10~\degre C, on n'a pas envie de boire du rhum à 45\degres.
\end{document}
```

## Avec l'extension « siunitx »

L’extension {ctanpkg}`siunitx` fournit plusieurs façon d’obtenir le symbole degré.

:::{warning}
Le symbole fourni par {ctanpkg}`siunitx` n’a pas la même taille que celui obtenu avec les méthodes précédentes.
:::

La commande `\celsius` est identique à `\degreCelsius`. Est utilisée ici la commande `\SI`, qui prend deux arguments obligatoire : le nombre et l’unité. La commande `\ang` est destinée à noter les mesures d’angles en degrés, minutes et secondes. Son usage est détourné dans l’exemple suivant.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{siunitx}

\begin{document}
Lorsqu'il fait \SI{20}{\celsius}, on n'a pas envie de boire du rhum à \ang{45}.
\end{document}
```

```latex
\documentclass[french]{article}
\usepackage{siunitx}
\usepackage{babel}
\pagestyle{empty}

\begin{document}
Lorsqu'il fait \SI{20}{\celsius}, on n'a pas envie de boire du rhum à \ang{45}.
\end{document}
```

## Avec l'extension « mathabx »

L'extension {ctanpkg}`mathabx` fournit une commande `\degree`, mais la taille du rond n'est pas la même que celle donnée par la commande `\textdegree`. De plus, elle nécessite une mise en exposant. Voici un exemple :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{mathabx}

\begin{document}
Lorsqu'il fait 20~$^{\degree}$C ou 10~\textdegree C, on n'a pas envie de boire du rhum à 45\textdegree.
\end{document}
```

```latex
\documentclass{article}
\usepackage{mathabx}
\pagestyle{empty}

\begin{document}
Lorsqu'il fait 20~$^{\degree}$C ou 10~\textdegree C, on n'a pas envie de boire du rhum à 45\textdegree.
\end{document}
```

## Avec l'extension textcomp

{octicon}`alert;1em;sd-text-warning` *L’extension* {ctanpkg}`textcomp` *est classée comme* {doc}`obsolète </1_generalites/histoire/liste_des_packages_obsoletes>`*. Ce qui suit est informatif.*

L'extension {ctanpkg}`textcomp` fournit également une commande `\textdegree`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,symbole degré,symbole numéroe,petit zéro,petit rond
```
