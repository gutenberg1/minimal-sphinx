```{role} latexlogo
```
```{role} tttexlogo
```
# Comment obtenir le point exclarrogatif ?

Le [point exclarrogatif](https://fr.wikipedia.org/wiki/Point_exclarrogatif) « ‽ » combine les fonctions de point d'exclamation et de point d'interrogation. En anglais, il se nomme *interrobang*.

Son glyphe est la superposition de ces deux signes de ponctuation.

- L'extension {ctanpkg}`textcomp` fournit la commande `\textinterrobang` :

```latex
\documentclass[14pt]{extarticle}
  \usepackage{textcomp}
  \pagestyle{empty}

\begin{document}
Comment veux-tu que je le sache\textinterrobang
\end{document}
```

- En superposant les deux symboles en question. Voci un exemple en police {ctanpkg}`Fourier <fourier>`, pour changer :

```latex
\documentclass[14pt]{extarticle}
  \usepackage{fourier}
  \pagestyle{empty}

\newcommand{\interrobang}{%
  {\ooalign{?\cr\hss!\kern .025em\hss}}%
}

\begin{document}
Comment veux-tu que je le sache\interrobang
\end{document}
```

- Enfin, le symbole existe dans Unicode, sous le code 8253, il est donc possible de le saisir directement. Mais de nombreuses polices n'ont pas le glyphe correspondant.

______________________________________________________________________

*Source :*

- [Negative space equal the length of a string?](https://tex.stackexchange.com/questions/215254/negative-space-equal-the-length-of-a-string)

```{eval-rst}
.. meta::
   :keywords: LaTeX,mise en page,caractères,point exclarrogatif,‽,interrobang,combine des signes,point d'interrogation,point d'exclamation
```
