```{role} latexlogo
```
```{role} tttexlogo
```
# Comment obtenir des chiffres entourés ?

L'extension {ctanpkg}`pifont` permet, avec la commande `\ding{`*nombre*`}`, d'afficher des symboles de la police [Zapf Dingbats](https://en.wikipedia.org/wiki/Zapf_Dingbats). En particulier, les symboles allant de 172 à 211 représentent quatre dizaines (de \$1\$ à \$10\$) de chiffres entourés.

```latex
\documentclass{article}
  \usepackage[french]{babel}
  \usepackage{pifont}
  \pagestyle{empty}

\begin{document}
Voici les numéros entourés de la police
\emph{Zapf Dingbats} :
\begin{itemize}
\item \ding{172} \ding{173} \ding{174} \ding{175}
 \ding{176} \ding{177} \ding{178} \ding{179}
 \ding{180} \ding{181} ;
\item \ding{182} \ding{183} \ding{184} \ding{185}
 \ding{186} \ding{187} \ding{188} \ding{189}
 \ding{190} \ding{191} ;
\item \ding{192} \ding{193} \ding{194} \ding{195}
 \ding{196} \ding{197} \ding{198} \ding{199}
 \ding{200} \ding{201} ;
\item \ding{202} \ding{203} \ding{204} \ding{205}
 \ding{206} \ding{207} \ding{208} \ding{209}
 \ding{210} \ding{211}.
\end{itemize}
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,dingbats,chiffre entouré,nombres entourés,cercles noirs avec chiffres,pifont
```
