```{role} latexlogo
```
```{role} tttexlogo
```
Comment obtenir le symbole « numéro » ? =====================================

La version actuelle de {latexlogo}`LaTeX` dispose par défaut d'une commande `\textnumero`. Mais ce symbole est à éviter dans un texte français (voir ci-après l'évocation de {ctanpkg}`textcomp`). D'où les solutions présentées ci-après.

# Avec l'extension babel

L'option `french` de l'extension {ctanpkg}`babel <babel-french>` fournit les commandes `\no` et `\No`. En voici un exemple.

```latex
\documentclass{article}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
Voici venir le \no 1 et le \No 2.
\end{document}
```

# Avec l'extension « e-french »

L'extension {ctanpkg}`e-french` fournit des commandes `\numero`, `\Numero`, `\numeros` et `\Numeros`.

//

```{eval-rst}
.. todo:: Ce dernier paragraphe appelle une révision : « Exemple compilable à ajouter. Mise à jour du moteur LaTeX du site à faire ?// »
```

# Avec l'extension « textcomp »

{octicon}`alert;1em;sd-text-warning` *L’extension* {ctanpkg}`textcomp` *est classée comme* {doc}`obsolète </1_generalites/histoire/liste_des_packages_obsoletes>`*. Ce qui suit est informatif, d'autant plus que le symbole obtenu n'est pas recommandé.*

L'extension {ctanpkg}`textcomp` fournit un symbole `\textnumero` qui, si la police active le permet, produit un glyphe qui est une abréviation de numéro plutôt d’usage dans les langues slaves. Ce glyphe n’est pas disponible avec l’extension {ctanpkg}`fourier`.

En voici un exemple :

```latex
\documentclass{article}
  \usepackage{textcomp}
  \pagestyle{empty}

\begin{document}
Voici venir le \textnumero 1.
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,numéro,symbole numéro,symbole degré,petit rond,petit o,o en exposant,petit zéro
```
