```{role} latexlogo
```
```{role} tttexlogo
```
# Comment obtenir un tilde ?

Le symbole du [tilde](https://fr.wikipedia.org/wiki/Tilde), « ~ », est pour {latexlogo}`LaTeX` un caractère interprété comme une {doc}`espace insécable </3_composition/texte/mots/inserer_une_espace_insecable>`. Plusieurs solutions sont ainsi possibles pour afficher dans un texte ce « ~ » :

- en mode texte : les commandes `\textasciitilde` (recommandé), `\string~` ou `\~{}` ;
- en mode mathématique : la commande `\sim`.

S'il s'agit d'écrire l'adresse d'une page web (une URL), mieux vaut alors utiliser la commande `\url` de l'extension {ctanpkg}`url`. Voir la question « {doc}`Comment gérer des adresses web (ou URL) ? </3_composition/texte/mots/mettre_en_forme_des_url_et_des_adresses_electroniques>` ».

Pour obtenir le tilde comme accentuation, il faut utiliser :

- en mode texte : `\~{`*lettre*`}` ;
- en mode mathématique : `\tilde{`*lettre*`}`.

Voici un exemple de ces commandes :

```latex
% !TEX noedit
\documentclass{article}

\begin{document}
Quelques variations sur le même thème : \textasciitilde, \string~, \~{}, $\sim$.

Et quelques accents : \~{a} et $\tilde{a}$.
\end{document}
```

```latex
\documentclass{article}
\pagestyle{empty}
\begin{document}
Quelques variations sur le même thème : \textasciitilde, \string~, \~{}, $\sim$.

Et quelques accents : \~{a} et $\tilde{a}$.
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,tilda,accents,lettres accentuées,caractères accentués
```
