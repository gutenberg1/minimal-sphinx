```{role} latexlogo
```
```{role} tttexlogo
```
# Utiliser des petites capitales grasses avec lmodern

## Quelques substitution de fontes utiles

La méta-famille de fontes latin modern remplace computer modern comme fontes standard. Sous LaTeX, on la charge en appelant le package lmodern. Elle souffre néanmoins pour l'instant deux défauts. Le premier est de n'être pas encore installée automatiquement dans la version de base de la plupart des distributions LaTeX. Elle est néanmoins facile à installer via le gestionnaire de paquets de votre distribution.

Le deuxième est de n'être pas encore aussi complète que computer modern : il manque par exemple les petites capitales grasses en famille romaine. L'objet de cette astuce est d'y remédier en allant chercher les fontes manquantes dans computer modern.

```latex
% !TEX noedit
\rmfamily
\DeclareFontShape{T1}{lmr}{b}{sc}{<->ssub*cmr/bx/sc}{}
\DeclareFontShape{T1}{lmr}{bx}{sc}{<->ssub*cmr/bx/sc}{}
```

Le code précédent demande à LaTeX de substituer aux petites capitales (`sc`) grasses (`b`) ou grasses étendues (`bx`) de *latin modern roman* (`lmr`) les petites capitales grasses étendues de *computer modern roman* (`cmr`). Si vous utilisez la variante sub au lieu de ssub, LaTeX émettra un avertissement avant d'opérer la substitution. Enfin, le `\rmfamily` sert à s'assurer que les macros qui vont être modifiées par les deux lignes suivantes sont préalablement crées.

*Remarque :* cette méthode est contestable dans le sens où elle mélange allègrement deux familles de fontes. Cependant, vu la proximité entre cmr et lmr, ce mélange me paraît légitime ici, et constitue la moins mauvaise solution. Il convient de surveiller l'évolution de latin modern pour ne plus utiliser cette substitution dès qu'elle ne sera plus utile.

*Archived copy:* <https://web.archive.org/web/20170314080827/https://elzevir.fr/imj/latex/tips.html#lmsub>
