```{role} latexlogo
```
```{role} tttexlogo
```
# Comment ajuster la position d'une diacritique sur une lettre donnée  ?

Il arrive parfois qu'une diacritique soit mal positionnée. Si la {doc}`sélection d'un autre moteur de rendu </3_composition/texte/symboles/polices/moteurs_rendu>` ne suffit pas à résoudre ce problème, il est possible d'ajuster la position manuellement.

Nous prendrons l'exemple des caractères `η̣` et `ḍ` dans la fonte Libertinus Serif.

## Avec des commandes de bas niveau (e-)TeX

```latex
\documentclass{article}
\usepackage{fontspec}
\setmainfont{Libertinus Serif}
\pagestyle{empty}
\begin{document}

\Huge η̣ ḍ

\newcommand*{\doteta}{η\llap{ \symbol{"0323}\kern0.1\fontcharwd\font`η}}

\newcommand*{\dotd}{d\llap{ \symbol{"0323}}}

\Huge \doteta{} \dotd

\end{document}
```

Dans les deux cas, nous créons une commande qui affiche le caractère de base (`η` ou `d`) , puis y superpose (avec `\llap`) une espace avec laquelle a été combiné le caractère unicode U+0323 « point en-dessous en combinaison » (*combining dot below*). Dans le cas du `η̣`, il faut en outre déplacer légèrement l'espace pointée vers la gauche grâce à la commande `\kern` : plus précisément, on la déplace d'un dixième de la largeur du caractère `η` dans la fonte courante. Notez que l'argument de `\kern` doit être modifié selon la police utilisé.

Dans le cas du `η̣`, nous avons créé un nom de commande ad hoc car il n'existe pas en Unicode de caractère simple qui y corresponde : cette forme est nécessairement composée de l'addition de `η` (U+03B7) et du caractère U+0323. En revanche, le caractère simple `ḍ` existe (U+1E0D), ce qui permet de l'utiliser directement dans le document {doc}`en en faisant un caractère actif </2_programmation/macros/definir_un_caracteres_comme_une_macro>` (dont le *catcode* est 13, comme `~`) que l'on associe à notre commande.

:::{note}
représenter une espace pointée (par exemple pour indiquer une lettre illisible dans un manuscrit), vous pouvez vous inspirer de la commande suivante, qui définit l'espace comme un espace (ou « boîte ») vide de longueur fixe. Ainsi, la longueur de cette espace ne variera pas d'une ligne à l'autre en fonction de l'espacement entre les mots.

```latex
% !TEX noedit
\newcommand*{\dotspace}{\hbox to 0.4em{}\llap{ \symbol{"0323}\kern-0.04em}}
```
:::

## Avec l'extension stackengine

> {ctanpkg}`stackengine` est une extension dédiée à la superposition de caractères. Il fournit la commande `\stackinset` qui prend les arguments suivants :

- Alignement horizontal du symbole superposé par rapport au symbole de base ;
- Correction horizontale (longueur positive or negative)
- Alignement vertical ;
- Symbole superposé ;
- Symbole de base.

```latex
\documentclass{article}
\usepackage{fontspec}
\setmainfont{Libertinus Serif}
\usepackage{stackengine}
\pagestyle{empty}
\begin{document}

\Huge η̣ ḍ

\newcommand*{\doteta}{\stackinset{c}{0.15\fontcharwd\font`η}{b}{}{ \symbol{"0323}}{η}}

\newcommand*{\dotd}{\stackinset{c}{0.2\fontcharwd\font`d}{b}{-0.32\fontcharht\font`d}{ \symbol{"0323}}{d}}

\Huge \doteta{} \dotd

\end{document}
```

______________________________________________________________________

*Sources :*

- [Small p with dot below](https://tex.stackexchange.com/questions/551268/small-p-with-dot-below)
- [Adjust diacritic position in emph mode](https://tex.stackexchange.com/questions/437006/adjust-diacritic-position-in-emph-mode)

```{eval-rst}
.. meta::
   :keywords: fontes, placement, correction, point, macron, trait, caron, chevron, cédille, accent
```
