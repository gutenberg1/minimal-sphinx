```{role} latexlogo
```
```{role} tttexlogo
```
# Comment utiliser d'anciennes commandes comme `\tenrm` ?

{latexlogo}`LaTeX` 2.09 a défini un vaste ensemble de commandes pour accéder aux polices dont il disposait. Par exemple, la police *Computer Modern* (`cmr`) peut apparaître sous la forme `\fivrm`, `\sixrm`, `\sevrm`, `\egtrm`, `\ninrm`, `\tenrm`, `\elvrm`, `\twlrm`, `\frtnrm`, `\svtnrm`, `\twtyrm` et `\twfvrm`, selon la taille voulue. Ces commandes n'ont jamais été documentées mais certaines extensions les ont néanmoins utilisées pour obtenir les effets dont elles avaient besoin.

Ces commandes n'étant pas publiques, elles n'ont pas été incluses dans {latexlogo}`LaTeX`. Pour changer cela, vous devez également inclure le paquet {ctanpkg}`rawfonts` (qui fait partie de la distribution {latexlogo}`LaTeX`).

______________________________________________________________________

*Source :* {faquk}`Old LaTeX font references such as \\tenrm <FAQ-oldfontnames>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,ancienne commande,\\tenrm,polices de caractères,compatibilité avec LaTeX 2.09,anciens documents
```
