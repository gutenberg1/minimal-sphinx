```{role} latexlogo
```
```{role} tttexlogo
```
# Comment écrire en gras ?

- La commande `\textbf` compose son argument en caractères gras (`bf` veut dire *bold font*) :

```latex
Un \textbf{bel} exemple.
```

Il existe d'autres commandes du même genre pour passer en italique, en petites captiales, etc. Voir la question « {doc}`Comment changer la forme d'une police? </3_composition/texte/symboles/polices/changer_la_forme_d_une_fonte>` ».

## Ancienne syntaxe : `\bf`

Des documentations anciennes peuvent indiquer la commande `\bf` pour passer en caractères gras, avec cette syntaxe :

```latex
Un {\bf bel} exemple.
```

Cette syntaxe est {doc}`obsolète depuis 1994 </3_composition/texte/symboles/polices/pourquoi_ne_pas_utiliser_bf_et_it>`.

## Inventer du gras avec '\\pmb''

Il peut arriver que certains symboles ne soient pas disponibles en gras. Dans ce cas, vous pouvez essayer d'« inventer » leur forme grasse avec la commande `\pmb` du package {ctanpkg}`amsmath`.

Cette commande écrit son argument trois fois l'une sur l'autre, avec un léger décalage, pour donner l'impression visuelle que les traits sont plus épais. C'est le *gras du pauvre* (d'où le nom de la commande : « \**p*oor\* **m***an's* **b***old* »). Le rendu n'est généralement pas très beau (voir l'exemple de texte ci-dessous), et il reste toujours préférable d'utiliser un vrai caractère gras.

```latex
\documentclass{article}
  \usepackage{graphicx}
  \usepackage{lmodern}
  \usepackage{bbding}
  \usepackage{amsmath}
  \pagestyle{empty}

\begin{document}
\scalebox{1.5}{\Peace{} \textbf{\Peace} \pmb{\Peace}}

\scalebox{1.5}{Un \pmb{moins bel} exemple.}

\scalebox{1.5}{Un \textbf{moins bel} exemple.}
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,caractères gras,police grasse,graisse du texte,passer en gras
```
