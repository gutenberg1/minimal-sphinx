```{role} latexlogo
```
```{role} tttexlogo
```
# Comment obtenir un interligne double ?

Le double interligne sert souvent à mettre à disposition un document que vos relecteurs peuvent utiliser pour faire une correction d'épreuves (l'espace permettant d'exprimer plus clairement les remarques et signes de correction). Dans les autres cas, il convient juste de noter que ce type d'espacement ne sera pas sans doute très esthétique sur un document finalisé (la {faquk}`FAQ anglaise <FAQ-linespace>` l'estime d'ailleurs peu cohérent avec l'utilisation de {latexlogo}`LaTeX`).

## Avec les commandes de base

La méthode la plus simple est de changer la définition de la commande gérant l'étirement de l'interligne, `\baselinestretch`, en utilisant :

```latex
% !TEX noedit
\linespread{1.2}
```

ou, de manière équivalente :

```latex
% !TEX noedit
\renewcommand{\baselinestretch}{1.2}
```

Notez que les modifications de `\baselinestretch` ne prennent effet que lorsque vous sélectionnez une nouvelle police. Effectuez donc plutôt la modification dans le préambule avant de sélectionner une police. Par ailleurs, n'essayez pas de changer `\baselineskip` : sa valeur est en effet réinitialisée par n'importe quelle commande de changement de taille des caractères. Ceci explique pourquoi il faut passer par la commande d'étirement.

## Avec l'extension « setspace »

Pour tout document conséquent, il convient d'utiliser l'extension {ctanpkg}`setspace`, en précisant `\singlespacing`, `\onehalfspacing` ou `\doublespacing` Par défaut, elle désactive le double interligne dans les notes de bas de page, les légendes de figure, etc.

{texdoc}`La documentation de setspace <setspace>` apparaît sous forme de commentaires {tttexlogo}`TeX` dans le fichier de l'extension elle-même.

## Avec l'extension « doublespace »

{octicon}`alert;1em;sd-text-warning` *L’extension* {ctanpkg}`doublespace` *est classée comme* {doc}`obsolète </1_generalites/histoire/liste_des_packages_obsoletes>`*. Ce qui suit est informatif.*

L'extension {ctanpkg}`doublespace` permettait d'obtenir aussi cet effet mais elle génèrait souvent des anomalies et n'est plus recommandée.

______________________________________________________________________

*Source :* {faquk}`Double-spaced documents in LaTeX <FAQ-linespace>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,interligne,interligne double,changer l'interligne,agrandir l'interligne,espacer les lignes
```
