```{role} latexlogo
```
```{role} tttexlogo
```
# Comment annuler l'effet des commandes « `\raggedleft` » et « `\raggedright` » ?

{latexlogo}`LaTeX` fournit les commandes `\raggedright` et `\raggedleft` pour mettre le texte au [fer à gauche ou à droite](<https://fr.wikipedia.org/wiki/Justification_(typographie)>) (respectivement), mais aucune pour annuler leur effet. La commande `\centering` est implémentée de la même manière que les commandes `\ragged...` et pose le même problème pour revenir à l'alignement standard.

```latex
\documentclass{article}
  \usepackage[width=6.2cm]{geometry}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
\setlength{\parindent}{0ex}

\raggedleft
Ce texte d'exemple est au fer à droite.
On dit aussi qu'il est \emph{en drapeau à gauche}.
\end{document}
```

## Avec un groupe

La façon la plus courante pour limiter l'effet de ces commandes consiste à les utiliser à l'intérieur d'un groupe (ou bloc), délimité par des accolades ouvrante et fermante, ce qui limite l'action des commandes `\raggedright` et `\raggedleft` à ce seul groupe :

```latex
\documentclass{article}
  \usepackage[width=6.2cm]{geometry}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
\setlength{\parindent}{0ex}

Texte aligné à gauche.

{\raggedleft
Ce texte d'exemple est au fer à droite.
On dit aussi qu'il est \emph{en drapeau à gauche}.
\par
}

Retour à la normale.
\end{document}
```

## Avec des commandes de base

Le code suivant (à insérer dans votre propre fichier « .sty » ou [entre les commandes « \\makeatletter » et « \\makeatother »](/2_programmation/macros/makeatletter_et_makeatother)) définit une commande qui rétablit la justification à droite et à gauche, qui est la justification par défaut de {latexlogo}`LaTeX` :

```latex
% !TEX noedit
\def\flushboth{%
  \let\\\@normalcr
  \@rightskip\z@skip \rightskip\@rightskip
  \leftskip\z@skip
  \parindent 1.5em\relax}
```

Cependant un problème réside dans le paramètre `\parindent` dans ce code : nous le mettons à une valeur fixe (ici `1.5em`). Ceci est nécessaire parce que les deux commandes `\raggedright` et `\raggedleft` mettent `\parindent` à `0`, mais cette solution ne tient pas compte des éventuelles modifications apportées à ce paramètre par ailleurs :

- par exemple en mode `twocolumn`, la valeur par défaut de `\parindent` est `1em` et non `1.5em`;
- vous avez également pu aussi le modifier manuellement.

## Avec l'extension « ragged2e »

Une solution plus souple pour revenir au mode justifié après avoir mis le texte en drapeau est d'utiliser l'extension {ctanpkg}`ragged2e` de Martin Schröder, qui propose une commande `\justifying` annulant l'effet [de ses commandes « \\raggedleft » et « \\raggedright »](/3_composition/texte/paragraphes/justifier_un_paragraphe_a_droite_ou_a_gauche). L'extension fournit également un environnement `justify`, qui permet de justifier une portion de texte au milieu d'un document en drapeau.

:::{important}
Le paramètre `\parindent` est là encore modifié quand vous changez la justification du texte. Si vous souhaitez qu'il reprenne une valeur particulière quand vous appelez `\justifying`, et non la valeur par défaut de LaTeX, stockez cette valeur particulière dans `\JustifyingParindent`.

L'exemple ci-dessous met cette valeur à `0ex` :
:::

```latex
\documentclass{article}
  \usepackage[width=6.2cm]{geometry}
  \usepackage{ragged2e}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
\setlength{\parindent}{0ex}
\setlength{\JustifyingParindent}{0ex}

Texte aligné à gauche.

\raggedleft
Ce texte d'exemple est au fer à droite.
On dit aussi qu'il est \emph{en drapeau à gauche}.

\justifying
Retour à la normale.

\end{document}
```

Si on ne fait pas attention au contenu de `\JustifyingParindent`, on retrouve l'indentation par défaut de LaTeX (ici mise en évidence en rouge) :

```latex
\documentclass{article}
  \usepackage[width=6.2cm]{geometry}
  \usepackage{ragged2e}
  \usepackage{tikz}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
\setlength{\parindent}{0ex}

Texte aligné à gauche.

\raggedleft
Ce texte d'exemple est au fer à droite.
On dit aussi qu'il est \emph{en drapeau à gauche}.

\justifying
\tikz[overlay]\fill[red] (0,0) rectangle (-\parindent,1.5ex);Retour à la normale.

\end{document}
```

______________________________________________________________________

*Sources :*

- [Justification et alignement](http://classes.bnf.fr/ecritures/arret/signe/typo/14.htm) par Danièle Memet,
- {faquk}`Cancelling « \\ragged » commands <FAQ-flushboth>`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,mise en page,justification à droite,justification à gauche,au fer à droite,au fer à gauche,arrêter la justification
```
