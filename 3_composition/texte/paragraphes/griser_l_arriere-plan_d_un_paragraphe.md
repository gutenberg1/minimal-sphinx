```{role} latexlogo
```
```{role} tttexlogo
```
# Comment griser le fond d'un paragraphe ?

## Avec l'extension xcolor

On peut également utiliser l'extension {ctanpkg}`xcolor` :

Utilisation de la commande `\colorbox` :

```latex
\documentclass{article}
\usepackage{xcolor}
\usepackage[width=7cm]{geometry}
\pagestyle{empty}
\begin{document}
Voici \colorbox[gray]{0.8}{un peu de gris !}
\end{document}
```

## Avec l'extension soul

Lorsque l'extension {ctanpkg}`xcolor` a été chargée, il est possible d'utiliser la commande `\hl` de l'extension {ctanpkg}`soul`. La commande `\sethlcolor` permet de définir la couleur avec laquelle le texte sera surligné (voir exemple). Dans le `dvi`, on verra une barre noire. La couleur apparaîtra normalement dans le fichier PS ou PDF.

```latex
\documentclass{article}
\usepackage[width=7cm]{geometry}
\usepackage{color,soul}
\definecolor{gris}{rgb}%
            {.95,.95,.95}
\sethlcolor{gris}
\pagestyle{empty}
\begin{document}
Voici que ce texte \hl{est
surligné en gris clair.}
\end{document}
```

## Avec l'extension shadethm

L'extension {ctanpkg}`shadethm` défini l'environnement `shadebox` qui permet de griser un ou plusieurs paragraphes, ceux-ci pouvant contenir les listes et des formules mathématiques, comme l'illustre l'exemple ci-dessous :

```latex
\documentclass{article}
\usepackage[width=7cm]{geometry}
\usepackage{shadethm}
\pagestyle{empty}
\begin{document}
Il est important de noter que
\begin{shadebox}
  \begin{equation}
    0+0 = 0
  \end{equation}
\end{shadebox}
n'est-ce pas ?
\end{document}
```

## Avec l'extension psboxit

L'extension {ctanpkg}`psboxit`, et en particulier l'environnement `boxitpara`, permet d'obtenir du gris ou de la couleur par insertion de code Postcript.

```{eval-rst}
.. todo:: *Ajouter des exemples compilés sur cette extension et les suivantes.*
```

## Avec l'extension shadbox

L'extension {ctanpkg}`shadbox` permet de griser toute boîte, texte, figure...

## Avec l'extension shading

L'extension {ctanpkg}`shading` permet de griser un paragraphe.

## Avec l'extension shade

L'extension {ctanpkg}`shade` de Peter Schmitt définit une commande `\shade` permettant d'obtenir cet effet.

## Avec l'extension myColor

L'extension [myColor](https://texnik.dante.de/cgi-bin/mainFAQ.cgi?file=color/color#paragraph) (sur le site de Dante) utilise une minipage pour permettre de changer la couleur d'un ou plusieurs paragraphes. En voici le code fournissant la commande `\cminipage`.

```latex
\RequirePackage{xcolor,calc}
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesPackage{myColor}
              [2000/10/26 v1.0 LaTeX package for colored paragraphs.]
\newcommand{\cmcolor}{}
\newenvironment{cminipage}[1][white]%
  {%\setlength{\fboxsep}{-\fboxrule}
   \renewcommand{\cmcolor}{#1}\noindent%
   \begin{lrbox}{\@tempboxa}%
     \begin{minipage}{\linewidth-2\fboxsep}}%
  {  \end{minipage}%
   \end{lrbox}%
   \colorbox{\cmcolor}{\usebox{\@tempboxa}}}%
%%
%%
%% End of file `myColor.sty'.
```

```{eval-rst}
.. meta::
   :keywords: Format DVI,LaTeX
```
