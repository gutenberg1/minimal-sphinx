```{role} latexlogo
```
```{role} tttexlogo
```
# Comment définir des tabulations ?

Il faut utiliser l'environnement `tabbing` qui permet de placer des marques d'alignement dans un texte.

```latex
\begin{tabbing}
   Voici \= des \= marques \= de tabulation. \\
   \> Là je m'aligne sur la première. \\
   \> \> \> Là sur la troisième. \\
   \hspace{3cm} \= \hspace{2cm} \= \kill
   Un \> autre \> exemple.
\end{tabbing}
```

L'utilisation de cet environnement est développée à la question « {doc}`Comment créer un tableau avec des tabulations ? </3_composition/tableaux/tabulations/composer_un_tableau_avec_des_tabulations>` ».

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
