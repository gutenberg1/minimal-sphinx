```{role} latexlogo
```
```{role} tttexlogo
```
# Comment modifier l'interligne d'un document ?

:::{important}
Pour améliorer la lisibilité d'une document, il est souvent plus efficace et esthétique de diminuer la longueur des lignes plutôt qu'agrandir l'interligne.
:::

## Avec l'extension « setspace »

L'extension {ctanpkg}`setspace` est faite pour préparer des documents en double interligne (ou interligne 1,5). Elle définit :

- des commandes à mettre dans le préambule, pour modifier tout le document : `\singlespacing`, `\onehalfspacing` et `\doublespacing` ;
- des environnements pour une modification locale : `singlespace`, `onehalfspace` et `doublespace`.

Un exemple complet est fourni ci-dessous.

Elle gère également de façon homogène les autres espacements verticaux existant dans un document (tableaux, notes de bas de page...), même s'il reste parfois quelques ajustements à faire.

**Interligne simple**

______________________________________________________________________

Commande : `\singlespacing`

Environnement : `\begin{singlespace}`...`\end{singlespace}`

______________________________________________________________________

```latex
\documentclass{article}
  \usepackage[width=5cm,height=8cm]{geometry}
  \usepackage{lmodern}
  \usepackage{microtype}
  \usepackage{booktabs}
  \usepackage{setspace}
  \usepackage[french]{babel}
  \pagestyle{empty}

\singlespacing
\begin{document}

Longtemps, je me suis couché de bonne heure. Parfois, à peine ma bougie éteinte, mes yeux se fermaient si vite que je n'avais pas le temps de me dire : « Je m'endors. »\footnote{Une demi-heure après, la pensée qu'il était temps de chercher le sommeil éveillait le narrateur.}

\medskip
\begin{tabular}{l cc}
\toprule
  & Coucher & Lever \\
\cmidrule(lr){2-3}
Lundi & 19h05 & \dots \\
Mardi & 19h20 & \dots \\
\bottomrule
\end{tabular}
\end{document}
```

**Interligne 1,5**

______________________________________________________________________

Commande : `\onehalfspacing`

Environnement : `\begin{onehalfspace}`...`\end{onehalfspace}`

______________________________________________________________________

```latex
\documentclass{article}
  \usepackage[width=5cm,height=8cm]{geometry}
  \usepackage{lmodern}
  \usepackage{microtype}
  \usepackage{booktabs}
  \usepackage{setspace}
  \usepackage[french]{babel}
  \pagestyle{empty}

\onehalfspacing
\begin{document}

Longtemps, je me suis couché de bonne heure. Parfois, à peine ma bougie éteinte, mes yeux se fermaient si vite que je n'avais pas le temps de me dire : « Je m'endors. »\footnote{Une demi-heure après, la pensée qu'il était temps de chercher le sommeil éveillait le narrateur.}

\medskip
\begin{tabular}{l cc}
\toprule
  & Coucher & Lever \\
\cmidrule(lr){2-3}
Lundi & 19h05 & \dots \\
Mardi & 19h20 & \dots \\
\bottomrule
\end{tabular}
\end{document}
```

**Interligne double**

______________________________________________________________________

Commande : `\doublespacing`

Environnement : `\begin{doublespace}`...`\end{doublespace}`

______________________________________________________________________

```latex
\documentclass{article}
  \usepackage[width=5cm,height=8cm]{geometry}
  \usepackage{lmodern}
  \usepackage{microtype}
  \usepackage{booktabs}
  \usepackage{setspace}
  \usepackage[french]{babel}
  \pagestyle{empty}

\doublespacing
\begin{document}

Longtemps, je me suis couché de bonne heure. Parfois, à peine ma bougie éteinte, mes yeux se fermaient si vite que je n'avais pas le temps de me dire : « Je m'endors. »\footnote{Une demi-heure après, la pensée qu'il était temps de chercher le sommeil éveillait le narrateur.}

\medskip
\begin{tabular}{l cc}
\toprule
  & Coucher & Lever \\
\cmidrule(lr){2-3}
Lundi & 19h05 & \dots \\
Mardi & 19h20 & \dots \\
\bottomrule
\end{tabular}
\end{document}
```

Les trois exemples ci-dessus ont été écrits de cette façon :

```latex
\footnotesize{\normalsize Texte à interligne réduit, la
commande de changement de paragraphe est appelée pendant
que l'on est en petite taille, alors que le texte est en
taille normale.}\par

\normalsize
{\small On peut faire le contraire (le résultat est moins
laid, esthétiquement) en se basant sur les mêmes principes.
On n'est pas obligé d'appeler la commande
de fin de paragraphe, bien entendu, l'habituel changement de ligne suffit.}

{\setlength{\baselineskip}{1.2\baselineskip}
On peut manipuler la taille de l'interligne soit de façon absolue, soit de façon
relative, comme ici (plus 20\%), soit de manière absolue. Il est important de
terminer le paragraphe avant la fin des accolades, pour que l'interligne que
l'on a défini soit encore en vigueur à la fin du paragraphe (c'est en fait
le seul moment où il est pris en compte).\par} %%% <= terminer le paragraphe
                                               %%%    dans le bloc
{\advance\baselineskip -1pt On peut également ajuster directement l'interlignage
dans une partie du texte.\par}
```

Localement, on peut également utiliser la longueur `\baselineskip` comme le montre l'exemple ci-dessus.

## Avec l'extension « doublespace »

{octicon}`alert;1em;sd-text-warning` *L’extension* {ctanpkg}`doublespace` *est classée comme* {doc}`obsolète </1_generalites/histoire/liste_des_packages_obsoletes>`*. Ce qui suit est informatif.*

L'extension `doublespace` permet de répondre aussi à ce besoin mais son code contient de nombreuses erreurs.

```{eval-rst}
.. meta::
   :keywords: LaTeX,espace entre les lignes,interligne,double interligne
```
