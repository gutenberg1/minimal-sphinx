```{role} latexlogo
```
```{role} tttexlogo
```
# Comment mettre une note dans la marge ?

La commande par défaut pour obtenir une note marginale est `\marginpar{⟨texte de la note⟩}`.

```latex
\documentclass{article}
  \usepackage[width=9cm]{geometry}
  \usepackage{lmodern}
  \usepackage[french]{babel}

  \pagestyle{empty}
\begin{document}
La valeur du paramètre temps est fixée
à 12~minutes\marginpar{AC}. En tenant
compte de cette hypothèse, les résultats
obtenus sont les suivants : \ldots
\end{document}
```

Pour inverser les notes dans les marges (droite/gauche), il suffit de mettre dans le préambule la commande `\reversemarginpar` :

```latex
\documentclass{article}
  \usepackage[width=9cm]{geometry}
  \usepackage{lmodern}
  \usepackage[french]{babel}

  \pagestyle{empty}
  \reversemarginpar
\begin{document}
La valeur du paramètre temps est fixée
à 12~minutes\marginpar{AC}. En tenant
compte de cette hypothèse, les résultats
obtenus sont les suivants : \ldots
\end{document}
```

______________________________________________________________________

*Source :* [How do I get margin notes to appear on the left?](https://tex.stackexchange.com/questions/14527/how-do-i-get-margin-notes-to-appear-on-the-left)

```{eval-rst}
.. meta::
   :keywords: LaTeX,note de bas de page dans la marge,note marginale,note dans la marge de gauche,note dans la marge de droite
```
