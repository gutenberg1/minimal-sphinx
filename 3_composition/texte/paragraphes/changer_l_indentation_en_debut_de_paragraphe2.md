```{role} latexlogo
```
```{role} tttexlogo
```
# Comment changer l'indentation du premier paragraphe après un titre de sectionnement ?

{latexlogo}`LaTeX` implémente un style qui n'indente pas le premier paragraphe après un titre de sectionnement (un titre de section par exemple). Mais ce comportement peut être modifié. Pour montrer comment, voici un exemple de ce que restitue {latexlogo}`LaTeX` par défaut :

```latex
% !TEX noedit
\documentclass{article}
\begin{document}
\section{Indentation}
En typographie, une indentation correspond à un retrait de la première ligne
d'un paragraphe.

Son utilisation est la norme en typographie française mais pas une typographie
anglo-saxonne.
\end{document}
```

```latex
\documentclass{article}
\pagestyle{empty}
\begin{document}
\section{Indentation}
En typographie, une indentation correspond à un retrait de la première ligne
d'un paragraphe.

Son utilisation est la norme en typographie française mais pas une typographie
anglo-saxonne.
\end{document}
```

## Avec l'extension « indentfirst »

La très courte extension {ctanpkg}`indentfirst` de David Carlisle supprime ce mécanisme. Cela donne sur notre exemple le résultat suivant :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{indentfirst}

\begin{document}
\section{Indentation}
En typographie, une indentation correspond à un retrait de la première ligne
d'un paragraphe.

Son utilisation est la norme en typographie française mais pas une typographie
anglo-saxonne.
\end{document}
```

```latex
\documentclass{article}
\usepackage{indentfirst}
\pagestyle{empty}
\begin{document}
\section{Indentation}
En typographie, une indentation correspond à un retrait de la première ligne
d'un paragraphe.

Son utilisation est la norme en typographie française mais pas une typographie
anglo-saxonne.
\end{document}
```

## Avec l'extension « babel » et son option « french »

Pour un document de langue française, l'extension {ctanpkg}`babel` avec son option {ctanpkg}`french <babel-french>` procéde automatiquement à cette modification. Ce qui conduit à :

```latex
% !TEX noedit
\documentclass{article}
\usepackage[french]{babel}

\begin{document}
\section{Indentation}
En typographie, une indentation correspond à un retrait de la première ligne
d'un paragraphe.

Son utilisation est la norme en typographie française mais pas une typographie
anglo-saxonne.
\end{document}
```

```latex
\documentclass{article}
\usepackage[french]{babel}
\pagestyle{empty}
\begin{document}
\section{Indentation}
En typographie, une indentation correspond à un retrait de la première ligne
d'un paragraphe.

Son utilisation est la norme en typographie française mais pas une typographie
anglo-saxonne.
\end{document}
```

## Avec l'extension « polyglossia » et le français comme langue principale

L'extension {ctanpkg}`polyglossia` gère l'indentation des premiers paragraphes pour le français de la même panière que {ctanpkg}`babel`.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{polyglossia}
\setmainlanguage{french}

\begin{document}
\section{Indentation}
En typographie, une indentation correspond à un retrait de la première ligne
d'un paragraphe.

Son utilisation est la norme en typographie française mais pas une typographie
anglo-saxonne.
\end{document}
```

```latex
\documentclass{article}
\usepackage{polyglossia}
\setmainlanguage{french}
\pagestyle{empty}
\begin{document}
\section{Indentation}
En typographie, une indentation correspond à un retrait de la première ligne
d'un paragraphe.

Son utilisation est la norme en typographie française mais pas une typographie
anglo-saxonne.
\end{document}
```

______________________________________________________________________

*Source :* {faquk}`Indent after section headings <FAQ-secindent>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,indentation,paragraphe,titre de sectionnement
```
