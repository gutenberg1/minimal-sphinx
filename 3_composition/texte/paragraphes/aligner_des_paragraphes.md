```{role} latexlogo
```
```{role} tttexlogo
```
# Comment aligner des paragraphes ?

- On peut utiliser la commande `\parindent` de manière complètement détournée, comme ici :

```latex
% !TEX noedit
\settowidth{\parindent}
{Notes : }

\makebox[0pt][r]
{Notes : }La première note pour dire que...

La seconde pour préciser que...
```

- Si l'on veut aligner les lignes au sein d'un même paragraphe, on peut utiliser les commandes `\hangindent` pour indiquer le retrait des lignes (pas seulement de la première ligne) et `\hangafter` pour spécifier à partir de quelle ligne ce retrait (indentation en anglais) doit entrer en action.

Cet exemple utilise ces commandes pour faire une lettrine :

```latex
% !TEX noedit
\font\capfont=cmbx12 at 24.87 pt% or yinit, or... ?
\newbox\capbox \newcount\capl \def\a{A}
\def\docappar{%
  \medbreak\noindent
  \setbox\capbox\hbox{%
    \capfont\a\hskip0.15em}%
  \hangindent=\wd\capbox%
  \capl=\ht\capbox
  \divide\capl by\baselineskip
  \advance\capl by1%
  \hangafter=-\capl%
  \hbox{%
    \vbox to8pt{%
      \hbox to0pt{\hss\box\capbox}%
      \vss
    }%
  }%
}
\def\cappar{\afterassignment\docappar%
\noexpand\let\a }

\cappar Il était une fois un petit chaperon rouges
qui avait une grand-mère qui habitait de l'autre
côté de la forêt. Un jour, alors que sa
grand-mère était malade, le petit chaperon rouge
décida de lui rendre visite...
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
