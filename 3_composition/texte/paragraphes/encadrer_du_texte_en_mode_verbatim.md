```{role} latexlogo
```
```{role} tttexlogo
```
# Comment encadrer du texte verbatim ?

## Avec l'extension moreverb

L'extension {ctanpkg}`moreverb` propose un environnement encadré, `boxedverbatim` :

```latex
\documentclass{article}
\usepackage[francais]{babel}
\usepackage{moreverb}
\pagestyle{empty}
\begin{document}
Le source encadré ci-dessous :

\begin{boxedverbatim}
La commande \verb.\LaTeX{}. permet
d'appeler le logo \LaTeX{}.
\end{boxedverbatim}
produira :

La commande \verb.\LaTeX{}. permet
d'appeler le logo \LaTeX{}.
\end{document}
```

## Avec l'extension fancyvrb

L'extension {ctanpkg}`fancyvrb` permet d'écrire du texte `verbatim` encadré, en couleur, etc :

```latex
\documentclass{article}
\usepackage{fancyvrb}
\usepackage[width=9cm]{geometry}
\pagestyle{empty}
\begin{document}
\fvset{frame=single,numbers=left,numbersep=3pt,
  commandchars=\\\{\},label=Exemple}
\begin{Verbatim}
Ici, je mets mon programme.
Avec des maths : \(a, b, \alpha, \beta,\ldots\)
ou de l'\textit{italique}.

\large{Pratique, non ?}
\end{Verbatim}
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
