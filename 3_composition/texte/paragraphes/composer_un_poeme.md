```{role} latexlogo
```
```{role} tttexlogo
```
# Comment mettre en page un poème ?

## Avec les commandes de base

Il existe l'environnement `verse`. Il gère les marges, les retours à la ligne dans une strophe se font par `\\` et les strophes sont séparées par des lignes blanches :

```latex
% !TEX noedit
\begin{verse}
   J'ai un poney gris, \\
   Qui galope à travers la prairie.

   Grignote, grignote dans ma main, \\
   La carotte rousse du jardin.

   Mes cousins ont un poney blanc, \\
   Qui parcourt chemins et champs.
\end{verse}
```

## Avec l'extension « verse »

L'extension {ctanpkg}`verse` étend les fonctionnalités de l'environnement `verse`. Elle fournit entre autres une commande de titre de poème, le centrage global (l'ensemble du poème est centré mais les vers sont, sauf spécification contraire, alignés au fer à gauche.) du poème dans la page, la possibilité de régler l'espace vertical entre les strophes, la numérotation des lignes (ainsi que leurs étiquétes et références), etc.

Voici un exemple :

```latex
% !TEX noedit
\documentclass{article}
\usepackage[frenchb]{babel}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage{verse}
\newcommand{\attrib}[1]{\nopagebreak{\raggedleft\footnotesize #1\par}}
\setlength{\stanzaskip}{\baselineskip}

\begin{document}

\settowidth{\versewidth}{M'a fait trouver belles les femmes:}
\poemtitle{Gaspard Hauser chante}

{\shorthandoff{!}
\begin{verse}[\versewidth]
\poemlines{2}
Je suis venu, calme orphelin\\
Riche de mes seuls yeux tranquilles,\\
Vers les hommes des grandes villes:\\
Ils ne m'ont pas trouvé malin.\\!
À vingt ans un trouble nouveau,\\
Sous le nom d'amoureuses flammes,\\
M'a fait trouver belles les femmes:\\
Elles ne m'ont pas trouvé beau.\\!
Bien que sans patrie et sans roi\\
Et très brave ne l'étant guère,\\
J'ai voulu mourir à la guerre:\\
La mort n'a pas voulu de moi.\\!
Suis-je né trop tôt ou trop tard?\\
Qu'est-ce que je fais en ce monde?\\
Ô vous tous, ma peine est profonde:\\
Priez pour le pauvre Gaspard.\\!
\end{verse}}
\attrib{Paul \bsc{Verlaine}}

\end{document}
```

## Un cas de code personnalisé

Voici également un exemple de code pour mettre en forme un poème (de J.H.B. Nijhof), très légèrement modifié pour fonctionner avec {latexlogo}`LaTeX` :

```latex
% !TEX noedit
\documentclass[a4paper,12pt]{article}

\def\testline{%
  \par\noindent
  \hbox to 0pt{\hss*\hss}\hfill
  \hbox to 0pt{\hss*\hss}\hfill
  \hbox to 0pt{\hss*\hss}\par}
\def\centerstar{%
  \par\medskip\noindent
  \hbox to\hsize{\hss*\hss}\par
  \medskip}

\newbox\poembox
\newbox\widebox
\newdimen\centerx
\newcount\linecount
\newdimen\poemleftmargin
\def\newpoem{%
  \setbox0=\box\poembox
  \setbox0=\box\widebox
  \linecount=0} % boîte vide
\newpoem % probablement pas nécessaire
\def\poemline#1{\setbox0=\hbox{\strut #1}%
  \setbox\poembox=\vbox{\unvbox\poembox\copy0}%
  \setbox\widebox=\hbox{\unhbox\widebox\copy0}%
  \advance \linecount 1}
\def\setpoem{% vous aurez besoin d'un saut de page ici.
  \centerx=\wd\widebox
  \divide\centerx\linecount
  \divide\centerx 2%
  %maintenant centerx est le x du centre de gravité
  \poemleftmargin=0.5\hsize
  \advance \poemleftmargin-\centerx
  \noindent\kern\poemleftmargin\box\poembox
  \par
  \newpoem}

\begin{document}

\testline % pour voir les marges
\poemline{Rozen verwelken}
\poemline{schepen vergaan}
\poemline{maar onze liefde}
\poemline{zal blijven bestaan}
\setpoem

\centerstar

\poemline{Rozen verwelken}
\poemline{schepen vergaan}
\poemline{maar onze liefde zal blijven bestaan}
\setpoem

\centerstar

\poemline{Rozen verwelken, schepen vergaan}
\poemline{maar onze liefde zal blijven bestaan}
\setpoem

\centerstar

\poemline{Rozen verwelken}
\poemline{\qquad schepen vergaan}
\poemline{maar onze liefde}
\poemline{\qquad zal blijven bestaan}
\setpoem

\centerstar

\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
