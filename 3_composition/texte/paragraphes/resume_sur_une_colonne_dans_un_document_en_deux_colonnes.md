```{role} latexlogo
```
```{role} tttexlogo
```
# Comment obtenir un résumé en pleine largeur dans un document à deux colonnes ?

Dans de nombreux documents formels, il est exigé que le résumé d'un article apparaisse sur toute la largeur de la page alors même que l'ensemble de l'article est présenté sur deux colonnes. Plusieurs solutions existent ici :

## Avec des commandes de base

Une première solution revient à utiliser le code suivant :

```latex
% !TEX noedit
\documentclass[twocolumn]{article}
...
\begin{document}
... % \author, etc
\twocolumn[
  \begin{@twocolumnfalse}
    \maketitle
    \begin{abstract}
      ...
    \end{abstract}
  \end{@twocolumnfalse}
  ]
```

Malheureusement, avec ce code, la commande `\thanks` ne fonctionne pas dans la commande `\author`. Toutefois, si vous avez besoin de notes de bas de page ayant numérotation spécifique, vous pouvez les obtenir avec cet autre code :

```latex
% !TEX noedit
\title{Démonstration}
\author{Moi\thanks{}}
\twocolumn[
  ...
]
{
  \renewcommand{\thefootnote}%
    {\fnsymbol{footnote}}
  \footnotetext[1]{Ne me remerciez pas.}
}
```

## Avec l'extension « abstract »

Entre autres fonctionnalités, l'extension {ctanpkg}`abstract` fournit une commande `\saythanks` et un environnement `onecolabstract` qui suppriment le recours à des détournements de la commande `\thanks` et des notes de bas de page. En voici un exemple :

```latex
% !TEX noedit
\twocolumn[
  \maketitle             % titre en pleine largeur
  \begin{onecolabstract} % résumé en pleine largeur
    ... text
  \end{onecolabstract}
]
\saythanks            % pour saisir l'équivalent de \thanks
```

## Avec la classe « memoir »

La classe {ctanpkg}`memoir` dispose de toutes les fonctionnalités de l'extension {ctanpkg}`abstract`.

______________________________________________________________________

*Source :* {faquk}`1-column abstract in 2-column document <FAQ-onecolabs>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,résumé,deux colonnes,pleine largeur
```
