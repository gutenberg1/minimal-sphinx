```{role} latexlogo
```
```{role} tttexlogo
```
# Comment insérer l'heure dans un document ?

Cette page traite de la question de l'affichage de [l'heure](https://fr.wikipedia.org/wiki/Heure). Une page est par ailleurs dédiée à la question « {doc}`Comment insérer la date dans un document ? </3_composition/texte/mots/inserer_la_date_dans_un_document>` ».

## Avec l'extension « time »

L'extension {ctanpkg}`time` propose une fonction affichant l'heure en notation anglaise.

```latex
\documentclass{report}
  \usepackage{time}
  \pagestyle{empty}

\begin{document}
Il est \now.
\end{document}
```

## Avec l'extension « daytime »

L'extension {ctanpkg}`daytime` propose une solution similaire à la précédente avec une possibilité de variante sur l'écriture de l'heure :

```latex
\documentclass{report}
  \usepackage{daytime}
  \pagestyle{empty}

\begin{document}
Il est \daytime, autrement dit \Daytime.
\end{document}
```

## Avec l'extension « srctime »

L'extension {ctanpkg}`scrtime` (qui appartient à l'ensemble {ctanpkg}`KOMA-Script`) peut également restituer l'heure. Elle dispose d'une option d'extension (« `12h` » ou « `24h` », cette dernière étant la valeur par défaut) pour préciser comme l'afficher. La commande `\thistime` affiche l'heure ainsi souhaitée, sachant que :

- dans le cas de l'option « `12h` », les mentions « am » et « pm » ne sont pas affichées ;
- cette commande peut d'ailleurs prendre un argument optionnel pour préciser ce qui sépare les heures des minutes. Par défaut, ce sera « `:` » ;
- sa version étoilée supprime le zéro parfois en trop des minutes.

```latex
\documentclass{report}
  \usepackage{scrtime}
  \pagestyle{empty}

\begin{document}
Il est \thistime[ heures ] minutes.
\end{document}
```

## Avec l'extension « datetime »

L'extension {ctanpkg}`datetime` définit trois fonctions pour les heures :

- `\xxivtime` pour le format 24 heures ;
- `\ampmtime` pour le format 12 heures ;
- `\oclock` pour un format texte mais, malgré une prise en compte de {ctanpkg}`babel` (à placer avant l'appel à {ctanpkg}`datetime`), le résultat n'est clairement pas adapté. Il doit être réservé à la langue anglaise.

```latex
\documentclass{report}
  \usepackage[french]{babel}
  \usepackage{datetime}
  \pagestyle{empty}

\begin{document}
Il est \xxivtime, c'est-à-dire \ampmtime{}
ou \oclock.
\end{document}
```

Si on reste en anglais, le résultat est plus satisfaisant :

```latex
\documentclass{report}
  \usepackage{datetime}
  \pagestyle{empty}

\begin{document}
Il est \xxivtime, c'est-à-dire \ampmtime{}
ou \oclock.
\end{document}
```

## Avec l'extension « datetime2 »

L'extension {ctanpkg}`datetime2` est une réimplémentation complète de {ctanpkg}`datetime`, par la même auteure, Nicola Talbot. Cette réécriture a permis de proposer un ensemble de macros complètement développables, donc utilisables partout dans votre document, mais n'a pas conservé les noms des anciennes commandes.

Si vous souhaitez seulement l'heure courante, utilisez `\DTMcurrenttime` :

```latex
\documentclass{report}
  \usepackage[french]{babel}
  \usepackage{datetime2}
  \pagestyle{empty}

\begin{document}
Il est \DTMcurrenttime{}.
\end{document}
```

## Sans extension

La primitive `\time` contient le nombre de minutes écoulées depuis minuit. Avec un peu de programmation, il est possible d'en tirer l'heure. Voici un exemple de code permettant cette conversion :

```latex
\documentclass{report}
\makeatletter
\def\timenow{\@tempcnta\time
  \@tempcntb\@tempcnta
  \divide\@tempcntb60
  \ifnum10>\@tempcntb0\fi\number\@tempcntb
  \multiply\@tempcntb60
  \advance\@tempcnta-\@tempcntb
  \string:\ifnum10>\@tempcnta0\fi\number\@tempcnta}
\makeatother
\pagestyle{empty}

\begin{document}
Il est \timenow.
\end{document}
```

______________________________________________________________________

*Source :* {faquk}`Printing the time <FAQ-time>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,heure,mise en forme,calcul de l'heure
```
