```{role} latexlogo
```
```{role} tttexlogo
```
# Comment changer certains titres prédéfinis ?

Ce sujet est traité dans la question « {doc}`Comment changer les textes prédéfinis de LaTeX ? </3_composition/langues/traduire_le_titre_de_table_des_matieres_ou_bibliographie>` ».

```{eval-rst}
.. meta::
   :keywords: LaTeX,titre
```
