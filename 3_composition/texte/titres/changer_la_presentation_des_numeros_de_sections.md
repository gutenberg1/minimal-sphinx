```{role} latexlogo
```
```{role} tttexlogo
```
# Comment modifier la présentation de la numérotation des titres de sectionnement ?

Les problèmes généraux d'ajustement de l'apparence des titres de sectionnement sont assez complexes et sont traités dans la question sur « {doc}`Comment modifier le style des titres de sectionnement ? </3_composition/texte/titres/modifier_le_style_des_titres>` ».

Toutefois, si vous souhaitez simplement changer la façon dont la numérotation apparaît dans le titre de sectionnement et que vous n'avez pas peur d'écrire quelques commandes, cette réponse est alors faite pour vous.

Le numéro d'un titre de sectionnement est composé par la commande {doc}`interne LaTeX </2_programmation/macros/makeatletter_et_makeatother>` `\@seccntformat`, qui reçoit le « nom » du titre (section, sous-section, ...) comme argument. En temps normal, `\@seccntformat` renvoie simplement le numéro du titre de sectionnement, puis une espace d'un quadratin (avec la commande `\quad`).

```latex
% !TEX noedit
\renewcommand*{\@seccntformat}[1]{%
  \csname the#1\endcsname\quad
}
```

Supposons que vous vouliez mettre un point après chaque numéro de section (sous-section, sous-section, ...), un simple changement peut être implémenté par la modification de la commande suivante :

```latex
% !TEX noedit
\renewcommand*{\@seccntformat}[1]{%
  \csname the#1\endcsname.\quad
}
```

Cependant, vous pourriez souhaiter modifier les numéros de section, mais pas les numéros de sous-section ou autres. Pour cela, il faut obtenir un comportement différent de `\@seccntformat` en fonction de son argument. La technique suivante pour faire le travail est un peu besogneuse, mais elle est suffisamment efficace pour cette opération somme toute rare :

```latex
% !TEX noedit
\renewcommand*{\@seccntformat}[1]{%
  \csname the#1\endcsname
  \csname adddot@#1\endcsname\quad
}
```

La commande modifiée utilise ici une commande de second niveau pour fournir le point, s'il a été défini ; sinon, il ajoute simplement `\relax` (ce qui ne fait rien dans ce contexte). La définition de la commande de second niveau (ici, pour le cas d'une `section`) spécifie ce qu'il faut mettre après le numéro :

```latex
% !TEX noedit
\newcommand*{\adddot@section}{.}
```

Notez que toutes les définitions vues ci-dessus modifient des {doc}`commandes internes </2_programmation/macros/makeatletter_et_makeatother>`. Le code ci-dessus doit donc être, de préférence, placé dans une fichier d'extension.

Les classes {ctanpkg}`Koma-script` ont différentes commandes pour spécifier les modifications de la présentation de certains numéros de section : `\partformat`, `\chapterformat` et `\othersectionlevelsformat`, mais leurs fonctionnalités sont similaires à celles du {latexlogo}`LaTeX` standard.

______________________________________________________________________

*Source :* {faquk}`Adjusting the presentation of section numbers <FAQ-seccntfmt>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,latex,macros,programming
```
