```{role} latexlogo
```
```{role} tttexlogo
```
# Comment modifier le style des titres de sectionnement ?

Cette question traite de tous les titres de sectionnement. En complément de ce qui est indiqué ici, les chapitres font l'objet d'une question complémentaire : « {doc}`Comment modifier le style des titres de chapitre ? </3_composition/texte/titres/modifier_les_en-tetes_de_chapitre>` ».

Supposons que l'éditeur de votre journal préféré ait spécifié que les titres de section doivent être au centre, en petites capitales, et les titres de sous-section doivent être en italique. Si vous ne souhaitez pas vous impliquer dans le type de programmation décrit dans la section 2.2 du *The LaTeX Companion* (voir « {doc}`Que lire sur LaTeX ? </1_generalites/documentation/livres/documents_sur_latex>` »), la technique suivante satisfera probablement votre éditeur. Définissez-vous de nouvelles commandes :

```latex
% !TEX noedit
\newcommand{\ssection}[1]{%
  \section[#1]{\centering\normalfont\scshape #1}}
\newcommand{\ssubsection}[1]{%
  \subsection[#1]{\raggedright\normalfont\itshape #1}}
```

Vous aurez alors à utiliser `\ssection` et `\ssubsection` en lieu et place de `\section` et `\subsection`. Bien entendu, ce n'est vraiment pas une solution très heureuse : la numérotation des sections restera en gras et les formes étoilées des commandes devront être aussi redéfinies. Voici donc d'autres solutions plus pratiques ou plus détaillées.

## Avec l'extension « titlesec »

L'extension {ctanpkg}`titlesec` offre une approche structurée du problème, basée sur la redéfinition des commandes de sectionnement elles-mêmes. Cette approche lui permet d'offrir des modifications importantes : ses options fournissent ainsi une boîte à outils pour concevoir la sortie de vos propres commandes de sectionnement. L'extension propose en particulier un certain nombre de styles prédéfinis permettent de modifier rapidement la présentation des titres. Par exemple :

- `display` met au format des chapitres ;
- `hang` met au format des sections ;
- `runin` met au format des paragraphes ;
- `wrap` écrit le texte autour du titre ;
- `frame` est identique au style `display` avec un cadre.

La documentation de l'extension présente un certain nombre d'exemples, mais donnons-en tout de même un pour voir son fonctionnement. L'exemple suivant montre comment présenter une `\subsubsection` à la manière d'un `\paragraph`.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{titlesec}
\titleformat{\subsubsection}[runin]%
{\normalfont\normalsize\bfseries}%
{\thesubsubsection}{1em}{:\quad}

\begin{document}
\section{La section} Avec un premier texte.
\subsection{La sous-section} Avec un deuxième texte.
\subsubsection{La sous-sous-section} Avec un troisième texte.
\paragraph{Le paragraphe} Avec un dernier texte.
\end{document}
```

```latex
\documentclass{article}
\usepackage{titlesec}
\titleformat{\subsubsection}[runin]%
{\normalfont\normalsize\bfseries}%
{\thesubsubsection}{1em}{:\quad}
\pagestyle{empty}
\begin{document}
\section{La section} Avec un premier texte.
\subsection{La sous-section} Avec un deuxième texte.
\subsubsection{La sous-sous-section} Avec un troisième texte.
\paragraph{Le paragraphe} Avec un dernier texte.
\end{document}
```

## Avec l'extension « sectsty »

L'extension {ctanpkg}`sectsty` est moins puissante que {ctanpkg}`titlesec` mais est peut-être plus facile d'utilisation et sans doute préférable pour des modifications mineures dans la mesure où vous n'aurez besoin de lire qu'une petite partie de sa documentation pour pouvoir l'utiliser. Voici un exemple pour obtenir le soulignement des titres de sectionnement.

```latex
% !TEX noedit
\documentclass{article}
\usepackage[french]{babel}
\usepackage{sectsty}
\usepackage[normalem]{ulem}
\allsectionsfont{\sffamily\raggedright\underline}
\begin{document}
\section{Un titre de section assez long pour ne
  pas tenir sur une seule ligne}
\end{document}
```

```latex
\documentclass{article}
\usepackage[french]{babel}
\usepackage{sectsty}
\usepackage[normalem]{ulem}
\allsectionsfont{\sffamily\raggedright\underline}
\pagestyle{empty}
\begin{document}
\section{Un titre de section assez long pour ne
pas tenir sur une seule ligne}
\end{document}
```

## Avec l'extension « sfheaders »

L'extension {ctanpkg}`sfheaders`, de M. Loreti, permet d'écrire les titres avec une police de caractères sans empattement quelle que soit la classe de document utilisée.

## Avec la classe « memoir »

La classe {ctanpkg}`memoir` dispose de fonctionnalités qui correspondent aux extensions {ctanpkg}`sectsty` et {ctanpkg}`titlesec` ainsi qu'un ensemble de style de chapitre prédéfinis (incluant un équivalent à ce que fournit l'extension {ctanpkg}`anonchap`).

## Avec les classes « KOMA-script »

Les classes {ctanpkg}`KOMA-script` disposent également d'outils offrant des fonctionnalités équivalentes, notamment des spécifications de format comme `\partformat`, `\chapterformat`, `\sectionformat`... ainsi que des options de classe permettant de donner des spécifications générales de format.

## Avec des modifications manuelles

Les définitions de `\section`, `\sub(sub)section`, etc. se trouvent dans les fichiers de classe `cls` (`report.cls`, `article.cls` et `book.cls`).

:::{warning}
Il est vivement conseillé de ne pas modifier directement ces classes mais de constituer un ficheir d'extension (`.sty`) avec les nouvelles commandes ou d'utiliser `\makeatletter` et `\makeatother`.
:::

La syntaxe de définition d'une nouvelle section est, par exemple :

```latex
% !TEX noedit
\renewcommand\section%
     {\@startsection {section}{1}{\z@}%
       {-3.5ex \@plus -1ex \@minus -.2ex}%
       {2.3ex \@plus.2ex}%
       {\reset@font\Large\bfseries}}
```

Voici quelques explications sur cette redéfinition :

- la commande `\@startsection` permet de gérer : la table des matières, la numérotation des titres, les références, les titres des sections dans l'en-tête, etc. ;
- `{section}` indique qu'il s'agit d'une section ;
- `{1}` indique son niveau dans la table des matières ;
- `{z@}` indique la taille (largeur) de son retrait (zéro) ;
- `{-3.5ex \@plus -1ex \@minus -.2ex}` définit l'espace qui sera ajouté au-dessus du titre ;
- `{2.3ex \@plus.2ex}` définit l'espace qui sera ajouté au-dessous du titre. Si ce nombre est négatif alors il s'agit d'un espacement horizontal, pour avoir des titres « en ligne » ;
- `\@plus` et `\@minus` permettent de jouer sur l'élasticité de ces espaces ;
- `\reset@font\Large\bfseries` sont les commandes de mises en forme du titre.

Dans ce cadre, Vincent Zoonekynd propose des exemples illustrés montrant comment obtenir différents [styles de chapitre](http://zoonek.free.fr/LaTeX/LaTeX_samples_chapter/0.html) et [styles de section](http://zoonek.free.fr/LaTeX/LaTeX_samples_section/0.html).

______________________________________________________________________

*Source :* {faquk}`The style of section headings <FAQ-secthead>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,style des titres,apparence des titres,format des titres
```
