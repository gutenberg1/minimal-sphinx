```{role} latexlogo
```
```{role} tttexlogo
```
# Comment supprimer la date sur une page de titre ?

Il faut ajouter la commande `\date{}` dans le préambule du document, sans argument.

Si le sujet de la date (et de son insertion) vous intéresse, voir la question « {doc}`Comment insérer la date dans un document ? </3_composition/texte/mots/inserer_la_date_dans_un_document>` »

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
