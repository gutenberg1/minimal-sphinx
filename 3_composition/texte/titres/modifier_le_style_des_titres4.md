```{role} latexlogo
```
```{role} tttexlogo
```
# Comment modifier le style du titre de document ?

## Avec l'extension « titling »

L'extension {ctanpkg}`titling` fournit des fonctionnalités permettant de modifier le résultat d'une commande `\maketitle` ainsi que des commandes `\thanks` et autres qu'elle contient. Elle définit également un environnement configurable `titlingpage`, intermédiaire entre l'option `titlepage` des classes standard et l'environnement `titlepage`.

## Avec la classe « memoir »

La classe {ctanpkg}`memoir` inclut toutes les fonctionnalités de l'extension {ctanpkg}`titling`.

## Avec les classes « KOMA-script »

Les classes {ctanpkg}`KOMA-script` définissent leur propre gamme de styles de titres.

## Avec les commandes de base

Vincent Zoonekynd propose des exemples illustrés montrant comment obtenir différents [styles de titre de document](http://zoonek.free.fr/LaTeX/LaTeX_samples_title/0.html).

______________________________________________________________________

*Source :* {faquk}`The style of document titles <FAQ-titlsty>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,style,titre du document,titre principal
```
