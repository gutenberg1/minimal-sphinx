```{role} latexlogo
```
```{role} tttexlogo
```
# Comment personnaliser les environnements de listes ?

## Comment changer les puces et les espacements ?

- L'environnement `list` permet de définir son propre style de liste. Sa syntaxe est la suivante :

```latex
% !TEX noedit
\begin{list}{label}{mep}
...
\end{list}
```

L'argument `{label}` permet de définir le symbole qui sera associé à chaque élément de la liste, `{mep}` permet de définir la mise en page des éléments de la liste. Les paramètres utilisés pour définir cette mise en page sont les suivants :

- `topsep` espace vertical supplémentaire (ajoute à `\parskip`) inséré entre le texte précédant la liste et le premier objet de la liste;
- `partosep` espace vertical supplémentaire inséré devant la liste si celle-ci est précédée d'une ligne blanche;
- `itemsep` espace vertical supplémentaire (ajouté à `\parsep`) inséré entre les éléments d'une liste.

On peut écrire par exemple

```latex
\newenvironment{maliste}%
{ \begin{list}%
        {$\rightarrow$}%
        {\setlength{\labelwidth}{30pt}%
         \setlength{\leftmargin}{35pt}%
         \setlength{\itemsep}{\parsep}}}%
{ \end{list} }

Utilisation :
\begin{maliste}
   \item premier élément
   \item deuxième élément
   \begin{maliste}
      \item petit 1
      \item petit 2
   \end{maliste}
\end{maliste}
```

- Le petit bout de code qui suit définit les commandes :
- `\noitemsep` pour supprimer tout espacement vertical entre les items des environnements `itemize`, `enumerate` et `description`.
- `\doitemsep` pour les remettre.

Pour l'utiliser, il suffit de le sauvegarder dans un fichier `.sty` et de l'inclure dans son document par une commande `\usepackage`.

```latex
%%%% debut macro %%%%
%% -----------------------------------------------
%% Copyright (c) 1993 Hydro-Quebec
%%            mboyer@robot.ireq.ca
%% -----------------------------------------------

%% Bring items closer together in list environments
% Prevent infinite loops
\let\orig@Itemize =\itemize
\let\orig@Enumerate =\enumerate
\let\orig@Description =\description
% Zero the vertical spacing parameters
\def\Nospacing{\itemsep=0pt\topsep=0pt%
               \partopsep=0pt\parskip=0pt%
               \parsep=0pt}
% Redefinition de art12.sty pour commencer a la
% marge de gauche
%\leftmargini 1.2em      % 2.5em

\def\noitemsep{
% Redefine the environments in terms of the original
% values
\renewenvironment{itemize}
  {\orig@Itemize\Nospacing}{\endlist}
\renewenvironment{enumerate}
  {\orig@Enumerate\Nospacing}{\endlist}
\renewenvironment{description}
  {\orig@Description\Nospacing}%
{\endlist}
}

\def\doitemsep{
% Redefine the environments to the original values
\renewenvironment{itemize}
  {\orig@Itemize}{\endlist}
\renewenvironment{enumerate}
  {\orig@Enumerate}{\endlist}
\renewenvironment{description}
  {\orig@Description}{\endlist}
}
```

- Pour réduire l'espace entre les items de toutes les listes on peut également utiliser le bout de code suivant de Mark Wooding :

```latex
% !TEX noedit
\makeatletter
\toks@\expandafter{\@listI}
\edef\@listI{\the\toks@\setlength{\parsep}{1pt}}
\makeatother
```

## Comment changer le style de numérotation de « enumerate » ?

- La macro de Timothy Murphy présentée dans cet exemple permet de remplacer les numéros de l'environnement `enumerate` par des lettres grecques. On pourra préférer appliquer cette macro sur l'environnement `enumerate` modifié par le package {ctanpkg}`enumerate`.

```latex
% !TEX noedit
\makeatletter
\def\greek#1{\expandafter\@greek
  \csname c@#1\endcsname}
\def\@greek#1{\ifcase#1\or
  $\alpha$\or
  $\beta$\fi}% as many as you
             % need
\renewcommand{\theenumi}{\greek{enumi}}
\makeatother
...
\begin{enumerate}
\item Un
\item Deux
\item Trois
\end{enumerate}
```

## Comment changer les puces en fonction de la profondeur de l'élément ?

- Les définitions suivantes permettent de redéfinir les caractères utilisés par l'environnement `itemize` pour ces différents niveaux d'encapsulation :

```latex
% !TEX noedit
\renewcommand{\labelitemi}{\textbullet}
\renewcommand{\labelitemii}{---}
\renewcommand{\labelitemiii}{votre-label-niveau-iii}
\renewcommand{\labelitemiv}{votre-label-niveau-iv}
```

:::{note}
Avec certaines extensions, il faut placer ces nouvelles définitions de commande **après** le `\begin{document}`.
:::

## Comment changer le style des étiquettes avec « description » ?

De même, utilisez `\descriptionlabel` pour changer le style des étiquettes de l'environnent `description`.

Cet exemple produit des étiquettes en italique, avec deux points :

```latex
\renewcommand\descriptionlabel[1]{%
    \hspace\labelsep\normalfont
    \itshape #1:}

Exemple :
\begin{description}
   \item[Carte maîtresse] As
   \item[Carte maîtresse à l'atout] Valet
\end{description}
```

## Y a-t-il une extension qui m'aiderait à faire tout ça ?

- L'extension {ctanpkg}`mdwlist`, de Mark Wooding permet de redéfinir certains paramètres de mise en page des listes qui ne sont pas faciles d'accès sous {latexlogo}`LaTeX`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,listes à puces,style des listes,énumérations,listes à points,structure d'une liste,liste d'items,liste d'éléments
```
