```{role} latexlogo
```
```{role} tttexlogo
```
# Comment interrompre temporairement une liste numérotée ?

Il est souvent pratique d'avoir un texte de commentaire, « en dehors » de la liste, entre les entrées successives d'une liste. La situation est ici fonction du type d'environnement utilisé :

- avec l'environnement `itemize`, ce n'est pas un problème, car rien ne distingue les éléments successifs ;
- avec l'environnement `description`, les étiquettes des éléments sont sous le contrôle de l'utilisateur et il n'y a donc pas de problème de continuité ;
- avec l'environnement `enumerate`, la numérotation de la liste est générée automatiquement et est sensible au contexte. Il faut donc ici conserver l'état du compteur d'énumération pour poursuivre la liste après l'avoir interrompu. C'est ce cas qui est développé ici.

## Avec les commandes de base

L'approche consiste ici à conserver l'état de l'énumération dans un compteur créé pour l'occasion, puis à le restaurer lors de la reprise de l'énumération :

```latex
\documentclass{article}
  \newcounter{saveenum}
  \pagestyle{empty}

\begin{document}
Cette liste est :
\begin{enumerate}
  \item claire ;
  \item précise ;
  \setcounter{saveenum}{\value{enumi}}
\end{enumerate}
Cela n'était pas bien compliqué.
\begin{enumerate}
  \setcounter{enumi}{\value{saveenum}}
  \item et complète.
\end{enumerate}
\end{document}
```

Cette méthode est raisonnable à petites doses... Les problèmes, en dehors de la lisibilité, sont d'obtenir le bon niveau (faut-il utiliser le compteur `enumi`, `enumii`...) et d'éviter d'avoir des listes interrompues dans le commentaire lui-même.

Par ailleurs, si vous suspendez une {doc}`liste avec un style de numérotation modifié </3_composition/texte/listes/modifier_le_style_des_listes_numerotees>`, vous devez fournir à nouveau les paramètres optionnels requis par l'extension {ctanpkg}`enumerate` lors de la reprise la liste.

## Avec l'extension « mdwlist »

L'extension {ctanpkg}`mdwlist` définit les commandes `\suspend` et `\resume` pour simplifier ce processus :

```latex
\documentclass{article}
  \usepackage{mdwlist}
  \pagestyle{empty}

\begin{document}
Cette liste est :
\begin{enumerate}
  \item claire ;
  \item précise ;
\suspend{enumerate}
Cela n'était pas bien compliqué.
\resume{enumerate}
  \item et complète.
\end{enumerate}
\end{document}
```

L'extension permet de tenir compte d'un identifiant facultatif (sur le modèle `\suspend[⟨id⟩]{enumerate}`) pour vous permettre d'identifier une suspension particulière, et donc de bien l'isoler des autres pour gérer par exemple des suspensions imbriquées.

Tout comme dans la méthode basique, si vous suspendez une {doc}`liste avec un style de numérotation modifié </3_composition/texte/listes/modifier_le_style_des_listes_numerotees>`, vous devez fournir à nouveau les paramètres optionnels requis par l'extension {ctanpkg}`enumerate`. La tâche est ici un peu fastidieuse car l'argument optionnel doit être encapsulé, en entier, à l'intérieur d'un argument optionnel de `\resume` et ceci nécessite l'utilisation d'accolades supplémentaires :

```latex
\documentclass{article}
  \usepackage{mdwlist}
  \usepackage{enumerate}
  \pagestyle{empty}

\begin{document}
Cette liste est :
\begin{enumerate}[\textbf{Item} i]
  \item claire ;
  \item précise ;
\suspend{enumerate}
Cela n'était pas bien compliqué.
\resume{enumerate}[{[\textbf{Item} i]}]
  \item et complète.
\end{enumerate}
\end{document}
```

## Avec l'extension « expdlist »

L'extension {ctanpkg}`expdlist` a une manière différente d'aborder le problème, avec sa commande `\listpart`. L'argument de la commande devient en effet un commentaire entre les éléments de la liste :

```latex
\documentclass{article}
  \usepackage{expdlist}
  \pagestyle{empty}
\begin{document}
Cette liste est :
\begin{enumerate}
  \item claire ;
  \item précise ;
  \listpart{Cela n'était pas bien compliqué.}
  \item et complète.
\end{enumerate}
\end{document}
```

Ceci signifie qu'il n'a même pas à penser à suspendre ou à reprendre la liste, et bien sûr, cela fonctionne aussi bien dans n'importe quel environnement de liste.

## Avec l'extension « enumitem »

L'extension {ctanpkg}`enumitem`, dans ses versions récentes, vous permettra également d'interrompre et de reprendre des listes :

```latex
\documentclass{article}
  \usepackage{enumitem}
  \pagestyle{empty}

\begin{document}
Cette liste est :
\begin{enumerate}
  \item claire ;
  \item précise ;
\end{enumerate}
Cela n'était pas bien compliqué.
\begin{enumerate}[resume]
  \item et complète.
\end{enumerate}
\end{document}
```

Cette approche semble tout aussi naturelle que celle de {ctanpkg}`mdwlist` et a l'avantage de bien s'adapter aux autres fonctionnalités de l'extension {ctanpkg}`enumitem`.

L'extension {ctanpkg}`enumitem` permet également la suspension à plusieurs niveaux et la reprise des listes :

```latex
\documentclass{article}
  \usepackage{enumitem}
  \pagestyle{empty}

\begin{document}
Cette liste est :
\begin{enumerate}
\item claire ;
\end{enumerate}
En tant qu'exemple, elle se devait de l'être.
\begin{enumerate}[resume]
\item précise ;
\begin{enumerate}
\item par souci de formalisme,
\end{enumerate}
Oui, oui, toujours parce qu'elle est exemplaire.
\begin{enumerate}[resume]
\item et par souci de concision ;
\end{enumerate}
\item et complète
\end{enumerate}
\end{document}
```

Cependant, le commentaire présent dans l'énumération imbriquée apparaît comme s'il s'agissait d'un deuxième paragraphe au sein du deuxième élément de la liste principale, ce qui n'est guère satisfaisant.

______________________________________________________________________

*Source :* {faquk}`Interrupting enumerated lists <FAQ-interruptlist>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,listes et énumérations,couper une liste,interrompre une liste
```
