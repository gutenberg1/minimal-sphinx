```{role} latexlogo
```
```{role} tttexlogo
```
# Comment changer l'orientation de tout ou partie d'un document ?

Il est souvent nécessaire de composer tout ou partie d'un document en orientation [paysage](https://fr.wikipedia.org/wiki/Mode_paysage) (ou « format à l'italienne », par opposition à l'orientation [portrait](https://fr.wikipedia.org/wiki/Mode_portrait) ou « format à la française »). Pour y parvenir, il faut non seulement changer les dimensions de la page, mais aussi demander au périphérique de sortie d'imprimer différemment cette page particulière. Plusieurs solutions existent, classée ici selon l'amplitude souhaitée de cette modification.

## Pour l'ensemble d'un document

### Avec l'extension « geometry »

L'extension {ctanpkg}`geometry`, de Hideo Umeki et David Carlisle, est considérée comme une référence sur le sujet de la définition de la géométrie des pages d'un document.

Son option `landscape` permet d'obtenir cette orientation. Si vous passez aussi l'option `dvips` ou `pdftex` à l'extension, elle génère également les instructions de rotation pour que la sortie soit correctement orientée.

```latex
% !TEX noedit
\documentclass{report}
\usepackage[landscape]{geometry}

\begin{document}
Voici un document écrit dans un sens non usuel.
\end{document}
```

### Avec la classe « memoir »

La classe {ctanpkg}`memoir` a les mêmes fonctionnalités que {ctanpkg}`geometry`.

### Avec les classes standards

{octicon}`alert;1em;sd-text-warning` *Cette solution n'est indiquée que pour en présenter les limites.*

Les classes standards proposent une option `landscape` pour la commande `\documentclass`. Cependant, cette option ne change que les réglages de la zone de texte, pas *le format de la page* ! Cette demi-solution ne sera sans doute pas satisfaisante et vous demandera de modifier la géométrie de la page.

```latex
% !TEX noedit
\documentclass[landscape]{report}
  \usepackage[french]{babel}

\begin{document}
Voici un document écrit dans un sens non usuel.
\end{document}
```

### Avec le programme DVIPS

Dans le cas où vous auriez un fichier DVI en format paysage, vous pouvez obtenir un fichier PostScript propre en utilisant la commande :

```bash
dvips -t a4 -t landscape -o tmp.ps toto.dvi
```

### Avec le programme DocStrip

Le programme {ctanpkg}`DocStrip <docstrip>` pourrait le faire, bien que ce ne soit pas son but premier.

```{eval-rst}
.. todo:: Ce dernier paragraphe appelle une révision : « *À développer.* »
```

## Pour une partie d'un document

### Avec les extensions « lscape » ou « pdflscape »

Si vous avez une longue séquence d'éléments qui doivent être placés en orientation paysage (une liste de codes, un large environnement `tabbing`, ou une énorme table composée avec {ctanpkg}`longtable` ou {ctanpkg}`supertabular`), utilisez alors :

- l'extension {ctanpkg}`lscape` de David Carlisle ;
- si vous générez un document PDF avec `pdflatex` ou `dvips` puis `ps2pdf`, l'extension {ctanpkg}`pdflscape`.

Dans les deux cas, vous disposerez alors d'un environnement `landscape` : il efface la page courante et la compose en orientation paysage ; lors de sa clôture, il efface la page courante avant de revenir à une composition en orientation portrait. En voici un exemple d'utilisation :

```latex
% !TEX noedit
\documentclass[11pt]{report}
  \usepackage{lscape}

\begin{document}
\begin{landscape}
Un petit tour à la campagne...
\end{landscape}
et nous voici de retour dans la galerie, après
un  changement de page bien évidemment.
\end{document}
```

## Pour une partie de page

Si les solutions présentées par la suite permette d'avoir un élément en orientation paysage (par rotation), aucune extension actuellement disponible ne prévoit directement la composition en orientation portrait et paysage sur la même page car {tttexlogo}`TeX` n'est pas pensé pour faire. Si un tel comportement était une nécessité absolue, on pourrait utiliser les techniques décrites dans la question « {doc}`Comment faire couler du texte autour d'une figure ? </3_composition/flottants/habiller_une_image_avec_du_texte>` », et faire pivoter la partie à mettre en orientation paysage en utilisant les fonctions de rotation de l'extension {ctanpkg}`graphicx`. Le retour à l'orientation portrait serait un peu plus facile : la partie portrait de la page serait un flottant de bas de page à la fin de la section paysage, avec son contenu pivoté.

```{eval-rst}
.. todo:: Ce dernier paragraphe appelle une révision : « *Un exemple pourrait être intéressant pour illustrer ce point.* »
```

### Avec l'extension « rotation »

Si vous avez un seul objet flottant qui ne peut rentrer sur la page qu'en orientation paysage, utilisez l'extension {ctanpkg}`rotation`. Elle définit les environnements `sidewaysfigure` et `sidewaystable` qui créent des flottants qui occupent une page entière.

Notez que {ctanpkg}`rotation` pose problème dans des documents qui chargent également l'extension {ctanpkg}`float` (souvent recommandée dans cette FAQ, par exemple à la question « {doc}`Comment gérer proprement les flottants dans LaTeX </3_composition/flottants/pourquoi_faire_flotter_ses_figures_et_tableaux>` »). L'extension {ctanpkg}`rotfloat` charge {ctanpkg}`rotation` et fluidifie l'interaction avec {ctanpkg}`float`.

### Avec l'extension « rotating »

L'extension {ctanpkg}`rotating` propose également une fonctionnalité de rotation de contenu. L'exemple suivant en montre un usage un peu alambiqué :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{rotating}

\begin{document}
\newcount\wang
\newsavebox{\wangtext}
\newdimen\wangspace
\def\wheel#1{\savebox{\wangtext}{#1}%
\wangspace\wd\wangtext
\advance\wangspace by 1cm%
\centerline{%
\rule{0pt}{\wangspace}%
\rule[-\wangspace]{0pt}{\wangspace}%
\wang=-180\loop\ifnum\wang<180
\rlap{\begin{rotate}{\the\wang}%
\rule{1cm}{0pt}#1\end{rotate}}%
\advance\wang by 10 \repeat}}
\wheel{Save the whale}
\end{document}
```

:::{note}
La plupart des prévisualisateurs {tttexlogo}`TeX` actuels ne respectent pas les demandes de rotation dans les fichiers DVI. Le mieux est de convertir votre sortie en PostScript ou en PDF pour obtenir la présentation souhaitée.
:::

______________________________________________________________________

*Source :* {faquk}`Typesetting things in landscape orientation <FAQ-landscape>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,tourner,pivoter,format paysage,format à l'italienne,format allongé,rotation des pages,fichier DVI en landscape
```
