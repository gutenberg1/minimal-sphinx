```{role} latexlogo
```
```{role} tttexlogo
```
# Comment redéfinir les marges d'un document ?

## Avec l'extension geometry

L'extension {ctanpkg}`geometry` permet de redéfinir les marges d'un document ou de définir la mise en page. Les deux exemples ci-dessous permettent de voir l'effet de l'option `nohead`, qui supprime l'en-tête d'un document.

```latex
% !TEX noedit
\geometry{margin=5pt}
équivalent à
\geometry{hmargin=5pt, vmargin=5pt}
équivalent à
\geometry{margin={5pt,5pt}}
```

Voici donc l'utilisation de {ctanpkg}`geometry` avec l'option `nohead` :

```latex
% !TEX noedit
\documentclass{report}
\usepackage{geometry}
\geometry{scale=1.0, nohead}
\begin{document}
Ainsi, le texte appara\^it beaucoup plus haut
dans une page.
\end{document}
```

Et voici l'utilisation de {ctanpkg}`geometry` sans l'option `nohead` :

```latex
% !TEX noedit
\label{ex=geometry2}}
\documentclass{report}
\begin{document}
Voici une page normale pour comparer.
\end{document}
```

## Avec l'extension vmargin

L'extension {ctanpkg}`vmargin` de Volker Kuhlmann permet de redéfinir les marges de la totalité du document grâce la commande : `\setmarginsrb{1}{2}{3}{4}{5}{6}{7}{8}` où

- `1` est la marge gauche ;
- `2` est la marge en haut ;
- `3` est la marge droite ;
- `4` est la marge en bas ;
- `5` fixe la hauteur de l'en-tête ;
- `6` fixe la distance entre l'en-tête et le texte ;
- `7` fixe la hauteur du pied de page ;
- `8` fixe la distance entre le texte et le pied de page.

Vous pouvez également utiliser des valeurs calculées à partir du papier utilisé, comme par exemple : `\setpapersize{A4}`

## Avec l'extension chngpage

L'environnement `adjustwidth` de l'extension {ctanpkg}`chngpage` permet de modifier localement les marges d'un document. Il prend deux arguments : la marge gauche et la marge droite (ces arguments peuvent prendre des valeurs négatives). En voici un exemple :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{chngpage}
\begin{document}
\begin{adjustwidth}{2cm}{-1cm}
  Ceci permet d'augmenter la marge gauche de 2cm
  et de diminuer celle de droite de 1cm.
\end{adjustwidth}
À comparer avec un texte qui occupe toute
la largeur de la page, comme celui-ci par exemple.
\end{document}
```

## Avec l'extension truncate

L'extension {ctanpkg}`truncate` disponible sur permet de fixer la largeur d'un texte.

## Avec l'extension typearea

Il existe également l'extension {ctanpkg}`typearea`.

```{eval-rst}
.. todo:: Ce dernier paragraphe appelle une révision : « *À compléter.* »
```

## Avec l'extension narrow

L'environnement `narrow`, de Keith Reckdahl, disponible dans le fichier source `narrow.sty` permet d'encapsuler des paragraphes de largeurs différentes :

```latex
%-----------------------------------------------------------------
% \begin{narrow}{1.0in}{0.5in}   produces text which is narrowed
%          by 1.0 on left margin and 0.5 inches on right margin
% \begin{narrow}{-1.0in}{-0.5in} produces text which is widened
%          by 1.0 on left margin and 0.5 inches on right margin
% Narrow environments can be nested and are ended by \end{narrow}
%-----------------------------------------------------------------
\newenvironment{narrow}[2]{%
 \begin{list}{}{%
  \setlength{\topsep}{0pt}%
  \setlength{\leftmargin}{#1}%
  \setlength{\rightmargin}{#2}%
  \setlength{\listparindent}{\parindent}%
  \setlength{\itemindent}{\parindent}%
  \setlength{\parsep}{\parskip}%
 }%
\item[]}{\end{list}}
\end{narrow}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,marges,geometry,vmargin,chngpage,truncate,typearea,narrow
```
