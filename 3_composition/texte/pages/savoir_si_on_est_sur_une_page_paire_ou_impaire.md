```{role} latexlogo
```
```{role} tttexlogo
```
# Comment savoir si une page est un recto ou un verso ?

[Une autre question](/3_composition/texte/pages/footnotes/mettre_des_notes_marginales_a_droite) détaille le problème rencontré pour que la commande `\marginpar` positionne correctement les notes marginales dans les documents recto-verso : à gauche ou à droite de la page. Ce cas appartient à une famille plus large de problèmes où il faut savoir où un morceau de texte va se trouver mis en forme. En effet, la routine de sortie (qui produit les pages composées) est asynchrone et {latexlogo}`LaTeX` comme {tttexlogo}`TeX` vont traiter une bonne partie de la page suivante avant de déterminer où découper la page présente. Par conséquent, le compteur `page` (nommé dans {latexlogo}`LaTeX` `\c@page`) n'a de fiabilité qu'*au moment* du traitement de la routine de sortie.

La solution revient :

- à utiliser une version du mécanisme de `\label` pour déterminer sur quelle page vous êtes ;
- et à intégrer le fait que la valeur du compteur de page apparaissant dans une commande `\pageref` est insérée lors de la routine de sortie, ce qui la rend fiable.

Toutefois... `\pageref` elle-même n'est pas fiable ! En toute logique, l'instruction

```latex
% !TEX noedit
\ifthenelse{\isodd{\pageref{foo}}}{...recto...}{...verso...}
```

aurait normalement du faire le nécessaire mais les extensions {ctanpkg}`babel` et {ctanpkg}`hyperref` sont toutes deux connues pour interférer avec la valeur de `\pageref`. Soyez donc prudent !

## L'extension « changepage »

L'extension {ctanpkg}`changepage`, pour définir certaines de ses propres fonctionnalités, fournit une commande `\checkoddpage` : elle définit une « étiquette » pour son propre usage et l'élément de référence à la page de cette étiquette est ensuite examinée ({ctanpkg}`hyperref` ne pouvant la parasiter) afin de définir une variable conditionnelle `\ifoddpage` vraie si la commande a été émise sur un recto. La classe {ctanpkg}`memoir` propose d'ailleurs la même commande.

Les utilisateurs de {latexlogo}`LaTeX` qui ne sont pas familiers avec les commandes `\if...` de {tttexlogo}`TeX` peuvent utiliser l'extension {ctanpkg}`ifthen` :

```latex
% !TEX noedit
\usepackage{ifthen,changepage}
...
\checkoddpage
\ifthenelse{\boolean{oddpage}}{...recto...}{...verso...}
```

Bien sûr, ces nouvelles « étiquettes » contribuent à alimenter les messages d'erreur « Rerun to get cross-references » (autrement dit « Relancer le programme pour mettre à jour les références croisées ») de {latexlogo}`LaTeX`...

## Les classes KOMA-Script

Les classes {ctanpkg}`KOMA-Script <koma-script>` ont un environnement `addmargin*` qui fournit des fonctionnalités similaires à celles qu'offre {ctanpkg}`changepage`. La commande `\ifthispageodd{...recto...}{...verso...}` permet ici d'exécuter différentes tâches selon le numéro de page {doc}`documentation en français pour KOMA-Script </1_generalites/documentation/documents/documents_extensions/koma-script_en_francais>`).

## L'extension « ifoddpage »

L'extension {ctanpkg}`ifoddpage` est conçue pour fournir une fonctionnalité identique. Elle se comporte en tenant compte du fait que vous composez un document recto-verso ou recto uniquement. Comme l'extension {ctanpkg}`changepage`, elle utilise une commande de vérification nommée `\checkoddpage`. Les autres commandes de tests sont définies en utilisant des commandes conditionnelles de (Plain) {tttexlogo}`TeX`; elles sont définis localement, afin que vous puissiez minimiser leur utilisation de l'espace de travail de {tttexlogo}`TeX`. La documentation de l'extension détaille la délicate séquence impliquée. De plus, l'extension fournit une commande `\ifoddpageoroneside`, qui est vraie pour les pages de droite d'un document recto-verso, ou sur toutes les pages d'un document recto.

L'utilisation de cette extension se limite généralement à :

```latex
% !TEX noedit
\checkoddpage
\ifoddpage
  ...recto...
\else
  ...verso...
\fi
```

Le principe recommandé par l'auteur de l'extension consiste à inclure toute l'opération dans une boîte. Cela présente l'avantage d'avoir un test qui fonctionne toujours mais cette technique a aussi le désavantage des boîtes (un bloc ne pouvant se scinder). Dans la pratique, l'ensemble du travail sera réalisé à l'intérieur d'une boîte (comme, par exemple, dans le cas d'un flottant) : ainsi, le travail élaboré proposé par l'auteur ne sera pas nécessaire.

______________________________________________________________________

*Source :* {faquk}`Finding if you're on an odd or an even page <FAQ-oddpage>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,page de gauche,page de droite,recto,verso
```
