```{role} latexlogo
```
```{role} tttexlogo
```
# Comment modifier les marges d'un document ?

Changer la mise en page d'un document implique de tenir compte de nombreuses subtilités peu évidentes pour un débutant. Il existe en effet des interactions entre :

- les contraintes fondamentales de {tttexlogo}`TeX` ;
- les contraintes liées à la conception de {latexlogo}`LaTeX` ;
- et les bonnes pratiques de composition et de conception.

Ceci signifie que tout changement doit être très attentivement considéré, pour s'assurer d'une part qu'il fonctionne et d'autre part que le résultat est visuellement satisfaisant.

Si, pour un débutant, les valeurs par défaut de {latexlogo}`LaTeX` semblent parfois excessivement conservatrices, il y a de bonnes raisons derrière ces réglages. Même Leslie Lamport, dans son livre {doc}`LaTeX, a Document Preparation System </1_generalites/documentation/livres/documents_sur_latex>`, suggère d'éviter de toucher à ces réglages. Et à ceux qui voudraient tout de même faire des modifications, il ajoute : « tout ce que je peux faire ici est de vous mettre en garde contre l'erreur très courante de créer des lignes trop larges pour être lues facilement -- une erreur que vous ne ferez pas si vous suivez cette suggestion : utilisez des lignes qui ne contiennent pas plus de 75 caractères, y compris la ponctuation et les espaces. ».

Cette FAQ vous recommande d'utiliser une extension pour établir des paramètres cohérents des paramètres car chaque extension prend en charge les interrelations sans que vous *ayez* besoin d'y penser. Rappelez-vous cependant que les extensions fournissent des mécanismes cohérents et fonctionnels : elles n'analysent jamais la qualité de ce que vous faites.

Les questions suivantes traitent de la manière dont on peut choisir de procéder :

- « {doc}`Quelles sont les extensions utiles pour définir les pages et leurs dimensions ? </3_composition/texte/pages/packages_pour_modifier_les_marges_d_un_document>` » ;
- « {doc}`Comment définir les paramètres de page manuellement ? </3_composition/texte/pages/modifier_les_marges_d_un_document3>` » ;
- s'ajoute à cela une question connexe « {doc}`Comment modifier des marges en cours de document ? </3_composition/texte/pages/modifier_les_marges_en_cours_de_document>` ».

______________________________________________________________________

*Source :* {faquk}`Changing the margins in LaTeX <FAQ-changemargin>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,formatting
```
