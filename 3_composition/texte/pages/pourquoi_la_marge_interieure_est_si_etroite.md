```{role} latexlogo
```
```{role} tttexlogo
```
# Pourquoi la marge intérieure est-elle si étroite ?

Si vous donnez aux classes standard l'option `twosite` pour obtenir un document recto verso, elles définissent alors des marges étroites à gauche des pages impaires et à droite des pages paires. Si cela peut sembler étrange à première vue étrange, il se cache en fait derrière cela un besoin typographique de symétrie. Si vous posez une page paire à gauche d'une page impaire, vous verrez que vous avez trois morceaux égaux de papier non imprimé : la gauche marge de la page paire, la marge droite de la page impaire et les deux marges adjacentes.

Voilà pour la théorie. Dans la pratique, cela ne fonctionne pas toujours car la reliure joue ici un rôle important. Presque toutes les reliures de qualité vont accaparaer et masquer une part de vos marges intérieures. Un livre ainsi relié ne présentera donc pas la symétrie précieuse... à moins que vous n'ayez agi sur les paramètres de marge.

Les extensions recommandées dans la question « {doc}`Quelles sont les extensions utiles pour définir les pages et leurs dimensions ? </3_composition/texte/pages/packages_pour_modifier_les_marges_d_un_document>` » prévoient principalement un décalage de reliure ou une correction de reliure. Vous pouvez chercher le terme *binding* dans les documentations de ces extensions, hors celle de l'extension {ctanpkg}`vmargin` qui ne traite pas ce sujet.

Si vous faites le travail à la main (comme détaillé en question « {doc}`Comment définir les paramètres de page manuellement ? </3_composition/texte/pages/modifier_les_marges_d_un_document3>` »), vous devez :

- calculer vos dimensions de page et de marge comme d'habitude ;
- soustraire le décalage de reliure de `\evensidemargin` ;
- et ajouter le décalage de reliure à ''\\oddsidemargin ''.

En remplaçant ci-dessous *décalage* par une {doc}`mesure </2_programmation/syntaxe/longueurs/unites_de_mesure_de_tex>` {latexlogo}`LaTeX` (par exemple `5mm`), cette modification s'obtient avec :

```latex
% !TEX noedit
\addtolength{\evensidemargin}{-décalage}
\addtolength{\oddsidemargin}{décalage}
```

Bien entendu, ce qui précède n'est qu'une des manières de procéder (à valeur de `\textwidth` constante). Vous pourriez tout aussi bien choisir de modifier `\textwidth` en présence du décalage de reliure.

______________________________________________________________________

*Source :* {faquk}`Why is the inside margin so narrow? <FAQ-gutter>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,page,marge,marge intérieure,reliure
```
