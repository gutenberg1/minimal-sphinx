```{role} latexlogo
```
```{role} tttexlogo
```
# Comment modifier correctement le format de papier ?

Si votre sortie papier n'a pas la bonne taille et que vous avez vérifié que ce n'est pas dû à des {doc}`réglages propres à Adobe Reader </5_fichiers/pdf/imprimer_a_la_bonne_taille_avec_adobe_reader>`, le problème se situe vraisemblablement dans votre configuration de {latexlogo}`LaTeX`. Les formats de papier peuvent être pénibles : {latexlogo}`LaTeX` ne semble pas y prêter grand intérêt. De fait, il n'y a pas de commande DVI pour spécifier le papier sur lequel le document doit être imprimé, ce qui a conduit à une dichotomie où les commandes composent le texte en fonction des besoins de l'auteur tandis que le choix des pilotes de périphériques se fait indépendamment de ces commandes.

En pratique, {latexlogo}`LaTeX` se sert par défaut du {doc}`format de papier américain </3_composition/texte/pages/differences_entre_papier_a4_et_letter>` *Letter*. Et, comme la plupart des distributions proviennent actuellement d'Europe, les pilotes utilisent généralement par défaut le format de papier ISO A4. Tout ceci n'est pas vraiment heureux : les utilisateurs peuvent toutefois assez facilement sélectionner un format de papier différent pour leur document. En effet, {latexlogo}`LaTeX` propose une gamme de formats, sous forme d'options dans les classes standard. Néanmoins, l'utilisateur doit également être sûr que chaque fois que `xdvi`, `dvips` (ou autre) s'exécute, il utilise le format de papier pour lequel le document a été conçu.

Le format de papier par défaut pour les pilotes DVI peut être modifié par une commande de gestion de la distribution (`texconfig` pour TeX Live, l'application `Options` pour MiKTeX).

Un problème différent se pose pour les utilisateurs de pdfTeX. Le format PDF *dispose* du moyen d'exprimer la taille du papier et pdfTeX a les variables système `\pdfpagewidth` et `\pdfpageheight`, qui sont écrites dans le sortie du fichier PDF. Malheureusement, la plupart des logiciels de base sont antérieurs à pdfTeX. Même pdfLaTeX ne définit pas les valeurs correctes dans ces variables, pour correspondre au format de papier spécifié dans une option `\documentclass`.

Les pilotes DVI `dvips`, `dvipdfm` et leurs extensions (`dvipdfmx` et `xdvipdfmx`) définissent les commandes `\special` pour que le document spécifie son propre format de papier. Dans ces cas, comme lorsque pdfTeX est utilisé, le format de papier peut être programmé par le document. Les utilisateurs qui le souhaitent peuvent bien entendu consulter les manuels des différents programmes pour écrire le code nécessaire.

## Avec les extensions geometry et zwpagelayout

Les extensions {ctanpkg}`geometry` et {ctanpkg}`zwpagelayout` traitent des dimensions des différents éléments des pages. Elles prennent de ce fait note de la taille du papier sur lequel le document va être imprimé et peuvent émettre les commandes nécessaires pour garantir qu'un bon format de papier est utilisé. Si {ctanpkg}`geometry` est utilisé lorsqu'un document est en cours de traitement par pdfLaTeX, il peut définir les dimensions nécessaires « dans la sortie ». Si le document est traité par {latexlogo}`LaTeX` sur un moteur {tttexlogo}`TeX` ou ε-{tttexlogo}`TeX`, il existe des options d'extension qui indiquent à {ctanpkg}`geometry` quelles commandes `\special` utiliser. Notez d'ailleurs que les options sont ignorées si vous utilisez pdfLaTeX.

### L'extension geometry

Ainsi, une des solutions au problème, lorsque vous utilisez {latexlogo}`LaTeX`, consiste à ajouter :

```latex
% !TEX noedit
\usepackage[option-processeur,...]{geometry}
```

où `option-processeur` indique à l'extension ce qui produira votre sortie PostScript ou PDF. L'extension {ctanpkg}`geometry` connaît `dvips` et `dvipdfm` (qui sert également pour `dvipdfmx` et `xdvipdfmx`).

Si vous utilisez pdfLaTeX ou XeTeX, chargez :

```latex
% !TEX noedit
\usepackage[option-programme,...]{geometry}
```

où `option-programme` vaut `pdftex` ou `xetex`.

### L'extension zwpagelayout

De son côté, {ctanpkg}`zwpagelayout` demande une option `driver` (pilote) :

```latex
% !TEX noedit
\usepackage[driver=valeur,...]{zwpagelayout}
```

Les *valeurs* autorisées pour cette option sont `pdftex`, `xetex` et `dvips`. La valeur par défaut est `unknown` (inconnu).

## Avec les classes KOMA-script et memoir

Bien entendu, les deux « grandes » classes {ctanpkg}`KOMA-script <koma-script>` et {ctanpkg}`memoir` fournissent leurs propres moyens d'obtenir le *bon* format de papier.

### La classe KOMA-script

La classe {ctanpkg}`KOMA-script <koma-script>` fournit des fonctionnalités de mise en page par le biais de l'extension {ctanpkg}`typearea`. Chargez-la avec l'option `pagesize` et elle s'assurera que le bon papier est sélectionné, que ce soit pour la sortie PDF de pdfLaTeX ou la sortie PostScript via `dvips`.

### La classe memoir

La classe {ctanpkg}`memoir` dispose des format de papier des classes standard (`a4paper`, `letterpaper` et ainsi de suite) mais permet également à l'utilisateur de choisir un format de papier arbitraire, en définissant les registres de longueur `\stockheight` et `\stockwidth`. Les commandes `\fixdvipslayout` (pour le traitement {latexlogo}`LaTeX`) et `\fixpdflayout` (pour le traitement pdfLaTeX) ordonnent ensuite au processeur de produire une sortie qui indique le format de papier nécessaire.

______________________________________________________________________

*Source :* {faquk}`Getting the right paper geometry from (La)TeX <FAQ-papergeom>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,formatting
```
