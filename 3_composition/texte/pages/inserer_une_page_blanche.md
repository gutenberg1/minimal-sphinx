```{role} latexlogo
```
```{role} tttexlogo
```
# Comment insérer une page blanche ?

Pour forcer {latexlogo}`LaTeX` à laisser une page blanche dans un document, il faut utiliser successivement trois commandes :

```latex
% !TEX noedit
\newpage
\strut % ou ~ ou \mbox{} ou \null
\newpage
```

En effet, {latexlogo}`LaTeX` a besoin d'un contenu (ici invisible, ce que donne chacune des commandes citées) pour pouvoir considérer que la page a été commencée et qu'il peut alors changer de page à la demande.

Le plus souvent, on voudra en fait utiliser les réponses à la question « {doc}`Comment modifier un changement de page ? </3_composition/texte/pages/forcer_ou_empecher_un_saut_de_page>` ».

```{eval-rst}
.. meta::
   :keywords: LaTeX,saut de page,page blanche
```
