```{role} latexlogo
```
```{role} tttexlogo
```
# Comment modifier des marges en cours de document ?

L'une des surprises caractéristiques de l'utilisation de {tttexlogo}`TeX` est que vous ne pouvez pas changer la largeur ou la longueur de la zone du texte dans le document, même en modifiant directement les paramètres de taille du texte. {tttexlogo}`TeX` ne peut pas changer la largeur du texte à la volée, et {latexlogo}`LaTeX` ne regarde la hauteur du texte qu'au début de la construction d'une nouvelle page. La règle est donc que les paramètres ne doivent être modifiés que dans le préambule du document, c'est-à-dire avant l'instruction `\begin{document}`, avant toute composition. Voici comment contourner cette règle.

## Modification de la largeur de la page

### Avec des commandes de base

Pour ajuster la largeur du texte en cours de document, la méthode suivante passe par un environnement :

```latex
% !TEX noedit
\newenvironment{changemargin}[2]{%
  \begin{list}{}{%
    \setlength{\topsep}{0pt}%
    \setlength{\leftmargin}{#1}%
    \setlength{\rightmargin}{#2}%
    \setlength{\listparindent}{\parindent}%
    \setlength{\itemindent}{\parindent}%
    \setlength{\parsep}{\parskip}%
  }%
  \item[]}{\end{list}}
```

L'environnement prend deux arguments et indente les marges gauche et droite des valeurs de ces paramètres. Les valeurs négatives réduiront les marges et l'exemple suivant rétrécit les marges gauche et droite de 1 centimètre :

```latex
% !TEX noedit
\begin{changemargin}{-1cm}{-1cm}
...
\end{changemargin}
```

Cette solution marche car l'environnement (qui est proche de l'environnement {latexlogo}`LaTeX` `quote`) *ne change pas la largeur du texte*, ce qui évite tout problème avec {tttexlogo}`TeX`. L'environnement déplace simplement le texte à l'intérieur de la largeur que {tttexlogo}`TeX` connait.

### Avec l'extension « changepage »

L'extension {ctanpkg}`changepage` fournit des commandes prêtes à l'emploi pour faire ce qui précède. Elle inclut des réglages changeant les décalages appliqués à votre texte en fonction de sa disposition sur une page au recto ou au verso. La structure de {ctanpkg}`changepage` correspond à celle de la classe {ctanpkg}`memoir`.

### Avec l'extension « chngpage »

{octicon}`alert;1em;sd-text-warning` L’extension {ctanpkg}`chngpage` est classée comme {doc}`obsolète </1_generalites/histoire/liste_des_packages_obsoletes>`. Ce qui suit est informatif.

L'extension plus ancienne {ctanpkg}`chngpage` fournit les mêmes fonctionnalités, mais elle utilise une syntaxe assez différente. L'extension {ctanpkg}`changepage` doit être utilisée pour la remplacer pour tout nouveau document.

## Modification de la longueur de la page

(avec-des-commandes-de-base-1)=

### Avec des commandes de base

Changer les dimensions verticales d'une page est assez simple : la commande {latexlogo}`LaTeX` `\enlargethispage` ajuste la taille de la page courante en fonction de son argument. Voici un exemple courant augmentant la longueur de la page de la hauteur d'un ligne de texte :

```latex
% !TEX noedit
\enlargethispage{\baselineskip}
```

Et voici l'opération opposée, réduisant la longueur de la page de la hauteur d'un ligne de texte :

```latex
% !TEX noedit
\enlargethispage{-\baselineskip}
```

### Avec l'extension « addlines »

Le processus est (dans une certaine mesure) simplifié par l'extension {ctanpkg}`addlines` : sa commande `\addlines` prend comme argument le *nombre* de lignes à ajouter à la page (plutôt qu'une longueur) : la {texdoc}`documentation de l'extension <addlines>` présente une analyse du moment où la commande peut fonctionner ou pas.

______________________________________________________________________

*Source :* {faquk}`Changing margins "on the fly" <FAQ-chngmargonfly>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,mise en page,changer la taille de marges,une page plus grande
```
