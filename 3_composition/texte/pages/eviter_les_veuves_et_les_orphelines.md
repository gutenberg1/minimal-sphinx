```{role} latexlogo
```
```{role} tttexlogo
```
# Comment éviter les lignes veuves ou orphelines ?

Les [veuves](https://fr.wikipedia.org/wiki/Veuves_et_orphelines) (la dernière ligne d'un paragraphe au début d'une page) et les [orphelines](https://fr.wikipedia.org/wiki/Veuves_et_orphelines) (la première ligne de paragraphe à la fin d'une page) interrompent le flux de lecture et sont généralement considérées comme fautives. {tttexlogo}`TeX` et {latexlogo}`LaTeX` prennent quelques précautions pour les éviter mais une prévention complètement automatique est quasiment impossible. Si vous composez votre propre texte, demandez-vous toujours si vous pouvez vous résoudre à modifier légèrement celui-ci pour que ces problèmes soient évités.

Avant toute chose, vérifiez que vous utilisez {doc}`l'extension microtype </1_generalites/notions_typographie/microtype>`. Si ce n'est pas le cas, ajoutez-la dans votre préambule et voyez si le problème se pose toujours.

## Avec les paramètres de TeX et LaTeX

Lors de la formation d'une page, {tttexlogo}`TeX` et {latexlogo}`LaTeX` prennent en compte les variables `\widowpenalty` (pour les veuves) et `\clubpenalty` (pour les orphelines). Ces pénalités sont généralement fixées à la valeur modérée de `150`, ce qui décourage légèrement les mauvaises coupures de page. Vous pouvez augmenter les valeurs en indiquant par exemple :

```latex
% !TEX noedit
\widowpenalty = 500
```

Il faut noter que les pages sont constituées de listes verticales offrant assez peu de possibilités d'extensibilité ou de rétrécissement. De plus, si le générateur de pages doit choisir entre étirer ce qui devrait pas être étiré et être pénalisé, la pénalité l'emporte rarement. Par conséquent, pour les mises en page typiques, il n'y a que deux paramètres raisonnables pour les pénalités :

- fini (`150` ou `500`, peu importe) pour autoriser les veuves et les orphelins ;
- infini (`10000`) pour les interdire.

## Avec la commande « \\raggedbottom »

Le problème peut être évité en autorisant la construction de pages courtes avec la commande `\raggedbottom`. Cependant, de nombreux éditeurs insistent sur la valeur par défaut `\flushbottom`. Il est donc rarement acceptable d'introduire de l'extensibilité dans la liste verticale, sauf à des points (comme les titres de section) le permettant explicitement.

## Avec des mesures manuelles

Une fois que vous avez épuisé les mesures automatiques et que vous avez un projet final que vous souhaitez peaufiner, vous devez procéder aux mesures manuelles.

### Pour les orphelines

Se débarrasser d'une orpheline est simple : faites précéder le paragraphe de la commande `\clearpage` et le paragraphe ne peut plus commencer au mauvais endroit. Si toutefois vous souhaitez conserver le même nombre de lignes sur chaque page (par exemple pour la composition d'un roman), vous pouvez utiliser la commande `\looseness` comme indiqué ci-dessous.

### Pour les veuves

Se débarrasser d'une veuve peut être plus délicat. Les solutions dépendent souvent du contexte.

Lorsque la page précédente contient un long paragraphe avec une dernière ligne courte, il peut être possible de le rendre plus « serré », et donc de le raccourcir d'une ligne. Cela s'obtient en écrivant au début ou à la fin du paragraphe :

```latex
% !TEX noedit
\looseness = -1
```

Au contraire, si l'un des paragraphes précédents a sa dernière ligne presque pleine, on peut l'augmenter d'une ligne avec `\looseness=1`. Dans ce cas, il est conseillé de contrôler ensuite que la nouvelle dernière ligne ne soit pas trop courte.

Si l'utilisation de `\looseness` ne donne pas un résultat satisfaisant, un ajustement de la taille de la page pour « ajouter une ligne » à la page peut éventuellement placer le paragraphe entier sur une page. Cela s'obtient avec :

```latex
% !TEX noedit
\enlargethispage{\baselineskip}
```

À l'inverse, il est possible aussi de réduire la taille de la page pour produire une « veuve à deux lignes » (plus ou moins) acceptable :

```latex
% !TEX noedit
\enlargethispage{-\baselineskip}
```

______________________________________________________________________

*Source :* {faquk}`Controlling widows and orphans <FAQ-widows>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,typographie,lignes isolées,ligne en haut de page,mise en page,le détail en typographie,gris typographique
```
