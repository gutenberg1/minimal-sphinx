```{role} latexlogo
```
```{role} tttexlogo
```
# Comment éviter qu'une note de bas de page s'étale sur plusieurs pages ?

{tttexlogo}`TeX` découpe une note de bas de page sur plusieurs pages lorsqu'il ne trouve guère mieux à proposer. En règle générale, lorsque cela se produit, la marque de note de bas de page se trouve au bas de la page et la note associée, si elle était affiché complétement, surchargerait la page. Et ce sont les réglages de {tttexlogo}`TeX` qui l'ont conduit à préférer la coupure de la note de bas de page à toute autre possibilité.

La meilleure solution dans ce cas revient à changer votre texte pour que ce problème ne survienne pas. Mais, parfois, le texte ne doit pas être modifié... les solutions suivantes sont alors à étudier : elles reviennent essentiellement à redéfinir certains réglages de {tttexlogo}`TeX`.

## Avec une modification des paramètres de TeX

Cette première solution consiste à augmenter fortement la valeur de la *pénalité d'interligne dans les notes de bas de page*, contenue dans la variable `\interfootnotelinepenalty`. En effet, sous {tttexlogo}`TeX`, une *pénalité*, valeur comprise entre -10000 et 10000, est une indication donnée à {tttexlogo}`TeX` afin qu'il puisse savoir si la coupure de page (ou de ligne) est souhaitable ou pas à l’endroit associé à cette pénalité. Une pénalité de 10000 empêche la coupure de page (ou de ligne) ; une pénalite de -10000 impose la coupure.

Ici, la modification souhaitée est de ne pas avoir de coupure de page dans les notes en bas de page, ce qui conduit à :

```latex
% !TEX noedit
\interfootnotelinepenalty=10000
```

Cette modification pourra causer des messages « `Underfull \vbox` » (boîte verticale insuffisamment remplie) à moins que vous n'utilisiez aussi la commande `\raggedbottom`.

## Avec une modification de la taille de la page

Une autre solution consiste à agrandir ou réduire la taille d'une des pages associées à cette note qui s'étale sur plusieurs pages. L'agrandissement peut permettre d'affiche la note de bas de page dans son intégralité tandis que la réduction peut conduire à passer l'ensemble des lignes problématiques à la page suivante. Dans tous les cas, cela s'obtient avec la commande `\enlargethispage`. Par exemple, la commande suivante va ajouter une ligne de texte à la page courante :

```latex
% !TEX noedit
\enlargethispage{\baselineskip}
```

L'utilisation dans cette commande des mesures usuelles de {tttexlogo}`TeX`, comme `15mm` ou `-20pt`, est bien entendu possible.

## Avec l'extension fnbreak

L'extension {ctanpkg}`fnbreak` détecte les notes de bas de page divisées (et génère des avertissements associés).

______________________________________________________________________

*Source :* {faquk}`Why does LaTeX split footnotes across pages? <FAQ-splitfoot>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,note de bas de page
```
