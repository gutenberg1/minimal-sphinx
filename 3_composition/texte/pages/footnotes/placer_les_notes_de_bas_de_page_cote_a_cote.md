```{role} latexlogo
```
```{role} tttexlogo
```
# Comment placer les notes de bas de page les unes à la suite des autres ?

L'extension {ctanpkg}`footmisc` permet d'avoir des notes de bas de page qui s'enchaînent sans retour à ligne, en utilisant l'option `para`. En voici un exemple :

```latex
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage[para]{footmisc}

\begin{document}

Donald Knuth\footnote{Mathématicien américain.} a déclaré qu'il ne développe
plus \TeX ; il ne traite désormais plus que la correction des erreurs qui
lui sont remontées\footnote{et ces bugs sont rares !}.
\end{document}
```

Comparez :

```latex
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[total={7cm,3cm}]{geometry}
%\usepackage[para]{footmisc}

\begin{document}

Donald Knuth\footnote{Mathématicien américain.} a déclaré qu'il ne développe
plus \TeX ; il ne traite désormais plus que la correction des erreurs qui
lui sont remontées\footnote{et ces bugs sont rares !}.
\end{document}
```

```latex
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[total={7cm,3cm}]{geometry}
\usepackage[para]{footmisc}

\begin{document}

Donald Knuth\footnote{Mathématicien américain.} a déclaré qu'il ne développe
plus \TeX ; il ne traite désormais plus que la correction des erreurs qui
lui sont remontées\footnote{et ces bugs sont rares !}.
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,note de bas de page,retour à la ligne,footnote,notes l'une après l'autre
```
