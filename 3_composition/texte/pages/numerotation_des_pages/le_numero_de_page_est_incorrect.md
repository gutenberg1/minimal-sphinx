```{role} latexlogo
```
```{role} tttexlogo
```
# Pourquoi un numéro de page courante peut être faux en haut de page ?

C'est une longue histoire, dont les sources sont profondément ancrées dans le fonctionnement de {tttexlogo}`TeX` lui-même : tout découle de l'effort de {tttexlogo}`TeX` pour générer le meilleur résultat possible.

Le numéro de page est stocké dans `\count0`. Les utilisateurs de {latexlogo}`LaTeX` voient cela comme le compteur `page` et peuvent composer sa valeur en utilisant `\thepage`.

La valeur de `\count0` n'est mise à jour que lorsque {tttexlogo}`TeX` produit réellement une page. {tttexlogo}`TeX` n'essaye même de faire cela que lorsqu'il détecte un indice que ce pourrait être une bonne chose à faire. Du point de vue de {tttexlogo}`TeX`, la fin d'un paragraphe constitue un bon moment pour envisager de produire une page. Il créera une page s'il dispose de plus de matière qu'il n'en faut pour en faire une, ceci pour s'assurer qu'il pourra toujours tester certaines optimisations. En conséquence, la valeur de `\count0` est presque toujours fausse dans le premier paragraphe d'une page (l'exception étant le cas où le numéro de page a été « forcé », soit en changeant sa valeur directement, soit en plaçant un saut de page là où {tttexlogo}`TeX` ne l'aurait pas nécessairement placé).

{latexlogo}`LaTeX` fournit un moyen sûr de se référer au numéro de page, en utilisant les mécaniques de références.

Il faut donc éviter d'écrire :

```latex
% !TEX noedit
Nous sommes sur la page \thepage{}.
```

et écrire plutôt  :

```latex
% !TEX noedit
Nous sommes sur la page \pageref{here}\label{here}.
```

Il ne faut pas ici d'espace entre les commandes `\pageref` et `\label` car cet espace pourrait sinon potentiellement finir comme un espace de saut de page... ce qui va plutôt à l'encontre de l'objectif recherché.

______________________________________________________________________

*Source :* {faquk}`Page number is wrong at start of page <FAQ-wrongpn>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,errors
```
