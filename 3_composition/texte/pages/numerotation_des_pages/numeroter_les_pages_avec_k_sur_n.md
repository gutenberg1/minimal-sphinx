```{role} latexlogo
```
```{role} tttexlogo
```
# Comment obtenir la numérotation « page ⟨K⟩ sur ⟨N⟩ » ?

Il faut ici procéder en deux étapes.

## Obtenir le nombre de pages d'un document

Ce sujet est traité dans la question « {doc}`Comment compter le nombre de pages d'un document ? </3_composition/document/combien_de_pages_a_mon_document>` ».

## Modifier le format de numérotation des pages

La documentation de l'extension {ctanpkg}`fancyhdr` explique comment tirer parti de ce nombre de pages du document pour produire la numérotation des pages demandée.

______________________________________________________________________

*Source :* {faquk}`Page numbering "<n> of <m>" <FAQ-nofm>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,formatting
```
