```{role} latexlogo
```
```{role} tttexlogo
```
# Comment mettre une note marginale du bon côté ?

Dans un monde idéal, les notes marginales devraient être dans des positions similaires sur chaque page : traditionnellement, les notes sur les pages de gauche (ou versos) sont positionnées à gauche et celles des pages de droite (ou rectos) à droite. Un peu réflexion montre que la composition typographique des notes des pages de gauche doit être différente de celles des notes des pages de droite. À cet effet, la commande {latexlogo}`LaTeX` `\marginpar` prend deux arguments dans les documents recto-verso : `\marginpar[left text]{right text}`.

{latexlogo}`LaTeX` utilise un test simple pour positionner la note marginale du bon côté de la page... Mais ce test peut se tromper car {tttexlogo}`TeX` exécute son générateur de page de manière asynchrone. Si une commande `\marginpar` est traitée pendant qu'une page N est construite mais qu'elle n'est pas utilisée avant la page N+1, alors la note marginale se retrouvera du mauvais côté sur cette page N+1. Il s'agit d'un cas particulier d'un problème plus général, présenté dans « {doc}`Comment savoir si une page est un recto ou un verso ? </3_composition/texte/pages/savoir_si_on_est_sur_une_page_paire_ou_impaire>` ».

La solution à ce problème est d'aider {latexlogo}`LaTeX` à « se rappeler » sur quel côté de la page *devrait* se trouver la note marginale. L'extension {ctanpkg}`mparhack` le permet en utilisant des marques semblables à des labels dans le fichier `.aux`. La classe {ctanpkg}`memoir` procède de façon similaire.

______________________________________________________________________

*Source :* {faquk}`Getting \`\\marginpar\` on the right side <FAQ-marginparside>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,usage
```
