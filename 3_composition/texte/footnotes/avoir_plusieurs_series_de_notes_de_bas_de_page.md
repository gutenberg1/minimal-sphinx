```{role} latexlogo
```
```{role} tttexlogo
```
# Comment obtenir plus d'une plage de notes de bas de page ?

Pouvoir disposer de plusieurs plages de notes en bas de page est un besoin courant dans les éditions critiques (ou éditions savantes) pour mettre en forme [l'apparat critique](https://fr.wikipedia.org/wiki/Apparat_critique). Ce besoin peut parfois survenir aussi dans d'autres domaines.

## Avec l'extension reledmac

Différentes extensions, affiliées à {ctanpkg}`edmac` de {tttexlogo}`TeX` , traitent de l'édition critique et proposent cette possibilité. La dernière en date pour {latexlogo}`LaTeX`, est {ctanpkg}`reledmac <ledmac>`, de Maïeul Rouquette. Elle rend obsolète {ctanpkg}`ledmac` et {ctanpkg}`eledmac <ledmac>` mais sa documentation précise comment passer de ces anciennes versions à l'actuelle.

## Avec l'extension manyfoot et les extensions liées

L'extension {ctanpkg}`manyfoot` propose aussi des plages de notes de bas de page multiples aux utilisateurs de {latexlogo}`LaTeX`. Elle fournit d'ailleurs un large éventail d'options de présentation.

Deux autres extensions traitant de l'édition critique se basent sur {ctanpkg}`manyfoot` :

- {ctanpkg}`ednotes` qui inclut {ctanpkg}`manyfoot` comme mécanisme pour mettre en place plusieurs plages de notes de bas de page ;
- {ctanpkg}`bigfoot` utilise tout autant {ctanpkg}`manyfoot` dans le cadre de sa structure sophistiquée de fonctions de note de bas de page.

______________________________________________________________________

*Source :* {faquk}`More than one sequence of footnotes <FAQ-multfoot>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,usage
```
