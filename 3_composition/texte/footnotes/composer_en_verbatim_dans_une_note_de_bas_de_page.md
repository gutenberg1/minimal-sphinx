```{role} latexlogo
```
```{role} tttexlogo
```
# Comment utiliser le mode verbatim dans une note de bas de page ?

Le mode `verbatim` permet de saisir du texte qui apparaîtra tel quel dans le document, sans interprétation des caractères spéciaux. Il est spécialement adapté pour le code informatique :

```latex
Voici \verb+Un \textbf{exemple} de code \LaTeX+.
```

Mais le mode `verbatim` n'est généralement pas utilisable en argument d'une commande, et notamment pas utilisable dans une note de bas de page. Si on a juste besoin de quelques mots en style verbatim, on peut imaginer sélectionner la fonte utilisée par le mode `verbatim` avec `\texttt{}`, puis à faire le `verbatim` « à la main » en protégeant les caractères spéciaux...

Mais, heureusement, l'extension {ctanpkg}`fancyvrb` propose la commande `\VerbatimFootnotes`. Une fois cette commande utilisée dans le préambule, il devient possible d'inclure du `verbatim` dans l'argument d'une commande. En voici un exemple :

```latex
\documentclass{report}
\usepackage[width=9cm,height=4cm]{geometry}
\usepackage{lmodern}
\usepackage[french]{babel}
\usepackage{fancyvrb}
\VerbatimFootnotes
\DefineShortVerb{\|}  % voir la doc de fancyvrb

\begin{document}
On peut inclure du texte verbatim dans une
note\footnote{En écrivant directement |\verb|
dans la note de bas de page, avec le package
\textit{fancyvrb}.}, sans avoir besoin des
anciens mécanismes avec |\SaveVerb| et
|\UseVerb|.
\end{document}
```

:::{note}
Une ancienne méthode consistait à mémoriser le texte verbatim en dehors de la commande avec `\SaveVerb`, puis à inclure ce qui a été enregistré, avec `\UseVerb`.

Il vaut maintenant mieux utiliser le package {ctanpkg}`fancyvrb`.
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX,verbatim,note en bas de page
```
