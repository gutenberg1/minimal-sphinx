```{role} latexlogo
```
```{role} tttexlogo
```
# Comment composer une brochure ?

- Pour redéfinir un format de page, (par exemple un A4 plié en trois), il faut utiliser la commande `\setlength`. Il suffit de savoir quelles sont les longueurs à préciser, et pour cela, le meilleur moyen est de les visualiser avec la commande `\layout`, fournie par l'extension {ctanpkg}`layout`.

On pourra étudier avec intérêt le fichier ci-dessous :

```latex
% (Th. Bouche)
\ProvidesPackage{a6size}

% rien a voir avec la taille : ajustement du \baselineskip
\renewcommand\normalsize{%
   \@setfontsize\normalsize\@xiipt{13.5}%
   \abovedisplayskip 12\p@ \@plus3\p@ \@minus7\p@
   \abovedisplayshortskip \z@ \@plus3\p@
   \belowdisplayshortskip 6.5\p@ \@plus3.5\p@ \@minus3\p@
   \belowdisplayskip \abovedisplayskip
   \let\@listi\@listI}

\renewcommand\small{%
   \@setfontsize\small\@xipt{12.4}%
   \abovedisplayskip 11\p@ \@plus3\p@ \@minus6\p@
   \abovedisplayshortskip \z@ \@plus3\p@
   \belowdisplayshortskip 6.5\p@ \@plus3.5\p@ \@minus3\p@
   \def\@listi{\leftmargin\leftmargini
   \topsep 9\p@ \@plus3\p@ \@minus5\p@
   \parsep 4.5\p@ \@plus2\p@ \@minus\p@
   \itemsep \parsep}%
   \belowdisplayskip \abovedisplayskip}
\normalsize

\setlength\paperheight {148mm}%
\setlength\paperwidth  {105mm}%
%\voffset-1cm
%\hoffset-2cm
\setlength{\topmargin}{-1.3cm}%
\setlength{\oddsidemargin}{-.5cm}%
\setlength{\evensidemargin}{-1cm}%
\setlength{\marginparsep}{0\p@}%
\setlength{\headsep}{0\p@}%
% calcule la hauteur du texte en fonction du \baselineskip, pour
% que les lignes soient placées au même niveau sur toutes les pages
\setlength{\textheight}{\topskip}
\addtolength{\textheight}{22\baselineskip}%
\setlength{\textwidth}{7cm}%
\setlength{\footskip}{23\p@}% (originellement : 48)
%\setlength{\baselineskip}{13\p@}%
%\setlength{\marginparwidth}{0\p@} %
%\addtolength{\baselineskip}{.2\baselineskip}%
\setlength{\parindent}{0\p@}
%\addtolength{\headsep}{\headsep}
%\setlength{\push@skip}{.2\textwidth}
\newenvironment{page}{\vspace*{\stretch{1}}}
{\vspace*{\stretch{2.5}}\newpage}
\pagestyle{plain}
```

Il faut ensuite opérer ce que les imprimeurs appellent une imposition : imprimer la page tant à tel endroit de la x-ième feuille de telle sorte qu'il n'y ait plus qu'à plier la liasse pour obtenir un livre prêt à être relié. Cette étape est facile à réaliser soit à l'aide de `dvidvi` (mais qui n'autorise pas les rotations, ce qui peut en limiter l'intérêt pour des formats spéciaux), soit avec `pstops`, disponible dans l'extension {ctanpkg}`psutils` :

```bash
pstops "2:0L@.7(21cm,0)+1L@.7(21cm,14.85cm)" un.ps deux.ps
```

- L'option `a5paper` de {latexlogo}`LaTeX` permet également de faire cela. Il faut ensuite utiliser `dvidvi` et `dvips -t landscape`.
- Consulter également l'extension {ctanpkg}`2up` pour {latexlogo}`LaTeX`.
- L'extension {ctanpkg}`poligraf` permet d'agir sur la mise en page d'un document avant impression.
- `psnup` et `psbook` peuvent également permettre de faire de la composition mais il vaut alors mieux travailler avec des polices PS. Voir la question «[Comment manipuler un fichier Postscript?](/5_fichiers/postscript/manipuler_un_fichier_postscript) » pour savoir où télécharger ces programmes.

Exemple (J.A. Ferrez) :

```bash
#!/bin/sh
#
# Turn a (clean) PS file into a booklet
#
# input on stdin or file in arg
#
# psbook -- reorder the pages
#           see -s option for _very_ large files
# psnup  -- scale and place two pages onto one
# pstops -- reverse the odd pages for duplex printing
#
# outup on stdout
#
psbook $1 | psnup -2 | pstops "2:0,1U(21cm,29.7cm)"
```

- Le résultat est possible également en ne travaillant que sur le PS :

```bash
dvips -h twoup -t landscape fichier.dvi
```

- Sur PC, on peut utiliser `dvidrv`.
- Pour PC, `twoup` fait cela, mais ce n'est pas du domaine public.

```{eval-rst}
.. meta::
   :keywords: LaTeX,demi-A4,imprimer en livret,composer un livret,A4 plié,plusieurs pages par feuille
```
