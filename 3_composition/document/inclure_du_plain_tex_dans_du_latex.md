```{role} latexlogo
```
```{role} tttexlogo
```
# Comment inclure du code Plain TeX dans LaTeX ?

{latexlogo}`LaTeX`, bien qu'à l'origine {doc}`basé sur Plain TeX </1_generalites/bases/differences_entre_latex_et_tex>`, ne contient pas toutes les commandes de Plain {tttexlogo}`TeX`. Pire encore, certains noms de commandes Plain {tttexlogo}`TeX` apparaissent dans {latexlogo}`LaTeX`, avec une définition différente. En conséquence, des mesures spéciales doivent être prises pour permettre la composition de documents ou de parties de documents Plain {tttexlogo}`TeX` dans {latexlogo}`LaTeX`.

Le seul moyen fiable consiste à traduire les commandes Plain {tttexlogo}`TeX` pour produire un équivalent {latexlogo}`LaTeX`. Cependant, cette solution n'est pas vraiment pratique dans de nombreuses circonstances. Pour ces occasions, l'extension {ctanpkg}`plain` vient souvent à votre aide. Elle définit un environnement '' plain '' dans lequel un document Plain {tttexlogo}`TeX` peut être traité :

```latex
% !TEX noedit
\begin{plain}
  % Chargement du fichier document-plain.
  \input{document-plain}
\end{plain}
```

Cette extension peut ne pas fonctionner, par exemple avec des documents qui utilisent AMSTeX ou s'il est demandé de charger Eplain. Bien sûr, un peu de programmation peut surmonter ces problèmes même si ce n'est pas souvent évident.

______________________________________________________________________

*Source :* {faquk}`Including Plain TeX files in LaTeX <FAQ-inclplain>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,usage
```
