```{role} latexlogo
```
```{role} tttexlogo
```
# Comment compter le nombre de pages d'un document ?

## Avec quelques commandes

Il est aussi possible de définir soi-même une étiquette sur la dernière page « à la main », en insérant la commande `\label{PageFin}` sur la dernière page (`PageFin` n'étant ici qu'un exemple de nom possible).

On peut aussi, dans le préambule, faire cette déclaration :

```latex
% !TEX noedit
\AtEndDocument{\label{PageFin}}
```

Il suffira ensuite de faire référence à la page de cette étiquette avec `\pageref{PageFin}`.

Ces deux solutions sont à réserver à des documents simples.

## Avec l'extension « lastpage »

Les documents simples (ceux qui commencent à la page 1 et qui n'ont aucune interruption dans leur numérotation des pages jusqu'à leur dernière page) ne présentent aucun problème particulier. Le nombre de pages est indiqué par l'extension {ctanpkg}`lastpage` dans son étiquette `LastPage`, que l'on peut afficher avec `\pageref{LastPage}`.

Pour les documents plus compliqués (tels des livres comportant une première séquence de numéros de pages distincte de la suite), cette approche simple ne fonctionnera pas.

## Avec l'extension « count1to »

L'extension {ctanpkg}`count1to` définit une étiquette `TotalPages`. Elle prend sa valeur d'après celle de `\count1` à la fin du document, `\count1` étant un registre de comptage réservé de {tttexlogo}`TeX`.

{octicon}`alert;1em;sd-text-warning` Cette extension nécessite l'extension {ctanpkg}`everyshi`.

## Avec l'extension « totpages »

L'extension {ctanpkg}`totpages` définit une étiquette `TotPages` et elle rend également disponible le registre `TotPages`, qu'elle utilise comme compteur {latexlogo}`LaTeX`. Sa valeur est donc accessible par le biais de la commande `\theTotPages`. Bien sûr, le compteur `TotPages` est asynchrone, de la même façon que le sont les numéros de pages, mais sa valeur peut être utilisée en toute sécurité lors de l'exécution de la routine de sortie.

{octicon}`alert;1em;sd-text-warning` Cette extension nécessite l'extension {ctanpkg}`everyshi`.

## Avec l'extension « memoir »

La classe {ctanpkg}`memoir` définit deux compteurs `lastpage` et `lastsheet`, qui sont déterminés (après la première exécution d'un document) à l'image des étiquettes `LastPage` et `TotalPages`.

______________________________________________________________________

*Source :* {faquk}`How many pages are there in my document? <FAQ-howmanypp>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,compter les pages,numéro de la dernière page,référence à la dernière page,nombre total de pages
```
