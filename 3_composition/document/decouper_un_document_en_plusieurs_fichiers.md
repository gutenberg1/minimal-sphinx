```{role} latexlogo
```
```{role} tttexlogo
```
# Comment découper un document en plusieurs fichiers ?

Pour travailler sur un gros document, il est plus pratique de le découper en plusieurs fichiers plus petits. La démarche consiste à faire un fichier principal, duquel on appelle d'autres fichiers, dans lesquels on peut encore faire appel à d'autres fichiers, et ainsi de suite...

## Avec la commande « \\input »

La commande `\input{⟨fichier⟩}` permet d'*inclure* le fichier `fichier` dans le fichier principal. Cette commande réalise une importation pure et simple. Elle est plutôt réservée à l'importation de fichiers de macros ou de tableaux. `\input` revient à faire un copier-coller du contenu du fichier `fichier` à l'endroit où cette commande est utilisée.

En voici un exemple fictif faisant appel à deux autres fichiers :

```latex
% !TEX noedit
\documentclass{report}

\begin{document}
  \input{partie1.tex}
  \input{partie2.tex}
  \tableofcontents
\end{document}
```

## Avec les commandes « \\include » et « \\includeonly »

La commande `\include{⟨chapitre⟩}` permet d'*intégrer* le fichier `chapitre.tex` dans le document principal en commençant une nouvelle page. Cette commande réinitialise la numérotation des titres.

La commande `\includeonly{⟨chapitre1⟩,⟨chapitre3⟩}` permet de n'intégrer que les fichiers qu'elle cite, dès lors qu'ils sont appelés dans le document maître grâce à `\include`. L'exemple suivant n'affichera donc que deux des quatre chapitres :

```latex
% !TEX noedit
\documentclass{report}

\includeonly{chapitre1,chapitre3}

\begin{document}
  \tableofcontents
  \include{chapitre1}
  \include{chapitre2}
  \include{chapitre3}
  \include{chapitre4}
\end{document}
```

:::{note}
La commande `\include` ne permet pas d'intégrer un fichier contenant lui-même une commande `\include`.
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX,inclure un fichier dans un autre,double include,\\input,\\include
```
