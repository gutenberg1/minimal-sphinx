```{role} latexlogo
```
```{role} tttexlogo
```
# Comment renvoyer une référence en note de bas de page ?

La question peut se comprendre de plusieurs manières :

- placer le numéro de la référence en note de bas de page, tout en conservant la bibliographie à sa place usuelle ;
- placer tout le contenu de l'entrée citée en note de bas de page.

## Avec des commandes de base

Dans le premier cas, il suffit de redéfinir la commande `\@cite` comme suit :

```latex
% !TEX noedit
\makeatletter
\def\@cite#1#2{%
  \footnote{#1\if@tempswa, #2\fi}}
\makeatother
```

## Avec l'extension « footbib »

Dans le second cas, la solution consiste à passer par l'extension {ctanpkg}`footbib`. Bien que pratique, elle ne cohabite pas bien avec l'extension {ctanpkg}`multicol`, entre autres.

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie,note en bas de page
```
