```{role} latexlogo
```
```{role} tttexlogo
```
# Comment citer un document sans y faire référence ?

La commande `\nocite` permet de faire apparaître un document dans la {doc}`bibliographie </3_composition/annexes/bibliographie/start>` que la référence apparaisse dans le corps du texte. Elle s'utilise exactement comme la commande `\cite` :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
Le manuel de référence de \LaTeX%
[1] est concis et
clair, le \TeX{}book%
\nocite{Knuth84} est très détaillé,
très précis, mais volumineux\dots
\end{document}
```

Ici, la référence à `Knuth84` est invisible dans le texte, mais sera listée dans la bibliographie en fin de document.

Si le style bibliographique utilisé ne trie pas les entrées, la référence incluse par `\nocite` sera placée comme si elle avait été incluse par `\cite`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,annexes,bibliographie,citation invisible,référence invisible,références bibliographiques
```
