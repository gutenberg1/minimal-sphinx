```{role} latexlogo
```
```{role} tttexlogo
```
# Comment saisir une liste d'auteurs sous BibTeX ?

`BibTeX` a une syntaxe assez stricte pour les listes de noms d'auteurs (ou d'éditeurs) dans le fichier de données `.bib`. Si vous écrivez cette liste à votre manière, il y a de fortes chances que `BibTeX` ne vous comprenne pas : la sortie produite sera différente de ce que vous aviez espéré.

:::{note}
Il y a une raison simple à cette rigidité : la syntaxe permet à BibTeX de deviner ce qui est le prénom et le nom, pour pouvoir ensuite remettre les éléments dans l'ordre prescrit par le style bibliographique.

Une autre solution à ce problème serait de baliser complètement les prénoms et noms avec des `firstname={...}` et `lastname={...}`, par exemple. Mais ça alourdirait la syntaxe beaucoup plus que les conventions actuellement utilisées par BibTeX.
:::

Un nom d'auteur est, de façon générale, composé de quatre parties : le prénom, la particule ("de" ou "von", par exemple), le nom (de famille) et le « complément » ou « suffixe » ("Jr", par exemple). BibTeX connaît trois formats pour les noms :

- `Prénom particule Lastname`
- `particule Nom, Prénom`
- `particule Nom, Suffixe, Prénom`

Il est important de respecter ces syntaxes, y compris les virgules et majuscules. Voici quelques exemples :

```bibtex
Victor Hugo
Étienne de Crécy
van Beethoven, Ludwig
Dumas, Fils, Alexandre
```

Les deux dernières manières d'écrire permettent en particulier de gérer la présence de plusieurs prénoms ou de noms de famille multiples, car la virgule sépare clairement les éléments pour `BibTeX`.

Qui plus est, les différents noms devront impérativement être séparés par des `and`. Voici un exemple de ce qu'il ne faut **pas** faire :

```latex
% !TEX noedit
AUTHOR = {Jean L. Auteur, Anne O'Nyme \& Quelqu'un d'autre}
```

Ce bout de code tombe sous le coup d'une règle vue ci-dessus : la virgule, pleine de sens pour `BibTeX`, apparaît à un endroit incorrect. De plus, le `\&` n'est pas compris comme séparateur de noms. Aussi, le résultat de ce qui précède pourrait ressembler à ceci :

```latex
Anne O'Nyme \& Quelqu'un d'autre Jean L. Auteur
```

car `Anne O'Nyme \& Quelqu'un d'autre` est compris comme le « prénom », tandis que `Jean L. Auteur` est devenu le « nom de famille » d'une unique personne. La bonne façon d'écrire cette liste de noms est :

```latex
% !TEX noedit
AUTHOR = {Jean L. Auteur and Anne O'Nyme and Quelqu'un d'autre}
```

Certains styles bibliographiques font des acrobaties avec les très longues listes d'auteurs. Par exemple, ils peuvent autoriser la troncature d'une liste de noms en utilisant le pseudo-nom `others`, qui se traduira généralement par quelque chose comme « *et al.* » dans la sortie mise en forme. Aussi, si M. Auteur voulait attirer l'attention sur lui, il écrirait :

```latex
% !TEX noedit
AUTHOR = {Jean L. Auteur and others}
```

______________________________________________________________________

*Source :* {faquk}`BibTeX doesn't understand lists of names <FAQ-manyauthor>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographies,prénom et nom de famille,particules avec BibTeX,noms composés avec BibTeX
```
