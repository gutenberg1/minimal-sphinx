```{role} latexlogo
```
```{role} tttexlogo
```
# Comment changer le titre de la bibliographie ?

## Avec les commandes de base

Cette technique n'est qu'un cas particulier de la méthode présentée à la question « [Comment changer les textes prédéfinis de LaTeX ?](/3_composition/langues/traduire_le_titre_de_table_des_matieres_ou_bibliographie) ». Il suffit ainsi de redéfinir la commande contenant le nom de la bibliographie, cette commande dépendant pour une fois de la classe utilisée. Il faut soit redéfinir :

- `\refname` pour la classe {ctanpkg}`article` ;
- `\bibname` pour les classes {ctanpkg}`book` et {ctanpkg}`report`.

Par exemple :

```latex
% !TEX noedit
\renewcommand{\bibname}{Liste des documents cités}
```

## Avec l'extension « babel »

L'extension {ctanpkg}`babel` permet d'utiliser plusieurs langues. Son utilisation impose une manière particulière de redéfinir le titre de la table des matières. Par exemple pour le français :

```latex
% !TEX noedit
\addto\captionsfrench{%
  \renewcommand*{\bibname}{Liste des documents cités}%
}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie,titre
```
