```{role} latexlogo
```
```{role} tttexlogo
```
# Comment trier les références de la bibliographie ?

## Avec le programme BibTeX

`BibTeX` dispose d'une fonction de tri et la plupart des styles `BibTeX` trient la liste de références. Cela répond au besoin de la plupart des utilisateurs. La question « [Comment générer une bibliographie ?](/3_composition/annexes/bibliographie/construire_une_bibliographie) » détaille la méthode à retenir.

## Avec des commandes de base

Il est également possible d'écrire un environnement `thebibliography` qui *semble* provenir de `BibTeX`, solution parfois utile pour gagner du temps à court terme. Ici encore, la question « [Comment générer une bibliographie ?](/3_composition/annexes/bibliographie/construire_une_bibliographie) » présente cette solution.

Le problème se pose lorsque le rédacteur décide que ses références doivent être triées. Là, un malentendu courant consiste à insérer la commande `\bibliographystyle{alpha}` (ou similaire) et à s'attendre à ce que la sortie composée soit triée. Mais cela ne marche pas car il y a ici confusion :

- entre l'environnement `thebibliography` « fait main » dont vous devez trier vous-même le contenu : vous n'obtiendrez jamais rien d'autre que ce que vous avez saisi dans l'environnement ;
- et l'environnement `thebibliography` que construit automatiquement `BibTeX` lorsqu'il est utilisé. En effet, `BibTeX` se base sur la commande `\bibliographystyle` pour connaître le style de la bibliographie à appliquer. Avec cette information (et les autres commandes et options qu'il récupère), il compose un environnement `thebibliography` (placé dans un fichier d'extension `.bbl` que {latexlogo}`LaTeX` va incorporer à votre document), cet environnement pouvant être trié si le style choisi le demande.

______________________________________________________________________

*Source :* {faquk}`Sorting lists of citations <FAQ-sortbib>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographies
```
