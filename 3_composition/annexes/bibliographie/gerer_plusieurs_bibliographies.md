```{role} latexlogo
```
```{role} tttexlogo
```
# Comment obtenir plusieurs bibliographies dans un document ?

Plusieurs extensions traitent ce sujet. Certaines d'entre elles utilisent la structure du document pour générer les bibliographies, ce qui permet par exemple d'obtenir une bibliographie par chapitre, par section... Ces cas de liaison à la structure du document sont traités dans la question « [Comment obtenir des bibliographies séparées par chapitre ?](/3_composition/annexes/bibliographie/bibliographies_par_chapitre) ».

## Avec les commandes de base

Il est possible d'avoir plusieurs environnements `thebibliography` dans un document. La méthode « sans `BibTeX` » fonctionne donc sans aucun problème.

Par contre, avec `BibTeX`, seule une unique commande `bibliography` peut être utilisée puisqu'il n'y a qu'un fichier `.aux` généré par la compilation. Il existe cependant des extensions permettant de contourner cette limitation, qui vont donc s'efforcer de créer différents fichiers `.aux`.

## Avec l'extension « multibbl »

L'extension {ctanpkg}`multibbl` propose une interface très simple en mettant à disposition la commande `\newbibliography` pour définir une balise bibliographique. L'extension redéfinit les autres commandes de bibliographie de sorte que chaque fois que vous utilisez l'une d'entre elles, vous lui attribuez la balise de la bibliographie où vous souhaitez que les références citées apparaissent. La commande `\bibliography` elle-même prend également un autre argument supplémentaire qui indique quel titre donner à la section ou au chapitre résultant (elle patche donc les commandes [\\refname et \\bibname](/3_composition/langues/traduire_le_titre_de_table_des_matieres_ou_bibliographie) en respectant l'extension {ctanpkg}`babel`). On pourrait donc écrire :

```latex
% !TEX noedit
\usepackage{multibbl}
\newbibliography{liv}
\bibliographystyle{liv}{alpha}
\newbibliography{art}
\bibliographystyle{art}{plain}
(...)
\cite[p.~23--25]{liv}{milne:test}
(...)
\cite{art}{einstein:1905}
(...)
\bibliography{liv}{livre-bib}{Livres de référence}
\bibliography{art}{art-bib}{Articles de référence}
```

Notez que :

- l'argument optionnel de la commande `\cite` apparaît *avant* le nouvel argument contenant la balise ;
- les commandes `\bibliography` peuvent lister plus d'un fichier `bib`. De fait, les commandes `\bibliography` peuvent lister le même ensemble de fichiers.

Les données utiles à `BibTeX` seront stockées dans des fichiers dont les noms sont `⟨nom-de-balise⟩.aux`. Aussi, pour le cas ci-dessus, pour bien préparer vos bibliographies, vous devrez exécuter les commandes suivantes :

```latex
% !TEX noedit
bibtex liv
bibtex art
```

## Avec l'extension « multibib »

L'extension {ctanpkg}`multibib` permet de découper « la » bibliographie en plusieurs bibliographies différentes. Pour reprendre l'exemple ci-dessus, cela donnerait :

```latex
% !TEX noedit
\usepackage{multibib}
\newcites{liv,art}%
         {Livres de référence,%
          Articles de référence}
\bibliographystyleliv{alpha}
\bibliographystyleart{plain}
(...)
\citeliv[p.~23--25]{milne:test}
(...)
\citeart{einstein:1905}
(...)
\bibliographyliv{liv-bib}
\bibliographyart{art-bib}
```

Encore une fois, comme pour {ctanpkg}`multibbl`, toute commande `\bibliography...` peut recevoir n'importe quelle liste de fichiers `.bib`. Par ailleurs, le traitement `BibTeX` de fichiers utilisant {ctanpkg}`multibib` ressemble beaucoup à celui de fichiers utilisant {ctanpkg}`multibbl`. Pour l'exemple présent, il faudrait utiliser les commandes :

```latex
% !TEX noedit
bibtex liv
bibtex art
```

Cependant, contrairement à {ctanpkg}`multibbl`, {ctanpkg}`multibib` permet de générer une bibliographie simple et non modifiée (en plus des bibliographies par thème).

## Avec les extensions « bibtopic » et « placeins »

Les extensions {ctanpkg}`bibtopic` et {ctanpkg}`placeins` permettent aussi de découper la bibliographie en différentes sections. À l'endroit approprié dans votre document, vous placez une séquence d'environnements `btSect` (dont chacun indique une base de données bibliographiques à analyser) pour composer les bibliographies séparées. Avec notre exemple, cela donnerait :

```latex
% !TEX noedit
\usepackage[above,section]{placeins}
\usepackage{bibtopic}
\bibliographystyle{alpha}
(...)
\cite[p.~23--25]{milne:test}
(...)
\cite{einstein:1905}
(...)
\begin{btSect}{liv-bib}
\section{Livres de référence}
\btPrintCited
\end{btSect}

\begin{btSect}[plain]{art-bib}
\section{Articles de référence}
\btPrintAll
\end{btSect}
```

Notez ici deux choses :

- la commande `btPrintAll` est l'équivalent de `\nocite{*}` dans la mesure où elle permet de citer toutes les références du fichier `bib`. La commande `\btPrintNotCited` donne, pour sa part, le reste du contenu du fichier `.bib` : si aucune entrée de la base de données n'est citée, cela équivaut à `\nocite{*}` ;
- la manière de spécifier un style de bibliographie change : si vous voulez un style différent pour une bibliographie, il faut le donner comme argument facultatif à l'environnement `btSect`.

Le traitement avec `BibTeX` utilise des fichiers `.aux` dont les noms sont dérivés de celui du document de base. En supposant que votre fichier principal s'appelle `fichier.tex`, il faudra ici saisir les commandes :

```latex
% !TEX noedit
bibtex fichier1
bibtex fichier2
```

Cependant, la *grande* différence de cette extension avec {ctanpkg}`multibbl` et {ctanpkg}`multibib` est que la sélection de ce qui apparaît dans chaque section bibliographique est déterminée dans {ctanpkg}`bibtopic` par ce qui est dans les fichiers `bib`.

## Avec l'extension « splitbib »

L'extension {ctanpkg}`splitbib` adopte une approche totalement différente. Un environnement `category` placé dans le préambule de votre document vous permet de définir une catégorie de références pour lesquelles vous souhaitez une bibliographie distincte. Dans chaque environnement, vous listez les clés mises dans la commande `\cite` que vous voulez voir listées dans cette catégorie. La commande `\bibliography` (ou, plus précisément, l'environnement `thebibliography` qu'elle utilise) va trier les clés comme demandé et les clés non mentionnées dans un environnement `category` apparaissent dans une catégorie « divers » créée lors de ce tri.

Un exemple de code apparaît dans la {texdoc}`documentation <splitbib>` de l'extension.

______________________________________________________________________

*Source :* {faquk}`Multiple bibliographies? <FAQ-multbib>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie,bibliographies,plusieurs bibliographies,bibliographies multiples
```
