```{role} latexlogo
```
```{role} tttexlogo
```
# Comment créer un fichier bibliographique BibTeX ?

Un fichier bibliographique `BibTeX` est un fichier texte dont le contenu peut être comparé à une petite base de données où chaque entrée est composée d'éléments bibliographiques. Ces entrées peuvent être appelées par des références dans un autre document.

## Structure d'un fichier bibliographique BibTeX

Un fichier bibliographique `BibTeX` a toujours pour extension « .bib ». Il contient différentes entrées disposées les uns à la suite des autres, par exemple :

```bibtex
% !TEX noedit
@book{knuth_texbook_1986,
    author = "Knuth, Donald Ervin",
    title = "The {\TeX book}",
    publisher = "Addison-Wesley",
        series = "Computers \& typesetting",
    number = "A",
    year = "1986"
}
@book{mittelbach_latex_2005,
    title = "{\LaTeX} Companion",
    author = "Mittelbach, Frank and Goossens, Michel and Braams, Johannes and Carlisle, David and Rowley, Chris
                  and Detig, Christine and Schrod, Joachim",
    language = "French",
    publisher = "Pearson Education",
        edition = "2\superscript{e} édition",
    year = "2005"
}
```

La structure de chaque entrée est syntaxiquement la suivante (dans laquelle il faut bien faire attention à la présence des virgules) :

```bibtex
% !TEX noedit
@type{cle_unique,
    champ1 = "valeur1",
    champ2 = "valeur2",
    (...)
        champN = "valeurN"
}
```

Chaque entrée de la bibliographie a un *type*, une *clé* unique et différents *champs* variables. La bibliographie est lue par `BibTeX` en utilisant les détails spécifiés dans un *fichier de style bibliographique*. À partir de ce fichier de style, `BibTeX` détermine quelles entrées sont autorisées, quels *champs* chaque type d'entrée possède et comment formater l'ensemble de l'entrée.

### Le type

Le *type* spécifie le type de document auquel vous faites référence. La liste des types possibles est limitée : elle propose par exemple des valeurs comme `book` pour un livre, `article` pour un article d'un magazine ou d'une revue, `manual` pour un document technique, `proceedings` pour les actes d'une conférence, `misc` pour des documents inclassables.

### La clé unique

La *clé* unique est à votre libre choix (du moment que vous vous limitez à utiliser des chiffres, lettres et quelques caractères usuels comme « _ » ,« - », « : », « . »). Cette clé s'utilise lorsque vous voulez [citer une entrée](/3_composition/annexes/bibliographie/construire_une_bibliographie) dans votre document principal. Souvent, les utilisateurs créent une clé qui combine le nom de l'auteur (principal) et l'année de publication, éventuellement avec un marqueur pour distinguer les publications de la même année.

### Les champs

Les *champs* dépendant normalement du type de document que vous avez choisi. Certains champs sont obligatoires (comme le champ donnant l'auteur, `author`) et d'autres sont facultatifs (tel le champ `series` dans l'exemple ci-dessus). Tous peuvent contenir des commandes {latexlogo}`LaTeX`.

Le seul champ présentant une réelle particularité d'écriture est celui du nom des auteurs (comme l'exemple ci-dessus le montre avec l'ordre particulier du nom et du prénom des auteurs, la présence de la virgule entre eux et la présence du mot « and » entre chaque auteur). Voir sur ce sujet la question « [Comment saisir une liste d'auteurs sous BibTeX ?](/3_composition/annexes/bibliographie/liste_d_auteurs) » et le cas plus rare de la question « [Comment définir des initiales de prénom regroupant au moins deux lettres ?](3_composition/annexes/bibliographie/utiliser_les_deux_premieres_lettres_du_prenom_d_un_auteur) »

Un autre champ peut poser difficulté : celui des URL. Voir sur ce sujet la question « [Comment citer une URL avec BibTeX ?](/3_composition/annexes/bibliographie/citer_une_url) ».

## Outils pour faciliter la création de fichiers bibliographiques

Si les principes vus ci-dessus suffisent pour rédiger une base de données bibliographiques, une base de taille importante peut vite devenir fastidieuse à maintenir. Heureusement, il existe plusieurs outils pour vous aider.

### Outils de gestion de bases bibliographiques

En premier lieu, la plupart des bons {doc}`éditeurs LaTeX </6_distributions/editeurs/start>` proposent des modes de saisie de fichiers bibliographiques.

Il existe un certain nombre de systèmes de gestion de bibliographie `BibTeX`, dont certains avec interface graphique. S'ils ne sont pas disponibles avec les distributions {tttexlogo}`TeX` ordinaires, ils n'en sont pas moins accessibles :

- des outils tels que {ctanpkg}`Xbibfile <xbibfile>` (une interface utilisateur graphique), {ctanpkg}`ebib` (une application de base de données écrite pour s'exécuter au sein d`'Emacs`) et {ctanpkg}`btOOL <btool>` (un ensemble d'outils Perl pour créer des gestionnaires de bases de données `BibTeX`) sont disponibles sur le CTAN ;
- d'autres outils, tels que [Zotero](/3_composition/annexes/bibliographie/zotero_et_latex)'', `RefDB`, `BibDesk`, `pybliographer` ou bien encore l'outil Java `JabRef` (qui remplace l'ancien ''[Bibkeeper](http://freshmeat.net/projects/bibkeeper/)) sont disponibles depuis leurs sites de développement ;
- il existe bien entendu également des outils commerciaux permettant d'exporter des résultats au format `BibTeX`, tel [EndNote](https://www.endnote.com/).

Dans le cas où vous avez déjà un environnement `thebibliography` (ou que vous avez perdu le fichier bibliographique et qu'il ne vous reste plus que le fichier contenant cet environnement), le script Perl {ctanpkg}`tex2bib` vous sera probablement utile pour (re)construire alors un fichier bibliographique. Voir sur ce point la question « [Comment reconstruire un fichier « .bib » ?](/3_composition/annexes/bibliographie/reconstruire_un_fichier_bib) ».

### Outils de récupération de références en ligne

Les entrées de bases de données bibliographiques en ligne peuvent souvent être traduites au format `BibTeX` par des utilitaires disponibles sur CTAN. Par exemple, le script Perl {ctanpkg}`isi2bibtex` traduit les références de « [Web of Science](https://clarivate.com/webofsciencegroup/solutions/web-of-science/) » (un service d'abonnement, disponible pour les universitaires britanniques via BIDS). Les universitaires britanniques peuvent traduire les téléchargements BIDS en utilisant le script Perl {ctanpkg}`bids.to.bibtex <bidstobibtex>`.

Le site [Google Scholar](https://scholar.google.com) (décrit [ici](https://fr.wikipedia.org/wiki/Google_Scholar)) fournit un lien « Importer dans BibTeX » pour chaque référence qu'il trouve pour vous : cet lien vous donne une page contenant l'entrée `BibTeX` de la référence. Cette fonctionnalité s'obtient en procédant à une légère configuration de Google Scholar en allant chercher dans le menu compacté en haut à gauche de la page principale le lien « [Paramètres](https://scholar.google.ca/scholar_settings) » puis en cochant dans l'écran qui apparaît l'option « Afficher les liens permettant d'importer des citations dans » et en choisissant `BibTeX`.

Le site [Zoterobib](https://zbib.org/) permet également de rechercher et de récupérer en format `BibTeX` des références d'ouvrage en ligne.

______________________________________________________________________

*Source :* {faquk}`Creating a BibTeX bibliography file <FAQ-buildbib>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographies,références bibliographique,base bibliographique,fichier bib,éditeur de bibliographie,editeur pour BibTeX,jabRef,EndNotes
```
