```{role} latexlogo
```
```{role} tttexlogo
```
# Comment faire référence à une thèse française ou un mémoire en bibliographie ?

Il faut utiliser une entrée de type `\@phdthesis`.

Se pose alors le problème de la traduction en français du style bibliographique : voir ici la question « [Comment mettre en forme une bibliographie en langue non-anglaise ?](/3_composition/annexes/bibliographie/bibliographies_internationales) ».

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie,thèse,mémoire
```
