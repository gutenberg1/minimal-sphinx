```{role} latexlogo
```
```{role} tttexlogo
```
# Comment référencer les pages contenant des citations ?

- Le package {ctanpkg}`backref` permet de faire ça.

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie
```
