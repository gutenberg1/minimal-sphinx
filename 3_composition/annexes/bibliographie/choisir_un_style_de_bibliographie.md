```{role} latexlogo
```
```{role} tttexlogo
```
# Comment choisir un style de bibliographie BibTeX ?

Le style de la bibliographie est défini par la commande `\bibliographystyle`. Son argument indique le fichier de style (d'extension « `.bst` ») à utiliser.

Les fichiers de style bibliographique sont lus par `BibTeX`. Ils lui indiquent la mise en page à utiliser. Voici les caractéristiques des principaux styles en se basant sur le fichier bibliographique suivant :

```latex
@Book{complot,
author = "Machiavel, Nicolas",
title = "Le complot pour les nuls",
publisher = "{\'E}ditions Dubois",
year = "1522"
}

@Article{vu,
author = "Londres, Albert and Rochefort, Henri",
title = "Machiavel n'avait rien vu",
journal = "Le quotidien d'hier",
year = "1912"
}

@Book{titrer,
author = "Escher, Maurits Cornelis",
title = "Comment mal titrer un livre, y compris celui-ci",
publisher = "{\'E}ditions du labyrinthe",
month = jul,
year = "1956",
volume = "13"
}
```

## Les quatre styles de base de BibTeX

Les styles de base de `BibTeX` sont par défaut en anglais. Des {ctanpkg}`styles francisés <bib-fr>` existent et sont présentés dans les exemples qui suivent. Ceci dit, le cas du français et d'autres langues sont détaillés dans la question « [Comment mettre en forme une bibliographie en langue non-anglaise ?](/3_composition/annexes/bibliographie/bibliographies_internationales) ».

### Les styles « abbrv » et « abbrv-fr »

Le style {ctanpkg}`abbrv <bibtex>` (pour *abbreviated*) est similaire au style {ctanpkg}`plain <bibtex>` ci-dessous, mais les prénoms des auteurs ainsi que les noms des mois sont abrégés.

```latex
\documentclass[10pt]{article}
\usepackage[french]{babel}
\pagestyle{empty}

\begin{document}
Voici quelques références : [2], [3] et [1].
\begin{thebibliography}{1}

\bibitem{titrer}
M.~C. {\scshape Escher} :
\newblock {\em Comment mal titrer un livre, y compris celui-ci}, vol.~13.
\newblock {\'E}ditions du labyrinthe, juil. 1956.

\bibitem{vu}
A.~{\scshape Londres} et
  H.~{\scshape Rochefort} :
\newblock Machiavel n'avait rien vu.
\newblock {\em Le quotidien d'hier}, 1912.

\bibitem{complot}
N.~{\scshape Machiavel} :
\newblock {\em Le complot pour les nuls}.
\newblock {\'E}ditions Dubois, 1522.

\end{thebibliography}
\end{document}
```

### Les styles « alpha » et « alpha-fr »

Le style {ctanpkg}`alpha <bibtex>` (pour *alphabetic*) présente les clefs utilisées dans le document sous la forme mélangeant début du nom et année d'édition. Les entrées sont également triées dans l'ordre alphabétique des auteurs.

```latex
\documentclass[10pt]{article}
\usepackage[french]{babel}
\pagestyle{empty}

\begin{document}
Voici quelques références : [LR12], [Mac22] et [Esc56].
\begin{thebibliography}{Mac22}
\bibitem[Esc56]{titrer}
Maurits~Cornelis {\scshape Escher} :
\newblock {\em Comment mal titrer un livre, y compris celui-ci}, volume~13.
\newblock {\'E}ditions du labyrinthe, juillet 1956.

\bibitem[LR12]{vu}
Albert {\scshape Londres} et Henri {\scshape Rochefort} :
\newblock Machiavel n'avait rien vu.
\newblock {\em Le quotidien d'hier}, 1912.

\bibitem[Mac22]{complot}
Nicolas {\scshape Machiavel} :
\newblock {\em Le complot pour les nuls}.
\newblock {\'E}ditions Dubois, 1522.

\end{thebibliography}
\end{document}
```

### Les styles « plain » et « plain-fr »

Le style {ctanpkg}`plain <bibtex>` donne des clés numériques (par exemple « \[12\] »). Les entrées sont triées dans l'ordre alphabétique des auteurs.

```latex
\documentclass[10pt]{article}
\usepackage[french]{babel}
\pagestyle{empty}

\begin{document}
Voici quelques références : [2], [3] et [1].
\begin{thebibliography}{1}

\bibitem{titrer}
Maurits~Cornelis {\scshape Escher} :
\newblock {\em Comment mal titrer un livre, y compris celui-ci}, volume~13.
\newblock {\'E}ditions du labyrinthe, juillet 1956.

\bibitem{vu}
Albert {\scshape Londres} et Henri
  {\scshape Rochefort} :
\newblock Machiavel n'avait rien vu.
\newblock {\em Le quotidien d'hier}, 1912.

\bibitem{complot}
Nicolas {\scshape Machiavel} :
\newblock {\em Le complot pour les nuls}.
\newblock {\'E}ditions Dubois, 1522.

\end{thebibliography}
\end{document}
```

### Les styles « unsrt » et « unsrt-fr »

Le style {ctanpkg}`unsrt <bibtex>` (pour *unsorted*) donne également des clés numériques. Les entrées ne sont ici pas triées, elles apparaissent dans l'ordre où elles sont citées dans le document.

```latex
\documentclass[10pt]{article}
\usepackage[french]{babel}
\pagestyle{empty}

\begin{document}
Voici quelques références : [1], [2] et [3].
\begin{thebibliography}{1}

\bibitem{complot}
Nicolas {\scshape Machiavel} :
\newblock {\em Le complot pour les nuls}.
\newblock {\'E}ditions Dubois, 1522.

\bibitem{vu}
Albert {\scshape Londres} et Henri
  {\scshape Rochefort} :
\newblock Machiavel n'avait rien vu.
\newblock {\em Le quotidien d'hier}, 1912.

\bibitem{titrer}
Maurits~Cornelis {\scshape Escher} :
\newblock {\em Comment mal titrer un livre, y compris celui-ci}, volume~13.
\newblock {\'E}ditions du labyrinthe, juillet 1956.

\end{thebibliography}
\end{document}
```

Ce style peut demander certaines manipulations pour éviter des anomalies, comme le montre la question « [Comment gérer le style bibliographique « unsrt » avec des tables des matières ?](/3_composition/annexes/bibliographie/probleme_avec_le_style_unsrt) »

## Les styles « apalike » et « apalike-fr »

Le style {ctanpkg}`apalike <bibtex>` propose des clefs constituées des noms complets des auteurs, ainsi que de l'année. Le tri est fait suivant les noms des auteurs.

```latex
\documentclass[10pt]{article}
\usepackage[french]{babel}
\pagestyle{empty}

\begin{document}
Voici quelques références : [Londres et Rochefort, 1912], [Machiavel, 1522] et [Escher, 1956].
\begin{thebibliography}{}

\bibitem[Escher, 1956]{titrer}
{\scshape Escher}, M.~C. (1956).
\newblock {\em Comment mal titrer un livre, y compris celui-ci}, volume~13.
\newblock {\'E}ditions du labyrinthe.

\bibitem[Londres et Rochefort, 1912]{vu}
{\scshape Londres}, A. et
  {\scshape Rochefort}, H. (1912).
\newblock Machiavel n'avait rien vu.
\newblock {\em Le quotidien d'hier}.

\bibitem[Machiavel, 1522]{complot}
{\scshape Machiavel}, N. (1522).
\newblock {\em Le complot pour les nuls}.
\newblock {\'E}ditions Dubois.

\end{thebibliography}
\end{document}
```

L'aspect de ce style peut être modifié avec l'extension {ctanpkg}`apalike` : les étiquettes de référence sont alors entourées de parenthèses et les clés (très redondantes avec le contenu de chaque entrée) ne sont pas redonnées dans la bibliographie.

```latex
\documentclass[10pt]{article}
\usepackage[french]{babel}
\usepackage{apalike}
\pagestyle{empty}

\begin{document}
Voici quelques références : (Londres et Rochefort, 1912), (Machiavel, 1522) et (Escher, 1956).
\begin{thebibliography}{}

\bibitem[Escher, 1956]{titrer}
{\scshape Escher}, M.~C. (1956).
\newblock {\em Comment mal titrer un livre, y compris celui-ci}, volume~13.
\newblock {\'E}ditions du labyrinthe.

\bibitem[Londres et Rochefort, 1912]{vu}
{\scshape Londres}, A. et
  {\scshape Rochefort}, H. (1912).
\newblock Machiavel n'avait rien vu.
\newblock {\em Le quotidien d'hier}.

\bibitem[Machiavel, 1522]{complot}
{\scshape Machiavel}, N. (1522).
\newblock {\em Le complot pour les nuls}.
\newblock {\'E}ditions Dubois.

\end{thebibliography}
\end{document}
```

## Autres styles

Si beaucoup de gens se contentent avec bonheur de l'un des styles vus jusqu'ici, aucun de ces styles ne propose de références sous la forme « auteur-date », populaire dans de nombreux domaines. En revanche, il existe un très grand nombre de styles distribués séparément, qui prennent en charge ce format.

:::{note}
Notez que le format « auteur-date » a été inventé parce que le format de références produit par le style {ctanpkg}`plain <bibtex>`, pourtant simple et clair, serait une horreur à produire manuellement, en l'absence d'outil comme `BibTeX`.

{tttexlogo}`TeX`, {latexlogo}`LaTeX` et `BibTeX` éliminant les difficultés pendant la phase de préparation du manuscrit, vous avez de nouveau le choix parmi toute une panoplie de styles.
:::

De nombreux sites d'aide existent sur le web :

- un exemple de document, montrant les formats disponibles, peut être trouvé sur [le site web de Ken Turner](http://www.cs.stir.ac.uk/~kjt/software/latex/showbst.html);
- une excellente étude, qui répertorie et classe une grande variété de formats de citations, avec beaucoup d'exemples, s'intitule « [Choosing a BibTeX style](https://www.reed.edu/cis/help/LaTeX/bibtexstyles.html) », sur le site du Reed College.

Bien sûr, ces pages ne sont pas exhaustives... Le principal problème de l'utilisateur curieux sera de comprendre ce que font réellement les différents styles disponibles. Le meilleur moyen d'y parvenir (si les liens ci-dessus ne suffisent pas) est d'utiliser le fichier {ctanpkg}`xampl.bib <bibtex>`, fourni avec la {texdoc}`documentation <bibtex>` de `BibTeX` : il est possible de se faire une idée assez précise de ce que fait chaque style en utilisant ce fichier standard. Par exemple, pour tester le style `mon-style.bst`, vous pouvez écrire et compiler ce simple document {latexlogo}`LaTeX` :

```latex
% !TEX noedit
\documentclass{article}

\begin{document}
\bibliographystyle{mon-style}
\nocite{*}
\bibliography{xampl}
\end{document}
```

Cela produira un échantillon représentatif des formats de citations offerts par le style (mais {ctanpkg}`xampl.bib <bibtex>` est un peu extrême dans certains de ses exemples, et l'exécution de `BibTeX` vous montrera également une sélection intéressante de ses messages d'erreur...).

______________________________________________________________________

*Source :* {faquk}`Choosing a bibliography style <FAQ-whatbst>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographies,style bibliographique,style de bibliographie,bibtex
```
