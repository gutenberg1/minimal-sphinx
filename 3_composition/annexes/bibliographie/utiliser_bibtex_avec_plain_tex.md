```{role} latexlogo
```
```{role} tttexlogo
```
# Comment utiliser BibTeX avec Plain TeX ?

Le fichier `btxmac.tex`, qui fait partie des fichiers du [format](/1_generalites/glossaire/qu_est_ce_qu_un_format) {ctanpkg}`Eplain <eplain>`, contient des commandes et de la documentation pour utiliser `BibTeX` avec Plain {tttexlogo}`TeX`, soit directement, soit avec {doc}`Eplain </1_generalites/glossaire/qu_est_ce_que_eplain>`.

______________________________________________________________________

*Source :* {faquk}`Using BibTeX with Plain TeX <FAQ-bibplain>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographies
```
