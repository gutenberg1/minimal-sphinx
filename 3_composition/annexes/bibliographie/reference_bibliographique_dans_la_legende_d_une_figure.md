```{role} latexlogo
```
```{role} tttexlogo
```
# Comment utiliser la commande « `\cite` » dans une commande « `\caption` » ?

## Avec les commandes de base

La commande `\cite` est une commande dite « fragile », il faut la « [protéger](/2_programmation/syntaxe/c_est_quoi_la_protection) » avec la commande `\protect` pour l'inclure dans des commandes telles que `\caption`, les commandes de sectionnement, etc. Ce qui donnera par exemple :

```latex
% !TEX noedit
\caption{Une figure extraite de~\protect\cite{doc}}
```

:::{note}
Les versions récentes de {latexlogo}`LaTeX` définissent `\cite` à l'aide de `\DeclareRobustCommand` pour qu'elle ne soit pas fragile, mais on peut parfois tomber sur des extensions qui la redéfinissent comme une commande « fragile ».
:::

## Cas particulier avec le style bibliographique « unsrt »

Avec le style de bibliographie `unsrt` peut se produire un problème de numérotation. En effet, ce style permet que les références soient numérotées dans l'ordre où elles apparaissent. Cependant, le fait de rajouter une table des figures (ou des matières...) peut changer cet ordre. La question « [Comment gérer le style bibliographique « unsrt » avec des tables des matières ?](/3_composition/annexes/bibliographie/probleme_avec_le_style_unsrt) » traite ce point.

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie,protection de commande,référence bibliographique dans une légende,référence bibliographique dans un titre
```
