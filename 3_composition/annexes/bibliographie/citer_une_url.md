```{role} latexlogo
```
```{role} tttexlogo
```
# Comment citer une URL avec BibTeX ?

Par défaut, il n'y a pas de champ permettant d'indiquer les URL dans les styles `BibTeX` standards, bien qu'Oren Patashnik (l'auteur de `BibTeX`) envisage d'en développer un pour une utilisation avec la version `BibTeX` 1.0, qui reste à venir depuis de nombreuses années.

L'information réelle qui doit être disponible dans une citation d'une URL est discutée en détail dans un [extrait en ligne de l'ISO 690--2](http://www.collectionscanada.gc.ca/iso/tc46sc9/docs/sc9n473.pdf), en anglais. Les techniques présentées ci-dessous *ne satisfont pas* à toutes les exigences de l'ISO 690--2 mais elles offrent une réponse aux besoins immédiats des utilisateurs.

## En utilisant un autre champ

En attendant de nouveaux développements de `BibTeX`, la technique la plus simple
est d'utiliser le champ `howpublished` de la fonction `@misc` des styles standards. De façon similaire, il est possible d'utiliser le champ `note`.

```bibtex
@misc{...,
  ...,
  howpublished = "\url{http://...}"
}
```

Il est recommandé d'utiliser l'extension {ctanpkg}`url` ou {ctanpkg}`hyperref` dans ce contexte (voir d'ailleurs sur ce point la question « {doc}`Comment écrire des adresses web (ou URL) ? </3_composition/texte/mots/mettre_en_forme_des_url_et_des_adresses_electroniques>` »). En effet, `BibTeX` a l'habitude de diviser les lignes qu'il considère comme trop longues (78 caractères) : s'il n'y a pas de caractère espace à utiliser comme point d'arrêt naturel, `BibTeX` insérera un caractère `%` bien que ce dernier soit un caractère acceptable dans une URL. Les extensions {ctanpkg}`url` et {ctanpkg}`hyperref` détectent cette structure "`%`--fin-de-ligne" dans les URL et la suppriment. Vous pouvez aussi scinder l'URL directement.

```bibtex
note = "\url{ftp://ftp.fdn.org/pub/CTAN/macros/
       latex/contrib/supported/koma-script/
       contrib/adrconv/}"
```

## En utilisant certains styles bibliographiques

Une approche alternative consiste à remplacer les styles standard de `BibTeX` par des styles disposant du champ URL. En voici quelques uns :

- les styles {ctanpkg}`natbib` ({ctanpkg}`plainnat <natbib>`, {ctanpkg}`unsrtnat <natbib>` et {ctanpkg}`abbrvnat <natbib>`), extensions des styles standards à utiliser principalement avec {ctanpkg}`natbib` lui-même. Cependant, ils permettent bien d'obtenir le champ URL ainsi que quelques autres champs « modernes ». L'extension {ctanpkg}`custom-bib` du même auteur est également capable de générer des styles répondant à cette question ;
- l'ensemble {ctanpkg}`babelbib` qui propose des {doc}`bibliographies multilingues </3_composition/annexes/bibliographie/bibliographies_internationales>`. Il fournit également un ensemble d'équivalents de style standard qui disposent du champ URL ;
- des styles plus modernes tels que ceux de l'extension {ctanpkg}`harvard`. Les styles de bibliographie {ctanpkg}`harvard` incluent tous un champ `url` dans leur spécification. Toutefois, la composition proposée n'est pas réellement bien considérée (bien qu'elle reconnaisse et utilise les macros `LaTeX2HTML` si elles sont disponibles, pour créer des hyperliens).

## En constituant son propre style bibliographique

Il est aussi possible d'ajouter un type d'entrée pour citer des pages Web (adapté d'une solution de M. Moreau). Ceci peut se faire par exemple en recopiant le style de base, en changeant son nom et en ajoutant les lignes suivantes (en ne les plaçant pas au début du fichier, parce qu'il faut que les fonctions utilisées soient définies auparavant) :

```bibtex
FUNCTION {format.url}
{ url empty$
    {""}
    {"\texttt{" url "}" * * }
  if$
}

FUNCTION {pageweb}
{ output.bibitem
  format.authors output
  format.title "title" output.check
  new.block
  institution "institution" output.check
  format.date output
  new.block
  format.url "URL" output.check
  newblock
  note output
  fin.entry
}
```

De cette façon, on ajoute une entrée `@pageweb` dont les champs `title`, `institution` et `url` sont obligatoires et dont les champs `author`, `month`, `year` et `note` sont facultatifs.

______________________________________________________________________

*Source :* {faquk}`URLs in BibTeX bibliographies <FAQ-citeURL>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie,adresses web dans la bibliographie,citer une URL,citer un site web,référence à un site web,url
```
