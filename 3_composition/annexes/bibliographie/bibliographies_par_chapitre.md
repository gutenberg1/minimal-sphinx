```{role} latexlogo
```
```{role} tttexlogo
```
# Comment obtenir des bibliographies séparées par chapitre ?

## Avec l'extension « chapterbib »

Pour avoir une bibliographie séparée pour chaque « chapitre » d'un document, on peut utiliser l'extension {ctanpkg}`chapterbib` (qui fournit pas mal d'autres fonctionnalités utiles pour les bibliographies).

Cette extension vous permet d'avoir une bibliographie individuelle pour chaque fichier `\include` : ainsi, malgré le nom de l'extension, la construction des bibliographies se base sur les différents fichiers sources du document plutôt que sur les chapitres définis par sa structure logique. L'extension permet également d'avoir une bibliographie globale pour le document.

## Avec l'extension « bibunits »

L'extension {ctanpkg}`bibunits`, elle, lie les bibliographies aux unités logiques du document : elle traite les chapitres et les sections (comme définis par {latexlogo}`LaTeX`), et définit également un environnement `bibunit` afin que l'utilisateur puisse sélectionner sa propre structuration.

```latex
% !TEX noedit
\documentclass{report}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{bibunits}

\begin{document}

\bibliographyunit[\chapter]
\bibliographystyle{plain}
\bibliography{geometrie,algorithmes}

\chapter{Algorithmes.}
Présentation d'algorithmes. \nocite{*}
\putbib[algorithmes]

\chapter{Géométrie.}
Texte sur la géométrie. \nocite{*}
\putbib[geometrie]

\end{document}
```

La compilation de cet exemple va entraîner la création de plusieurs fichiers `.aux`, qu'il faudra ensuite passer à `BibTeX`.

## Avec l'extension « BibLaTeX » (et le programme « biber »)

L'extension {ctanpkg}`BibLaTeX <biblatex>`, avec {ctanpkg}`biber`, {doc}`fournit une fonction similaire </3_composition/annexes/bibliographie/remplacer_bibtex>` : ajoutez le texte pour lequel vous souhaitez une bibliographie locale dans un environnement `refsection`, et ajoutez à la fin de cet environnement la commande `\printbibliography` :

```latex
% !TEX noedit
\begin{refsection}
\chapter{Premier chapitre}
\section{Ma section}
Du texte\cite{Ceci} avec de références\cite{Cela}.

\printbibliography
\end{refsection}
```

Ensuite, compilez avec {latexlogo}`LaTeX` (pdfLaTeX, XeLaTeX, LuaLaTeX... peu importe) et lancez `biber` pour traiter la bibliographie. Notez que `\printbibliography` peut prendre un argument optionnel `heading=titre biblio` pour que la bibliographie ait un titre de (sous-)section.

______________________________________________________________________

*Sources :*

- {faquk}`Separate bibliographies per chapter? <FAQ-chapbib>`
- <https://stackoverflow.com/questions/2765209/latex-bibliography-per-chapter>

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographies,maquette,sous-bibliographies
```
