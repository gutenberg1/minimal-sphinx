```{role} latexlogo
```
```{role} tttexlogo
```
# Comment changer le style de la bibliographie ?

- Le package {ctanpkg}`overcite` permet de modifier le style des citations : au lieu de « bidule \[5,6,7\] », il donne « bidule⁵⁻⁷ ».
- Le style de la bibliographie est défini par la commande `\bibliographystyle`. fournie par Son argument indique le fichier de style (d'extension `.bst`) à utiliser.

Les fichiers de style bibliographique sont lus par {ctanpkg}`BibTeX <bibtex>`. Ils lui indiquent la mise en page à utiliser. Voici les caractéristiques des principaux styles :

- `abbrv.bst` : similaire au style `plain.bst` ci-dessous, mais les prénoms des auteurs ainsi que les noms des mois sont abrégés ;
- `alpha.bst` : les clefs utilisées dans le document sont de la forme « \[Lam94\] », et les références sont triées dans l'ordre alphabétique des auteurs ;
- `apalike.bst` : les clefs utilisées sont constituées des noms complets des auteurs, ainsi que de l'année. Le tri est fait suivant les noms des auteurs. Comme les labels générés sont longs et ne doivent pas être entourés de crochets, ce style nécessite l'inclusion du package {ctanpkg}`apalike`.
- `plain.bst` : les clefs sont numériques (par exemple « \[12\] »). Les références sont triées dans l'ordre alphabétique des auteurs ;
- `unsrt.bst` : clefs numériques. Les entrées ne sont pas triées, elles apparaissent dans l'ordre où elles sont citées dans le document.

Pour trouver d'autres styles bibliographiques, voir la question « {doc}`Comment choisir un style de bibliographie? </3_composition/annexes/bibliographie/choisir_un_style_de_bibliographie>` ».

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie,format des références bibliographiques
```
