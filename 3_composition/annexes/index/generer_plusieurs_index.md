```{role} latexlogo
```
```{role} tttexlogo
```
# Comment générer plusieurs index ?

Les capacités d'indexation usuelles de {latexlogo}`LaTeX` (fournies par l'extension {ctanpkg}`makeidx`) ne permettent d'obtenir qu'un seul index dans votre document. Cependant, même des documents assez modestes peuvent être améliorés par la présence d'index pour des sujets distincts.

## Avec l'extension « multind »

L'extension {ctanpkg}`multind` permet de générer facilement plusieurs index. Il modifie légèrement les commandes d'indexation afin que l'utilisateur puisse spécifier, pour chaque entrée, l'index concerné.

En pratique, vous balisez chaque commande `\makeindex`, `\index` et `\printindex` avec un nom de fichier et les commandes d'indexation sont écrites et lues dans les fichiers avec les extensions appropriées. La commande `\printindex` est modifiée à partir d'une ancienne version du standard {latexlogo}`LaTeX` 2.09 afin qu'elle ne crée pas son propre titre de chapitre ou de section. Ainsi, vous décidez quels noms (et niveaux de sectionnement) de vos index sans avoir à passer par la solution évoquée par la question « [Comment changer les textes prédéfinis de LaTeX ?](/3_composition/langues/traduire_le_titre_de_table_des_matieres_ou_bibliographie) ».

Voici un exemple donnant un index « fruits » et un index « légumes » :

```latex
% !TEX noedit
\documentclass{article}
\usepackage[french]{babel}
\usepackage{multind}
\makeindex{fruits}
\makeindex{legumes}
\begin{document}
(...) \index{fruits}{poire}
(...) \index{legumes}{haricot}
(...) \index{fruits}{pomme}
(...)
\printindex{fruits}{Index des fruits}
\printindex{legumes}{Index des légumes}
(...)
\end{document}
```

Il faut ensuite exécuter {latexlogo}`LaTeX` sur votre fichier suffisamment de fois pour que les étiquettes, etc., soient stables, puis exécuter les commandes suivantes (si vous utilisez `makeindex`) :

```latex
% !TEX noedit
makeindex fruits
makeindex legumes
```

Puis, il faudra compiler à nouveau votre document avec {latexlogo}`LaTeX` pour obtenir le document avec les index.

Notez ici que les noms des fichiers d'index à traiter ne sont pas du tout liés au nom du fichier {latexlogo}`LaTeX` que vous traitez, ce qui est différent de la manière de procéder vue à la question « [Comment générer un index ?](/3_composition/annexes/index/commandes_de_base) ». Notez aussi qu'il n'y a pas de documentation fournie avec cette extension ! Ce que vous lisez ci-dessus sera à peu près tout ce que vous trouverez sur le sujet.

L'extension {ctanpkg}`multind` ne fonctionne pas avec les [classes AMSLaTeX](/1_generalites/glossaire/que_sont_ams-tex_et_ams-latex), mais il existe un substitut : l'extension {ctanpkg}`amsmidx`. Cette extension s'utilise à peu près de la même manière que {ctanpkg}`multind`. Toutefois, si les choses ne sont pas claires, une {texdoc}`documentation <amsmidx>` est là bien disponible.

## Avec l'extension « index »

L'extension {ctanpkg}`index` fournit un ensemble complet de fonctions d'indexation, y compris une commande `\newindex` qui permet la définition de nouveaux styles d'index. `\newindex` prend une *étiquette* (à utiliser dans les commandes d'indexation pour les rattacher à un index), des remplacements pour les extensions de fichier `idx` et `ind`, et un titre pour l'index lorsqu'il est finalement imprimé. Cette commande peut également changer la référence présentée en index (par exemple, on peut avoir un index d'artistes référencés par le numéro de figure où leur travail est présenté plutôt que par le numéro de page).

En utilisant cette extension pour créer un index d'auteur avec un index classique, il faudrait commencer par les commandes suivantes en préambule :

```latex
% !TEX noedit
\usepackage{index}
\makeindex
\newindex{aut}{adx}{and}{Index des auteurs}
```

Ceci charge l'extension, définit un index principal, puis définit un index d'auteur. Dans le corps du document, nous pourrons trouver des commandes telles que :

```latex
% !TEX noedit
\index[aut]{Anne O'Nyme}
(...)
\index{FAQ}
```

Ces commandes placent respectivement une entrée dans l'index des auteurs et une entrée dans l'index principal. Plus loin se trouveront deux commandes :

```latex
% !TEX noedit
\printindex
\printindex[aut]
```

Ceci affiche l'index principal puis l'index des auteurs. En supposant que tout ceci soit dans un fichier `mondoc.tex`, après un nombre suffisant de compilations pour stabiliser les étiquettes, exécutez les commandes suivantes (ici, des commandes shell Unix mais le principe est le même quel que soit le système que vous utilisez) :

```latex
% !TEX noedit
makeindex mondoc
makeindex mondoc.adx -o mondoc.and
```

Ensuite, recompilez le document avec {latexlogo}`LaTeX`. Les commandes `makeindex` traitent `mondoc.idx` pour obtenir `mondoc.ind` (l'action par défaut) et `mondoc.adx` pour obtenir `mondoc.and`. Les fichiers `mondoc.idx` et `mondoc.and` seront appelés par les deux commandes `\printindex` dans `monfichier.tex`.

## Avec l'extension « splitidx »

L'extension {ctanpkg}`splitindex` peut fonctionner de la même manière que les autres : chargez-la avec l'option `split` et déclarez chaque index avec une commande `\newindex` :

```latex
% !TEX noedit
\newindex[⟨nom de l'index⟩]{⟨raccourci⟩}
```

Dans ce cas, {ctanpkg}`splitidx <splitindex>` générera un fichier `\jobname.⟨raccourci⟩` pour recevoir les entrées d'index générées par des commandes telles que `\sindex[⟨raccourci⟩]{⟨élément⟩}`. Comme avec les autres extensions, cette méthode est limitée par le nombre total de fichiers de sortie de {tttexlogo}`TeX`.

Cependant, {ctanpkg}`splitindex` est également fourni avec un petit exécutable `splitindex` (disponible pour plusieurs systèmes d'exploitation). Si vous utilisez ce programme auxiliaire (et n'utilisez pas `split`), il n'y a pas de limite au nombre d'index. En dehors de cette astuce, {ctanpkg}`splitindex` prend en charge les mêmes fonctionnalités que {ctanpkg}`index`. Un exemple d'utilisation figure dans la documentation.

## Avec l'extension « imakeidx »

L'extension {ctanpkg}`imakeidx` dispose d'un large éventails de possibilités. Par exemple, il peut exécuter un générateur d'index via les commandes (avec la méthode détaillée à la question « [Comment lancer un sous-processus pendant la compilation ?](/2_programmation/compilation/write18) » afin de simplifier la réalisation d'un document.

Cette extension peut créer plusieurs index :

- de manière conventionnelle, comme le fait({ctanpkg}`multind`) ;
- ou de façon plus fine avec le script externe `splitindex` fourni avec l'extension {ctanpkg}`splitindex`.

Cette organisation permet un fonctionnement efficace avec un petit nombre d'index, tout en conservant la flexibilité d'autoriser un grand nombre d'index sans toucher à la restriction du nombre de flux de sortie actifs.

## Avec la classe « memoir »

La classe {ctanpkg}`memoir` a sa propre fonctionnalité d'index multiples. Elle dispose aussi de ses propres options de disposition d'index, que d'autres extensions délèguent au fichier de style d'index utilisé par `makeindex`.

______________________________________________________________________

*Source :* {faquk}`Multiple indexes <FAQ-multind>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,annexes,index,multind
```
