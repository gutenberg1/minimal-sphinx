```{role} latexlogo
```
```{role} tttexlogo
```
# Comment obtenir des annexes ?

## Avec les commandes de base

{latexlogo}`LaTeX` fournit un mécanisme extrêmement simple pour les annexes : la commande `\appendix`. Celle-ci fait passer le document de la génération de sections (dans la classe {ctanpkg}`article`) ou de chapitres (dans les classes {ctanpkg}`report` et {ctanpkg}`book`) à la génération d'annexes. La numérotation des sections ou chapitres est réinitialisée et la représentation du compteur passe en mode alphabétique. Voici un exemple pour la classe {ctanpkg}`article` :

```latex
\documentclass[french]{article}
  \usepackage[utf8]{inputenc}
  \usepackage[T1]{fontenc}
  \pagestyle{empty} %% très important !
\begin{document}
\section{L'idée}
(...)
\section{Conséquences}
(...)
\appendix
\section{Génèse de l'idée}
(...)
\end{document}
```

Cette méthode suffit pour des besoins ordinaires. Notez cependant qu'une fois que vous êtes passé dans les annexes, {latexlogo}`LaTeX` ne vous offre aucun moyen de revenir en arrière : une fois que vous avez une annexe, vous ne pouvez plus avoir une commande `\section` ou `\chapter` ordinaire.

## Avec l'extension « appendix »

L'extension {ctanpkg}`appendix` propose plusieurs fonctionnalités un peu plus avancées. Elle permet par défaut d'avoir un titre introduisant les annexes, à la fois dans le corps du document et dans la table des matières. Ceci s'obtient par :

```latex
% !TEX noedit
\usepackage{appendix}
\renewcommand{\appendixpagename}{Annexes}
\renewcommand{\appendixtocname}{Annexes}
(...)
\appendix
\appendixpage
\addappheadtotoc
```

La commande `\appendixpage` ajoute par défaut le titre « Appendices » au-dessus de la première annexe. tandis que la commande `\addappheadtotoc` insère ce titre dans la table des matières. Ces modifications simples couvrent les besoins de nombreuses personnes en matière d'annexes. Toutefois, pour un utilisateur français, il convient d'ajouter les deux redéfinitions pour franciser (ou modifier) les titres par défaut.

L'extension fournit également un environnement `appendices`. L'environnement est en particulier contrôlé par les options de l'extension. L'exemple précédent serait obtenu avec le code suivant :

```latex
% !TEX noedit
\usepackage[toc,page]{appendix}
(...)
\begin{appendices}
(...)
\end{appendices}
```

Mais voici le point le plus intéressant de cet environnement : une fois qu'il est clos, vous pouvez reprendre votre utilisation normale des sections ou des chapitres. La numérotation n'est pas affectée par les annexes intermédiaires.

L'extension propose une manière alternative de définir des annexes, en tant que divisions inférieures dans le document. L'environnement `subappendices` vous permet de placer des annexes séparées pour une section particulière, codée à l'image de la commande `\subsection`, ou pour un chapitre particulier, codée à l'image de `\section`. On pourrait donc écrire :

```latex
\documentclass[french]{article}
  \usepackage[utf8]{inputenc}
  \usepackage[T1]{fontenc}
  \pagestyle{empty} %% très important !
  \usepackage{appendix}
\begin{document}
\section{L'idée}
(...)
\begin{subappendices}
\subsection{Génèse de l'idée}
(...)
\end{subappendices}
\section{Conséquences}
(...)
\end{document}
```

Cette extension offre d'autres possibilités : la documentation de l'extension apporte plus d'informations.

## Avec les classes « memoir » et « KOMA-script »

La classe {ctanpkg}`memoir` inclut les fonctionnalités de l'extension {ctanpkg}`appendix` et les classes {ctanpkg}`KOMA-script` proposent une commande `\appendixprefix` pour modifier l'apparence des annexes.

______________________________________________________________________

*Source :* {faquk}`Appendixes <FAQ-appendix>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,annexes,annexe,appendix
```
