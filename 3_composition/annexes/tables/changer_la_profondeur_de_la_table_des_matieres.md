```{role} latexlogo
```
```{role} tttexlogo
```
# Comment changer la profondeur de la table des matières ?

Les compteurs `secnumdepth` et `tocdepth` permettent de contrôler respectivement la *profondeur* jusqu'à laquelle les niveaux de sectionnement d'un document sont numérotés et la profondeur jusqu'à laquelle ils entrent dans la table des matières. La profondeur est une valeur numérique associée aux différents niveaux de sectionnement :

- une partie (associée à la commande `\part`) a une profondeur égale à -1 ;
- un chapitre (`\chapter`) a une profondeur de 0 ;
- une section (`\section`) a une profondeur de 1 ;
- et ainsi de suite...

Aussi, par exemple, pour que les sections soient incluses dans la table des matières, mais pas les sous-sections, il faut écrire :

```latex
% !TEX noedit
\setcounter{tocdepth}{1}
```

De façon similaire, pour que les sous-sections soient numérotées, mais pas les sous-sous-sections, il faut indiquer :

```latex
% !TEX noedit
\setcounter{secnumdepth}{2}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
