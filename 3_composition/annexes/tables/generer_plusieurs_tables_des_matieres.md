```{role} latexlogo
```
```{role} tttexlogo
```
# Comment générer plusieurs tables des matières ?

## Avec l'extension « minitoc »

L'extension {ctanpkg}`minitoc` permet de créer une table des matières restreinte pour chaque partie, chapitre ou section selon les possibilitéss des classes.

Son utilisation est présentée à la question : « [Comment obtenir une table des matières par partie, chapitre ou section ?](/3_composition/annexes/tables/generer_une_table_des_matieres_par_chapitre) »

## Avec l'extension « shorttoc »

L'extension {ctanpkg}`shorttoc` permet de créer plusieurs tables des matières avec différentes profondeurs, afin par exemple d'avoir une vue plus globale pour un long document.

```{eval-rst}
.. todo:: *Ajouter un exemple.*
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,tables des matières,table des matières,mini-tables,mini-table,minitoc,shorttoc
```
