```{role} latexlogo
```
```{role} tttexlogo
```
# Que sont les groupes d'utilisateurs de TeX ?

Les groupes d'utilisateurs (*TeX User groups* en anglais) sont des associations à but non lucratif. En mettant en commmun leurs moyens humains ou financiers, leurs membres permettent le développement des projets et la publication d'articles sur {tttexlogo}`TeX`/{latexlogo}`LaTeX`, promeuvent l'usage de {tttexlogo}`TeX`/{latexlogo}`LaTeX` et favorisent l'entraide entre utilisateurs.

Ces groupes ont souvent été créés dès l'apparition de {doc}`TeX </1_generalites/glossaire/qu_est_ce_que_tex>`, dans les années 80. Leur premier rôle, maintenant mineur, était de distribuer {tttexlogo}`TeX` sur des disquettes, puis des CD, avant que l'accès à internet ne se généralise.

## Le TUG international

Le TUG ({tttexlogo}`TeX` Users Group), à proprement parler, est le groupe international basé aux États-Unis. Ce groupe publie la revue [TUGboat](https://www.tug.org/tugboat/) (en anglais), quatre fois par an. *TUGboat* contient les compte-rendus de la conférence annuelle organisée par le TUG, ainsi que des articles généraux sur TeX, LaTeX, la typographie ou les techniques d'impression...

Pour plus de renseignements, consultez le site web <http://www.tug.org/> ou contactez ses membres par mail à l'adresse [tug@tug.org](mailto:tug@tug.org).

## L'association GUTenberg

GUTenberg, le « Groupe francophone des Utilisateurs de {tttexlogo}`TeX` », a pour but de promouvoir l'utilisation de {tttexlogo}`TeX` dans les pays francophones et d'offrir à ses adhérents un ensemble de services aidant à la connaissance et à l'utilisation de {tttexlogo}`TeX` et de son environnement.

[La présente FAQ](http://www.gutenberg.eu.org/faq) est, par exemple, hébergée sur un serveur de l'association GUTenberg : <http://faq.gutenberg.eu.org/>.

Ce groupe d'utilisateurs publie la *Lettre GUTenberg* ainsi que [les cahiers GUTenberg](http://cahiers.gutenberg.eu.org), ces éléments étant décrits sur le [site de l'association](http://www.gutenberg.eu.org/).

:::{note}
Le [Cahier GUTenberg n°23 (1996)](http://cahiers.gutenberg.eu.org/cg-bin/feuilleter?id=CG_1996___23) a publié la FAQ {latexlogo}`LaTeX` de l'époque.
:::

## L'association AsTeX

L'association AsTeX a comme objectif principal d'essayer de faire du travail utile au plus grand nombre, dans le domaine des logiciels scientifiques, et d'essayer de faire ce travail aussi bien que les éditeurs privés, mais dans un esprit de service public.

Cela passe par l'écriture d'utilitaires d'installation et de configuration automatisés (pour que le débutant en {tttexlogo}`TeX`/{latexlogo}`LaTeX` n'ait pas à lire 1 000 pages de docs disparates, en anglais de surcroît, avant de pouvoir imprimer « Bonjour »), par l'écriture de documentations raisonnablement bien rédigées et agréables à consulter, par des distributions sur disquettes bien présentées. Cela passe également par la traduction de documentations originales dans un français correct, etc.

Vous trouverez plus de renseignements ici : [http://www.univ-orleans.fr/EXT/ASTEX/](https://web.archive.org/web/20130420195511/https://www.univ-orleans.fr/EXT/ASTEX/) (version archivée, datée du 20 avril 2013).

AsTeX semble actuellement avoir une activité ralentie (en 2021).

______________________________________________________________________

*Source :* {faquk}`TeX User Groups <FAQ-TUGstar>`

```{eval-rst}
.. meta::
   :keywords: TUG,Association GUTenberg,LaTeX,communauté LaTeX,développement de LaTeX,TeX Users' Groups,TeX User Group
```
