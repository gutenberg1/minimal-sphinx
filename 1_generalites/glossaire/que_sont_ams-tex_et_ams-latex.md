```{role} latexlogo
```
```{role} tttexlogo
```
# Que sont AMS-TeX et AMS-LaTeX ?

{ctanpkg}`AMS-LaTeX <amslatex>` est un terme utilisé pour désigner un ensemble de fichiers distribués par l'[American Mathematical Society](https://fr.wikipedia.org/wiki/American_Mathematical_Society) (AMS). L'essentiel de ces extensions a été conçu par Frank Mittelbach, Michael Downes (décédé) et Rainer Schöpf, et s'appuie sur {latexlogo}`LaTeX`. C'est un outil complémentaire à {latexlogo}`LaTeX` pour écrire des mathématiques (il devient d'ailleurs rapidement indispensable). Quant à {ctanpkg}`AMS-TeX <amstex>`, il est l'équivalent de AMS-LaTeX pour Plain {tttexlogo}`TeX`.

AMS-LaTeX est divisée en deux parties :

- {ctanpkg}`amscls` pour écrire des articles ou livres de l'AMS. Ces classes sont {ctanpkg}`amsart` (pour écrire des article), {ctanpkg}`amsbook` (pour des livres) et {ctanpkg}`amsproc` (pour des comptes-rendus de réunion). Elles font appel à certaines des extensions citées ci-après ;
- {ctanpkg}`amsmath` qui fournit des commandes et environnements supplémentaires pour l'écriture des mathématiques.

Les extensions proposées par `amsmath` sont :

- {ctanpkg}`amsmath` : la base, assez complète. Elle charge en particulier {ctanpkg}`amstext`, {ctanpkg}`amsopn`, and {ctanpkg}`amsbsy` ;
- {ctanpkg}`amstext` : pour écrire du texte en mode mathématique ;
- {ctanpkg}`amsbsy` : pour les symboles gras ;
- {ctanpkg}`amsopn` : pour la déclaration d'opérateurs ;
- {ctanpkg}`amsthm` : pour les environnements `proof` et `theorem` ;
- {ctanpkg}`amsintx` : pour étendre la syntaxe des sommes et intégrales ;
- {ctanpkg}`amscd` : pour les diagrammes commutatifs ;
- {ctanpkg}`amsxtra` : contient quelques fonctions pour la compatibilité avec les anciennes versions ;
- {ctanpkg}`upref` : pour les références croisées.

S'ajoute à AMS-LaTeX et AMS-TeX la collection {ctanpkg}`AMS-Fonts <amsfonts>` contenant de nombreuses polices utiles pour écrire des mathématiques.

Début 2016, la maintenance d`'amsmath` a été transférée à l'équipe du [LaTeX Project](https://www.latex-project.org/). *A contrario*, la responsabilité des classes de documents et de l'extension `amsthm` reste à l'AMS (et sont stockées de façon distincte sur le CTAN).

## Ressources et documentation

Le [site de l'AMS](http://www.ams.org/publications/authors/tex/tex) présente très largement les différents éléments cités ci-dessus.

Par ailleurs, les différentes extensions sont disponibles par le CTAN, les répertoires correspondants sont :

- {ctanpkg}`amslatex` pour AMS-LaTeX ;
- {ctanpkg}`amstex` pour AMS-TeX ;
- {ctanpkg}`amsfonts` pour AMS-Fonts.

## Historique

Toutes ces classes et extensions sont dérivées d'AMS-TeX, une extension {tttexlogo}`TeX` basé sur `Plain` {tttexlogo}`TeX`, écrit à l'origine par Michael Spivak pour l'AMS de 1983 à 1985 et décrite dans le livre {doc}`The Joy of TeX </1_generalites/documentation/livres/documents_sur_tex>`. AMS-TeX fournit de nombreuses fonctionnalités pour améliorer la mise en forme des formules mathématiques en simplifiant la tâche des auteurs. Elle prête attention aux détails les plus fins du dimensionnement et du positionnement auxquels sont attachés les éditeurs d'ouvrages mathématiques. Cependant, elle ne traitait pas les questions de numérotation automatique et de références croisées et, lorsque {latexlogo}`LaTeX` a gagné en popularité, les auteurs ont cherché à soumettre des articles à l'AMS en {latexlogo}`LaTeX`. C'est ainsi qu'AMS-LaTeX a été développé.

L'AMS ne recommande plus l'utilisation d'AMS-TeX et exhorte ses auteurs à utiliser les classes et extensions {latexlogo}`LaTeX` à la place.

______________________________________________________________________

*Source :* {faquk}`What are the AMS packages (amsmath, etc.)? <FAQ-AMSpkg>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,amslatex,amstex,amsmath,ams
```
