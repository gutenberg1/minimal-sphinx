```{role} latexlogo
```
```{role} tttexlogo
```
# Quelle est l'histoire de TeX et LaTeX ?

## TeX

{tttexlogo}`TeX` (1978) est le formateur de texte de [Donald E. Knuth](https://fr.wikipedia.org/wiki/Donald_Knuth). À l'origine, Knuth a développé {tttexlogo}`TeX` (en WEB) notamment pour réaliser de beaux documents et écrire des formules mathématiques.

## Plain TeX

Plain {tttexlogo}`TeX`, écrit également par Donald. E. Knuth, était le premier jeu (minimal) de macros par dessus {tttexlogo}`TeX`. De même Eplain, de [Karl Berry](http://tug.org/interviews/berry.html), est un jeu de macros intermédiaire entre {tttexlogo}`TeX` et {latexlogo}`LaTeX`.

## LaTeX

{latexlogo}`LaTeX`, écrit par [Leslie Lamport](https://fr.wikipedia.org/wiki/Leslie_Lamport) en 1982 est un jeu de macros au-dessus de {tttexlogo}`TeX`, plus facile à utiliser que ce dernier. Il propose notamment différents styles de documents, auxquels correspondent des classes de document et une grande diversité de macros qui répondent à divers besoins des auteurs. {latexlogo}`LaTeX` a été conçu pour rédiger des articles, des rapports, des thèses, des livres ou pour préparer des transparents. On peut insérer dans le texte des dessins, des tableaux, des formules mathématiques et des images sans avoir à se soucier (ou presque) de leur mise en page. Les documents produits avec {latexlogo}`LaTeX` et {tttexlogo}`TeX` sont d'une excellente qualité typographique.

Suite à une large utilisation de {latexlogo}`LaTeX`, beaucoup d'extensions ont été créées par différents utilisateurs. Loi de Murphy oblige, ces extensions ont introduit un certain nombre d'incompatibilités et ont porté atteinte à la portabilité de {latexlogo}`LaTeX`. C'est de cette situation qu'est né le projet de normalisation {latexlogo}`LaTeX`, sous la direction des gourous {latexlogo}`LaTeX` : L. Lamport, F. Mittelbach, C. Rowley, R. Schopf et tant d'autres... Pour plus de détails, consultez le site du [LaTeX Projet](http://www.latex-project.org/).

Toutefois, pour ne pas perturber les actuels utilisateurs de {latexlogo}`LaTeX`, la version provisoire normalisée s'appelle {latexlogo}`LaTeX` (1994) et elle est compatible (dans la mesure du possible) avec les anciens standards et ne devrait plus beaucoup évoluer. Ainsi tous les documents écrits pour {latexlogo}`LaTeX` 2.09 peuvent être compilés sous {latexlogo}`LaTeX` en mode « LaTeX 2.09 compatibility mode ».

:::{note}
Cette compatibilité sera amenée à disparaître au fur et à mesure des évolutions vers {latexlogo}`LaTeX`.
:::

[Signalons également NTS](https://en.wikipedia.org/wiki/New_Typesetting_System), un projet ambitieux qui visait d'abord à réimplémenter {tttexlogo}`TeX` en Java, puis l'enrichir de nouvelles fonctionnalités et n'en garder probablement que les concepts. Une version alpha a été publiée en 2000 et son développement a cessé peu après.

```{eval-rst}
.. todo:: Ce dernier paragraphe appelle une révision : « *Ce texte devrait sans doute être reporté ailleurs car le projet est mort depuis un bon moment et n'a qu'un caractère historique anecdotique.*) »
```

______________________________________________________________________

*Sources :*

- [Page personnelle de Donald Knuth](https://www-cs-faculty.stanford.edu/~knuth/) (en anglais),
- [Just what is TeX?](https://www.tug.org/whatis.html) (en anglais),
- [A brief history of TeX](http://www.gust.org.pl/bachotex/EuroBachoTeX2007/presentations/bhot.pdf/view?set_language=en) (en anglais),
- [A brief history of TEX, volume II](https://www.tug.org/TUGboat/tb29-1/tb91reutenauer.pdf), TUGboat, Volume 29 (2007) N{sup}`o`1 (en anglais).

```{eval-rst}
.. meta::
   :keywords: LaTeX,TeX, histoire,latex3,latex2e,latex 2.09
```
