```{role} latexlogo
```
```{role} tttexlogo
```
# Quels sont les projets de développement de TeX ?

Alors que Donald Knuth a déclaré que {tttexlogo}`TeX` {doc}`ne changera jamais de manière substantielle </1_generalites/histoire/quel_futur_pour_tex>`, il reste des fonctionnalités qui aurait pu être faites différemment ou être tout simplement implémentées. Celles-ci ont été explorées dans un certain nombre de moteurs, dont certains ont des histoires quelque peu complexes.

Le premier ensemble d'ajouts majeurs au {tttexlogo}`TeX` de Donald Knuth a été fourni par le projet NTS avec {doc}`ε-TeX </1_generalites/glossaire/qu_est_ce_que_etex>` : ces ajouts sont aujourd'hui disponibles dans tous les moteurs récents. Le programme {doc}`pdfTeX </1_generalites/glossaire/qu_est_ce_que_pdftex>` a ajouté une gamme de nouvelles primitives et beaucoup d'entre elles ont également été reprises par d'autres moteurs.

## Projets actifs

### pdfTeX

Le programme {doc}`pdfTeX </1_generalites/glossaire/qu_est_ce_que_pdftex>` a été développé par Hàn Thế Thành : il intègre la possibilité de créer une sortie PDF directement dans le moteur {tttexlogo}`TeX`. Bien qu'à l'origine développé en parallèle, `pdfTeX` inclut aujourd'hui les extensions ε-{tttexlogo}`TeX` en standard. Le développement de `pdfTeX` s'est en grande part arrêté : les changements courants sont en grande partie axés sur les corrections d'erreurs de code. Cependant, l'importance centrale de `pdfTeX` en tant que moteur « standard » dans les distributions {tttexlogo}`TeX` modernes signifie que des changements ultérieurs pourraient survenir, en particulier dans le domaine de l'accessibilité.

Ce projet a commencé comme un sujet pour la thèse de maîtrise de Hàn Thế Thành et semble avoir été publié pour la première fois dans TUGboat 18 (4), en 1997 (bien qu'il ait été discuté lors de la conférence TUG'96 en Russie). Alors que le monde faisait bon usage des « pré-versions » de `pdfTeX`, Thành l'a utilisé comme banc d'essai pour la micro-typographie qui était le sujet de recherche principal de son doctorat. Depuis que Thành a obtenu son doctorat, la maintenance et le développement de `pdfTeX` ont été confiés à un [groupe de mainteneurs](http://tug.org/applications/pdftex/) (qui comprend Thành) qui a réussi à maintenir une plate-forme stable pour une utilisation générale.

### XeTeX

[XeTeX](/1_generalites/glossaire/qu_est_ce_que_xetex), développé à l'origine par Jonathan Kew, est un moteur Unicode {tttexlogo}`TeX` capable de charger des polices système à l'aide de la [bibliothèque HarfBuzz](https://fr.wikipedia.org/wiki/HarfBuzz), ce qu'il obtient par une syntaxe étendue de la primitive `\font`. Il s'appuie sur ε-{tttexlogo}`TeX` et ajoute également une gamme de nouvelles primitives utiles pour travailler avec des langues non latines. Par ailleurs, le processus de composition est essentiellement le même que celui de {tttexlogo}`TeX`. Comme pour `pdfTeX`, il n'y a pas de développement majeur en cours sur le [site du projet](http://xetex.sourceforge.net/), bien que les corrections d'erreurs de code et la compatibilité croisée avec d'autres moteurs continuent.

### LuaTeX

Comme détaillé ci-dessus, le développement de `pdfTeX` est « achevé ». Mais, alors que le développement de `pdfTeX` s'essoufflait, le développement d'un nouveau système, {doc}`LuaTeX </1_generalites/glossaire/qu_est_ce_que_luatex>` a commencé. [Lua](http://www.lua.org/) est un interpréteur conçu pour être intégré dans d'autres applications. {doc}`LuaTeX </1_generalites/glossaire/qu_est_ce_que_luatex>` consiste en un moteur de type {tttexlogo}`TeX` avec un interpréteur Lua « intégré » en lui. L'interpréteur `lua` a accès à de nombreuses structures de données utilisées pour la composition, de sorte que le programmeur peut placer des morceaux de code Lua dans ses commandes {tttexlogo}`TeX` ou {latexlogo}`LaTeX`, ou effectuer des « rappels » (*call-backs*) pour modifier le comportement de {tttexlogo}`TeX` lorsqu'il effectue certaines opérations.

Cet arrangement offre la perspective d'un moteur de composition « assoupli » : il présente le comportement classique mais l'utilisateur peut redéfinir une fonctionnalité si besoin est --- il n'est plus nécessaire de persuader le reste du monde puis de trouver un développeur disposé à travailler sur le sujet.

Le [LuaTeX project](http://www.luatex.org/) a poursuivi des pistes que de nombreux autres projets avaient en vue, notamment les représentations de caractères Unicode et la prise en charge des polices [OpenType](https://fr.wikipedia.org/wiki/OpenType). Cela incluait l'incorporation des extensions développées par Aleph (voir ci-dessous). L'actuel {doc}`LuaTeX </1_generalites/glossaire/qu_est_ce_que_luatex>` (v1.12.0 en 2020) est considéré comme presque complet par l'équipe de développement : il poursuit cependant son évolution.

## Projets historiques

### ε-TeX

Dans le cadre des travaux du projet NTS (voir ci-dessous), un certain nombre d'extensions au {tttexlogo}`TeX` de Donald Knuth ont été développées. Beaucoup d'entre elles prennent en charge la programmation d'une manière qui rend possibles ou plus simples certaines tâches dans {tttexlogo}`TeX`. D'autres étendent la gamme des registres disponibles dans {tttexlogo}`TeX` ou mettent à disposition plus d'informations internes.

En fait, dans les distributions {tttexlogo}`TeX` actuelles, les extensions ε-{tttexlogo}`TeX` sont disponibles (et souramment utilisées) pour presque tous les formats autres que le format `tex` original de Donald Knuth.

### Omega et Aleph

[Omega (Ω)](<https://fr.wikipedia.org/wiki/Omega_(logiciel)>) a été développé comme une extension de {tttexlogo}`TeX` pour des textes multilingues utilisant une grande variété d'encodages en entrée. Omega utilisait des caractères 16 bits codés Unicode. Il a apporté de nombreux concepts innovants, dont notamment le « processus de traduction » qui prend un flux de caractères et le transforme selon différents processus qui peuvent être spécifiés en interne ou être un programme distinct.

Alors qu'Omega promettait beaucoup lors de son annonce au milieu des années 1990, ses progrès ont été lents et son développement est devenu moribond au moment où l'un des développeurs d'origine s'est retiré (emmenant avec lui un groupe d'étudiants en recherche).

Avant cet événement pénible, un courant de développement séparé avait déjà commencé afin de produire un programme appelé [Aleph](<https://en.wikipedia.org/wiki/Omega_(TeX)#Aleph_and_LuaTeX>), qui fusionnait les fonctionnalités de ε-{tttexlogo}`TeX` dans une base de code Omega stable et qui ajoutait d'autres extensions. Aleph s'est également avéré former une plate-forme attrayante pour de nombreuses personnes mais son développement s'est également arrêté.

Une partie du travail de ces projets a été incorporée dans LuaTeX.

### New Typesetting System (NTS)

Le [projet NTS](https://en.wikipedia.org/wiki/New_Typesetting_System) a entrepris de produire un logiciel pour remplacer {tttexlogo}`TeX` : ce *New Typesetting System* (nouveau système de composition) partagerait les objectifs de Donald Knuth, mais mettrait en œuvre le travail de manière moderne en tenant compte des leçons apprises avec {tttexlogo}`TeX`. Le projet a été présenté aux [journées GUTenberg 2000](https://web.archive.org/web/20070428230734/http://www.gutenberg.eu.org/manif/gut2000.html). Si une première mouture de NTS est apparue, elle n'était pas encore utilisable en pratique et le projet ne semble plus actif.

Parallèlement à ce travail, le NTS a développé un ensemble d'extensions qui peuvent être utilisées avec un système {tttexlogo}`TeX`. Un système ainsi modifié est connu sous le nom de système ε-{tttexlogo}`TeX`, et le concept s'est avéré être un succès.

### EχTeX

Le [projet EχTeX](http://www.extex.org/) s'appuyait sur l'expérience de nombreux projets de développement et d'extension de {tttexlogo}`TeX` pour développer un nouveau système semblable à {tttexlogo}`TeX`. Le système a été développé en Java.

ExTeX a été conçu pour accepter des entrées [Unicode](https://fr.wikipedia.org/wiki/Unicode) et pour implémenter toutes les primitives de {tttexlogo}`TeX` avec, pour certaines d'entre elles vues comme obsolètes, des alternatives modernes. Des extensions présentes avec {doc}`ε-TeX </1_generalites/glossaire/qu_est_ce_que_etex>`, {doc}`pdfTeX </1_generalites/glossaire/qu_est_ce_que_pdftex>` et Omega ont été identifiées pour y être incorporées.

______________________________________________________________________

*Sources :*

- {faquk}`TeX Engine development <FAQ-enginedev>`,
- [NTS : a New Typesetting System](https://web.archive.org/web/20160303223139/http://nts.tug.org/).

```{eval-rst}
.. meta::
   :keywords: omegaleph,LaTeX,luatex,développement,historique,pdftex,xetex,histoire de LaTeX,NTS
```
