```{role} latexlogo
```
```{role} tttexlogo
```
# Qu'est devenu initex ?

À l'origine, {tttexlogo}`TeX` et {latexlogo}`LaTeX` titillaient les limites de capacité de chaque système sur lequel ils étaient portés. Il y avait donc un avantage à réduire la taille de leurs exécutables. Une façon de faire consistait à disposer d'un exécutable séparé, `initex`, qui contenait des éléments non nécessaires pour l'exécution de documents ordinaires, notamment les commandes `\patterns` (qui construit des tables de césure) et `\dump` (qui écrit un format).

Pour les systèmes modernes, la taille de ce code est insignifiante par rapport à la mémoire disponible. De plus, le maintien de programmes séparés s'est avéré suffisamment sujet aux erreurs pour que les distributions système libres de style Unix aient supprimé `initex` et ses acolytes tels que `inipdftex`. Cette suppression s'est faite en faveur d'un seul exécutable (c'est-à-dire juste `tex` ou `pdftex`) qui fait ce que le programme `initex` et proches avaient l'habitude de faire s'il détecte le option de commande `-ini`.

Ce changement s'est produit avec lors de la sortie de la version 3.0 de teTeX (début 2005). Dès lors, deux courants sont apparus, fonction des {doc}`distributions </6_distributions/trouver_les_sources_pour_les_differents_systemes_d_exploitation2>` de {latexlogo}`LaTeX` :

- à cette époque, la distribution TeX Live suivait teTeX, de sorte qu'elle a abandonné `initex`.
- les développeurs de MiKTeX ont choisi de conserver l'exécutable `initex`.

______________________________________________________________________

*Source :* {faquk}`What's happened to \\initex? <FAQ-initex>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,errors
```
