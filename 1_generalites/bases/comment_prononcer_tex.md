```{role} latexlogo
```
```{role} tttexlogo
```
# Comment prononcer « TeX » ou « LaTeX » ?

## Cas de TeX

Le « X » est en fait la lettre grecque chi (𝜒); elle est donc prononcée par les anglophones soit comme le « ch » du mot écossais « *loch* » (un peu comme une *jota* espagnole, son noté \[x\] en alphabet phonétique international) soit comme un « K ». Les francophones préfèrent généralement cette seconde option.

Ça ne doit de toute façon **jamais** être prononcé « ks » comme dans « textile ».

L'explication de cette curieuse prononciation curieuse est données par Donald Knuth dans son {ctanpkg}`TeXbook <texbook>` : le nom de son logiciel vient du mot grec « τέχνη » qui veut dire « art » (au sens du savoir-faire de l'artisan) ou « technique » (dont il est l'origine étymologique).

Le logo {tttexlogo}`TeX` que Knuth a dessiné est simplement constitué des versions majuscules des trois premières lettres grecques du mot, tau, epsilon et chi (τ, έ, χ), un peu décalées verticalement (voir aussi {doc}`logos-latex </3_composition/texte/symboles/logos/logos-latex>`).

## Cas de LaTeX

Leslie Lamport n'a jamais donné de recommandation particulière pour la prononciation de *LaTeX* :

- en anglais, la plupart des gens prononcent « Lay TeX » ou « Lah TeX » (voir ci-dessus pour la pronciation de « TeX ») ;
- en français, c'est « La-TeX ». Dans tous les cas, ça ne se prononce **pas** comme le latex qui sert à faire les gants ou les pneus.

## Cas de LaTeX2ε

\$\\LaTeXe\$ se termine par la lettre grecque epsilon (ε), mais la plupart des gens choisissent de prononcer « LaTeX deux euh » en français (et en anglais : « *LaTeX-two-ee* »).

## Cas de XeTeX

D'après son auteur original, Jonathan Kew, « XeTeX » se prononce \[ziːtɛk\].

Néanmoins, certains locuteurs francophones le pronconcent \[keːtɛk\] ou \[gzeːtɛk\].

______________________________________________________________________

*Sources :*

- {faquk}`How should I pronounce « TeX »? <FAQ-TeXpronounce>`
- {faquk}`How should I pronounce « LaTeX2e »? <FAQ-latexpronounce>`
- [What is the correct pronunciation of TeX and LaTeX?](https://tex.stackexchange.com/questions/17502/what-is-the-correct-pronunciation-of-tex-and-latex)
- [Proper pronunciation of XeTeX, XeLaTeX](https://tex.stackexchange.com/questions/274617/proper-pronunciation-of-xetex-xelatex).

```{eval-rst}
.. meta::
   :keywords: LaTeX,TeX,prononciation,prononciation en français
```
