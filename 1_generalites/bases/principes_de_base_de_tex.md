```{role} latexlogo
```
```{role} tttexlogo
```
# Quels sont les principes de base de TeX ?

- Le principe de base de {tttexlogo}`TeX` est la boîte ! {tttexlogo}`TeX` met tout dans des boîtes et l'assemblage de ces boîtes suivant des règles données permet de mettre en page des documents.

À l'origine, {tttexlogo}`TeX` a été conçu aussi bien pour créer des documents d'une page de texte, que des documents de plusieurs centaines de pages contenant des formules mathématiques, des tableaux, des figures, etc. {tttexlogo}`TeX` travaille donc comme un imprimeur, sans pour autant subir les contraintes mécaniques inhérentes aux outils de ce dernier. Sa précision est sans limite puisque son unité de base est le point et que les calculs qu'il suscite sont effectués par les ordinateurs.

- Un des avantages de {tttexlogo}`TeX` est qu'il possède une vision globale des choses. {tttexlogo}`TeX` ne se place pas uniquement au niveau du caractère que l'auteur saisit mais aussi au niveau du mot, de la ligne, du paragraphe, de la page ou du document dans son ensemble pour évaluer ses critères de beauté. La dimension esthétique du document est ainsi prise en compte et gérée de manière à ce qu'elle soit maximale.

Les critères de beauté utilisés dépendent le plus souvent des règles typographiques attachées à la langue ou au langage employé (mathématiques par exemple), mais {tttexlogo}`TeX` peut également, à tout moment, prendre en compte les goûts de l'auteur. {tttexlogo}`TeX` gère ainsi la ponctuation, les ligatures, les coupures de mots et les justifications verticales et horizontales.

Un autre avantage de {tttexlogo}`TeX` est la facilité avec laquelle il donne accès à toutes ces possibilités de composition. En mathématiques, par exemple, il offre une quantité incroyable de symboles et connaît leurs conventions de mise en page (taille, fonte, espacement, etc.).

Un dernier avantage couvre tous les problèmes de numérotation des paragraphes, de tables des matières, des figures, de références croisées, de bibliographie, d'index, etc. Toutes ces aides de lecture sont automatiquement gérées par {tttexlogo}`TeX`. L'auteur a peu à s'en soucier, il n'a qu'à les déclarer.

En conclusion, vous n'avez qu'à penser au contenu de votre document, {tttexlogo}`TeX` se charge du reste.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
