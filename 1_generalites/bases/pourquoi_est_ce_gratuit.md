```{role} latexlogo
```
```{role} tttexlogo
```
# Pourquoi TeX est-il gratuit s'il marche si bien ?

{tttexlogo}`TeX` est gratuit parce que Donald E. Knuth a choisi qu'il le soit. Toutefois, Knuth ne voit pas d'inconvénient à ce qu'on puisse gagner de l'argent en vendant des services ou des produits liés à {tttexlogo}`TeX` : Knuth lui-même gagne de l'argent par les droits d'auteur de ses livres sur {tttexlogo}`TeX`.

En termes de licence, beaucoup d'outils et d'extensions sont offertes dans le cadre de la [licence publique générale GNU](https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU), parfois schématisée par la notion de *copyleft*. {tttexlogo}`TeX`, lui, est fourni dans le cadre d'une [licence assez permissive rédigée](https://www.ctan.org/license/knuth) par Knuth dont voici le texte et une traduction :

- // This software is copyright and you are explicitly granted a license which gives you, the « user » of the software, legal permission to copy, distribute and/or modify the software, so long as if you modify the software then it carry a different name from the original software. //
- Ce logiciel est soumis à un droit d'auteur et vous disposez explicitement d'une licence qui vous donne, en tant qu' « utilisateur » de ce logiciel, le droit de le copier, de le distribuer et/ou de le modifier, sous réserve que si vous modifiez le logiciel alors il doit porter un nom différent de celui du logiciel original.

Il existe des {doc}`versions commerciales </6_distributions/implementations_commerciales>` de {tttexlogo}`TeX`. Pour certains utilisateurs, il est en effet rassurant de disposer d'un support technique payant. De plus, certaines de ces versions disposent de fonctionnalités non disponibles dans les versions gratuites (mais l'inverse est tout aussi vrai). La présente FAQ se concentre sur les distributions libres de {tttexlogo}`TeX` mais elle liste les {doc}`principaux vendeurs </6_distributions/implementations_commerciales>`.

______________________________________________________________________

*Source :* {faquk}`If TeX is so good, how come it's free? <FAQ-whyfree>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,logiciel libre
```
