```{role} latexlogo
```
```{role} tttexlogo
```
# Comment distinguer TeX, LaTeX, pdfTeX et pdfLaTeX ?

{latexlogo}`LaTeX` n'est pas un programme indépendant. Ce n'est qu'un (énorme) jeu de macros pour {tttexlogo}`TeX`. Pour utiliser {latexlogo}`LaTeX`, on pourrait en fait utiliser {tttexlogo}`TeX`, et commencer par lui faire lire avant notre document (avant le `\documentclass`) les définitions de centaines de macros. Pour des raisons d'efficacité, on utilise en fait un « format », c'est-à-dire que quand on tape `latex`, c'est le programme {tttexlogo}`TeX` qui est appelé, mais avec toutes les définitions de LaTeX préchargées.

Sur les installations modernes, ce premier point n'est pas exact : ce n'est pas le programme {tttexlogo}`TeX` mais {doc}`pdfTeX </1_generalites/glossaire/qu_est_ce_que_pdftex>` qui est appelé. Ce dernier est une version moderne de {tttexlogo}`TeX`, qui possède de nombreuses fonctionnalités supplémentaires :

- la possibilité d'écrire de droite à gauche ;
- des {doc}`fonctionnalités micro-typographiques </3_composition/texte/paragraphes/latex_fait_des_lignes_trop_longues>` ;
- la capacité de créer des liens hypertextes ;
- quelques nouvelles primitives utiles (dont `\middle` pour les {doc}`délimiteurs en mathématique </4_domaines_specialises/mathematiques/structures/delimiteurs/ajuster_la_taille_des_delimiteurs>`) ;
- et enfin la possibilité de produire un DVI ou un PDF.

Ce dernier point justifie le nom, mais le nom prête à confusion : `pdftex` sait en effet très bien produire des DVI. Ce qui va déterminer le mode de compilation, c'est le fait que vous l'appeliez :

- avec `latex`, commande équivalente à `pdftex -fmt latex (...).tex` ;
- avec `pdflatex`, commande équivalente à `pdftex -fmt latex -output-format pdf (...).tex`.

En fait, la seule commande qui lancera effectivement {tttexlogo}`TeX` (avec le format `plain`) est la commande... `tex` ! Vous pouvez vous amuser à taper `latex -``-version` pour vous convaincre du fait que c'est bien `pdftex` qui travaille, même en mode DVI.

```
$ latex --version
pdfTeX 3.14159265-2.6-1.40.20 (TeX Live 2019/Debian)
kpathsea version 6.3.1
[...]
```

Quelques explications :

- 3.14159265 est le {doc}`numéro de version actuel </1_generalites/histoire/quel_futur_pour_tex>` de {tttexlogo}`TeX`, sur lequel est basé `pdfTeX` ;
- `pdftex` intègre les fonctionnalités d'une autre extension de {tttexlogo}`TeX`, eTeX (version 2.6), et son numéro de version actuel est le 1.40.20 restant ;
- `kpathsea` est une bibliothèque de recherche qui aide {tttexlogo}`TeX` à trouver rapidement ses fichiers dans {doc}`vos arborescences </5_fichiers/tds/la_tds>` `texmf`.

______________________________________________________________________

*Source :* <https://web.archive.org/web/20170314080827/https://elzevir.fr/imj/latex/tips.html#moteur>

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentations pour débutants
```
