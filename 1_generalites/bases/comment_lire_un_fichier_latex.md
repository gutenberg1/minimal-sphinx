```{role} latexlogo
```
```{role} tttexlogo
```
# Comment lire un fichier (La)TeX ?

Vous avez donc reçu un fichier {tttexlogo}`TeX` ou {latexlogo}`LaTeX` : qu'allez-vous en faire ?

## En l'ouvrant avec un simple éditeur de texte

Vous pouvez, effectivement, « simplement le lire », car il s'agit avant tout d'un fichier texte brut. Le problème est que les balises (commandes précédées du caractère « \\ » et autres accolades ouvrantes ou fermantes) dans le document peuvent s'avérer gênantes. La plupart du temps, même les experts compileront un tel fichier avant d'essayer de le lire. Ne vous inquiétez donc pas trop si vous ne pouvez rien tirer de votre fichier : il est conçu pour être traité par un « compilateur »... et les compilateurs n'ont pas grand chose en commun avec les lecteurs humains.

## En le compilant

L'étape suivante peut donc consister à utiliser un {doc}`éditeur LaTeX en ligne </2_programmation/compilation/compiler_en_ligne>`.

Si aucun compilateur en ligne ne vous aide, vous devez composer le document « vous-même ». La bonne nouvelle est que les systèmes {tttexlogo}`TeX` sont disponibles, gratuitement, pour la plupart des ordinateurs. La mauvaise nouvelle est que vous avez besoin d'un système {tttexlogo}`TeX` assez complet même pour lire un seul fichier, et ce type de système, une *distribution*, s'avère assez volumineux. Pour plus de détails sur les types de choses disponibles, voir « {doc}`Quelles sont les versions de (La)TeX selon le système d'exploitation ? </6_distributions/trouver_les_sources_pour_les_differents_systemes_d_exploitation2>` » ou « {doc}`Quelles sont les versions commerciales de (La)TeX ? </6_distributions/implementations_commerciales>` ».

Mais attention... {tttexlogo}`TeX` ne tente pas de ressembler au système WYSIWYG auquel vous êtes probablement habitué (voir « {doc}`Pourquoi TeX n'est pas WYSIWYG ? </1_generalites/bases/wysiwyg>` »). Alors que de nombreuses versions modernes de {tttexlogo}`TeX` ont un cycle « compilation/vue » qui rivalise avec les meilleurs traitements de texte commerciaux dans sa réactivité, ce que vous tapez reste généralement un texte avec du *balisage*, ce dernier définissant généralement une structure logique (plutôt que visuelle) de ce que vous voulez composer.

## Conclusion

Il y a donc un équilibre à trouver entre la simplicité du document original (balisé), qui peut être à peu près lu dans *n'importe quel* éditeur, et l'investissement assez important nécessaire à l'installation d'un système {tttexlogo}`TeX` pour lire un document « comme prévu ».

Si vous êtes rebuté par tout cela, rappelez-vous que {tttexlogo}`TeX` et {latexlogo}`LaTeX` produisent de bons PDF : pourquoi ne pas demander à la personne qui a envoyé le fichier {tttexlogo}`TeX` de vous envoyer une copie PDF ?

______________________________________________________________________

*Source :* {faquk}`Reading (La)TeX files <FAQ-readtex>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,lecture,fichier tex,compilation
```
