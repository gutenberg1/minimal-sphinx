```{role} latexlogo
```
```{role} tttexlogo
```
# Que lire sur LaTeX 2.09 ?

{octicon}`alert;1em;sd-text-warning` {ctanpkg}`LaTeX 2.09 <latex209>` est obsolète, sa dernière version ayant été mise à disposition en 1993. Son utilisation n'est donc pas recommandée.

- Chantal Simian, [LaTeX - Manuel utilisateur simplifié](https://tex.loria.fr/general/latex-intro.ps.gz), 1992.

```{eval-rst}
.. meta::
   :keywords: LaTeX,LaTeX 2.09
```
