```{role} latexlogo
```
```{role} tttexlogo
```
# Où trouver ce symbole ?

Cette question est traitée dans les sections :

- la section {doc}`Symboles </3_composition/texte/symboles/start>` ;
- la section {doc}`Symboles mathématiques </4_domaines_specialises/mathematiques/symboles/start>`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentation,symboles
```
