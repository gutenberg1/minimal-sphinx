```{role} latexlogo
```
```{role} tttexlogo
```
# Que lire sur BibTeX ?

{ctanpkg}`BibTeX <bibtex>`, un programme initialement conçu pour produire des bibliographies en conjonction avec {latexlogo}`LaTeX`, est expliqué dans la section 4.3 et l'annexe B du {doc}`LaTeX reference manual </1_generalites/documentation/livres/documents_sur_latex>`// de Leslie Lamport (on pourrait aussi citer le / {doc}`LaTeX Companion </1_generalites/documentation/livres/documents_sur_latex>` qui évoque également l'écriture de fichiers de style {ctanpkg}`BibTeX <bibtex>`). Il existe cependant plusieurs documents libres sur le sujet.

## En anglais

- Alexander Feder, [BibTeX.org -- Your BibTeX resource](http://www.bibtex.org). Ce site propose une introduction solide, mais n'entre pas dans les détails.
- David Hoadley et Michael Shell, {ctanpkg}`BibTeX Tips and FAQ <bibtex>`. Ce document fait partie de la documentation officielle de {ctanpkg}`BibTeX <bibtex>`.
- Nicolas Markey, [Tame the BeaST (The B to X of BibTeX)](https://ctan.org/pkg/tamethebeast/). Ce tutoriel décrit la procédure complète d'utilisation de {ctanpkg}`BibTeX <bibtex>`.
- Oren Patashnik, {texdoc}`BibTeXing <bibtex>`. Il s'agit de la documentation officielle de {ctanpkg}`BibTeX <bibtex>`. Elle développe le chapitre du livre de Lamport.
- Oren Patashnik, {texdoc}`Designing BibTeX Styles <btxhak>`. Ce document explique le langage utilisé pour écrire les styles BibTeX (fichiers `bst`). Ce document fait partie de la documentation officielle de {ctanpkg}`BibTeX <bibtex>`.
- Oren Patashnik et Howard Trickey, [BibTeX 'plain' family](http://mirrors.ctan.org/biblio/bibtex/base/btxbst.doc). Ce fichierdonne le modèle pour les quatre styles standard ({ctanpkg}`plain <bibtex>`, {ctanpkg}`abbrv <bibtex>`, {ctanpkg}`alpha <bibtex>` et {ctanpkg}`unsrt <bibtex>`). Il contient également leur documentation et fait partie de la documentation officielle de {ctanpkg}`BibTeX <bibtex>`.

______________________________________________________________________

*Source :* {faquk}`BibTeX Documentation <FAQ-BibTeXing>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentation
```
