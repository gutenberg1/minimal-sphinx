```{role} latexlogo
```
```{role} tttexlogo
```
# Où trouver des groupes de discussion sur (La)TeX ?

Quelques [groupes usenet](https://fr.wikipedia.org/wiki/Usenet) (ou *newsgroups*) existent sur le sujet de {tttexlogo}`TeX` et {latexlogo}`LaTeX`.

## En français

- [fr.comp.text.tex](https://groups.google.com/g/fr.comp.text.tex) (archive Google Groups). Nommé `fctt` par les habitués, ce groupe forme une communauté compétente et sympathique : ces contributeurs ont en particulier mis à jour la FAQ {latexlogo}`LaTeX` francophone en 2004.

## En anglais

- [comp.text.tex](http://groups.google.com/group/comp.text.tex) (archive Google Groups).

## Qu'est-ce qu'un groupe Usenet ?

### Très bref historique

Le but de Usenet est de proposer une sorte de forum, où les gens peuvent discuter en groupe depuis leur ordinateur, de façon asynchrone (c'est à dire sans être tous connectés en même temps).

S'il semble actuellement normal d'utiliser des forums web ou des services commerciaux comme ceux de Facebook, il faut se souvenir que le web et internet n'ont pas toujours existé... Usenet date de 1979, alors que le web est arrivé en 1989!

L'idée générale est que vous allez vous connecter au serveur Usenet le plus proche de vous (typiquement celui de votre fournisseur d'accès à internet, ou de votre université), en utilisant un logiciel adapté. Vous pourrez consulter les messages stockés sur le serveur. Si vous envoyez vous-même un message, votre serveur va s'occuper de le transmettre à tous les autres serveurs dans le monde, par un protocole spécifique, NNTP ([Network News Transfer Protocol](https://fr.wikipedia.org/wiki/Network_News_Transfer_Protocol)). Tous les serveurs se synchronisent régulièrement pour permettre les discussions entre utilisateurs. Dans les temps anciens, cette synchronisation se faisait une ou deux fois par jour. Actuellement, elle est quasiment continue, ce qui permet des échanges fluides.

Les discussions sont structurées de façon hiérarchique. Chaque groupe est nommé en fonction de sa thématique, par une série de mots du plus général au plus précis. Ainsi `fr.comp.text.tex` signifie *french* > *computing* > *text documents* > *TeX*, pour pour « le groupe de discussion sur l’informatique, concernant les documents textuels, et plus spécifiquement TeX/LaTeX ».

`fctt` et `ctt` ne permettent d'échanger que des messages contenant du texte, mais il faut savoir que Usenet supporte maintenant l'échange de fichiers binaires. À la faveur de cette évolution, son usage premier a été détourné et le protocole NNTP est maintenant souvent utilisé pour échanger vidéos et logiciels.

:::{note}
Si vous cherchez de l'aide à propos de Usenet aujourd'hui, vous trouverez de nombreuses pages à propos des échanges de gros fichiers, et comparativement peu à propos de discussions entre contributeurs humains...
:::

### Comment utiliser Usenet ?

```{eval-rst}
.. todo:: Doit-on expliquer pour les différents clients de messagerie ?
```

______________________________________________________________________

*Sources :*

- <https://www.meilleurnewsgroup.com/usenet-definition/Que%20sont%20les%20newsgroup%20et%20usenet%E2%80%AF?%20Guide%20complet%20pour%20tout%20comprendre>.

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentation,groupe de discussion,usenet,usenet-fr,aide sur LaTeX
```
