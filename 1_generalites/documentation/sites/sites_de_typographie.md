```{role} latexlogo
```
```{role} tttexlogo
```
# Quels sites visiter sur la typographie ?

## En français

- [Deux ou trois choses en typographie ou autres](https://jacques-andre.fr/). Il s'agit du site de Jacques André.
- Le [blog du web design](https://www.blogduwebdesign.com/typographie/). Il propose une série d'articles sur le sujet de la typographie.
- [Typographie & Civilisation](http://www.typographie.org/). Ce site fournit des éléments historiques sur le sujet (la navigation n'y est pas tout à fait intuitive).
- La [liste typographie](http://listetypo.free.fr/).
- Le [site de Jean Méron](http://jean-meron.fr/page32.html) propose une liste de références documentaires.

## En anglais

- Le [site officiel de Monotype](http://www.monotype.com/). Monotype est l'une des grandes fonderies.
- [Type arts](http://www.typearts.com/index.html). Il s'agit du site de quelques passionnés (sans doute un peu daté).

```{eval-rst}
.. meta::
   :keywords: livres,documentation,site web,LaTeX,typographie
```
