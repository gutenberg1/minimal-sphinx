```{role} latexlogo
```
```{role} tttexlogo
```
# Où trouver des wikis sur (La)TeX ?

Un [wiki](https://fr.wikipedia.org/wiki/Wiki) peut être une aubaine pour tous, s'il est utilisé à bon escient : il permet *à n'importe qui* d'ajouter des éléments ou d'éditer des éléments que quelqu'un d'autre a ajoutés. Par ailleurs, s'il existe un risque de chaos dans la ligne éditoriale, il est prouvé qu'une communauté d'utilisateurs forte peut garder un wiki sous contrôle et en faire une [référence respectable](https://fr.wikipedia.org/wiki/wikipédia).

## En français

- Le [wikilivre de LaTeX](https://fr.wikibooks.org/wiki/LaTeX).
- La [présente FAQ](https://faq.gutenberg.eu.org/), dans sa version actuelle, est un wiki.

## En anglais

- Le [wiki de ConTeXt](http://wiki.contextgarden.net/Main_Page), dont le succès a contribué à mettre en place les deux références suivantes.
- Le [wikilivre de (Plain) TeX](https://en.wikibooks.org/wiki/TeX).
- Le [wikilivre de LaTeX](https://en.wikibooks.org/wiki/LaTeX).

Selon la {faquk}`FAQ anglaise < >`, ces différents wikis anglophones sont très appréciés, qu'ils soient vus comme sources de référence ou textes d'introduction.

## En allemand

- La [FAQ germanophone](https://texfragen.de/) fonctionne de façon similaire à celle-ci.

______________________________________________________________________

*Source :* {faquk}`WIKI books for TeX and friends <FAQ-doc-wiki>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentation,travail collaboratif,communauté LaTeX,communauté francophone,utilisateurs de LaTeX
```
