```{role} latexlogo
```
```{role} tttexlogo
```
# Où trouver une bibliographie complète ?

## En français

Il n'existe pas de liste des ouvrages français traitant de {tttexlogo}`TeX` et {latexlogo}`LaTeX`.

## En anglais

Une large liste des ouvrages en anglais est disponible sur le site du [LORIA](http://tex.loria.fr/english/texbib.html). Sa dernière mise à jour date de 2006.

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie
```
