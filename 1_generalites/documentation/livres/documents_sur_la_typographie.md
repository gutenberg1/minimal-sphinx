```{role} latexlogo
```
```{role} tttexlogo
```
# Que lire sur la typographie ?

## En français

### Ouvrages généraux

- *Lexique des règles typographiques en usage à l'imprimerie nationale*, 5{sup}`e` édition, Imprimerie nationale, 2002, ISBN-10 2-7433-0482-0, ISBN-13 : 978-2-7433-0482-9
- Gérard Blanchard, *Pour une sémiologie de la typographie*, Magermans, 1978. Il s'agit de la partie *illustrations* d'une importante thèse de G. Blanchard avec quand même un peu de texte... Le texte complet n'est disponible qu'en italien : *L'eredita Gutenberg*, Gianfranco Altieri Editore. Cet ouvrage peut être trouvé sur le site de [Persée](https://www.persee.fr/doc/colan_0336-1500_1978_num_40_1_1269) et il est vendu par *Rencontres de Lure*, BP 533 -- 71010 Macon cedex.
- John Dreyfus et François Richaudeau, *La chose imprimée*, Retz, 1985.
- [Hans E. Meier](https://fr.wikipedia.org/wiki/Hans_Eduard_Meier), *Die Schrift-Entwicklung / The Development of Script and Type / Le développement des caractères*, 2{sup}`e` édition, Niggli Verlag, 2009, ISBN-10 3-7212-0434-4, ISBN-13 978-3-7212-0434-6.
- Yves Perousseaux, *Règles de l'écriture typographique du français*, 10{sup}`e` édition, Atelier Perrousseaux, 2020, ISBN-10 2-3676-5025X, ISBN-13 978-2-3676-5025-8. Voir sa description sur le [site de l'éditeur](https://www.adverbum.fr/atelier-perrousseaux/yves-perrousseaux/regles-de-l-ecriture-typographique-du-francais-nouvelle-edition_m915m19bqjd8.html).

### Ouvrages sur les caractères et leur histoire

- Stephen Coles et Erik Spiekermann, *Manuel d'anatomie typographique*, Pyramyd, 2013, ISBN-10 2-3501-7291-0, ISBN-13 978-2-3501-7291-0. Il est décrit sur le [site de l'éditeur](https://pyramyd-editions.com/collections/all/products/manuel-d-anatomie-typographique).
- Paul Mcneil, *Histoire visuelle de l'art typographique*, Actes Sud, 2019, ISBN-10 2-3301-2372-8, ISBN-13 978-2-3301-2372-7. Il est décrit sur le [site de l'éditeur](https://www.actes-sud.fr/catalogue/arts-du-livre-calligraphie/histoire-visuelle-de-lart-typographique-1454-2015).
- David Rault, *Guide pratique de choix typographique*, Adverbum, 2015, ISBN-10 2-9112-2093-5, ISBN-13 978-2-9112-2093-7. Il est décrit sur le [site de l'éditeur](https://www.adverbum.fr/atelier-perrousseaux/david-rault/guide-pratique-de-choix-typographique_4yrc07ab75lc.html).

## En anglais

(ouvrages-generaux-1)=

### Ouvrages généraux

- Robert Bringhurst, *The Elements of Typographic Style*, Hartley & Marks, 1992, ISBN-10 0-88179-033-8. Il semble être l'ouvrage le plus recommandé sur le sujet.
- Geoffrey Dowding, *Finer Points in the Spacing & Arrangement of Type*, Hartley & Marks, 1996, ISBN-10 0-88179-119-9.
- David Kindersley et Lida Cardozo Kindersley, *Optical Letter Spacing*, 2001, ISBN-10 1-874426-139. Voir aussi le [site des auteurs](http://www.kindersleyworkshop.co.uk/).
- Ruari McLean, *The Thames & Hudson Manual of Typography*, Thames & Hudson, 1980, ISBN-10 0-500-68022-1.
- Jan Tschichold, *The Form of the Book*, Lund Humphries, 1991, ISBN-10 0-85331-623-6.
- Colin Wheildon, *Type & Layout - Are You Communicating or Just Making Pretty Shapes*, Worsley Press Australia, 2005, ISBN-10 1-8757-5022-3, ISBN-13 978-1-8757-5022-1.
- Adrian Wilson, *The Design of Books*, Chronicle Books, 1993, ISBN-10 0-8118-0304-X.

(ouvrages-sur-les-caracteres-et-leur-histoire-1)=

### Ouvrages sur les caractères et leur histoire

- Warren Chappell et Robert Bringhurst, *A Short History of the Printed Word*, Hartley & Marks, 1999, ISBN-10 0-88179-154-7.
- Lewis F. Day, *Alphabets Old & New*, Senate, 1995, ISBN-10 1-85958-160-9.
- Geoffrey Dowding, *An Introduction to the History of Printing Types*, British Library, 1998, UK ISBN-10 0-7123-4563-9; USA ISBN-10 1-884718-44-2.
- Richard A. Firmage, *The Alphabet Abecedarium*, David R. Godine, 1993, ISBN-10 0-87923-998-0.
- Frederick Goudy, *The Alphabet and Elements of Lettering*, Dover, 1963, ISBN-10 0-486-20792-7.
- Alexander Lawson, *Anatomy of a Typeface*, David R. Godine, 1990, ISBN-10 0-87923-338-8.
- Stanley Morison, *A Tally of Types*, David R. Godine, 1999, ISBN-10 1-56792-004-7.
- Fred Smeijers, *Counterpunch*, Hyphen, 1996, ISBN-10 0-907259-06-5.
- Jan Tschichold, *Treasury of Alphabets and Lettering*, W. W. Norton, 1992, ISBN-10 0-393-70197-2.

______________________________________________________________________

*Source :* {faquk}`Books on Type <FAQ-type-books>`

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentation,typographie,bibliographie
```
