```{role} latexlogo
```
```{role} tttexlogo
```
% FAQ LaTeX GUTenberg documentation master file, created by
% sphinx-quickstart on Fri Nov 13 11:00:08 2020.
% You can adapt this file completely to your liking, but it should at least
% contain the root `toctree` directive.

# Bienvenue sur la FAQ LaTeX francophone

```{toctree}
:glob: true
:hidden: true
:maxdepth: 1

*/start
```

## Foire aux questions / Frequently asked questions (FAQ)

Vous trouverez ici les réponses aux questions les plus courantes
sur {tttexlogo}`TeX`, {latexlogo}`LaTeX` et outils associés.

### Que faire pour aider ?

Cette FAQ rassemble le contenu de {doc}`trois précédentes FAQ
</0_cette_faq/historique/sources_et_contributeurs>`. Vous pouvez facilement commencer
à contribuer :

% en retravaillant les :doc:`les questions à réviser
% </0_cette_faq/espace_contributeurs/questions_a_reviser>` ;

- en fusionnant des pages en double ;
- en traduisant des pages en anglais ;
- et bien entendu en ajoutant vos propres trucs et astuces.

Si vous ne trouvez pas les réponses que vous cherchez, vous pouvez regarder dans
[la bibliographie du site de GUTenberg](https://www.gutenberg.eu.org/-Gerbe-de-liens-), ou vous [abonner à la liste
GUT.](https://www.gutenberg.eu.org/listes)

Enfin, vous pouvez également poser vos questions sur les sites [Stack Exchange](https://tex.stackexchange.com/) (en anglais) ou [TeXnique.fr](https://www.texnique.fr/osqa/) (en français), et revenir contribuer à cette
FAQ avec les réponses les plus pertinentes !

### Contribuer

- Issue Tracker: github.com/\$project/\$project/issues
- Source Code: github.com/\$project/\$project

### Ça ne marche pas !

Ce site est en développement. Si vous rencontrez un problème ou si vous
avez des questions ou des suggestions, [n'hésitez pas à nous
contacter.](mailto:faq@gutenberg.eu.org)

# Indices and tables

- {ref}`genindex`
- {ref}`modindex`
- {ref}`search`
